
from apps import conf
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___804 = conf.get_session(Cell('loggedin'))
    __r__ = Cell(___804, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___805 = is_logged_in()[0]
    ___806 = ___805
    __trace__.add_inputs(___806.inputs)
    __stack__.push()
    __stack__.add(___806.inputs, bot=True)
    if ___806:
        ___808 = conf.me()
        ___809 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___808]))
        __r__ = Cell(___809[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('message' not in locals()):
        message = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___810 = messages
    for message in ___810:
        ___812 = conf.check(__c__, __trace__, Cell('Service'), message)
        ___813 = ___812
        __stack__.push()
        __stack__.add(___813.inputs)
        messages2.adopt(__stack__.all())
        messages2.adopt(___813.inputs)
        if ___813:
            messages2 = (messages2 + Cell([message]))
            messages2.adopt(__stack__.all())
        __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_admin():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___815 = conf.me()
    ___816 = conf.sql(Cell('SELECT level FROM user_profiles WHERE user_id = ?0'), Cell([___815]))
    level = ___816[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level == Cell('chair'))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def about_view():
    global __stack__
    __k__ = [Cell('which_page')]
    ___817 = conf.render(Cell('about.html'), Cell({
        __k__[0].value: Cell('about'),
    }, inputs=[__k__[0].inputs]))
    __r__ = Cell(___817, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/about')
def _about_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = about_view()
    return (__c__, __r__, __s__, __trace__)

def papers_view():
    global __stack__
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('user_names' not in locals()):
        user_names = Cell(None)
    if ('author_name' not in locals()):
        author_name = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('papers' not in locals()):
        papers = Cell(None)
    if ('paper_data_1' not in locals()):
        paper_data_1 = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('paper_versions' not in locals()):
        paper_versions = Cell(None)
    if ('paper_data' not in locals()):
        paper_data = Cell(None)
    ___818 = me_user()[0]
    user = ___818
    user.adopt(__stack__.all())
    ___819 = non(user)
    __trace__.add_inputs(___819.inputs)
    __stack__.push()
    __stack__.add(___819.inputs, bot=True)
    if ___819:
        ___821 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___821, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    user_name = user[Cell(2)]
    user_name.adopt(__stack__.all())
    ___822 = conf.sql(Cell('SELECT * FROM papers'))
    papers = ___822
    papers.adopt(__stack__.all())
    ___823 = db_len(papers)
    paper_data = (Cell([Cell(None)]) * ___823)
    paper_data.adopt(__stack__.all())
    __k__ = []
    user_names = Cell({
        
    }, inputs=[])
    user_names.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___824 = papers
    for paper in ___824:
        ___826 = conf.sql(Cell('SELECT title FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC'), Cell([paper[Cell(0)]]))
        paper_versions = ___826
        paper_versions.adopt(__stack__.all())
        ___827 = conf.sql(Cell('SELECT name FROM user_profiles WHERE user_id = ?0'), Cell([paper[Cell(2)]]))
        author_name = ___827[Cell(0)][Cell(0)]
        author_name.adopt(__stack__.all())
        __k__ = [Cell('paper'), Cell('author_name'), Cell('latest')]
        paper_data_1 = Cell({
            __k__[0].value: paper,
            __k__[1].value: author_name,
            __k__[2].value: paper_versions[Cell(0)][Cell(0)],
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs])
        paper_data_1.adopt(__stack__.all())
        ___828 = conf.check(__c__, __trace__, Cell('Service'), paper)
        ___829 = conf.check(__c__, __trace__, Cell('Service'), author_name)
        ___830 = Cell((___828.value and ___829.value), inputs=(___828.inputs + ___829.inputs))
        __stack__.push()
        __stack__.add(___830.inputs)
        paper_data.adopt(__stack__.all())
        paper_data.adopt(___830.inputs)
        if ___830:
            __slv__ = [i]
            __w__ = paper_data
            for __i__ in range(1):
                __w__.adopt(__slv__[__i__].inputs)
                if (__i__ == 0):
                    __w__[__slv__[__i__]] = paper_data_1
                elif (__slv__[__i__] in __w__):
                    __w__ = __w__[__slv__[__i__]]
                else:
                    break
            paper_data.adopt(__stack__.all())
            ___832 = conf.check(__c__, __trace__, Cell('Service'), paper_versions[Cell(0)][Cell(0)])
            ___833 = non(___832)
            __stack__.push()
            __stack__.add(___833.inputs)
            paper_data.adopt(__stack__.all())
            paper_data.adopt(___833.inputs)
            if ___833:
                __slv__ = [i, Cell('latest')]
                __w__ = paper_data
                for __i__ in range(2):
                    __w__.adopt(__slv__[__i__].inputs)
                    if (__i__ == 1):
                        __w__[__slv__[__i__]] = Cell(None)
                    elif (__slv__[__i__] in __w__):
                        __w__ = __w__[__slv__[__i__]]
                    else:
                        break
                paper_data.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
        i = (i + Cell(1))
        i.adopt(__stack__.all())
    __k__ = [Cell('papers'), Cell('which_page'), Cell('paper_data'), Cell('name'), Cell('is_logged_in'), Cell('is_admin')]
    ___835 = is_admin()[0]
    ___836 = conf.render(Cell('papers.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), papers)),
        __k__[1].value: Cell((Cell('Service'), Cell('home'))),
        __k__[2].value: Cell((Cell('Service'), paper_data)),
        __k__[3].value: Cell((Cell('Service'), user_name)),
        __k__[4].value: Cell((Cell('Service'), Cell(True))),
        __k__[5].value: Cell((Cell('Service'), ___835)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___836, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/')
def _papers_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = papers_view()
    return (__c__, __r__, __s__, __trace__)

def paper_view():
    global __stack__
    if ('author' not in locals()):
        author = Cell(None)
    if ('reviews' not in locals()):
        reviews = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('latest_abstract' not in locals()):
        latest_abstract = Cell(None)
    if ('link' not in locals()):
        link = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('comments' not in locals()):
        comments = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('latest_title' not in locals()):
        latest_title = Cell(None)
    if ('paper_versions' not in locals()):
        paper_versions = Cell(None)
    if ('coauthors' not in locals()):
        coauthors = Cell(None)
    ___837 = me_user()[0]
    user = ___837
    user.adopt(__stack__.all())
    ___838 = non(user)
    __trace__.add_inputs(___838.inputs)
    __stack__.push()
    __stack__.add(___838.inputs, bot=True)
    if ___838:
        ___840 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___840, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___841 = conf.get('paper_view', Cell('id'))
    ___842 = conf.sql(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([___841]))
    paper = ___842[Cell(0)]
    paper.adopt(__stack__.all())
    ___843 = conf.method()
    ___844 = Cell((___843 == Cell('POST')))
    __trace__.add_inputs(___844.inputs)
    __stack__.push()
    __stack__.add(___844.inputs, bot=True)
    conf.add_sql_inputs('conf_paperversion', __stack__.all())
    conf.add_sql_inputs('conf_paperversion', ___844.inputs)
    conf.add_sql_inputs('reviews', __stack__.all())
    conf.add_sql_inputs('reviews', ___844.inputs)
    conf.add_sql_inputs('comments', __stack__.all())
    conf.add_sql_inputs('comments', ___844.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___844.inputs)
    if ___844:
        ___846 = Cell((paper != Cell(None)))
        __stack__.push()
        __stack__.add(___846.inputs)
        conf.add_sql_inputs('conf_paperversion', __stack__.all())
        conf.add_sql_inputs('conf_paperversion', ___846.inputs)
        conf.add_sql_inputs('reviews', __stack__.all())
        conf.add_sql_inputs('reviews', ___846.inputs)
        conf.add_sql_inputs('comments', __stack__.all())
        conf.add_sql_inputs('comments', ___846.inputs)
        contents.adopt(__stack__.all())
        contents.adopt(___846.inputs)
        if ___846:
            ___848 = conf.get('paper_view', Cell('add_comment'))
            ___849 = Cell((___848 == Cell('true')))
            __stack__.push()
            __stack__.add(___849.inputs)
            conf.add_sql_inputs('conf_paperversion', __stack__.all())
            conf.add_sql_inputs('conf_paperversion', ___849.inputs)
            conf.add_sql_inputs('reviews', __stack__.all())
            conf.add_sql_inputs('reviews', ___849.inputs)
            conf.add_sql_inputs('comments', __stack__.all())
            conf.add_sql_inputs('comments', ___849.inputs)
            contents.adopt(__stack__.all())
            contents.adopt(___849.inputs)
            if ___849:
                ___851 = conf.me()
                ___852 = conf.get('paper_view', Cell('comment'))
                ___853 = conf.sql(Cell('INSERT INTO comments (paper_id, user_id, contents) VALUES (?0, ?1, ?2)'), Cell([paper[Cell(0)], ___851, ___852]))
            else:
                ___854 = conf.get('paper_view', Cell('add_review'))
                ___855 = Cell((___854 == Cell('true')))
                __stack__.push()
                __stack__.add(___855.inputs)
                conf.add_sql_inputs('conf_paperversion', __stack__.all())
                conf.add_sql_inputs('conf_paperversion', ___855.inputs)
                conf.add_sql_inputs('reviews', __stack__.all())
                conf.add_sql_inputs('reviews', ___855.inputs)
                contents.adopt(__stack__.all())
                contents.adopt(___855.inputs)
                if ___855:
                    ___857 = conf.me()
                    ___858 = conf.get('paper_view', Cell('review'))
                    ___859 = conf.get('paper_view', Cell('score_novelty'))
                    ___860 = db_int(___859)
                    ___861 = conf.get('paper_view', Cell('score_presentation'))
                    ___862 = db_int(___861)
                    ___863 = conf.get('paper_view', Cell('score_technical'))
                    ___864 = db_int(___863)
                    ___865 = conf.get('paper_view', Cell('score_confidence'))
                    ___866 = db_int(___865)
                    ___867 = conf.sql(Cell('\n                  INSERT INTO reviews (paper_id, reviewer_id, contents, score_novelty, \n                                       score_presentation, score_technical, score_confidence) \n                  VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([paper[Cell(0)], ___857, ___858, ___860, ___862, ___864, ___866]))
                else:
                    ___868 = conf.get('paper_view', Cell('new_version'))
                    ___869 = Cell((___868 == Cell('true')))
                    __stack__.push()
                    __stack__.add(___869.inputs)
                    conf.add_sql_inputs('conf_paperversion', __stack__.all())
                    conf.add_sql_inputs('conf_paperversion', ___869.inputs)
                    contents.adopt(__stack__.all())
                    contents.adopt(___869.inputs)
                    if ___869:
                        ___871 = conf.post('paper_view', Cell('contents'))
                        contents = ___871
                        contents.adopt(__stack__.all())
                        ___872 = Cell((contents != Cell(None)))
                        __stack__.push()
                        __stack__.add(___872.inputs)
                        conf.add_sql_inputs('conf_paperversion', __stack__.all())
                        conf.add_sql_inputs('conf_paperversion', ___872.inputs)
                        if ___872:
                            ___874 = conf.get('paper_view', Cell('title'))
                            ___875 = conf.get('paper_view', Cell('abstract'))
                            ___876 = conf.now_utc()
                            ___877 = conf.sql(Cell('\n                  INSERT INTO conf_paperversion (paper_id, title, contents, abstract, time)\n                  VALUES (?0, ?1, ?2, ?3)'), Cell([paper[Cell(0)], ___874, contents, ___875, ___876]))
                        __stack__.pop()
                    __stack__.pop()
                __stack__.pop()
            __stack__.pop()
        __stack__.pop()
    __stack__.pop()
    ___878 = Cell((paper != Cell(None)))
    __stack__.push()
    __stack__.add(___878.inputs)
    author.adopt(__stack__.all())
    author.adopt(___878.inputs)
    link.adopt(__stack__.all())
    link.adopt(___878.inputs)
    coauthors.adopt(__stack__.all())
    coauthors.adopt(___878.inputs)
    comments.adopt(__stack__.all())
    comments.adopt(___878.inputs)
    latest_abstract.adopt(__stack__.all())
    latest_abstract.adopt(___878.inputs)
    latest_title.adopt(__stack__.all())
    latest_title.adopt(___878.inputs)
    paper.adopt(__stack__.all())
    paper.adopt(___878.inputs)
    reviews.adopt(__stack__.all())
    reviews.adopt(___878.inputs)
    paper_versions.adopt(__stack__.all())
    paper_versions.adopt(___878.inputs)
    if ___878:
        ___880 = conf.sql(Cell('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC'), Cell([paper[Cell(0)]]))
        ___881 = filter_check(___880)[0]
        paper_versions = ___881
        paper_versions.adopt(__stack__.all())
        ___882 = conf.sql(Cell('SELECT * FROM conf_papercoauthor WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___883 = filter_check(___882)[0]
        coauthors = ___883
        coauthors.adopt(__stack__.all())
        latest_abstract = Cell(None)
        latest_abstract.adopt(__stack__.all())
        latest_title = Cell(None)
        latest_title.adopt(__stack__.all())
        ___884 = db_len(paper_versions)
        ___885 = ___884
        __stack__.push()
        __stack__.add(___885.inputs)
        latest_abstract.adopt(__stack__.all())
        latest_abstract.adopt(___885.inputs)
        latest_title.adopt(__stack__.all())
        latest_title.adopt(___885.inputs)
        if ___885:
            latest_abstract = paper_versions[Cell(0)][Cell(3)]
            latest_abstract.adopt(__stack__.all())
            latest_title = paper_versions[Cell(0)][Cell(1)]
            latest_title.adopt(__stack__.all())
        __stack__.pop()
        ___887 = conf.check(__c__, __trace__, Cell('Service'), latest_abstract)
        ___888 = non(___887)
        __stack__.push()
        __stack__.add(___888.inputs)
        latest_abstract.adopt(__stack__.all())
        latest_abstract.adopt(___888.inputs)
        if ___888:
            latest_abstract = Cell(None)
            latest_abstract.adopt(__stack__.all())
        __stack__.pop()
        ___890 = conf.check(__c__, __trace__, Cell('Service'), latest_title)
        ___891 = non(___890)
        __stack__.push()
        __stack__.add(___891.inputs)
        latest_title.adopt(__stack__.all())
        latest_title.adopt(___891.inputs)
        if ___891:
            latest_title = Cell(None)
            latest_title.adopt(__stack__.all())
        __stack__.pop()
        ___893 = conf.link(paper_versions[Cell(0)][Cell(2)])
        link = ___893
        link.adopt(__stack__.all())
        ___894 = conf.sql(Cell('SELECT * FROM reviews WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___895 = filter_check(___894)[0]
        reviews = ___895
        reviews.adopt(__stack__.all())
        ___896 = conf.sql(Cell('SELECT * FROM comments WHERE paper_id = ?0'), Cell([paper[Cell(0)]]))
        ___897 = filter_check(___896)[0]
        comments = ___897
        comments.adopt(__stack__.all())
        ___898 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([paper[Cell(2)]]))
        ___899 = filter_check(___898[Cell(0)])[0]
        author = ___899
        author.adopt(__stack__.all())
    else:
        paper = Cell(None)
        paper.adopt(__stack__.all())
        paper_versions = Cell([])
        paper_versions.adopt(__stack__.all())
        coauthors = Cell([])
        coauthors.adopt(__stack__.all())
        latest_abstract = Cell(None)
        latest_abstract.adopt(__stack__.all())
        latest_title = Cell(None)
        latest_title.adopt(__stack__.all())
        reviews = Cell([])
        reviews.adopt(__stack__.all())
        comments = Cell([])
        comments.adopt(__stack__.all())
        author = Cell(None)
        author.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('paper_versions'), Cell('author'), Cell('coauthors'), Cell('latest_abstract'), Cell('latest_title'), Cell('reviews'), Cell('comments'), Cell('profile'), Cell('which_page'), Cell('link'), Cell('review_score_fields'), Cell('is_logged_in'), Cell('is_admin')]
    ___900 = is_admin()[0]
    ___901 = conf.render(Cell('paper.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), paper_versions)),
        __k__[2].value: Cell((Cell('Service'), author)),
        __k__[3].value: Cell((Cell('Service'), coauthors)),
        __k__[4].value: Cell((Cell('Service'), latest_abstract)),
        __k__[5].value: Cell((Cell('Service'), latest_title)),
        __k__[6].value: Cell((Cell('Service'), reviews)),
        __k__[7].value: Cell((Cell('Service'), comments)),
        __k__[8].value: Cell((Cell('Service'), user)),
        __k__[9].value: Cell((Cell('Service'), Cell('paper'))),
        __k__[10].value: Cell((Cell('Service'), link)),
        __k__[11].value: Cell((Cell('Service'), Cell([Cell((Cell('Novelty'), Cell('score_novelty'), Cell(10))), Cell((Cell('Presentation'), Cell('score_presentation'), Cell(10))), Cell((Cell('Technical'), Cell('score_technical'), Cell(10))), Cell((Cell('Confidence'), Cell('score_confidence'), Cell(10)))]))),
        __k__[12].value: Cell((Cell('Service'), Cell(True))),
        __k__[13].value: Cell((Cell('Service'), ___900)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs, __k__[10].inputs, __k__[11].inputs, __k__[12].inputs, __k__[13].inputs]))
    __r__ = Cell(___901, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/paper', methods=['GET', 'POST'])
def _paper_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = paper_view()
    return (__c__, __r__, __s__, __trace__)

def submit_view():
    global __stack__
    if ('conflict' not in locals()):
        conflict = Cell(None)
    if ('pc' not in locals()):
        pc = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('pcs2' not in locals()):
        pcs2 = Cell(None)
    if ('pcs' not in locals()):
        pcs = Cell(None)
    if ('coauthor' not in locals()):
        coauthor = Cell(None)
    if ('new_pc_conflict' not in locals()):
        new_pc_conflict = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('pc_conflicts' not in locals()):
        pc_conflicts = Cell(None)
    if ('coauthors' not in locals()):
        coauthors = Cell(None)
    if ('abstract' not in locals()):
        abstract = Cell(None)
    ___902 = is_logged_in()[0]
    ___903 = non(___902)
    __trace__.add_inputs(___903.inputs)
    __stack__.push()
    __stack__.add(___903.inputs, bot=True)
    if ___903:
        ___905 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___905, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___906 = conf.method()
    ___907 = Cell((___906 == Cell('POST')))
    __trace__.add_inputs(___907.inputs)
    __stack__.push()
    __stack__.add(___907.inputs, bot=True)
    conf.add_sql_inputs('conf_papercoauthor', __stack__.all())
    conf.add_sql_inputs('conf_papercoauthor', ___907.inputs)
    conf.add_sql_inputs('papers', __stack__.all())
    conf.add_sql_inputs('papers', ___907.inputs)
    conf.add_sql_inputs('conf_paperversion', __stack__.all())
    conf.add_sql_inputs('conf_paperversion', ___907.inputs)
    coauthors.adopt(__stack__.all())
    coauthors.adopt(___907.inputs)
    title.adopt(__stack__.all())
    title.adopt(___907.inputs)
    new_pc_conflict.adopt(__stack__.all())
    new_pc_conflict.adopt(___907.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___907.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___907.inputs)
    conf.add_sql_inputs('conf_paperpcconflict', __stack__.all())
    conf.add_sql_inputs('conf_paperpcconflict', ___907.inputs)
    abstract.adopt(__stack__.all())
    abstract.adopt(___907.inputs)
    if ___907:
        ___909 = conf.getlist('submit_view', Cell('coauthors[]'))
        coauthors = ___909
        coauthors.adopt(__stack__.all())
        ___910 = conf.post('submit_view', Cell('title'))
        title = ___910
        title.adopt(__stack__.all())
        ___911 = conf.post('submit_view', Cell('abstract'))
        abstract = ___911
        abstract.adopt(__stack__.all())
        ___912 = conf.post('submit_view', Cell('contents'))
        contents = ___912
        contents.adopt(__stack__.all())
        ___913 = conf.me()
        ___914 = conf.sql(Cell('INSERT INTO papers (author_id, accepted) VALUES (?0, ?1)'), Cell([___913, Cell(False)]))
        ___915 = conf.me()
        ___916 = conf.sql(Cell('SELECT id FROM papers WHERE author_id = ?0 ORDER BY id DESC LIMIT 1'), Cell([___915]))
        paper_id = ___916[Cell(0)][Cell(0)]
        paper_id.adopt(__stack__.all())
        ___917 = coauthors
        for coauthor in ___917:
            ___919 = conf.sql(Cell('INSERT INTO conf_papercoauthor (paper_id, author) VALUES (?0, ?1)'), Cell([paper_id, coauthor]))
        ___920 = conf.now_utc()
        ___921 = conf.sql(Cell('INSERT INTO conf_paperversion (paper_id, time, title, abstract, contents) \n                    VALUES (?0, ?1, ?2, ?3, ?4)'), Cell([paper_id, ___920, title, abstract, contents]))
        ___922 = conf.getlist('submit_view', Cell('pc_conflicts[]'))
        ___923 = ___922
        for conflict in ___923:
            ___925 = conf.sql(Cell('SELECT * FROM user_profiles WHERE username = ?0'), Cell([conflict]))
            new_pc_conflict = ___925[Cell(0)][Cell(0)]
            new_pc_conflict.adopt(__stack__.all())
            ___926 = conf.sql(Cell('INSERT INTO conf_paperpcconflict (paper_id, pc_id) values (?0, ?1)'), Cell([paper_id, new_pc_conflict]))
        ___927 = db_str(paper_id)
        ___928 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___927))
        __r__ = Cell(___928, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___929 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    pcs = ___929
    pcs.adopt(__stack__.all())
    pc_conflicts = Cell([])
    pc_conflicts.adopt(__stack__.all())
    pcs2 = Cell([])
    pcs2.adopt(__stack__.all())
    ___930 = conf.me()
    ___931 = conf.sql(Cell('SELECT pc_id FROM conf_userpcconflict WHERE id = ?0'), Cell([___930]))
    ___932 = ___931
    for pc in ___932:
        pc_conflicts = (pc_conflicts + Cell([pc[Cell(0)]]))
        pc_conflicts.adopt(__stack__.all())
    ___934 = pcs
    for pc in ___934:
        __k__ = [Cell('pc'), Cell('conflict')]
        pcs2 = (pcs2 + Cell([Cell({
            __k__[0].value: pc,
            __k__[1].value: pc.in_(pc_conflicts),
        }, inputs=[__k__[0].inputs, __k__[1].inputs])]))
        pcs2.adopt(__stack__.all())
    __k__ = [Cell('coauthors'), Cell('title'), Cell('abstract'), Cell('contents'), Cell('error'), Cell('pcs'), Cell('pc_conflicts'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___936 = is_admin()[0]
    ___937 = conf.render(Cell('submit.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), Cell([]))),
        __k__[1].value: Cell((Cell('Service'), Cell(''))),
        __k__[2].value: Cell((Cell('Service'), Cell(''))),
        __k__[3].value: Cell((Cell('Service'), Cell(''))),
        __k__[4].value: Cell((Cell('Service'), Cell(''))),
        __k__[5].value: Cell((Cell('Service'), pcs2)),
        __k__[6].value: Cell((Cell('Service'), pc_conflicts)),
        __k__[7].value: Cell((Cell('Service'), Cell('submit'))),
        __k__[8].value: Cell((Cell('Service'), Cell(True))),
        __k__[9].value: Cell((Cell('Service'), ___936)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs]))
    __r__ = Cell(___937, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit', methods=['GET', 'POST'])
def _submit_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_view()
    return (__c__, __r__, __s__, __trace__)

def profile_view():
    global __stack__
    if ('pc' not in locals()):
        pc = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('pcs2' not in locals()):
        pcs2 = Cell(None)
    if ('profile' not in locals()):
        profile = Cell(None)
    if ('pcs' not in locals()):
        pcs = Cell(None)
    if ('confl' not in locals()):
        confl = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('pc_conflicts' not in locals()):
        pc_conflicts = Cell(None)
    if ('uppc' not in locals()):
        uppc = Cell(None)
    if ('acm_number' not in locals()):
        acm_number = Cell(None)
    if ('new_pc_conflict' not in locals()):
        new_pc_conflict = Cell(None)
    if ('affiliation' not in locals()):
        affiliation = Cell(None)
    ___938 = is_logged_in()[0]
    ___939 = non(___938)
    __trace__.add_inputs(___939.inputs)
    __stack__.push()
    __stack__.add(___939.inputs, bot=True)
    if ___939:
        ___941 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___941, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___942 = conf.me()
    ___943 = conf.sql(Cell('SELECT * FROM user_profiles where user_id = ?0'), Cell([___942]))
    profile = ___943[Cell(0)]
    profile.adopt(__stack__.all())
    ___944 = profile
    __stack__.push()
    __stack__.add(___944.inputs)
    profile[6].adopt(__stack__.all())
    profile[6].adopt(___944.inputs)
    if ___944:
        __slv__ = [Cell(6)]
        __w__ = profile
        for __i__ in range(1):
            __w__.adopt(__slv__[__i__].inputs)
            if (__i__ == 0):
                __w__[__slv__[__i__]] = Cell('normal')
            elif (__slv__[__i__] in __w__):
                __w__ = __w__[__slv__[__i__]]
            else:
                break
        profile.adopt(__stack__.all())
    __stack__.pop()
    ___946 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    pcs = ___946
    pcs.adopt(__stack__.all())
    ___947 = conf.method()
    ___948 = Cell((___947 == Cell('POST')))
    __trace__.add_inputs(___948.inputs)
    __stack__.push()
    __stack__.add(___948.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___948.inputs)
    pc_conflicts.adopt(__stack__.all())
    pc_conflicts.adopt(___948.inputs)
    email.adopt(__stack__.all())
    email.adopt(___948.inputs)
    affiliation.adopt(__stack__.all())
    affiliation.adopt(___948.inputs)
    new_pc_conflict.adopt(__stack__.all())
    new_pc_conflict.adopt(___948.inputs)
    acm_number.adopt(__stack__.all())
    acm_number.adopt(___948.inputs)
    conf.add_sql_inputs('conf_userpcconflict', __stack__.all())
    conf.add_sql_inputs('conf_userpcconflict', ___948.inputs)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___948.inputs)
    if ___948:
        ___950 = conf.post('profile_view', Cell('name'))
        name = ___950
        name.adopt(__stack__.all())
        ___951 = conf.post('profile_view', Cell('affiliation'))
        affiliation = ___951
        affiliation.adopt(__stack__.all())
        ___952 = conf.post('profile_view', Cell('acm_number'))
        acm_number = ___952
        acm_number.adopt(__stack__.all())
        ___953 = conf.post('profile_view', Cell('email'))
        email = ___953
        email.adopt(__stack__.all())
        ___954 = conf.me()
        ___955 = conf.sql(Cell('DELETE FROM user_profiles WHERE user_id = ?0'), Cell([___954]))
        ___956 = conf.me()
        ___957 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level, password)\n                    VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6, ?7)'), Cell([___956, profile[Cell(1)], email, name, affiliation, acm_number, profile[Cell(6)], profile[Cell(7)]]))
        pc_conflicts = Cell([])
        pc_conflicts.adopt(__stack__.all())
        ___958 = conf.getlist('profile_view', Cell('pc_conflicts[]'))
        ___959 = ___958
        for confl in ___959:
            ___961 = conf.sql(Cell('SELECT id FROM user_profiles WHERE username = ?0'), Cell([confl]))
            new_pc_conflict = ___961[Cell(0)][Cell(0)]
            new_pc_conflict.adopt(__stack__.all())
            ___962 = conf.me()
            ___963 = conf.sql(Cell('INSERT INTO conf_userpcconflict (user_id, pc_id) VALUES (?0, ?1)'), Cell([___962, new_pc_conflict]))
            pc_conflicts = (pc_conflicts + Cell([new_pc_conflict]))
            pc_conflicts.adopt(__stack__.all())
    else:
        pc_conflicts = Cell([])
        pc_conflicts.adopt(__stack__.all())
        ___964 = conf.me()
        ___965 = conf.sql(Cell('SELECT id FROM conf_userpcconflict WHERE id = ?0'), Cell([___964]))
        ___966 = ___965
        for uppc in ___966:
            pc_conflicts = (pc_conflicts + Cell([uppc[Cell(0)]]))
            pc_conflicts.adopt(__stack__.all())
    __stack__.pop()
    pcs2 = Cell([])
    pcs2.adopt(__stack__.all())
    ___968 = pcs
    for pc in ___968:
        __k__ = [Cell('pc'), Cell('conflict')]
        pcs2 = (pcs2 + Cell([Cell({
            __k__[0].value: pc,
            __k__[1].value: pc.in_(pc_conflicts),
        }, inputs=[__k__[0].inputs, __k__[1].inputs])]))
        pcs2.adopt(__stack__.all())
    __k__ = [Cell('name'), Cell('affiliation'), Cell('acm_number'), Cell('pc_conflicts'), Cell('email'), Cell('which_page'), Cell('pcs'), Cell('is_logged_in'), Cell('is_admin')]
    ___970 = is_admin()[0]
    ___971 = conf.render(Cell('profile.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), profile[Cell(3)])),
        __k__[1].value: Cell((Cell('Service'), profile[Cell(4)])),
        __k__[2].value: Cell((Cell('Service'), profile[Cell(5)])),
        __k__[3].value: Cell((Cell('Service'), pc_conflicts)),
        __k__[4].value: Cell((Cell('Service'), profile[Cell(2)])),
        __k__[5].value: Cell((Cell('Service'), Cell('profile'))),
        __k__[6].value: Cell((Cell('Service'), pcs2)),
        __k__[7].value: Cell((Cell('Service'), Cell(True))),
        __k__[8].value: Cell((Cell('Service'), ___970)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs]))
    __r__ = Cell(___971, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/profile/', methods=['GET', 'POST'])
def _profile_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile_view()
    return (__c__, __r__, __s__, __trace__)

def submit_review_view():
    global __stack__
    if ('score_novelty' not in locals()):
        score_novelty = Cell(None)
    if ('score_confidence' not in locals()):
        score_confidence = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    if ('score_technical' not in locals()):
        score_technical = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('score_presentation' not in locals()):
        score_presentation = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    if ('reviewer_id' not in locals()):
        reviewer_id = Cell(None)
    ___972 = me_user()[0]
    user = ___972
    user.adopt(__stack__.all())
    ___973 = non(user)
    __trace__.add_inputs(___973.inputs)
    __stack__.push()
    __stack__.add(___973.inputs, bot=True)
    if ___973:
        ___975 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___975, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___976 = conf.method()
    ___977 = Cell((___976 == Cell('GET')))
    __stack__.push()
    __stack__.add(___977.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___977.inputs)
    if ___977:
        ___979 = conf.get('submit_review_view', Cell('id'))
        ___980 = db_int(___979)
        paper_id = ___980
        paper_id.adopt(__stack__.all())
    else:
        ___981 = conf.method()
        ___982 = Cell((___981 == Cell('POST')))
        __stack__.push()
        __stack__.add(___982.inputs)
        paper_id.adopt(__stack__.all())
        paper_id.adopt(___982.inputs)
        if ___982:
            ___984 = conf.get('submit_review_view', Cell('id'))
            ___985 = db_int(___984)
            paper_id = ___985
            paper_id.adopt(__stack__.all())
        __stack__.pop()
    __stack__.pop()
    ___986 = query_db(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([paper_id]))[0]
    paper = ___986[Cell(0)]
    paper.adopt(__stack__.all())
    ___987 = conf.method()
    ___988 = Cell((___987 == Cell('POST')))
    __trace__.add_inputs(___988.inputs)
    __stack__.push()
    __stack__.add(___988.inputs, bot=True)
    score_technical.adopt(__stack__.all())
    score_technical.adopt(___988.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___988.inputs)
    conf.add_sql_inputs('reviews', __stack__.all())
    conf.add_sql_inputs('reviews', ___988.inputs)
    score_confidence.adopt(__stack__.all())
    score_confidence.adopt(___988.inputs)
    score_novelty.adopt(__stack__.all())
    score_novelty.adopt(___988.inputs)
    score_presentation.adopt(__stack__.all())
    score_presentation.adopt(___988.inputs)
    if ___988:
        ___990 = conf.get('submit_review_view', Cell('contents'))
        contents = ___990
        contents.adopt(__stack__.all())
        ___991 = conf.get('submit_review_view', Cell('score_novelty'))
        score_novelty = ___991
        score_novelty.adopt(__stack__.all())
        ___992 = conf.get('submit_review_view', Cell('score_presentation'))
        score_presentation = ___992
        score_presentation.adopt(__stack__.all())
        ___993 = conf.get('submit_review_view', Cell('score_technical'))
        score_technical = ___993
        score_technical.adopt(__stack__.all())
        ___994 = conf.get('submit_review_view', Cell('score_confidence'))
        score_confidence = ___994
        score_confidence.adopt(__stack__.all())
        ___995 = conf.sql(Cell('INSERT INTO reviews (contents, score_novelty, score_presentation, \n                                    score_technical, score_confidence, paper_id, reviewer_id)\n               VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([contents, score_novelty, score_presentation, score_technical, score_confidence, paper_id, reviewer_id]))
        ___996 = db_str(paper_id)
        ___997 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___996))
        __r__ = Cell(___997, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('contents'), Cell('score_novelty'), Cell('score_presentation'), Cell('score_technical'), Cell('score_confidence'), Cell('paper_id'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___998 = is_admin()[0]
    ___999 = conf.render(Cell('submit_review.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), contents)),
        __k__[2].value: Cell((Cell('Service'), score_novelty)),
        __k__[3].value: Cell((Cell('Service'), score_presentation)),
        __k__[4].value: Cell((Cell('Service'), score_technical)),
        __k__[5].value: Cell((Cell('Service'), score_confidence)),
        __k__[6].value: Cell((Cell('Service'), paper_id)),
        __k__[7].value: Cell((Cell('Service'), Cell('submit_review'))),
        __k__[8].value: Cell((Cell('Service'), Cell(True))),
        __k__[9].value: Cell((Cell('Service'), ___998)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs, __k__[9].inputs]))
    __r__ = Cell(___999, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit_review', methods=['GET', 'POST'])
def _submit_review_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_review_view()
    return (__c__, __r__, __s__, __trace__)

def users_view():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('user_profiles' not in locals()):
        user_profiles = Cell(None)
    ___1000 = me_user()[0]
    user = ___1000
    user.adopt(__stack__.all())
    ___1001 = non(user)
    __trace__.add_inputs(___1001.inputs)
    __stack__.push()
    __stack__.add(___1001.inputs, bot=True)
    if ___1001:
        ___1003 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___1003, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1004 = Cell((user[(- Cell(1))] != Cell('chair')))
    __trace__.add_inputs(___1004.inputs)
    __stack__.push()
    __stack__.add(___1004.inputs, bot=True)
    if ___1004:
        ___1006 = conf.redirect(Cell('Service'), Cell('/papers_view'))
        __r__ = Cell(___1006, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1007 = conf.sql(Cell('SELECT * FROM user_profiles'))
    user_profiles = ___1007
    user_profiles.adopt(__stack__.all())
    __k__ = [Cell('user_profiles'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1008 = conf.render(Cell('users_view.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_profiles)),
        __k__[1].value: Cell((Cell('Service'), Cell('users'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1008, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/users', methods=['GET', 'POST'])
def _users_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users_view()
    return (__c__, __r__, __s__, __trace__)

def set_level(level, user_id):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1009 = me_user()[0]
    user = ___1009
    user.adopt(__stack__.all())
    ___1010 = user
    __trace__.add_inputs(___1010.inputs)
    __stack__.push()
    __stack__.add(___1010.inputs, bot=True)
    user.adopt(__stack__.all())
    user.adopt(___1010.inputs)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___1010.inputs)
    if ___1010:
        ___1012 = Cell((Cell((user[(- Cell(1))] == Cell('chair'))).value and level.in_(Cell([Cell('normal'), Cell('pc'), Cell('chair')])).value), inputs=(Cell((user[(- Cell(1))] == Cell('chair'))).inputs + level.in_(Cell([Cell('normal'), Cell('pc'), Cell('chair')])).inputs))
        __stack__.push()
        __stack__.add(___1012.inputs)
        user.adopt(__stack__.all())
        user.adopt(___1012.inputs)
        conf.add_sql_inputs('user_profiles', __stack__.all())
        conf.add_sql_inputs('user_profiles', ___1012.inputs)
        if ___1012:
            ___1014 = conf.sql(Cell('SELECT (username, email, name, affiliation, acm_number, level)\n                               FROM user_profiles WHERE user_id = ?0'), Cell([user_id]))
            user = ___1014[Cell(0)]
            user.adopt(__stack__.all())
            ___1015 = conf.sql(Cell('DELETE FROM user_profiles WHERE user_id = ?0'), Cell([user_id]))
            ___1016 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)\n                      VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([user_id, user[Cell(0)], user[Cell(1)], user[Cell(2)], user[Cell(3)], user[Cell(4)], level]))
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/set_level/<level>/<user_id>', methods=['GET'])
def _set_level(level, user_id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    level = conf.register('set_level', 'level', level)
    user_id = conf.register('set_level', 'user_id', user_id)
    (__r__, __s__) = set_level(level, user_id)
    return (__c__, __r__, __s__, __trace__)

def submit_comment_view():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('paper_id' not in locals()):
        paper_id = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('contents' not in locals()):
        contents = Cell(None)
    ___1017 = me_user()[0]
    user = ___1017
    user.adopt(__stack__.all())
    ___1018 = non(user)
    __trace__.add_inputs(___1018.inputs)
    __stack__.push()
    __stack__.add(___1018.inputs, bot=True)
    if ___1018:
        ___1020 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1020, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1021 = conf.method()
    ___1022 = Cell((___1021 == Cell('GET')))
    __stack__.push()
    __stack__.add(___1022.inputs)
    paper_id.adopt(__stack__.all())
    paper_id.adopt(___1022.inputs)
    if ___1022:
        ___1024 = conf.get('submit_comment_view', Cell('id'))
        ___1025 = db_int(___1024)
        paper_id = ___1025
        paper_id.adopt(__stack__.all())
    else:
        ___1026 = conf.method()
        ___1027 = Cell((___1026 == Cell('POST')))
        __stack__.push()
        __stack__.add(___1027.inputs)
        paper_id.adopt(__stack__.all())
        paper_id.adopt(___1027.inputs)
        if ___1027:
            ___1029 = conf.get('submit_comment_view', Cell('id'))
            ___1030 = db_int(___1029)
            paper_id = ___1030
            paper_id.adopt(__stack__.all())
        __stack__.pop()
    __stack__.pop()
    ___1031 = conf.sql(Cell('SELECT * FROM papers WHERE id = ?0'), Cell([paper_id]))
    paper = ___1031[Cell(0)]
    paper.adopt(__stack__.all())
    ___1032 = conf.method()
    ___1033 = Cell((___1032 == Cell('POST')))
    __trace__.add_inputs(___1033.inputs)
    __stack__.push()
    __stack__.add(___1033.inputs, bot=True)
    conf.add_sql_inputs('comments', __stack__.all())
    conf.add_sql_inputs('comments', ___1033.inputs)
    contents.adopt(__stack__.all())
    contents.adopt(___1033.inputs)
    if ___1033:
        ___1035 = conf.get('submit_comment_view', Cell('contents'))
        contents = ___1035
        contents.adopt(__stack__.all())
        ___1036 = conf.me()
        ___1037 = conf.sql(Cell('INSERT INTO comments (contents, paper_id, user_id) VALUES (?0, ?1, ?2)'), Cell([contents, paper_id, ___1036]))
        ___1038 = db_str(paper_id)
        ___1039 = conf.redirect(Cell('Service'), (Cell('/paper?id=') + ___1038))
        __r__ = Cell(___1039, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('paper'), Cell('contents'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1040 = is_admin()[0]
    ___1041 = conf.render(Cell('submit_comment.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), paper)),
        __k__[1].value: Cell((Cell('Service'), contents)),
        __k__[2].value: Cell((Cell('Service'), Cell('submit_comment'))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
        __k__[4].value: Cell((Cell('Service'), ___1040)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1041, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/submit_comment', methods=['GET', 'POST'])
def _submit_comment_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = submit_comment_view()
    return (__c__, __r__, __s__, __trace__)

def assign_reviews_view():
    global __stack__
    if ('reviewer' not in locals()):
        reviewer = Cell(None)
    if ('papers' not in locals()):
        papers = Cell(None)
    if ('reviewers' not in locals()):
        reviewers = Cell(None)
    if ('possible_reviewers' not in locals()):
        possible_reviewers = Cell(None)
    if ('paper' not in locals()):
        paper = Cell(None)
    if ('reviewer_id' not in locals()):
        reviewer_id = Cell(None)
    if ('papers_data' not in locals()):
        papers_data = Cell(None)
    ___1042 = is_logged_in()[0]
    ___1043 = non(___1042)
    __trace__.add_inputs(___1043.inputs)
    __stack__.push()
    __stack__.add(___1043.inputs, bot=True)
    if ___1043:
        ___1045 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1045, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1046 = conf.sql(Cell("SELECT * FROM user_profiles WHERE level = 'pc'"))
    possible_reviewers = ___1046
    possible_reviewers.adopt(__stack__.all())
    ___1047 = conf.get('assign_reviews_view', Cell('reviewer_username'))
    reviewer_id = ___1047
    reviewer_id.adopt(__stack__.all())
    ___1048 = reviewer_id
    __stack__.push()
    __stack__.add(___1048.inputs)
    papers_data.adopt(__stack__.all())
    papers_data.adopt(___1048.inputs)
    papers.adopt(__stack__.all())
    papers.adopt(___1048.inputs)
    reviewers.adopt(__stack__.all())
    reviewers.adopt(___1048.inputs)
    reviewer.adopt(__stack__.all())
    reviewer.adopt(___1048.inputs)
    if ___1048:
        ___1050 = conf.sql(Cell('SELECT * FROM papers'))
        papers = ___1050
        papers.adopt(__stack__.all())
        papers_data = Cell([])
        papers_data.adopt(__stack__.all())
        ___1051 = conf.sql(Cell('SELECT * FROM reviewers WHERE id = ?0'), Cell([reviewer_id]))
        reviewers = ___1051
        reviewers.adopt(__stack__.all())
        ___1052 = papers
        for paper in ___1052:
            __k__ = [Cell('paper'), Cell('author_name'), Cell('latest_version'), Cell('assignment'), Cell('has_conflict')]
            ___1054 = conf.sql(Cell('SELECT name FROM user_profiles WHERE user_id = ?0'), Cell([reviewer_id]))
            ___1055 = conf.sql(Cell('SELECT * FROM conf_paperversion WHERE paper_id = ?0 ORDER BY time DESC'), Cell([paper[Cell(0)]]))
            ___1056 = conf.sql(Cell('SELECT * FROM review_assignments WHERE paper_id = ?0 AND user_id = ?1'), Cell([paper[Cell(0)], reviewer_id]))
            ___1057 = conf.sql(Cell('SELECT * FROM conf_paperpcconflict WHERE pc_id = ?0 and paper_id = ?1'), Cell([reviewer_id, paper[Cell(0)]]))
            papers = (papers + Cell([Cell({
                __k__[0].value: paper[Cell(0)],
                __k__[1].value: ___1054[Cell(0)][Cell(0)],
                __k__[2].value: ___1055[(- Cell(1))],
                __k__[3].value: ___1056,
                __k__[4].value: Cell((___1057[Cell(0)] != Cell(None))),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs])]))
            papers.adopt(__stack__.all())
    else:
        reviewer = Cell(None)
        reviewer.adopt(__stack__.all())
        papers_data = Cell([])
        papers_data.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('reviewer'), Cell('possible_reviewers'), Cell('papers_data'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1058 = is_admin()[0]
    ___1059 = conf.render(Cell('assign_reviews.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), reviewer)),
        __k__[1].value: Cell((Cell('Service'), possible_reviewers)),
        __k__[2].value: Cell((Cell('Service'), papers_data)),
        __k__[3].value: Cell((Cell('Service'), Cell('assign_reviews'))),
        __k__[4].value: Cell((Cell('Service'), Cell(True))),
        __k__[5].value: Cell((Cell('Service'), ___1058)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___1059, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/assign_reviews', methods=['GET', 'POST'])
def _assign_reviews_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = assign_reviews_view()
    return (__c__, __r__, __s__, __trace__)

def assign_reviewer(paper_id, reviewer_id, value):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1060 = me_user()[0]
    user = ___1060
    user.adopt(__stack__.all())
    ___1061 = user
    __trace__.add_inputs(___1061.inputs)
    __stack__.push()
    __stack__.add(___1061.inputs, bot=True)
    conf.add_sql_inputs('review_assignments', __stack__.all())
    conf.add_sql_inputs('review_assignments', ___1061.inputs)
    if ___1061:
        ___1063 = Cell((user[(- Cell(1))] == Cell('chair')))
        __stack__.push()
        __stack__.add(___1063.inputs)
        conf.add_sql_inputs('review_assignments', __stack__.all())
        conf.add_sql_inputs('review_assignments', ___1063.inputs)
        if ___1063:
            ___1065 = Cell((value == Cell('yes')))
            __stack__.push()
            __stack__.add(___1065.inputs)
            conf.add_sql_inputs('review_assignments', __stack__.all())
            conf.add_sql_inputs('review_assignments', ___1065.inputs)
            if ___1065:
                ___1067 = conf.sql(Cell('INSERT INTO review_assignments (assign_type, paper_id, user_id) VALUES (?0, ?1, ?2)'), Cell([Cell('assigned'), paper_id, reviewer_id]))
            else:
                ___1068 = conf.sql(Cell('DELETE FROM review_assignments WHERE paper_id = ?0 AND reviewer_id = ?1'), Cell([paper_id, reviewer_id]))
            __stack__.pop()
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/assign_reviewer/<paper_id>/<reviewer_id>/<value>', methods=['GET'])
def _assign_reviewer(paper_id, reviewer_id, value):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    paper_id = conf.register('assign_reviewer', 'paper_id', paper_id)
    reviewer_id = conf.register('assign_reviewer', 'reviewer_id', reviewer_id)
    value = conf.register('assign_reviewer', 'value', value)
    (__r__, __s__) = assign_reviewer(paper_id, reviewer_id, value)
    return (__c__, __r__, __s__, __trace__)

def search_view():
    global __stack__
    if ('authors' not in locals()):
        authors = Cell(None)
    if ('reviewers' not in locals()):
        reviewers = Cell(None)
    ___1069 = is_logged_in()[0]
    ___1070 = non(___1069)
    __trace__.add_inputs(___1070.inputs)
    __stack__.push()
    __stack__.add(___1070.inputs, bot=True)
    if ___1070:
        ___1072 = conf.redirect(Cell('Service'), Cell('/login'))
        __r__ = Cell(___1072, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1073 = conf.sql(Cell('SELECT * FROM user_profiles'))
    reviewers = ___1073
    reviewers.adopt(__stack__.all())
    ___1074 = conf.sql(Cell('SELECT * FROM user_profiles'))
    authors = ___1074
    authors.adopt(__stack__.all())
    __k__ = [Cell('reviewers'), Cell('authors'), Cell('which_page'), Cell('is_logged_in'), Cell('is_admin')]
    ___1075 = is_admin()[0]
    ___1076 = conf.render(Cell('search.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), reviewers)),
        __k__[1].value: Cell((Cell('Service'), authors)),
        __k__[2].value: Cell((Cell('Service'), Cell('search'))),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
        __k__[4].value: Cell((Cell('Service'), ___1075)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1076, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/search')
def _search_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = search_view()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    ___1077 = is_logged_in()[0]
    ___1078 = ___1077
    __trace__.add_inputs(___1078.inputs)
    __stack__.push()
    __stack__.add(___1078.inputs, bot=True)
    if ___1078:
        ___1080 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1080, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1081 = conf.render(Cell('registration/login.html'))
    __r__ = Cell(___1081, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def do_login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1082 = is_logged_in()[0]
    ___1083 = ___1082
    __trace__.add_inputs(___1083.inputs)
    __stack__.push()
    __stack__.add(___1083.inputs, bot=True)
    if ___1083:
        ___1085 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1085, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1086 = conf.me()
    ___1087 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___1086]))
    user = ___1087
    user.adopt(__stack__.all())
    ___1088 = db_len(user)
    ___1089 = Cell((___1088 > Cell(0)))
    __trace__.add_inputs(___1089.inputs)
    __stack__.push()
    __stack__.add(___1089.inputs, bot=True)
    conf.add_session_inputs('loggedin', __stack__.all())
    conf.add_session_inputs('loggedin', ___1089.inputs)
    if ___1089:
        ___1091 = conf.set_session(Cell('loggedin'), Cell(True))
        ___1092 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1092, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('error')]
        ___1093 = conf.render(Cell('registration/login.html'), Cell({
            __k__[0].value: Cell((Cell('Service'), Cell('Invalid user (register to access)'))),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___1093, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@conf.route('/accounts/do_login')
def _do_login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = do_login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('acm_number' not in locals()):
        acm_number = Cell(None)
    if ('username' not in locals()):
        username = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    if ('affiliation' not in locals()):
        affiliation = Cell(None)
    ___1094 = is_logged_in()[0]
    ___1095 = ___1094
    __trace__.add_inputs(___1095.inputs)
    __stack__.push()
    __stack__.add(___1095.inputs, bot=True)
    if ___1095:
        ___1097 = conf.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1097, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1098 = conf.me()
    user_id = ___1098
    user_id.adopt(__stack__.all())
    ___1099 = conf.name()
    username = ___1099
    username.adopt(__stack__.all())
    ___1100 = conf.method()
    ___1101 = Cell((___1100 == Cell('POST')))
    __trace__.add_inputs(___1101.inputs)
    __stack__.push()
    __stack__.add(___1101.inputs, bot=True)
    affiliation.adopt(__stack__.all())
    affiliation.adopt(___1101.inputs)
    email.adopt(__stack__.all())
    email.adopt(___1101.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1101.inputs)
    acm_number.adopt(__stack__.all())
    acm_number.adopt(___1101.inputs)
    name.adopt(__stack__.all())
    name.adopt(___1101.inputs)
    level.adopt(__stack__.all())
    level.adopt(___1101.inputs)
    conf.add_sql_inputs('user_profiles', __stack__.all())
    conf.add_sql_inputs('user_profiles', ___1101.inputs)
    if ___1101:
        ___1103 = conf.me()
        ___1104 = conf.sql(Cell('SELECT * FROM user_profiles WHERE user_id = ?0'), Cell([___1103]))
        already_registered = ___1104
        already_registered.adopt(__stack__.all())
        ___1105 = db_len(already_registered)
        ___1106 = Cell((___1105 == Cell(0)))
        __stack__.push()
        __stack__.add(___1106.inputs)
        affiliation.adopt(__stack__.all())
        affiliation.adopt(___1106.inputs)
        email.adopt(__stack__.all())
        email.adopt(___1106.inputs)
        acm_number.adopt(__stack__.all())
        acm_number.adopt(___1106.inputs)
        name.adopt(__stack__.all())
        name.adopt(___1106.inputs)
        level.adopt(__stack__.all())
        level.adopt(___1106.inputs)
        conf.add_sql_inputs('user_profiles', __stack__.all())
        conf.add_sql_inputs('user_profiles', ___1106.inputs)
        if ___1106:
            ___1108 = conf.post('register', Cell('email'))
            email = ___1108
            email.adopt(__stack__.all())
            ___1109 = conf.post('register', Cell('name'))
            name = ___1109
            name.adopt(__stack__.all())
            ___1110 = conf.post('register', Cell('affiliation'))
            affiliation = ___1110
            affiliation.adopt(__stack__.all())
            ___1111 = conf.post('register', Cell('acm_number'))
            acm_number = ___1111
            acm_number.adopt(__stack__.all())
            level = Cell('normal')
            level.adopt(__stack__.all())
            ___1112 = conf.sql(Cell('INSERT INTO user_profiles (user_id, username, email, name, affiliation, acm_number, level)\n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5, ?6)'), Cell([user_id, username, email, name, affiliation, acm_number, level]))
        __stack__.pop()
        ___1113 = conf.redirect(Cell('Service'), Cell('/accounts/do_login'))
        __r__ = Cell(___1113, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('username')]
    ___1114 = conf.render(Cell('registration/register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), username)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1114, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1115 = conf.pop_session(Cell('loggedin'))
    ___1116 = conf.redirect(Cell('Service'), Cell('/accounts/login'))
    __r__ = Cell(___1116, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@conf.route('/accounts/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)
