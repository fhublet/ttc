
from apps import minitwitplus
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___0 = minitwitplus.get_session(Cell('loggedin'))
    __r__ = Cell(___0, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1 = is_logged_in()[0]
    ___2 = ___1
    __trace__.add_inputs(___2.inputs)
    __stack__.push()
    __stack__.add(___2.inputs, bot=True)
    if ___2:
        ___4 = minitwitplus.me()
        ___5 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___4]))
        __r__ = Cell(___5[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('message' not in locals()):
        message = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___6 = messages
    for message in ___6:
        ___8 = minitwitplus.check(__c__, __trace__, Cell('Service'), message)
        ___9 = ___8
        __stack__.push()
        __stack__.add(___9.inputs)
        messages2.adopt(__stack__.all())
        messages2.adopt(___9.inputs)
        if ___9:
            messages2 = (messages2 + Cell([message]))
            messages2.adopt(__stack__.all())
        __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def get_user_id(username):
    global __stack__
    if ('rv' not in locals()):
        rv = Cell(None)
    ___11 = minitwitplus.sql(Cell('SELECT id FROM user WHERE username = ?0'), Cell([username]))
    rv = ___11
    rv.adopt(__stack__.all())
    ___12 = db_len(rv)
    ___13 = Cell((___12 > Cell(0)))
    __trace__.add_inputs(___13.inputs)
    __stack__.push()
    __stack__.add(___13.inputs, bot=True)
    if ___13:
        __r__ = Cell(rv[Cell(0)][Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def generate_ad(messages):
    global __stack__
    if ('message' not in locals()):
        message = Cell(None)
    if ('all_message_texts' not in locals()):
        all_message_texts = Cell(None)
    all_message_texts = Cell('')
    all_message_texts.adopt(__stack__.all())
    ___15 = messages
    for message in ___15:
        all_message_texts = (all_message_texts + message[Cell(3)])
        all_message_texts.adopt(__stack__.all())
    ___17 = Cell('editor').in_(all_message_texts)
    __trace__.add_inputs(___17.inputs)
    __stack__.push()
    __stack__.add(___17.inputs, bot=True)
    if ___17:
        __k__ = [Cell('title'), Cell('text'), Cell('link')]
        __r__ = Cell(Cell({
            __k__[0].value: Cell('Emacs'),
            __k__[1].value: Cell('... is the best text editor'),
            __k__[2].value: Cell('https://emacs.org'),
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___19 = Cell('OS').in_(all_message_texts)
        __stack__.push()
        __stack__.add(___19.inputs)
        if ___19:
            __k__ = [Cell('title'), Cell('text'), Cell('link')]
            __r__ = Cell(Cell({
                __k__[0].value: Cell('Linux'),
                __k__[1].value: Cell('... has the best kernel'),
                __k__[2].value: Cell('https://linux.org'),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        else:
            __k__ = [Cell('title'), Cell('text'), Cell('link')]
            __r__ = Cell(Cell({
                __k__[0].value: Cell('Lausanne'),
                __k__[1].value: Cell('... has the best conferences'),
                __k__[2].value: Cell('https://lausanne.ch'),
            }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        __stack__.pop()
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages1' not in locals()):
        messages1 = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('messages12' not in locals()):
        messages12 = Cell(None)
    ___21 = me_user()[0]
    user = ___21
    user.adopt(__stack__.all())
    ___22 = non(user)
    __trace__.add_inputs(___22.inputs)
    __stack__.push()
    __stack__.add(___22.inputs, bot=True)
    if ___22:
        ___24 = minitwitplus.redirect(Cell('Service'), Cell('/public'))
        __r__ = Cell(___24, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___25 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       JOIN follower ON follower.whom_id = user.id\n       WHERE follower.who_id = ?0\n       ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages1 = ___25
    messages1.adopt(__stack__.all())
    ___26 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages2 = ___26
    messages2.adopt(__stack__.all())
    ___27 = db_sorted_by0((messages1 + messages2))
    ___28 = filter_check(___27)[0]
    messages12 = ___28
    messages12.adopt(__stack__.all())
    messages = Cell([])
    messages.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___29 = db_len(messages12)
    ___30 = Cell((Cell((i < Cell(30))).value and Cell((i < ___29)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___29)).inputs))
    __stack__.push()
    __stack__.add(___30.inputs)
    i.adopt(__stack__.all())
    i.adopt(___30.inputs)
    messages.adopt(__stack__.all())
    messages.adopt(___30.inputs)
    while ___30:
        messages = (messages + Cell([messages12[i]]))
        messages.adopt(__stack__.all())
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___30 = Cell((Cell((i < Cell(30))).value and Cell((i < ___29)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___29)).inputs))
        __stack__.push()
        __stack__.add(___30.inputs)
        i.adopt(__stack__.all())
        i.adopt(___30.inputs)
        messages.adopt(__stack__.all())
        messages.adopt(___30.inputs)
    __stack__.pop()
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('flash'), Cell('ad')]
    ___32 = minitwitplus.unflash()
    ___33 = generate_ad(messages)[0]
    ___34 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), user)),
        __k__[2].value: Cell((Cell('Service'), Cell('My Timeline'))),
        __k__[3].value: Cell((Cell('Service'), ___32)),
        __k__[4].value: Cell((Cell('Marketing'), ___33)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___34, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/')
def _timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = timeline()
    return (__c__, __r__, __s__, __trace__)

def public_timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    ___35 = minitwitplus.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       ORDER BY message.pub_date DESC LIMIT 30'))
    messages = ___35
    messages.adopt(__stack__.all())
    ___36 = filter_check(messages)[0]
    messages = ___36
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('ad')]
    ___37 = me_user()[0]
    ___38 = generate_ad(messages)[0]
    ___39 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), ___37)),
        __k__[2].value: Cell((Cell('Service'), Cell('Public Timeline'))),
        __k__[3].value: Cell((Cell('Marketing'), ___38)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___39, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/public')
def _public_timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = public_timeline()
    return (__c__, __r__, __s__, __trace__)

def user_timeline(username):
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('followed' not in locals()):
        followed = Cell(None)
    if ('profile_user' not in locals()):
        profile_user = Cell(None)
    ___40 = minitwitplus.sql(Cell('SELECT * FROM user WHERE username = ?0'), Cell([username]))
    profile_user = ___40
    profile_user.adopt(__stack__.all())
    ___41 = db_len(profile_user)
    ___42 = Cell((___41 == Cell(0)))
    __trace__.add_inputs(___42.inputs)
    __stack__.push()
    __stack__.add(___42.inputs, bot=True)
    if ___42:
        ___44 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___44, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    followed = Cell(False)
    followed.adopt(__stack__.all())
    ___45 = me_user()[0]
    user = ___45
    user.adopt(__stack__.all())
    ___46 = user
    __trace__.add_inputs(___46.inputs)
    __stack__.push()
    __stack__.add(___46.inputs)
    followed.adopt(__stack__.all())
    followed.adopt(___46.inputs)
    if ___46:
        ___48 = minitwitplus.sql(Cell('\n      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1'), Cell([user[Cell(0)], profile_user[Cell(0)][Cell(0)]]))
        ___49 = db_len(___48)
        followed = Cell((___49 > Cell(0)))
        followed.adopt(__stack__.all())
    __stack__.pop()
    ___50 = minitwitplus.sql(Cell('\n      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n             user.user_id, user.username FROM message\n      JOIN user ON user.id = message.author_id\n      WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([profile_user[Cell(0)][Cell(0)]]))
    messages = ___50
    messages.adopt(__stack__.all())
    ___51 = filter_check(messages)[0]
    messages = ___51
    messages.adopt(__stack__.all())
    ___52 = minitwitplus.send(__c__, __trace__, Cell('trustedanalytics.com'), Cell('Analytics'), username)
    ___53 = minitwitplus.send(__c__, __trace__, Cell('evilanalytics.com'), Cell('Analytics'), username)
    __k__ = [Cell('messages'), Cell('followed'), Cell('profile_user'), Cell('user'), Cell('flash'), Cell('ad')]
    ___54 = minitwitplus.unflash()
    ___55 = generate_ad(messages)[0]
    ___56 = minitwitplus.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), followed)),
        __k__[2].value: Cell((Cell('Service'), profile_user[Cell(0)])),
        __k__[3].value: Cell((Cell('Service'), user)),
        __k__[4].value: Cell((Cell('Service'), ___54)),
        __k__[5].value: Cell((Cell('Marketing'), ___55)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs]))
    __r__ = Cell(___56, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>')
def _user_timeline(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('user_timeline', 'username', username)
    (__r__, __s__) = user_timeline(username)
    return (__c__, __r__, __s__, __trace__)

def follow_user(username):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    ___57 = me_user()[0]
    user = ___57
    user.adopt(__stack__.all())
    ___58 = non(user)
    __trace__.add_inputs(___58.inputs)
    __stack__.push()
    __stack__.add(___58.inputs, bot=True)
    if ___58:
        ___60 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___60, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___61 = get_user_id(username)[0]
    whom_id = ___61
    whom_id.adopt(__stack__.all())
    ___62 = non(whom_id)
    __trace__.add_inputs(___62.inputs)
    __stack__.push()
    __stack__.add(___62.inputs, bot=True)
    if ___62:
        ___64 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___64, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___65 = minitwitplus.sql(Cell('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)'), Cell([user[Cell(0)], whom_id]))
    ___66 = minitwitplus.flash(((Cell('You are now following "') + username) + Cell('"')))
    ___67 = minitwitplus.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___67, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>/follow')
def _follow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('follow_user', 'username', username)
    (__r__, __s__) = follow_user(username)
    return (__c__, __r__, __s__, __trace__)

def unfollow_user(username):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    ___68 = me_user()[0]
    user = ___68
    user.adopt(__stack__.all())
    ___69 = non(user)
    __trace__.add_inputs(___69.inputs)
    __stack__.push()
    __stack__.add(___69.inputs, bot=True)
    if ___69:
        ___71 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___71, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___72 = get_user_id(username)[0]
    whom_id = ___72
    whom_id.adopt(__stack__.all())
    ___73 = non(whom_id)
    __trace__.add_inputs(___73.inputs)
    __stack__.push()
    __stack__.add(___73.inputs, bot=True)
    if ___73:
        ___75 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___75, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___76 = minitwitplus.sql(Cell('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1'), Cell([user[Cell(0)], whom_id]))
    ___77 = minitwitplus.flash(((Cell('You are no longer following "') + username) + Cell('"')))
    ___78 = minitwitplus.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___78, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/<username>/unfollow')
def _unfollow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwitplus.register('unfollow_user', 'username', username)
    (__r__, __s__) = unfollow_user(username)
    return (__c__, __r__, __s__, __trace__)

def add_message():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___79 = me_user()[0]
    user = ___79
    user.adopt(__stack__.all())
    ___80 = non(user)
    __trace__.add_inputs(___80.inputs)
    __stack__.push()
    __stack__.add(___80.inputs, bot=True)
    if ___80:
        ___82 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___82, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___83 = minitwitplus.method()
    ___84 = Cell((___83 == Cell('POST')))
    __trace__.add_inputs(___84.inputs)
    __stack__.push()
    __stack__.add(___84.inputs, bot=True)
    minitwitplus.add_sql_inputs('message', __stack__.all())
    minitwitplus.add_sql_inputs('message', ___84.inputs)
    if ___84:
        ___86 = minitwitplus.post('add_message', Cell('text'))
        ___87 = minitwitplus.now_utc()
        ___88 = minitwitplus.sql(Cell('INSERT INTO message (author_id, text, pub_date) \n          VALUES (?0, ?1, ?2)'), Cell([user[Cell(0)], ___86, ___87]))
        ___89 = minitwitplus.flash(Cell('Your message was recorded'))
    __stack__.pop()
    ___90 = minitwitplus.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___90, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/add_message', methods=['POST'])
def _add_message():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_message()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___91 = is_logged_in()[0]
    ___92 = ___91
    __trace__.add_inputs(___92.inputs)
    __stack__.push()
    __stack__.add(___92.inputs, bot=True)
    if ___92:
        ___94 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___94, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___95 = minitwitplus.me()
    ___96 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___95]))
    user = ___96
    user.adopt(__stack__.all())
    ___97 = db_len(user)
    ___98 = Cell((___97 > Cell(0)))
    __stack__.push()
    __stack__.add(___98.inputs)
    minitwitplus.add_session_inputs('loggedin', __stack__.all())
    minitwitplus.add_session_inputs('loggedin', ___98.inputs)
    if ___98:
        ___100 = minitwitplus.set_session(Cell('loggedin'), Cell(True))
        ___101 = minitwitplus.flash(Cell('You were logged in'))
    else:
        ___102 = minitwitplus.flash(Cell('Invalid user (register to access)'))
    __stack__.pop()
    ___103 = minitwitplus.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___103, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('new_user' not in locals()):
        new_user = Cell(None)
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    ___104 = is_logged_in()[0]
    ___105 = ___104
    __trace__.add_inputs(___105.inputs)
    __stack__.push()
    __stack__.add(___105.inputs, bot=True)
    if ___105:
        ___107 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___107, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___108 = minitwitplus.me()
    user_id = ___108
    user_id.adopt(__stack__.all())
    ___109 = minitwitplus.name()
    user_name = ___109
    user_name.adopt(__stack__.all())
    ___110 = minitwitplus.method()
    ___111 = Cell((___110 == Cell('POST')))
    __trace__.add_inputs(___111.inputs)
    __stack__.push()
    __stack__.add(___111.inputs, bot=True)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___111.inputs)
    new_user.adopt(__stack__.all())
    new_user.adopt(___111.inputs)
    minitwitplus.add_sql_inputs('user', __stack__.all())
    minitwitplus.add_sql_inputs('user', ___111.inputs)
    if ___111:
        ___113 = minitwitplus.me()
        ___114 = minitwitplus.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___113]))
        already_registered = ___114
        already_registered.adopt(__stack__.all())
        ___115 = db_len(already_registered)
        ___116 = Cell((___115 == Cell(0)))
        __stack__.push()
        __stack__.add(___116.inputs)
        new_user.adopt(__stack__.all())
        new_user.adopt(___116.inputs)
        minitwitplus.add_sql_inputs('user', __stack__.all())
        minitwitplus.add_sql_inputs('user', ___116.inputs)
        if ___116:
            ___118 = minitwitplus.sql(Cell('INSERT INTO user (user_id, username) VALUES (?0, ?1)'), Cell([user_id, user_name]))
            new_user = ___118
            new_user.adopt(__stack__.all())
            ___119 = minitwitplus.flash(Cell('You were successfully registered'))
        else:
            ___120 = minitwitplus.flash(Cell('You were already registered'))
        __stack__.pop()
        ___121 = minitwitplus.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___121, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('name')]
    ___122 = minitwitplus.render(Cell('register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), user_name)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___122, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___123 = minitwitplus.flash(Cell('You were logged out'))
    ___124 = minitwitplus.pop_session(Cell('loggedin'))
    ___125 = minitwitplus.redirect(Cell('Service'), Cell('/public'))
    __r__ = Cell(___125, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwitplus.route('/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)
