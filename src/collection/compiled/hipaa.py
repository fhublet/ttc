
from apps import hipaa
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___1117 = hipaa.get_session(Cell('loggedin'))
    __r__ = Cell(___1117, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1118 = is_logged_in()[0]
    ___1119 = ___1118
    __trace__.add_inputs(___1119.inputs)
    __stack__.push()
    __stack__.add(___1119.inputs, bot=True)
    if ___1119:
        ___1121 = hipaa.me()
        ___1122 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1121]))
        __r__ = Cell(___1122[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def error():
    global __stack__
    if ('msg' not in locals()):
        msg = Cell(None)
    msg = Cell('You are not allowed to access this page: Not a User')
    msg.adopt(__stack__.all())
    __k__ = [Cell('message')]
    ___1123 = hipaa.render(Cell('error.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), msg)),
    }, inputs=[__k__[0].inputs]))
    __r__ = Cell(___1123, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/error')
def _error():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = error()
    return (__c__, __r__, __s__, __trace__)

def filter_check(messages):
    global __stack__
    if ('message' not in locals()):
        message = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___1124 = messages
    for message in ___1124:
        ___1126 = hipaa.check(__c__, __trace__, Cell('Service'), message)
        ___1127 = ___1126
        __stack__.push()
        __stack__.add(___1127.inputs)
        messages2.adopt(__stack__.all())
        messages2.adopt(___1127.inputs)
        if ___1127:
            messages2 = (messages2 + Cell([message]))
            messages2.adopt(__stack__.all())
        __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def profile():
    global __stack__
    if ('email' not in locals()):
        email = Cell(None)
    if ('profiletype' not in locals()):
        profiletype = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('current_user' not in locals()):
        current_user = Cell(None)
    ___1129 = me_user()[0]
    current_user = ___1129
    current_user.adopt(__stack__.all())
    ___1130 = non(current_user)
    __trace__.add_inputs(___1130.inputs)
    __stack__.push()
    __stack__.add(___1130.inputs, bot=True)
    if ___1130:
        ___1132 = hipaa.redirect(Cell('Service'), Cell('/error'))
        __r__ = Cell(___1132, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1133 = hipaa.method()
    ___1134 = Cell((___1133 == Cell('POST')))
    __trace__.add_inputs(___1134.inputs)
    __stack__.push()
    __stack__.add(___1134.inputs, bot=True)
    profiletype.adopt(__stack__.all())
    profiletype.adopt(___1134.inputs)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1134.inputs)
    email.adopt(__stack__.all())
    email.adopt(___1134.inputs)
    name.adopt(__stack__.all())
    name.adopt(___1134.inputs)
    if ___1134:
        ___1136 = hipaa.post('profile', Cell('email'))
        email = ___1136
        email.adopt(__stack__.all())
        ___1137 = hipaa.post('profile', Cell('profiletype'))
        profiletype = ___1137
        profiletype.adopt(__stack__.all())
        ___1138 = hipaa.post('profile', Cell('name'))
        name = ___1138
        name.adopt(__stack__.all())
        ___1139 = hipaa.me()
        ___1140 = hipaa.sql(Cell('DELETE FROM UserProfile WHERE user_id = ?0'), Cell([___1139]))
        ___1141 = hipaa.me()
        ___1142 = hipaa.name()
        ___1143 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name) \n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([___1141, ___1142, Cell(1), email, profiletype, name]))
        ___1144 = hipaa.redirect(Cell('Service'), Cell('/accounts/profile'))
        __r__ = Cell(___1144, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1145 = me_user()[0]
    current_user = ___1145
    current_user.adopt(__stack__.all())
    __k__ = [Cell('profile'), Cell('which_page'), Cell('is_logged_in')]
    ___1146 = hipaa.render(Cell('profile.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), current_user)),
        __k__[1].value: Cell((Cell('Service'), Cell('profile'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1146, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/profile', methods=['GET', 'POST'])
def _profile():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile()
    return (__c__, __r__, __s__, __trace__)

def index():
    global __stack__
    if ('current_user' not in locals()):
        current_user = Cell(None)
    if ('entities' not in locals()):
        entities = Cell(None)
    if ('patients' not in locals()):
        patients = Cell(None)
    ___1147 = me_user()[0]
    current_user = ___1147
    current_user.adopt(__stack__.all())
    ___1148 = non(current_user)
    __trace__.add_inputs(___1148.inputs)
    __stack__.push()
    __stack__.add(___1148.inputs, bot=True)
    if ___1148:
        ___1150 = hipaa.redirect(Cell('Service'), Cell('/accounts/login'))
        __r__ = Cell(___1150, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1151 = hipaa.sql(Cell('SELECT * FROM Individual'))
    ___1152 = filter_check(___1151)[0]
    patients = ___1152
    patients.adopt(__stack__.all())
    ___1153 = hipaa.sql(Cell('SELECT * FROM CoveredEntity'))
    ___1154 = filter_check(___1153)[0]
    entities = ___1154
    entities.adopt(__stack__.all())
    __k__ = [Cell('patients'), Cell('entities'), Cell('name'), Cell('is_logged_in')]
    ___1155 = hipaa.render(Cell('index.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), patients)),
        __k__[1].value: Cell((Cell('Service'), entities)),
        __k__[2].value: Cell((Cell('Service'), current_user[Cell(6)])),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1155, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/')
def _index():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = index()
    return (__c__, __r__, __s__, __trace__)

def about_view():
    global __stack__
    __k__ = [Cell('which_page'), Cell('is_logged_in')]
    ___1156 = is_logged_in()[0]
    ___1157 = hipaa.render(Cell('about.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), Cell('about'))),
        __k__[1].value: Cell((Cell('Service'), ___1156)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1157, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/about')
def _about_view():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = about_view()
    return (__c__, __r__, __s__, __trace__)

def users():
    global __stack__
    if ('user_profiles' not in locals()):
        user_profiles = Cell(None)
    if ('current_user' not in locals()):
        current_user = Cell(None)
    ___1158 = me_user()[0]
    current_user = ___1158
    current_user.adopt(__stack__.all())
    ___1159 = Cell((current_user[Cell(5)] != Cell(3)))
    __trace__.add_inputs(___1159.inputs)
    __stack__.push()
    __stack__.add(___1159.inputs, bot=True)
    if ___1159:
        ___1161 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1161, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1162 = hipaa.sql(Cell('SELECT * FROM UserProfile'))
    ___1163 = filter_check(___1162)[0]
    user_profiles = ___1163
    user_profiles.adopt(__stack__.all())
    __k__ = [Cell('user_profiles'), Cell('which_page'), Cell('is_logged_in')]
    ___1164 = hipaa.render(Cell('users_view.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_profiles)),
        __k__[1].value: Cell((Cell('Service'), Cell('users'))),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1164, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/users', methods=['GET', 'POST'])
def _users():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users()
    return (__c__, __r__, __s__, __trace__)

def set_level(level, user_id):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1165 = me_user()[0]
    user = ___1165
    user.adopt(__stack__.all())
    ___1166 = user
    __trace__.add_inputs(___1166.inputs)
    __stack__.push()
    __stack__.add(___1166.inputs, bot=True)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1166.inputs)
    user.adopt(__stack__.all())
    user.adopt(___1166.inputs)
    if ___1166:
        ___1168 = Cell((Cell((user[Cell(5)] == Cell(3))).value and level.in_(Cell([Cell('0'), Cell('1'), Cell('2'), Cell('3'), Cell('4'), Cell('5'), Cell('6')])).value), inputs=(Cell((user[Cell(5)] == Cell(3))).inputs + level.in_(Cell([Cell('0'), Cell('1'), Cell('2'), Cell('3'), Cell('4'), Cell('5'), Cell('6')])).inputs))
        __stack__.push()
        __stack__.add(___1168.inputs)
        hipaa.add_sql_inputs('UserProfile', __stack__.all())
        hipaa.add_sql_inputs('UserProfile', ___1168.inputs)
        user.adopt(__stack__.all())
        user.adopt(___1168.inputs)
        if ___1168:
            ___1170 = hipaa.sql(Cell('SELECT username, is_active, email, name\n                               FROM UserProfile WHERE user_id = ?0'), Cell([user_id]))
            user = ___1170[Cell(0)]
            user.adopt(__stack__.all())
            ___1171 = hipaa.sql(Cell('DELETE FROM UserProfile WHERE user_id = ?0'), Cell([user_id]))
            ___1172 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)\n                      VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([user_id, user[Cell(0)], user[Cell(1)], user[Cell(2)], level, user[Cell(3)]]))
        __stack__.pop()
    __stack__.pop()
    ___1173 = hipaa.render(Cell('error.html'))
    __r__ = Cell(___1173, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/set_level/<level>/<user_id>', methods=['GET'])
def _set_level(level, user_id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    level = hipaa.register('set_level', 'level', level)
    user_id = hipaa.register('set_level', 'user_id', user_id)
    (__r__, __s__) = set_level(level, user_id)
    return (__c__, __r__, __s__, __trace__)

def patient_treatment(id):
    global __stack__
    if ('treatments' not in locals()):
        treatments = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1174 = is_logged_in()[0]
    ___1175 = non(___1174)
    __trace__.add_inputs(___1175.inputs)
    __stack__.push()
    __stack__.add(___1175.inputs, bot=True)
    if ___1175:
        ___1177 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1177, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1178 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1178[Cell(0)]
    p.adopt(__stack__.all())
    ___1179 = hipaa.sql(Cell('SELECT * FROM Treatment WHERE patient_id = ?0'), Cell([p[Cell(0)]]))
    treatments = ___1179
    treatments.adopt(__stack__.all())
    __k__ = [Cell('first_name'), Cell('last_name'), Cell('treatments'), Cell('is_logged_in')]
    ___1180 = hipaa.render(Cell('treatments.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p[Cell(0)])),
        __k__[1].value: Cell((Cell('Service'), p[Cell(1)])),
        __k__[2].value: Cell((Cell('Service'), treatments)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1180, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/treatments')
def _patient_treatment(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('patient_treatment', 'id', id)
    (__r__, __s__) = patient_treatment(id)
    return (__c__, __r__, __s__, __trace__)

def patient_diagnoses(id):
    global __stack__
    if ('newDiagnoses' not in locals()):
        newDiagnoses = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1181 = is_logged_in()[0]
    ___1182 = non(___1181)
    __trace__.add_inputs(___1182.inputs)
    __stack__.push()
    __stack__.add(___1182.inputs, bot=True)
    if ___1182:
        ___1184 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1184, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1185 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1185[Cell(0)]
    p.adopt(__stack__.all())
    ___1186 = hipaa.sql(Cell('SELECT * FROM Diagnosis WHERE patient_id = ?0'), Cell([p[Cell(0)]]))
    ___1187 = filter_check(___1186)[0]
    newDiagnoses = ___1187
    newDiagnoses.adopt(__stack__.all())
    __k__ = [Cell('first_name'), Cell('last_name'), Cell('diagnoses'), Cell('is_logged_in')]
    ___1188 = hipaa.render(Cell('treatments.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p[Cell(0)])),
        __k__[1].value: Cell((Cell('Service'), p[Cell(1)])),
        __k__[2].value: Cell((Cell('Service'), newDiagnoses)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1188, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/diagnoses')
def _patient_diagnoses(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('patient_diagnoses', 'id', id)
    (__r__, __s__) = patient_diagnoses(id)
    return (__c__, __r__, __s__, __trace__)

def info(id):
    global __stack__
    if ('dataset' not in locals()):
        dataset = Cell(None)
    if ('p' not in locals()):
        p = Cell(None)
    ___1189 = is_logged_in()[0]
    ___1190 = non(___1189)
    __trace__.add_inputs(___1190.inputs)
    __stack__.push()
    __stack__.add(___1190.inputs, bot=True)
    if ___1190:
        ___1192 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1192, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1193 = hipaa.sql(Cell('SELECT * FROM Individual WHERE id = ?0'), Cell([id]))
    p = ___1193[Cell(0)]
    p.adopt(__stack__.all())
    dataset = Cell([])
    dataset.adopt(__stack__.all())
    dataset = (dataset + Cell([Cell((Cell('Sex'), p[Cell(6)], Cell(False)))]))
    dataset.adopt(__stack__.all())
    __k__ = [Cell('patient'), Cell('dataset'), Cell('is_logged_in')]
    ___1194 = hipaa.render(Cell('info.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), p)),
        __k__[1].value: Cell((Cell('Service'), dataset)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1194, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/patients/<int:id>/')
def _info(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('info', 'id', id)
    (__r__, __s__) = info(id)
    return (__c__, __r__, __s__, __trace__)

def entity_transaction(id):
    global __stack__
    if ('transactions' not in locals()):
        transactions = Cell(None)
    if ('entity' not in locals()):
        entity = Cell(None)
    if ('other_transactions' not in locals()):
        other_transactions = Cell(None)
    ___1195 = is_logged_in()[0]
    ___1196 = non(___1195)
    __trace__.add_inputs(___1196.inputs)
    __stack__.push()
    __stack__.add(___1196.inputs, bot=True)
    if ___1196:
        ___1198 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1198, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1199 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1199[Cell(0)]
    entity.adopt(__stack__.all())
    ___1200 = hipaa.sql(Cell('SELECT * FROM Transaction WHERE FirstParty = ?0'), Cell([entity[Cell(0)]]))
    ___1201 = filter_check(___1200)[0]
    transactions = ___1201
    transactions.adopt(__stack__.all())
    ___1202 = hipaa.sql(Cell('SELECT * FROM Transaction WHERE SecondParty = ?0'), Cell([entity[Cell(0)]]))
    ___1203 = filter_check(___1202)[0]
    other_transactions = ___1203
    other_transactions.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('transactions'), Cell('other_transactions'), Cell('is_logged_in')]
    ___1204 = hipaa.render(Cell('transactions.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), transactions)),
        __k__[2].value: Cell((Cell('Service'), other_transactions)),
        __k__[3].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1204, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/transactions')
def _entity_transaction(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_transaction', 'id', id)
    (__r__, __s__) = entity_transaction(id)
    return (__c__, __r__, __s__, __trace__)

def entity_associate(id):
    global __stack__
    if ('associates' not in locals()):
        associates = Cell(None)
    if ('entity' not in locals()):
        entity = Cell(None)
    ___1205 = is_logged_in()[0]
    ___1206 = non(___1205)
    __trace__.add_inputs(___1206.inputs)
    __stack__.push()
    __stack__.add(___1206.inputs, bot=True)
    if ___1206:
        ___1208 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1208, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1209 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1209[Cell(0)]
    entity.adopt(__stack__.all())
    ___1210 = hipaa.sql(Cell('SELECT UserProfile.* FROM UserProfile JOIN BusinessAssociateAgreement \n    ON BusinessAssociateAgreement.BusinessAssociateID = UserProfile.id\n    WHERE BusinessAssociateAgreement.CoveredEntityID = ?0'), Cell([entity[Cell(0)]]))
    associates = ___1210
    associates.adopt(__stack__.all())
    ___1211 = filter_check(associates)[0]
    associates = ___1211
    associates.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('associates'), Cell('is_logged_in')]
    ___1212 = hipaa.render(Cell('associates.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), associates)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1212, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/associates')
def _entity_associate(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_associate', 'id', id)
    (__r__, __s__) = entity_associate(id)
    return (__c__, __r__, __s__, __trace__)

def entity_directory(id):
    global __stack__
    if ('visits' not in locals()):
        visits = Cell(None)
    if ('entity' not in locals()):
        entity = Cell(None)
    ___1213 = is_logged_in()[0]
    ___1214 = non(___1213)
    __trace__.add_inputs(___1214.inputs)
    __stack__.push()
    __stack__.add(___1214.inputs, bot=True)
    if ___1214:
        ___1216 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1216, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1217 = hipaa.sql(Cell('SELECT * FROM CoveredEntity WHERE id = ?0'), Cell([id]))
    entity = ___1217[Cell(0)]
    entity.adopt(__stack__.all())
    ___1218 = hipaa.sql(Cell('SELECT * FROM HospitalVisit WHERE hospitalID = ?0'), Cell([entity[Cell(0)]]))
    ___1219 = filter_check(___1218)[0]
    visits = ___1219
    visits.adopt(__stack__.all())
    __k__ = [Cell('entity'), Cell('visits'), Cell('is_logged_in')]
    ___1220 = hipaa.render(Cell('directory.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), entity)),
        __k__[1].value: Cell((Cell('Service'), visits)),
        __k__[2].value: Cell((Cell('Service'), Cell(True))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1220, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/entities/<int:id>/')
def _entity_directory(id):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    id = hipaa.register('entity_directory', 'id', id)
    (__r__, __s__) = entity_directory(id)
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    ___1221 = is_logged_in()[0]
    ___1222 = ___1221
    __trace__.add_inputs(___1222.inputs)
    __stack__.push()
    __stack__.add(___1222.inputs, bot=True)
    if ___1222:
        ___1224 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1224, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1225 = hipaa.render(Cell('registration/login.html'))
    __r__ = Cell(___1225, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def do_login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1226 = is_logged_in()[0]
    ___1227 = ___1226
    __trace__.add_inputs(___1227.inputs)
    __stack__.push()
    __stack__.add(___1227.inputs, bot=True)
    if ___1227:
        ___1229 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1229, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1230 = hipaa.me()
    ___1231 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1230]))
    user = ___1231
    user.adopt(__stack__.all())
    ___1232 = db_len(user)
    ___1233 = Cell((___1232 > Cell(0)))
    __trace__.add_inputs(___1233.inputs)
    __stack__.push()
    __stack__.add(___1233.inputs, bot=True)
    hipaa.add_session_inputs('loggedin', __stack__.all())
    hipaa.add_session_inputs('loggedin', ___1233.inputs)
    if ___1233:
        ___1235 = hipaa.set_session(Cell('loggedin'), Cell(True))
        ___1236 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1236, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('error')]
        ___1237 = hipaa.render(Cell('registration/login.html'), Cell({
            __k__[0].value: Cell((Cell('Service'), Cell('Invalid user (register to access)'))),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___1237, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@hipaa.route('/accounts/do_login')
def _do_login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = do_login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('email' not in locals()):
        email = Cell(None)
    if ('username' not in locals()):
        username = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___1238 = is_logged_in()[0]
    ___1239 = ___1238
    __trace__.add_inputs(___1239.inputs)
    __stack__.push()
    __stack__.add(___1239.inputs, bot=True)
    if ___1239:
        ___1241 = hipaa.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1241, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1242 = hipaa.me()
    user_id = ___1242
    user_id.adopt(__stack__.all())
    ___1243 = hipaa.name()
    username = ___1243
    username.adopt(__stack__.all())
    ___1244 = hipaa.method()
    ___1245 = Cell((___1244 == Cell('POST')))
    __trace__.add_inputs(___1245.inputs)
    __stack__.push()
    __stack__.add(___1245.inputs, bot=True)
    email.adopt(__stack__.all())
    email.adopt(___1245.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1245.inputs)
    hipaa.add_sql_inputs('UserProfile', __stack__.all())
    hipaa.add_sql_inputs('UserProfile', ___1245.inputs)
    name.adopt(__stack__.all())
    name.adopt(___1245.inputs)
    if ___1245:
        ___1247 = hipaa.me()
        ___1248 = hipaa.sql(Cell('SELECT * FROM UserProfile WHERE user_id = ?0'), Cell([___1247]))
        already_registered = ___1248
        already_registered.adopt(__stack__.all())
        ___1249 = db_len(already_registered)
        ___1250 = Cell((___1249 == Cell(0)))
        __stack__.push()
        __stack__.add(___1250.inputs)
        email.adopt(__stack__.all())
        email.adopt(___1250.inputs)
        hipaa.add_sql_inputs('UserProfile', __stack__.all())
        hipaa.add_sql_inputs('UserProfile', ___1250.inputs)
        name.adopt(__stack__.all())
        name.adopt(___1250.inputs)
        if ___1250:
            ___1252 = hipaa.post('register', Cell('email'))
            email = ___1252
            email.adopt(__stack__.all())
            ___1253 = hipaa.post('register', Cell('name'))
            name = ___1253
            name.adopt(__stack__.all())
            ___1254 = hipaa.sql(Cell('INSERT INTO UserProfile (user_id, username, is_active, email, profiletype, name)\n                     VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([user_id, username, Cell(1), email, Cell(1), name]))
        __stack__.pop()
        ___1255 = hipaa.redirect(Cell('Service'), Cell('/accounts/do_login'))
        __r__ = Cell(___1255, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('username')]
    ___1256 = hipaa.render(Cell('registration/register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), username)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1256, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1257 = hipaa.pop_session(Cell('loggedin'))
    ___1258 = hipaa.redirect(Cell('Service'), Cell('/accounts/login'))
    __r__ = Cell(___1258, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@hipaa.route('/accounts/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)
