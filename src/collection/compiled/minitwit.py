
from apps import minitwit
from databank.imports import *
__stack__ = None

def is_logged_in():
    global __stack__
    ___1259 = minitwit.get_session(Cell('loggedin'))
    __r__ = Cell(___1259, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def me_user():
    global __stack__
    ___1260 = is_logged_in()[0]
    ___1261 = ___1260
    __trace__.add_inputs(___1261.inputs)
    __stack__.push()
    __stack__.add(___1261.inputs, bot=True)
    if ___1261:
        ___1263 = minitwit.me()
        ___1264 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1263]))
        __r__ = Cell(___1264[Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def filter_check(messages):
    global __stack__
    if ('message' not in locals()):
        message = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    messages2 = Cell([])
    messages2.adopt(__stack__.all())
    ___1265 = messages
    for message in ___1265:
        ___1267 = minitwit.check(__c__, __trace__, Cell('Service'), message)
        ___1268 = ___1267
        __stack__.push()
        __stack__.add(___1268.inputs)
        messages2.adopt(__stack__.all())
        messages2.adopt(___1268.inputs)
        if ___1268:
            messages2 = (messages2 + Cell([message]))
            messages2.adopt(__stack__.all())
        __stack__.pop()
    __r__ = Cell(messages2, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def get_user_id(username):
    global __stack__
    if ('rv' not in locals()):
        rv = Cell(None)
    ___1270 = minitwit.sql(Cell('SELECT id FROM user WHERE username = ?0'), Cell([username]))
    rv = ___1270
    rv.adopt(__stack__.all())
    ___1271 = db_len(rv)
    ___1272 = Cell((___1271 > Cell(0)))
    __trace__.add_inputs(___1272.inputs)
    __stack__.push()
    __stack__.add(___1272.inputs, bot=True)
    if ___1272:
        __r__ = Cell(rv[Cell(0)][Cell(0)], adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell(Cell(None), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('messages1' not in locals()):
        messages1 = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('messages2' not in locals()):
        messages2 = Cell(None)
    if ('messages12' not in locals()):
        messages12 = Cell(None)
    ___1274 = me_user()[0]
    user = ___1274
    user.adopt(__stack__.all())
    ___1275 = non(user)
    __trace__.add_inputs(___1275.inputs)
    __stack__.push()
    __stack__.add(___1275.inputs, bot=True)
    if ___1275:
        ___1277 = minitwit.redirect(Cell('Service'), Cell('/public'))
        __r__ = Cell(___1277, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1278 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       JOIN follower ON follower.whom_id = user.id\n       WHERE follower.who_id = ?0\n       ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages1 = ___1278
    messages1.adopt(__stack__.all())
    ___1279 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([user[Cell(0)]]))
    messages2 = ___1279
    messages2.adopt(__stack__.all())
    ___1280 = db_sorted_by0((messages1 + messages2))
    ___1281 = filter_check(___1280)[0]
    messages12 = ___1281
    messages12.adopt(__stack__.all())
    messages = Cell([])
    messages.adopt(__stack__.all())
    i = Cell(0)
    i.adopt(__stack__.all())
    ___1282 = db_len(messages12)
    ___1283 = Cell((Cell((i < Cell(30))).value and Cell((i < ___1282)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___1282)).inputs))
    __stack__.push()
    __stack__.add(___1283.inputs)
    i.adopt(__stack__.all())
    i.adopt(___1283.inputs)
    messages.adopt(__stack__.all())
    messages.adopt(___1283.inputs)
    while ___1283:
        messages = (messages + Cell([messages12[i]]))
        messages.adopt(__stack__.all())
        i = (i + Cell(1))
        i.adopt(__stack__.all())
        ___1283 = Cell((Cell((i < Cell(30))).value and Cell((i < ___1282)).value), inputs=(Cell((i < Cell(30))).inputs + Cell((i < ___1282)).inputs))
        __stack__.push()
        __stack__.add(___1283.inputs)
        i.adopt(__stack__.all())
        i.adopt(___1283.inputs)
        messages.adopt(__stack__.all())
        messages.adopt(___1283.inputs)
    __stack__.pop()
    __k__ = [Cell('messages'), Cell('user'), Cell('title'), Cell('flash')]
    ___1285 = minitwit.unflash()
    ___1286 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), user)),
        __k__[2].value: Cell((Cell('Service'), Cell('My Timeline'))),
        __k__[3].value: Cell((Cell('Service'), ___1285)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___1286, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/')
def _timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = timeline()
    return (__c__, __r__, __s__, __trace__)

def public_timeline():
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    ___1287 = minitwit.sql(Cell('\n       SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n              user.user_id, user.username FROM message\n       JOIN user ON message.author_id = user.id\n       ORDER BY message.pub_date DESC LIMIT 30'))
    messages = ___1287
    messages.adopt(__stack__.all())
    ___1288 = filter_check(messages)[0]
    messages = ___1288
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('user'), Cell('title')]
    ___1289 = me_user()[0]
    ___1290 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), ___1289)),
        __k__[2].value: Cell((Cell('Service'), Cell('Public Timeline'))),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs]))
    __r__ = Cell(___1290, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/public')
def _public_timeline():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = public_timeline()
    return (__c__, __r__, __s__, __trace__)

def user_timeline(username):
    global __stack__
    if ('messages' not in locals()):
        messages = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('followed' not in locals()):
        followed = Cell(None)
    if ('profile_user' not in locals()):
        profile_user = Cell(None)
    ___1291 = minitwit.sql(Cell('SELECT * FROM user WHERE username = ?0'), Cell([username]))
    profile_user = ___1291
    profile_user.adopt(__stack__.all())
    ___1292 = db_len(profile_user)
    ___1293 = Cell((___1292 == Cell(0)))
    __trace__.add_inputs(___1293.inputs)
    __stack__.push()
    __stack__.add(___1293.inputs, bot=True)
    if ___1293:
        ___1295 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1295, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    followed = Cell(False)
    followed.adopt(__stack__.all())
    ___1296 = me_user()[0]
    user = ___1296
    user.adopt(__stack__.all())
    ___1297 = user
    __stack__.push()
    __stack__.add(___1297.inputs)
    followed.adopt(__stack__.all())
    followed.adopt(___1297.inputs)
    if ___1297:
        ___1299 = minitwit.sql(Cell('\n      SELECT 1 FROM follower WHERE follower.who_id = ?0 AND follower.whom_id = ?1'), Cell([user[Cell(0)], profile_user[Cell(0)][Cell(0)]]))
        ___1300 = db_len(___1299)
        followed = Cell((___1300 > Cell(0)))
        followed.adopt(__stack__.all())
    __stack__.pop()
    ___1301 = minitwit.sql(Cell('\n      SELECT message.pub_date, message.id, message.author_id, message.text, user.id, \n             user.user_id, user.username FROM message\n      JOIN user ON user.id = message.author_id\n      WHERE user.id = ?0 ORDER BY message.pub_date DESC LIMIT 30'), Cell([profile_user[Cell(0)][Cell(0)]]))
    messages = ___1301
    messages.adopt(__stack__.all())
    ___1302 = filter_check(messages)[0]
    messages = ___1302
    messages.adopt(__stack__.all())
    __k__ = [Cell('messages'), Cell('followed'), Cell('profile_user'), Cell('user'), Cell('flash')]
    ___1303 = minitwit.unflash()
    ___1304 = minitwit.render(Cell('timeline.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), messages)),
        __k__[1].value: Cell((Cell('Service'), followed)),
        __k__[2].value: Cell((Cell('Service'), profile_user[Cell(0)])),
        __k__[3].value: Cell((Cell('Service'), user)),
        __k__[4].value: Cell((Cell('Service'), ___1303)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
    __r__ = Cell(___1304, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>')
def _user_timeline(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('user_timeline', 'username', username)
    (__r__, __s__) = user_timeline(username)
    return (__c__, __r__, __s__, __trace__)

def follow_user(username):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    ___1305 = me_user()[0]
    user = ___1305
    user.adopt(__stack__.all())
    ___1306 = non(user)
    __trace__.add_inputs(___1306.inputs)
    __stack__.push()
    __stack__.add(___1306.inputs, bot=True)
    if ___1306:
        ___1308 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1308, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1309 = get_user_id(username)[0]
    whom_id = ___1309
    whom_id.adopt(__stack__.all())
    ___1310 = non(whom_id)
    __trace__.add_inputs(___1310.inputs)
    __stack__.push()
    __stack__.add(___1310.inputs, bot=True)
    if ___1310:
        ___1312 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1312, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1313 = minitwit.sql(Cell('INSERT INTO follower (who_id, whom_id) VALUES (?0, ?1)'), Cell([user[Cell(0)], whom_id]))
    ___1314 = minitwit.flash(((Cell('You are now following "') + username) + Cell('"')))
    ___1315 = minitwit.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___1315, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>/follow')
def _follow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('follow_user', 'username', username)
    (__r__, __s__) = follow_user(username)
    return (__c__, __r__, __s__, __trace__)

def unfollow_user(username):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('whom_id' not in locals()):
        whom_id = Cell(None)
    ___1316 = me_user()[0]
    user = ___1316
    user.adopt(__stack__.all())
    ___1317 = non(user)
    __trace__.add_inputs(___1317.inputs)
    __stack__.push()
    __stack__.add(___1317.inputs, bot=True)
    if ___1317:
        ___1319 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1319, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1320 = get_user_id(username)[0]
    whom_id = ___1320
    whom_id.adopt(__stack__.all())
    ___1321 = non(whom_id)
    __trace__.add_inputs(___1321.inputs)
    __stack__.push()
    __stack__.add(___1321.inputs, bot=True)
    if ___1321:
        ___1323 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1323, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1324 = minitwit.sql(Cell('DELETE FROM follower WHERE who_id = ?0 AND whom_id = ?1'), Cell([user[Cell(0)], whom_id]))
    ___1325 = minitwit.flash(((Cell('You are no longer following "') + username) + Cell('"')))
    ___1326 = minitwit.redirect(Cell('Service'), (Cell('/') + username))
    __r__ = Cell(___1326, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/<username>/unfollow')
def _unfollow_user(username):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    username = minitwit.register('unfollow_user', 'username', username)
    (__r__, __s__) = unfollow_user(username)
    return (__c__, __r__, __s__, __trace__)

def add_message():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1327 = me_user()[0]
    user = ___1327
    user.adopt(__stack__.all())
    ___1328 = non(user)
    __trace__.add_inputs(___1328.inputs)
    __stack__.push()
    __stack__.add(___1328.inputs, bot=True)
    if ___1328:
        ___1330 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1330, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1331 = minitwit.method()
    ___1332 = Cell((___1331 == Cell('POST')))
    __trace__.add_inputs(___1332.inputs)
    __stack__.push()
    __stack__.add(___1332.inputs, bot=True)
    minitwit.add_sql_inputs('message', __stack__.all())
    minitwit.add_sql_inputs('message', ___1332.inputs)
    if ___1332:
        ___1334 = minitwit.post('add_message', Cell('text'))
        ___1335 = minitwit.now_utc()
        ___1336 = minitwit.sql(Cell('INSERT INTO message (author_id, text, pub_date) \n          VALUES (?0, ?1, ?2)'), Cell([user[Cell(0)], ___1334, ___1335]))
        ___1337 = minitwit.flash(Cell('Your message was recorded'))
    __stack__.pop()
    ___1338 = minitwit.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___1338, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/add_message', methods=['POST'])
def _add_message():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_message()
    return (__c__, __r__, __s__, __trace__)

def login():
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    ___1339 = is_logged_in()[0]
    ___1340 = ___1339
    __trace__.add_inputs(___1340.inputs)
    __stack__.push()
    __stack__.add(___1340.inputs, bot=True)
    if ___1340:
        ___1342 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1342, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1343 = minitwit.me()
    ___1344 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1343]))
    user = ___1344
    user.adopt(__stack__.all())
    ___1345 = db_len(user)
    ___1346 = Cell((___1345 > Cell(0)))
    __stack__.push()
    __stack__.add(___1346.inputs)
    minitwit.add_session_inputs('loggedin', __stack__.all())
    minitwit.add_session_inputs('loggedin', ___1346.inputs)
    if ___1346:
        ___1348 = minitwit.set_session(Cell('loggedin'), Cell(True))
        ___1349 = minitwit.flash(Cell('You were logged in'))
    else:
        ___1350 = minitwit.flash(Cell('Invalid user (register to access)'))
    __stack__.pop()
    ___1351 = minitwit.redirect(Cell('Service'), Cell('/'))
    __r__ = Cell(___1351, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/login')
def _login():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = login()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('new_user' not in locals()):
        new_user = Cell(None)
    if ('user_name' not in locals()):
        user_name = Cell(None)
    if ('already_registered' not in locals()):
        already_registered = Cell(None)
    ___1352 = is_logged_in()[0]
    ___1353 = ___1352
    __trace__.add_inputs(___1353.inputs)
    __stack__.push()
    __stack__.add(___1353.inputs, bot=True)
    if ___1353:
        ___1355 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1355, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___1356 = minitwit.me()
    user_id = ___1356
    user_id.adopt(__stack__.all())
    ___1357 = minitwit.name()
    user_name = ___1357
    user_name.adopt(__stack__.all())
    ___1358 = minitwit.method()
    ___1359 = Cell((___1358 == Cell('POST')))
    __trace__.add_inputs(___1359.inputs)
    __stack__.push()
    __stack__.add(___1359.inputs, bot=True)
    new_user.adopt(__stack__.all())
    new_user.adopt(___1359.inputs)
    already_registered.adopt(__stack__.all())
    already_registered.adopt(___1359.inputs)
    minitwit.add_sql_inputs('user', __stack__.all())
    minitwit.add_sql_inputs('user', ___1359.inputs)
    if ___1359:
        ___1361 = minitwit.me()
        ___1362 = minitwit.sql(Cell('SELECT * FROM user WHERE user_id = ?0'), Cell([___1361]))
        already_registered = ___1362
        already_registered.adopt(__stack__.all())
        ___1363 = db_len(already_registered)
        ___1364 = Cell((___1363 == Cell(0)))
        __stack__.push()
        __stack__.add(___1364.inputs)
        new_user.adopt(__stack__.all())
        new_user.adopt(___1364.inputs)
        minitwit.add_sql_inputs('user', __stack__.all())
        minitwit.add_sql_inputs('user', ___1364.inputs)
        if ___1364:
            ___1366 = minitwit.sql(Cell('INSERT INTO user (user_id, username) VALUES (?0, ?1)'), Cell([user_id, user_name]))
            new_user = ___1366
            new_user.adopt(__stack__.all())
            ___1367 = minitwit.flash(Cell('You were successfully registered'))
        else:
            ___1368 = minitwit.flash(Cell('You were already registered'))
        __stack__.pop()
        ___1369 = minitwit.redirect(Cell('Service'), Cell('/'))
        __r__ = Cell(___1369, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __k__ = [Cell('id'), Cell('name')]
    ___1370 = minitwit.render(Cell('register.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), user_id)),
        __k__[1].value: Cell((Cell('Service'), user_name)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___1370, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/register', methods=['GET', 'POST'])
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def logout():
    global __stack__
    ___1371 = minitwit.flash(Cell('You were logged out'))
    ___1372 = minitwit.pop_session(Cell('loggedin'))
    ___1373 = minitwit.redirect(Cell('Service'), Cell('/public'))
    __r__ = Cell(___1373, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@minitwit.route('/logout')
def _logout():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = logout()
    return (__c__, __r__, __s__, __trace__)
