
from apps import meetup
from databank.imports import *
__stack__ = None

def main():
    global __stack__
    ___126 = meetup.render(Cell('index.html'))
    __r__ = Cell(___126, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/')
def _main():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = main()
    return (__c__, __r__, __s__, __trace__)

def location_html(location):
    global __stack__
    if ('location_' not in locals()):
        location_ = Cell(None)
    ___127 = meetup.sql(Cell('SELECT * FROM locations WHERE id = ?0'), Cell([location]))
    location_ = ___127[Cell(0)]
    location_.adopt(__stack__.all())
    ___128 = Cell((location_ == Cell(None)))
    __trace__.add_inputs(___128.inputs)
    __stack__.push()
    __stack__.add(___128.inputs, bot=True)
    if ___128:
        __r__ = Cell(Cell(''), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __r__ = Cell((((location_[Cell(2)] + Cell(' (')) + location_[Cell(1)]) + Cell(')')), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

def error_html(msg):
    global __stack__
    __r__ = Cell(((Cell('<div class="alert alert-danger" role="alert">') + msg) + Cell('</div>')), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def location_select(selected):
    global __stack__
    if ('sel' not in locals()):
        sel = Cell(None)
    if ('options' not in locals()):
        options = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    options = Cell('<option value="0"></option>')
    options.adopt(__stack__.all())
    ___130 = meetup.me()
    ___131 = meetup.sql(Cell('SELECT locations.* FROM locations INNER JOIN friends ON locations.owner = friends.friend_id WHERE friends.user_id = ?0 OR locations.owner = ?0'), Cell([___130]))
    locations = ___131
    locations.adopt(__stack__.all())
    ___132 = locations
    for location in ___132:
        ___134 = Cell((selected == location[Cell(0)]))
        __stack__.push()
        __stack__.add(___134.inputs)
        sel.adopt(__stack__.all())
        sel.adopt(___134.inputs)
        if ___134:
            sel = Cell(' selected="selected"')
            sel.adopt(__stack__.all())
        else:
            sel = Cell('')
            sel.adopt(__stack__.all())
        __stack__.pop()
        ___136 = db_str(location[Cell(0)])
        options = (options + ((((((((Cell('<option value="') + ___136) + Cell('"')) + sel) + Cell('>')) + location[Cell(2)]) + Cell(' (')) + location[Cell(1)]) + Cell(')</option>')))
        options.adopt(__stack__.all())
    __r__ = Cell(((Cell('<select name="location" id="location" class="form-select">') + options) + Cell('</select>')), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def event_html(event, details):
    global __stack__
    if ('leave' not in locals()):
        leave = Cell(None)
    if ('attended' not in locals()):
        attended = Cell(None)
    if ('time_date' not in locals()):
        time_date = Cell(None)
    if ('invitation' not in locals()):
        invitation = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('html' not in locals()):
        html = Cell(None)
    if ('invitations' not in locals()):
        invitations = Cell(None)
    title = event[Cell(1)]
    title.adopt(__stack__.all())
    description = event[Cell(2)]
    description.adopt(__stack__.all())
    ___137 = Cell((event[Cell(3)] > Cell(0)))
    __stack__.push()
    __stack__.add(___137.inputs)
    location.adopt(__stack__.all())
    location.adopt(___137.inputs)
    if ___137:
        ___139 = location_html(event[Cell(3)])[0]
        location = ___139
        location.adopt(__stack__.all())
    else:
        location = Cell('')
        location.adopt(__stack__.all())
    __stack__.pop()
    time_date = ((event[Cell(4)] + Cell(' ')) + event[Cell(5)])
    time_date.adopt(__stack__.all())
    ___140 = details
    __stack__.push()
    __stack__.add(___140.inputs)
    invitations.adopt(__stack__.all())
    invitations.adopt(___140.inputs)
    leave.adopt(__stack__.all())
    leave.adopt(___140.inputs)
    attended.adopt(__stack__.all())
    attended.adopt(___140.inputs)
    invitation.adopt(__stack__.all())
    invitation.adopt(___140.inputs)
    if ___140:
        ___142 = meetup.me()
        ___143 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([event[Cell(0)], ___142]))
        ___144 = db_len(___143)
        attended = ___144
        attended.adopt(__stack__.all())
        ___145 = Cell((attended > Cell(0)))
        __stack__.push()
        __stack__.add(___145.inputs)
        leave.adopt(__stack__.all())
        leave.adopt(___145.inputs)
        if ___145:
            ___147 = db_str(event[Cell(0)])
            leave = ((Cell('<tr><td colspan"2"><a href="/2/leave_event/') + ___147) + Cell('">Leave</a></td></tr>'))
            leave.adopt(__stack__.all())
        else:
            ___148 = db_str(event[Cell(0)])
            leave = ((Cell('<tr><td colspan"2"><a href="/2/request_attendance/') + ___148) + Cell('">Request attendance</a></td></tr>'))
            leave.adopt(__stack__.all())
        __stack__.pop()
        ___149 = meetup.me()
        ___150 = meetup.sql(Cell('SELECT * FROM invitations WHERE invitee = ?0 AND event = ?1'), Cell([___149, event[Cell(0)]]))
        invitations = ___150
        invitations.adopt(__stack__.all())
        ___151 = db_len(invitations)
        ___152 = Cell((___151 > Cell(0)))
        __stack__.push()
        __stack__.add(___152.inputs)
        invitation.adopt(__stack__.all())
        invitation.adopt(___152.inputs)
        leave.adopt(__stack__.all())
        leave.adopt(___152.inputs)
        if ___152:
            invitation = invitations[Cell(0)][Cell(0)]
            invitation.adopt(__stack__.all())
            ___154 = db_str(invitation)
            ___155 = db_str(invitation)
            leave = (leave + ((((Cell('<tr><td colspan"2"><a href="/2/accept_invitation/') + ___154) + Cell('">Accept</a>&nbsp;<a href="/2/reject_invitation/')) + ___155) + Cell('">Reject</a></td></tr>')))
            leave.adopt(__stack__.all())
        __stack__.pop()
    else:
        leave = Cell('')
        leave.adopt(__stack__.all())
    __stack__.pop()
    ___156 = db_str(event[Cell(0)])
    leave = (leave + ((Cell('<tr><td colspan"2"><a href="/2/event/') + ___156) + Cell('">Details</a></td></tr>')))
    leave.adopt(__stack__.all())
    html = ((((((((((Cell('<table class="table"><thead><tr><th colspan="2">') + title) + Cell('</th></tr></thead><tbody><tr><td>Description</td><td>')) + description) + Cell('</td></tr><tr><td>Location</td><td>')) + location) + Cell('</td></tr><tr><td>Time/date</td><td>')) + time_date) + Cell('</td></tr>')) + leave) + Cell('</tbody></table>'))
    html.adopt(__stack__.all())
    __r__ = Cell(html, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_admin():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___157 = meetup.me()
    ___158 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___157]))
    level = ___158[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level == Cell(3))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_premium():
    global __stack__
    if ('level' not in locals()):
        level = Cell(None)
    ___159 = meetup.me()
    ___160 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___159]))
    level = ___160[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    __r__ = Cell(Cell((level >= Cell(2))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_registered():
    global __stack__
    ___161 = meetup.me()
    ___162 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([___161]))
    ___163 = db_len(___162)
    __r__ = Cell(Cell((___163 > Cell(0))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_moderator(ev):
    global __stack__
    ___164 = meetup.me()
    ___165 = meetup.sql(Cell('SELECT cat_mod.* FROM cat_mod JOIN ev_cat ON ev_cat.category = cat_mod.category WHERE cat_mod.moderator = ?0 AND ev_cat.event = ?1'), Cell([___164, ev]))
    ___166 = db_len(___165)
    __r__ = Cell(Cell((___166 > Cell(0))), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_manager(ev):
    global __stack__
    ___167 = meetup.me()
    ___168 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1 AND level = 2'), Cell([ev, ___167]))
    ___169 = db_len(___168)
    ___170 = meetup.sql(Cell('SELECT owner FROM events WHERE id = ?0'), Cell([ev]))
    ___171 = meetup.me()
    __r__ = Cell(Cell((Cell((___169 > Cell(0))).value or Cell((___170[Cell(0)][Cell(0)] == ___171)).value), inputs=(Cell((___169 > Cell(0))).inputs + Cell((___170[Cell(0)][Cell(0)] == ___171)).inputs)), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def is_attendee(ev):
    global __stack__
    ___172 = meetup.me()
    ___173 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, ___172]))
    ___174 = db_len(___173)
    ___175 = meetup.sql(Cell('SELECT owner FROM events WHERE id = ?0'), Cell([ev]))
    ___176 = meetup.me()
    __r__ = Cell(Cell((Cell((___174 > Cell(0))).value or Cell((___175[Cell(0)][Cell(0)] == ___176)).value), inputs=(Cell((___174 > Cell(0))).inputs + Cell((___175[Cell(0)][Cell(0)] == ___176)).inputs)), adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

def profile():
    global __stack__
    if ('owned_events_html' not in locals()):
        owned_events_html = Cell(None)
    if ('managed_events_html' not in locals()):
        managed_events_html = Cell(None)
    if ('managed_event' not in locals()):
        managed_event = Cell(None)
    if ('owned_events' not in locals()):
        owned_events = Cell(None)
    if ('invited_events' not in locals()):
        invited_events = Cell(None)
    if ('owned_event' not in locals()):
        owned_event = Cell(None)
    if ('managed_events' not in locals()):
        managed_events = Cell(None)
    if ('attended_event' not in locals()):
        attended_event = Cell(None)
    if ('invited_event' not in locals()):
        invited_event = Cell(None)
    if ('attended_events_html' not in locals()):
        attended_events_html = Cell(None)
    if ('invited_events_html' not in locals()):
        invited_events_html = Cell(None)
    if ('inviter' not in locals()):
        inviter = Cell(None)
    if ('attended_events' not in locals()):
        attended_events = Cell(None)
    ___177 = is_registered()[0]
    ___178 = ___177
    __trace__.add_inputs(___178.inputs)
    __stack__.push()
    __stack__.add(___178.inputs, bot=True)
    attended_events.adopt(__stack__.all())
    attended_events.adopt(___178.inputs)
    managed_events_html.adopt(__stack__.all())
    managed_events_html.adopt(___178.inputs)
    owned_events.adopt(__stack__.all())
    owned_events.adopt(___178.inputs)
    invited_events.adopt(__stack__.all())
    invited_events.adopt(___178.inputs)
    managed_events.adopt(__stack__.all())
    managed_events.adopt(___178.inputs)
    invited_events_html.adopt(__stack__.all())
    invited_events_html.adopt(___178.inputs)
    inviter.adopt(__stack__.all())
    inviter.adopt(___178.inputs)
    attended_events_html.adopt(__stack__.all())
    attended_events_html.adopt(___178.inputs)
    owned_events_html.adopt(__stack__.all())
    owned_events_html.adopt(___178.inputs)
    if ___178:
        owned_events_html = Cell('')
        owned_events_html.adopt(__stack__.all())
        ___180 = meetup.me()
        ___181 = meetup.sql(Cell('SELECT * FROM events WHERE owner = ?0'), Cell([___180]))
        owned_events = ___181
        owned_events.adopt(__stack__.all())
        ___182 = owned_events
        for owned_event in ___182:
            ___184 = event_html(owned_event, Cell(False))[0]
            owned_events_html = (owned_events_html + (Cell('<br>') + ___184))
            owned_events_html.adopt(__stack__.all())
        managed_events_html = Cell('')
        managed_events_html.adopt(__stack__.all())
        ___185 = meetup.me()
        ___186 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 2'), Cell([___185]))
        managed_events = ___186
        managed_events.adopt(__stack__.all())
        ___187 = managed_events
        for managed_event in ___187:
            ___189 = event_html(managed_event, Cell(False))[0]
            managed_events_html = (managed_events_html + (Cell('<br>') + ___189))
            managed_events_html.adopt(__stack__.all())
        attended_events_html = Cell('')
        attended_events_html.adopt(__stack__.all())
        ___190 = meetup.me()
        ___191 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_att ON events.id = ev_att.event WHERE ev_att.attendee = ?0 AND ev_att.level = 1'), Cell([___190]))
        attended_events = ___191
        attended_events.adopt(__stack__.all())
        ___192 = attended_events
        for attended_event in ___192:
            ___194 = event_html(attended_event, Cell(False))[0]
            attended_events_html = (attended_events_html + (Cell('<br>') + ___194))
            attended_events_html.adopt(__stack__.all())
        invited_events_html = Cell('')
        invited_events_html.adopt(__stack__.all())
        ___195 = meetup.me()
        ___196 = meetup.sql(Cell('SELECT events.* FROM events JOIN invitations ON events.id = invitations.event WHERE invitations.invitee = ?0'), Cell([___195]))
        invited_events = ___196
        invited_events.adopt(__stack__.all())
        ___197 = invited_events
        for invited_event in ___197:
            ___199 = meetup.sql(Cell('SELECT name FROM users WHERE user_id = ?0'), Cell([invited_event[Cell(6)]]))
            inviter = ___199[Cell(0)][Cell(0)]
            inviter.adopt(__stack__.all())
            ___200 = event_html(invited_event, Cell(True))[0]
            invited_events_html = (invited_events_html + (((Cell('<br><strong>') + inviter) + Cell('</strong> invited you to ')) + ___200))
            invited_events_html.adopt(__stack__.all())
        __k__ = [Cell('is_registered'), Cell('owned_events_html'), Cell('managed_events_html'), Cell('attended_events_html'), Cell('invited_events_html')]
        ___201 = meetup.render(Cell('profile.html'), Cell({
            __k__[0].value: Cell(True),
            __k__[1].value: owned_events_html,
            __k__[2].value: managed_events_html,
            __k__[3].value: attended_events_html,
            __k__[4].value: invited_events_html,
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs]))
        __r__ = Cell(___201, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('is_registered')]
        ___202 = meetup.render(Cell('profile.html'), Cell({
            __k__[0].value: Cell(False),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___202, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/profile')
def _profile():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = profile()
    return (__c__, __r__, __s__, __trace__)

def leave_event(ev):
    global __stack__
    ___203 = meetup.me()
    ___204 = meetup.sql(Cell('DELETE FROM ev_att WHERE attendee = ?0 AND event = ?1 AND level = 1'), Cell([___203, ev]))
    ___205 = profile()[0]
    __r__ = Cell(___205, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/leave_event/<int:ev>')
def _leave_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('leave_event', 'ev', ev)
    (__r__, __s__) = leave_event(ev)
    return (__c__, __r__, __s__, __trace__)

def request_attendance(ev):
    global __stack__
    ___206 = meetup.me()
    ___207 = meetup.sql(Cell('INSERT INTO requests (requester, event) VALUES (?0, ?1)'), Cell([___206, ev]))
    ___208 = events()[0]
    __r__ = Cell(___208, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/request_attendance/<int:ev>')
def _request_attendance(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('request_attendance', 'ev', ev)
    (__r__, __s__) = request_attendance(ev)
    return (__c__, __r__, __s__, __trace__)

def reject_invitation(invitation):
    global __stack__
    ___209 = meetup.me()
    ___210 = meetup.sql(Cell('DELETE FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___209, invitation]))
    ___211 = profile()[0]
    __r__ = Cell(___211, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/reject_invitation/<int:invitation>')
def _reject_invitation(invitation):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    invitation = meetup.register('reject_invitation', 'invitation', invitation)
    (__r__, __s__) = reject_invitation(invitation)
    return (__c__, __r__, __s__, __trace__)

def accept_invitation(invitation):
    global __stack__
    if ('event_id' not in locals()):
        event_id = Cell(None)
    ___212 = meetup.me()
    ___213 = meetup.sql(Cell('SELECT event FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___212, invitation]))
    event_id = ___213
    event_id.adopt(__stack__.all())
    event_id = event_id[Cell(0)][Cell(0)]
    event_id.adopt(__stack__.all())
    ___214 = meetup.me()
    ___215 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([event_id, ___214]))
    ___216 = meetup.me()
    ___217 = meetup.sql(Cell('DELETE FROM invitations WHERE invitee = ?0 AND id = ?1'), Cell([___216, invitation]))
    ___218 = profile()[0]
    __r__ = Cell(___218, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/accept_invitation/<int:invitation>')
def _accept_invitation(invitation):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    invitation = meetup.register('accept_invitation', 'invitation', invitation)
    (__r__, __s__) = accept_invitation(invitation)
    return (__c__, __r__, __s__, __trace__)

def unregister():
    global __stack__
    ___219 = meetup.me()
    ___220 = meetup.sql(Cell('DELETE FROM users WHERE user_id = ?0'), Cell([___219]))
    ___221 = meetup.me()
    ___222 = meetup.sql(Cell('DELETE FROM cat_mod WHERE moderator = ?0'), Cell([___221]))
    ___223 = meetup.me()
    ___224 = meetup.sql(Cell('DELETE FROM cat_sub WHERE subscriber = ?0'), Cell([___223]))
    ___225 = meetup.me()
    ___226 = meetup.sql(Cell('DELETE FROM ev_att WHERE attendee = ?0'), Cell([___225]))
    ___227 = meetup.me()
    ___228 = meetup.sql(Cell('DELETE FROM events WHERE owner = ?0'), Cell([___227]))
    ___229 = meetup.me()
    ___230 = meetup.sql(Cell('DELETE FROM friends WHERE user_id = ?0 OR friend_id = ?0'), Cell([___229]))
    ___231 = meetup.me()
    ___232 = meetup.sql(Cell('DELETE FROM invitations WHERE inviter = ?0 OR invitee = ?0'), Cell([___231]))
    ___233 = meetup.me()
    ___234 = meetup.sql(Cell('DELETE FROM locations WHERE owner = ?0'), Cell([___233]))
    ___235 = meetup.me()
    ___236 = meetup.sql(Cell('DELETE FROM requests WHERE requester = ?0'), Cell([___235]))
    ___237 = profile()[0]
    __r__ = Cell(___237, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/unregister')
def _unregister():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = unregister()
    return (__c__, __r__, __s__, __trace__)

def register():
    global __stack__
    if ('address' not in locals()):
        address = Cell(None)
    if ('ID' not in locals()):
        ID = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___238 = is_registered()[0]
    ___239 = non(___238)
    __trace__.add_inputs(___239.inputs)
    __stack__.push()
    __stack__.add(___239.inputs, bot=True)
    address.adopt(__stack__.all())
    address.adopt(___239.inputs)
    level.adopt(__stack__.all())
    level.adopt(___239.inputs)
    ID.adopt(__stack__.all())
    ID.adopt(___239.inputs)
    name.adopt(__stack__.all())
    name.adopt(___239.inputs)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___239.inputs)
    if ___239:
        ___241 = meetup.me()
        ID = ___241
        ID.adopt(__stack__.all())
        ___242 = meetup.get('register', Cell('name'))
        name = ___242
        name.adopt(__stack__.all())
        level = Cell(1)
        level.adopt(__stack__.all())
        ___243 = meetup.get('register', Cell('location'))
        ___244 = db_int(___243)
        address = ___244
        address.adopt(__stack__.all())
        ___245 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([ID, name, level, address]))
    __stack__.pop()
    ___246 = profile()[0]
    __r__ = Cell(___246, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/register')
def _register():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = register()
    return (__c__, __r__, __s__, __trace__)

def users():
    global __stack__
    if ('i' not in locals()):
        i = Cell(None)
    if ('user' not in locals()):
        user = Cell(None)
    if ('friends' not in locals()):
        friends = Cell(None)
    if ('table' not in locals()):
        table = Cell(None)
    ___247 = is_registered()[0]
    ___248 = ___247
    __trace__.add_inputs(___248.inputs)
    __stack__.push()
    __stack__.add(___248.inputs, bot=True)
    table.adopt(__stack__.all())
    table.adopt(___248.inputs)
    i.adopt(__stack__.all())
    i.adopt(___248.inputs)
    friends.adopt(__stack__.all())
    friends.adopt(___248.inputs)
    if ___248:
        ___250 = meetup.me()
        ___251 = meetup.sql(Cell('SELECT users.* FROM friends LEFT JOIN users ON friends.friend_id = users.user_id WHERE friends.user_id = ?0 OR users.user_id = ?0'), Cell([___250]))
        friends = ___251
        friends.adopt(__stack__.all())
        table = Cell('')
        table.adopt(__stack__.all())
        ___252 = friends
        for user in ___252:
            ___254 = meetup.check(__c__, __trace__, user[Cell(1)])
            ___255 = meetup.check(__c__, __trace__, user[Cell(2)])
            ___256 = meetup.check(__c__, __trace__, user[Cell(3)])
            ___257 = meetup.check(__c__, __trace__, Cell((Cell((___254.value and ___255.value), inputs=(___254.inputs + ___255.inputs)).value and ___256.value), inputs=(Cell((___254.value and ___255.value), inputs=(___254.inputs + ___255.inputs)).inputs + ___256.inputs)))
            ___258 = meetup.check(__c__, __trace__, user[Cell(4)])
            ___259 = Cell((___257.value and ___258.value), inputs=(___257.inputs + ___258.inputs))
            __stack__.push()
            __stack__.add(___259.inputs)
            table.adopt(__stack__.all())
            table.adopt(___259.inputs)
            if ___259:
                ___261 = db_str(user[Cell(1)])
                table = (table + ((((Cell('<tr><td>') + ___261) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td>')))
                table.adopt(__stack__.all())
                ___262 = Cell((user[Cell(3)] == Cell(1)))
                __stack__.push()
                __stack__.add(___262.inputs)
                table.adopt(__stack__.all())
                table.adopt(___262.inputs)
                if ___262:
                    table = (table + Cell('free'))
                    table.adopt(__stack__.all())
                else:
                    ___264 = Cell((user[Cell(3)] == Cell(2)))
                    __stack__.push()
                    __stack__.add(___264.inputs)
                    table.adopt(__stack__.all())
                    table.adopt(___264.inputs)
                    if ___264:
                        table = (table + Cell('premium'))
                        table.adopt(__stack__.all())
                    else:
                        ___266 = Cell((user[Cell(3)] == Cell(3)))
                        __stack__.push()
                        __stack__.add(___266.inputs)
                        table.adopt(__stack__.all())
                        table.adopt(___266.inputs)
                        if ___266:
                            table = (table + Cell('administrator'))
                            table.adopt(__stack__.all())
                        __stack__.pop()
                    __stack__.pop()
                __stack__.pop()
                ___268 = Cell((user[Cell(4)] > Cell(0)))
                __stack__.push()
                __stack__.add(___268.inputs)
                table.adopt(__stack__.all())
                table.adopt(___268.inputs)
                if ___268:
                    ___270 = location_html(user[Cell(4)])[0]
                    table = (table + ((Cell('</td><td>') + ___270) + Cell('</td>')))
                    table.adopt(__stack__.all())
                else:
                    table = (table + Cell('</td><td></td>'))
                    table.adopt(__stack__.all())
                __stack__.pop()
                ___271 = meetup.me()
                ___272 = Cell((user[Cell(1)] == ___271))
                __stack__.push()
                __stack__.add(___272.inputs)
                table.adopt(__stack__.all())
                table.adopt(___272.inputs)
                if ___272:
                    ___274 = db_str(user[Cell(1)])
                    ___275 = db_str(user[Cell(1)])
                    table = (table + ((((Cell('<td><a href="/2/update_user/') + ___274) + Cell('">Update</a>&nbsp;<a href="/2/user_categories/')) + ___275) + Cell('">Categories</a></td></tr>')))
                    table.adopt(__stack__.all())
                else:
                    ___276 = is_admin()[0]
                    ___277 = ___276
                    __stack__.push()
                    __stack__.add(___277.inputs)
                    table.adopt(__stack__.all())
                    table.adopt(___277.inputs)
                    if ___277:
                        ___279 = db_str(user[Cell(1)])
                        ___280 = db_str(user[Cell(1)])
                        ___281 = db_str(user[Cell(1)])
                        table = (table + ((((((Cell('<td><a href="/2/delete_friend/') + ___279) + Cell('">Delete friend</a>&nbsp;<a href="/2/update_user/')) + ___280) + Cell('">Update</a>&nbsp;<a href="/2/user_categories/')) + ___281) + Cell('">Categories</a></td></tr>')))
                        table.adopt(__stack__.all())
                    else:
                        ___282 = db_str(user[Cell(1)])
                        ___283 = db_str(user[Cell(1)])
                        table = (table + ((((Cell('<td><a href="/2/delete_friend/') + ___282) + Cell('">Delete friend</a>&nbsp;<a href="/2/user_categories/')) + ___283) + Cell('">Categories</a></td></tr>')))
                        table.adopt(__stack__.all())
                    __stack__.pop()
                __stack__.pop()
            __stack__.pop()
            i = (i + Cell(1))
            i.adopt(__stack__.all())
        __k__ = [Cell('table_html'), Cell('is_registered')]
        ___284 = meetup.render(Cell('users.html'), Cell({
            __k__[0].value: table,
            __k__[1].value: Cell(True),
        }, inputs=[__k__[0].inputs, __k__[1].inputs]))
        __r__ = Cell(___284, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        __k__ = [Cell('is_registered')]
        ___285 = meetup.render(Cell('users.html'), Cell({
            __k__[0].value: Cell(False),
        }, inputs=[__k__[0].inputs]))
        __r__ = Cell(___285, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/users')
def _users():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = users()
    return (__c__, __r__, __s__, __trace__)

def delete_friend(user):
    global __stack__
    ___286 = meetup.me()
    ___287 = meetup.sql(Cell('DELETE FROM friends WHERE user_id = ?0 AND friend_id = ?1'), Cell([___286, user]))
    ___288 = users()[0]
    __r__ = Cell(___288, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/delete_friend/<int:user>')
def _delete_friend(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('delete_friend', 'user', user)
    (__r__, __s__) = delete_friend(user)
    return (__c__, __r__, __s__, __trace__)

def add_friend():
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    if ('friend_id' not in locals()):
        friend_id = Cell(None)
    ___289 = meetup.me()
    user_id = ___289
    user_id.adopt(__stack__.all())
    ___290 = meetup.get('add_friend', Cell('ID'))
    ___291 = db_int(___290)
    friend_id = ___291
    friend_id.adopt(__stack__.all())
    ___292 = meetup.sql(Cell('SELECT * FROM friends WHERE user_id = ?0 AND friend_id = ?1'), Cell([user_id, friend_id]))
    ___293 = db_len(___292)
    ___294 = Cell((___293 == Cell(0)))
    __trace__.add_inputs(___294.inputs)
    __stack__.push()
    __stack__.add(___294.inputs, bot=True)
    meetup.add_sql_inputs('friends', __stack__.all())
    meetup.add_sql_inputs('friends', ___294.inputs)
    if ___294:
        ___296 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([friend_id]))
        ___297 = db_len(___296)
        ___298 = Cell((___297 > Cell(0)))
        __stack__.push()
        __stack__.add(___298.inputs)
        meetup.add_sql_inputs('friends', __stack__.all())
        meetup.add_sql_inputs('friends', ___298.inputs)
        if ___298:
            ___300 = meetup.sql(Cell('INSERT INTO friends (user_id, friend_id) VALUES (?0, ?1)'), Cell([user_id, friend_id]))
        else:
            ___301 = db_str(friend_id)
            ___302 = error_html(((Cell('User ') + ___301) + Cell(' is not registered.<br>\n')))[0]
            ___303 = users()[0]
            __r__ = Cell((___302 + ___303), adopt=__stack__.all())
            __s__ = __stack__.all()
            __stack__.pop()
            __stack__.pop()
            return (__r__, __s__)
        __stack__.pop()
    __stack__.pop()
    ___304 = users()[0]
    __r__ = Cell(___304, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/add_friend')
def _add_friend():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_friend()
    return (__c__, __r__, __s__, __trace__)

def update_user(user):
    global __stack__
    if ('ID' not in locals()):
        ID = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('premium' not in locals()):
        premium = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('free' not in locals()):
        free = Cell(None)
    if ('administrator' not in locals()):
        administrator = Cell(None)
    if ('address' not in locals()):
        address = Cell(None)
    ___305 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([user]))
    user = ___305[Cell(0)]
    user.adopt(__stack__.all())
    ___306 = db_str(user[Cell(0)])
    id_ = ___306
    id_.adopt(__stack__.all())
    ___307 = db_str(user[Cell(1)])
    ID = ___307
    ID.adopt(__stack__.all())
    name = user[Cell(2)]
    name.adopt(__stack__.all())
    ___308 = Cell((user[Cell(3)] == Cell(1)))
    __stack__.push()
    __stack__.add(___308.inputs)
    free.adopt(__stack__.all())
    free.adopt(___308.inputs)
    if ___308:
        free = Cell(' selected')
        free.adopt(__stack__.all())
    else:
        free = Cell('')
        free.adopt(__stack__.all())
    __stack__.pop()
    ___310 = Cell((user[Cell(3)] == Cell(2)))
    __stack__.push()
    __stack__.add(___310.inputs)
    premium.adopt(__stack__.all())
    premium.adopt(___310.inputs)
    if ___310:
        premium = Cell(' selected')
        premium.adopt(__stack__.all())
    else:
        premium = Cell('')
        premium.adopt(__stack__.all())
    __stack__.pop()
    ___312 = Cell((user[Cell(3)] == Cell(3)))
    __stack__.push()
    __stack__.add(___312.inputs)
    administrator.adopt(__stack__.all())
    administrator.adopt(___312.inputs)
    if ___312:
        administrator = Cell(' selected')
        administrator.adopt(__stack__.all())
    else:
        administrator = Cell('')
        administrator.adopt(__stack__.all())
    __stack__.pop()
    address = user[Cell(4)]
    address.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('ID'), Cell('name'), Cell('free'), Cell('premium'), Cell('administrator'), Cell('location_select')]
    ___314 = location_select(address)[0]
    ___315 = meetup.render(Cell('update_user.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: ID,
        __k__[2].value: name,
        __k__[3].value: free,
        __k__[4].value: premium,
        __k__[5].value: administrator,
        __k__[6].value: ___314,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___315, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_user/<int:user>')
def _update_user(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('update_user', 'user', user)
    (__r__, __s__) = update_user(user)
    return (__c__, __r__, __s__, __trace__)

def update_user_do():
    global __stack__
    if ('ID' not in locals()):
        ID = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('address' not in locals()):
        address = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    ___316 = meetup.get('update_user_do', Cell('id_'))
    ___317 = db_int(___316)
    id_ = ___317
    id_.adopt(__stack__.all())
    ___318 = meetup.get('update_user_do', Cell('ID'))
    ___319 = db_int(___318)
    ID = ___319
    ID.adopt(__stack__.all())
    ___320 = meetup.get('update_user_do', Cell('name'))
    name = ___320
    name.adopt(__stack__.all())
    ___321 = meetup.get('update_user_do', Cell('level'))
    ___322 = db_int(___321)
    level = ___322
    level.adopt(__stack__.all())
    ___323 = meetup.get('update_user_do', Cell('location'))
    ___324 = db_int(___323)
    address = ___324
    address.adopt(__stack__.all())
    ___325 = is_admin()[0]
    ___326 = meetup.me()
    ___327 = Cell((___325.value or Cell((id_ == ___326)).value), inputs=(___325.inputs + Cell((id_ == ___326)).inputs))
    __trace__.add_inputs(___327.inputs)
    __stack__.push()
    __stack__.add(___327.inputs, bot=True)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___327.inputs)
    if ___327:
        ___329 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([id_]))
        ___330 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([ID, name, level, address]))
        ___331 = users()[0]
        __r__ = Cell(___331, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___332 = error_html(Cell('Illegal operation: Update user'))[0]
        ___333 = users()[0]
        __r__ = Cell((___332 + ___333), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_user_do')
def _update_user_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_user_do()
    return (__c__, __r__, __s__, __trace__)

def user_categories(user):
    global __stack__
    if ('usr' not in locals()):
        usr = Cell(None)
    if ('categories_html' not in locals()):
        categories_html = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    ___334 = meetup.sql(Cell('SELECT * FROM users WHERE user_id = ?0'), Cell([user]))
    usr = ___334[Cell(0)]
    usr.adopt(__stack__.all())
    ___335 = meetup.sql(Cell('SELECT * FROM categories'))
    categories = ___335
    categories.adopt(__stack__.all())
    ___336 = categories
    for category in ___336:
        categories_html = (categories_html + ((Cell('<tr><td>') + category[Cell(1)]) + Cell('</td><td>')))
        categories_html.adopt(__stack__.all())
        ___338 = meetup.sql(Cell('SELECT id FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([category[Cell(0)], user]))
        ___339 = db_len(___338)
        ___340 = Cell((___339 > Cell(0)))
        __stack__.push()
        __stack__.add(___340.inputs)
        categories_html.adopt(__stack__.all())
        categories_html.adopt(___340.inputs)
        events.adopt(__stack__.all())
        events.adopt(___340.inputs)
        if ___340:
            ___342 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0'), Cell([category[Cell(0)]]))
            events = ___342
            events.adopt(__stack__.all())
            ___343 = events
            for ev in ___343:
                ___345 = event_html(ev, Cell(True))[0]
                categories_html = (categories_html + (___345 + Cell('<br>')))
                categories_html.adopt(__stack__.all())
            ___346 = is_admin()[0]
            ___347 = meetup.me()
            ___348 = Cell((___346.value or Cell((user == ___347)).value), inputs=(___346.inputs + Cell((user == ___347)).inputs))
            __stack__.push()
            __stack__.add(___348.inputs)
            categories_html.adopt(__stack__.all())
            categories_html.adopt(___348.inputs)
            if ___348:
                ___350 = db_str(category[Cell(0)])
                ___351 = db_str(user)
                categories_html = (categories_html + ((((Cell('</td><td><a href="/2/unsubscribe/') + ___350) + Cell('/')) + ___351) + Cell('">Unsubscribe</a></td></tr>')))
                categories_html.adopt(__stack__.all())
            else:
                categories_html = (categories_html + Cell('</td><td></td></tr>'))
                categories_html.adopt(__stack__.all())
            __stack__.pop()
        else:
            ___352 = is_admin()[0]
            ___353 = meetup.me()
            ___354 = Cell((___352.value or Cell((user == ___353)).value), inputs=(___352.inputs + Cell((user == ___353)).inputs))
            __stack__.push()
            __stack__.add(___354.inputs)
            categories_html.adopt(__stack__.all())
            categories_html.adopt(___354.inputs)
            if ___354:
                ___356 = db_str(category[Cell(0)])
                ___357 = db_str(user)
                categories_html = (categories_html + ((((Cell('</td><td><a href="/2/subscribe/') + ___356) + Cell('/')) + ___357) + Cell('">Subscribe</a></td></tr>')))
                categories_html.adopt(__stack__.all())
            else:
                categories_html = (categories_html + Cell('</td><td></td></tr>'))
                categories_html.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('usr'), Cell('categories_html')]
    ___358 = meetup.render(Cell('user_categories.html'), Cell({
        __k__[0].value: usr[Cell(2)],
        __k__[1].value: categories_html,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___358, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/user_categories/<int:user>')
def _user_categories(user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    user = meetup.register('user_categories', 'user', user)
    (__r__, __s__) = user_categories(user)
    return (__c__, __r__, __s__, __trace__)

def unsubscribe(category, user):
    global __stack__
    ___359 = is_admin()[0]
    ___360 = meetup.me()
    ___361 = Cell((___359.value or Cell((user == ___360)).value), inputs=(___359.inputs + Cell((user == ___360)).inputs))
    __trace__.add_inputs(___361.inputs)
    __stack__.push()
    __stack__.add(___361.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___361.inputs)
    if ___361:
        ___363 = meetup.sql(Cell('DELETE FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([category, user]))
        ___364 = user_categories(user)[0]
        __r__ = Cell(___364, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___365 = error_html(Cell('Illegal operation: Unsubscribe'))[0]
        ___366 = user_categories(user)[0]
        __r__ = Cell((___365 + ___366), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/unsubscribe/<int:category>/<int:user>')
def _unsubscribe(category, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    category = meetup.register('unsubscribe', 'category', category)
    user = meetup.register('unsubscribe', 'user', user)
    (__r__, __s__) = unsubscribe(category, user)
    return (__c__, __r__, __s__, __trace__)

def subscribe(category, user):
    global __stack__
    if ('subscriptions' not in locals()):
        subscriptions = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    ___367 = is_admin()[0]
    ___368 = meetup.me()
    ___369 = Cell((___367.value or Cell((user == ___368)).value), inputs=(___367.inputs + Cell((user == ___368)).inputs))
    __trace__.add_inputs(___369.inputs)
    __stack__.push()
    __stack__.add(___369.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___369.inputs)
    level.adopt(__stack__.all())
    level.adopt(___369.inputs)
    subscriptions.adopt(__stack__.all())
    subscriptions.adopt(___369.inputs)
    if ___369:
        ___371 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([user]))
        level = ___371[Cell(0)][Cell(0)]
        level.adopt(__stack__.all())
        ___372 = Cell((level == Cell(1)))
        __stack__.push()
        __stack__.add(___372.inputs)
        subscriptions.adopt(__stack__.all())
        subscriptions.adopt(___372.inputs)
        if ___372:
            ___374 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE subscriber = ?0'), Cell([user]))
            subscriptions = ___374
            subscriptions.adopt(__stack__.all())
            ___375 = db_len(subscriptions)
            ___376 = Cell((___375 > Cell(2)))
            __stack__.push()
            __stack__.add(___376.inputs)
            if ___376:
                ___378 = user_categories(user)[0]
                __r__ = Cell((Cell('Illegal operation: Subscribe<br>\n') + ___378), adopt=__stack__.all())
                __s__ = __stack__.all()
                __stack__.pop()
                __stack__.pop()
                __stack__.pop()
                return (__r__, __s__)
            __stack__.pop()
        __stack__.pop()
        ___379 = meetup.sql(Cell('INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)'), Cell([category, user]))
        ___380 = user_categories(user)[0]
        __r__ = Cell(___380, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___381 = error_html(Cell('Illegal operation: Subscribe'))[0]
        ___382 = user_categories(user)[0]
        __r__ = Cell((___381 + ___382), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/subscribe/<int:category>/<int:user>')
def _subscribe(category, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    category = meetup.register('subscribe', 'category', category)
    user = meetup.register('subscribe', 'user', user)
    (__r__, __s__) = subscribe(category, user)
    return (__c__, __r__, __s__, __trace__)

def categories():
    global __stack__
    if ('cat' not in locals()):
        cat = Cell(None)
    if ('table' not in locals()):
        table = Cell(None)
    ___383 = meetup.me()
    ___384 = meetup.sql(Cell('SELECT categories.* FROM friends LEFT JOIN categories ON categories.owner = friends.friend_id WHERE friends.user_id = ?0 OR categories.owner = ?0'), Cell([___383]))
    categories = ___384
    categories.adopt(__stack__.all())
    table = Cell('')
    table.adopt(__stack__.all())
    ___385 = categories
    for cat in ___385:
        ___387 = meetup.check(__c__, __trace__, cat[Cell(0)])
        ___388 = meetup.check(__c__, __trace__, cat[Cell(1)])
        ___389 = Cell((___387.value and ___388.value), inputs=(___387.inputs + ___388.inputs))
        __stack__.push()
        __stack__.add(___389.inputs)
        table.adopt(__stack__.all())
        table.adopt(___389.inputs)
        if ___389:
            ___391 = is_admin()[0]
            ___392 = ___391
            __stack__.push()
            __stack__.add(___392.inputs)
            table.adopt(__stack__.all())
            table.adopt(___392.inputs)
            if ___392:
                ___394 = db_str(cat[Cell(1)])
                ___395 = db_str(cat[Cell(0)])
                ___396 = db_str(cat[Cell(0)])
                ___397 = db_str(cat[Cell(0)])
                table = (table + ((((((((Cell('<tr><td>') + ___394) + Cell('</td><td><a href="/2/delete_category/')) + ___395) + Cell('">Delete</a>&nbsp;<a href="/2/update_category/')) + ___396) + Cell('">Update</a>&nbsp;<a href="/2/category/')) + ___397) + Cell('">Details</a></td></tr>')))
                table.adopt(__stack__.all())
            else:
                ___398 = db_str(cat[Cell(1)])
                table = (table + ((Cell('<tr><td>') + ___398) + Cell('</td><td></td></tr>')))
                table.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('table_html'), Cell('is_admin')]
    ___399 = is_admin()[0]
    ___400 = meetup.render(Cell('categories.html'), Cell({
        __k__[0].value: table,
        __k__[1].value: ___399,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___400, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/categories')
def _categories():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = categories()
    return (__c__, __r__, __s__, __trace__)

def delete_category(cat):
    global __stack__
    ___401 = is_admin()[0]
    ___402 = ___401
    __trace__.add_inputs(___402.inputs)
    __stack__.push()
    __stack__.add(___402.inputs, bot=True)
    meetup.add_sql_inputs('cat_sub', __stack__.all())
    meetup.add_sql_inputs('cat_sub', ___402.inputs)
    meetup.add_sql_inputs('categories', __stack__.all())
    meetup.add_sql_inputs('categories', ___402.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___402.inputs)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___402.inputs)
    if ___402:
        ___404 = meetup.sql(Cell('DELETE FROM categories WHERE id = ?0'), Cell([cat]))
        ___405 = meetup.sql(Cell('DELETE FROM cat_mod WHERE category = ?0'), Cell([cat]))
        ___406 = meetup.sql(Cell('DELETE FROM cat_sub WHERE category = ?0'), Cell([cat]))
        ___407 = meetup.sql(Cell('DELETE FROM ev_cat WHERE category = ?0'), Cell([cat]))
        ___408 = categories()[0]
        __r__ = Cell(___408, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___409 = error_html(Cell('Illegal operation: Delete category'))[0]
        ___410 = categories()[0]
        __r__ = Cell((___409 + ___410), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_category/<int:cat>')
def _delete_category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('delete_category', 'cat', cat)
    (__r__, __s__) = delete_category(cat)
    return (__c__, __r__, __s__, __trace__)

def add_category():
    global __stack__
    if ('name' not in locals()):
        name = Cell(None)
    ___411 = is_admin()[0]
    ___412 = ___411
    __trace__.add_inputs(___412.inputs)
    __stack__.push()
    __stack__.add(___412.inputs, bot=True)
    name.adopt(__stack__.all())
    name.adopt(___412.inputs)
    meetup.add_sql_inputs('categories', __stack__.all())
    meetup.add_sql_inputs('categories', ___412.inputs)
    if ___412:
        ___414 = meetup.get('add_category', Cell('name'))
        name = ___414
        name.adopt(__stack__.all())
        ___415 = meetup.me()
        ___416 = meetup.sql(Cell('INSERT INTO categories (name, owner) VALUES (?0, ?1)'), Cell([name, ___415]))
        ___417 = categories()[0]
        __r__ = Cell(___417, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___418 = error_html(Cell('Illegal operation: Add category'))[0]
        ___419 = categories()[0]
        __r__ = Cell((___418 + ___419), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_category')
def _add_category():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_category()
    return (__c__, __r__, __s__, __trace__)

def update_category(cat):
    global __stack__
    if ('cat_' not in locals()):
        cat_ = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___420 = meetup.sql(Cell('SELECT * FROM categories WHERE id = ?0'), Cell([cat]))
    cat_ = ___420[Cell(0)]
    cat_.adopt(__stack__.all())
    ___421 = db_str(cat_[Cell(0)])
    id_ = ___421
    id_.adopt(__stack__.all())
    name = cat_[Cell(1)]
    name.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('name')]
    ___422 = meetup.render(Cell('update_category.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: name,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___422, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_category/<int:cat>')
def _update_category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('update_category', 'cat', cat)
    (__r__, __s__) = update_category(cat)
    return (__c__, __r__, __s__, __trace__)

def update_category_do():
    global __stack__
    if ('ev_cat' not in locals()):
        ev_cat = Cell(None)
    if ('ev_cats' not in locals()):
        ev_cats = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('cat_mod' not in locals()):
        cat_mod = Cell(None)
    if ('cat_mods' not in locals()):
        cat_mods = Cell(None)
    if ('cat_sub' not in locals()):
        cat_sub = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('cat_subs' not in locals()):
        cat_subs = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    ___423 = is_admin()[0]
    ___424 = non(___423)
    __trace__.add_inputs(___424.inputs)
    __stack__.push()
    __stack__.add(___424.inputs, bot=True)
    if ___424:
        ___426 = error_html(Cell('Illegal operation: Update category'))[0]
        ___427 = categories()[0]
        __r__ = Cell((___426 + ___427), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    ___428 = meetup.get('update_category_do', Cell('id_'))
    ___429 = db_int(___428)
    id_ = ___429
    id_.adopt(__stack__.all())
    ___430 = meetup.get('update_category_do', Cell('name'))
    name = ___430
    name.adopt(__stack__.all())
    ___431 = meetup.sql(Cell('SELECT owner FROM categories WHERE id = ?0'), Cell([id_]))
    owner = ___431[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___432 = meetup.sql(Cell('DELETE FROM categories WHERE id = ?0'), Cell([id_]))
    ___433 = meetup.sql(Cell('INSERT INTO categories (name, owner) VALUES (?0, ?1)'), Cell([name, owner]))
    new_id = ___433[Cell(0)]
    new_id.adopt(__stack__.all())
    ___434 = meetup.sql(Cell('SELECT * FROM cat_mod WHERE category = ?0'), Cell([id_]))
    cat_mods = ___434
    cat_mods.adopt(__stack__.all())
    ___435 = cat_mods
    __trace__.add_inputs(___435.inputs)
    for cat_mod in ___435:
        ___437 = meetup.sql(Cell('DELETE FROM cat_mod WHERE id = ?0'), Cell([cat_mod[Cell(0)]]))
        ___438 = meetup.sql(Cell('INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)'), Cell([new_id, cat_mod[Cell(2)]]))
    ___439 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE category = ?0'), Cell([id_]))
    cat_subs = ___439
    cat_subs.adopt(__stack__.all())
    ___440 = cat_subs
    __trace__.add_inputs(___440.inputs)
    for cat_sub in ___440:
        ___442 = meetup.sql(Cell('DELETE FROM cat_sub WHERE id = ?0'), Cell([cat_sub[Cell(0)]]))
        ___443 = meetup.sql(Cell('INSERT INTO cat_sub (category, subscriber) VALUES (?0, ?1)'), Cell([new_id, cat_sub[Cell(2)]]))
    ___444 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE category = ?0'), Cell([id_]))
    ev_cats = ___444
    ev_cats.adopt(__stack__.all())
    ___445 = ev_cats
    __trace__.add_inputs(___445.inputs)
    for ev_cat in ___445:
        ___447 = meetup.sql(Cell('DELETE FROM ev_cat WHERE id = ?0'), Cell([ev_cat[Cell(0)]]))
        ___448 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([ev_cat[Cell(1)], new_id]))
    ___449 = categories()[0]
    __r__ = Cell(___449, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_category_do')
def _update_category_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_category_do()
    return (__c__, __r__, __s__, __trace__)

def category(cat):
    global __stack__
    if ('events_html' not in locals()):
        events_html = Cell(None)
    if ('moderators' not in locals()):
        moderators = Cell(None)
    if ('moderator' not in locals()):
        moderator = Cell(None)
    if ('subscribers_html' not in locals()):
        subscribers_html = Cell(None)
    if ('is_subscriber' not in locals()):
        is_subscriber = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('moderators_html' not in locals()):
        moderators_html = Cell(None)
    if ('subscribers' not in locals()):
        subscribers = Cell(None)
    if ('subscriber' not in locals()):
        subscriber = Cell(None)
    ___450 = meetup.sql(Cell('SELECT name FROM categories WHERE id = ?0'), Cell([cat]))
    name = ___450[Cell(0)][Cell(0)]
    name.adopt(__stack__.all())
    ___451 = meetup.me()
    ___452 = meetup.sql(Cell('SELECT * FROM cat_sub WHERE category = ?0 AND subscriber = ?1'), Cell([cat, ___451]))
    ___453 = db_len(___452)
    is_subscriber = Cell((___453 > Cell(0)))
    is_subscriber.adopt(__stack__.all())
    moderators_html = Cell('')
    moderators_html.adopt(__stack__.all())
    subscribers_html = Cell('')
    subscribers_html.adopt(__stack__.all())
    ___454 = is_subscriber
    __stack__.push()
    __stack__.add(___454.inputs)
    subscribers_html.adopt(__stack__.all())
    subscribers_html.adopt(___454.inputs)
    moderators_html.adopt(__stack__.all())
    moderators_html.adopt(___454.inputs)
    subscribers.adopt(__stack__.all())
    subscribers.adopt(___454.inputs)
    moderators.adopt(__stack__.all())
    moderators.adopt(___454.inputs)
    if ___454:
        ___456 = is_admin()[0]
        ___457 = is_moderator(cat)[0]
        ___458 = Cell((___456.value or ___457.value), inputs=(___456.inputs + ___457.inputs))
        __stack__.push()
        __stack__.add(___458.inputs)
        if ___458:
            ___460 = db_str(cat)
        __stack__.pop()
        moderators_html = Cell('')
        moderators_html.adopt(__stack__.all())
        ___461 = meetup.sql(Cell('SELECT users.* FROM users JOIN cat_mod ON cat_mod.moderator = users.id WHERE cat_mod.category = ?0'), Cell([cat]))
        moderators = ___461
        moderators.adopt(__stack__.all())
        ___462 = moderators
        for moderator in ___462:
            ___464 = db_str(moderator[Cell(1)])
            moderators_html = (moderators_html + ((((Cell('<tr><td>') + ___464) + Cell('</td><td>')) + moderator[Cell(2)]) + Cell('</td></tr>')))
            moderators_html.adopt(__stack__.all())
        subscribers_html = Cell('')
        subscribers_html.adopt(__stack__.all())
        ___465 = meetup.sql(Cell('SELECT users.* FROM users JOIN cat_sub ON cat_sub.subscriber = users.id WHERE cat_sub.category = ?0'), Cell([cat]))
        subscribers = ___465
        subscribers.adopt(__stack__.all())
        ___466 = subscribers
        for subscriber in ___466:
            ___468 = db_str(subscriber[Cell(1)])
            subscribers_html = (subscribers_html + ((((Cell('<tr><td>') + ___468) + Cell('</td><td>')) + subscriber[Cell(2)]) + Cell('</td></tr>')))
            subscribers_html.adopt(__stack__.all())
    __stack__.pop()
    events_html = Cell('')
    events_html.adopt(__stack__.all())
    ___469 = meetup.sql(Cell('SELECT events.* FROM events JOIN ev_cat ON ev_cat.event = events.id WHERE ev_cat.category = ?0'), Cell([cat]))
    events = ___469
    events.adopt(__stack__.all())
    ___470 = events
    for ev in ___470:
        ___472 = event_html(ev, Cell(True))[0]
        events_html = (events_html + ___472)
        events_html.adopt(__stack__.all())
    __k__ = [Cell('name'), Cell('cat'), Cell('is_admin_or_moderator'), Cell('is_subscriber'), Cell('moderators_html'), Cell('subscribers_html'), Cell('events_html')]
    ___473 = db_str(cat)
    ___474 = is_admin()[0]
    ___475 = is_moderator(cat)[0]
    ___476 = meetup.render(Cell('category.html'), Cell({
        __k__[0].value: name,
        __k__[1].value: ___473,
        __k__[2].value: Cell((___474.value or ___475.value), inputs=(___474.inputs + ___475.inputs)),
        __k__[3].value: is_subscriber,
        __k__[4].value: moderators_html,
        __k__[5].value: subscribers_html,
        __k__[6].value: events_html,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___476, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/category/<int:cat>')
def _category(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('category', 'cat', cat)
    (__r__, __s__) = category(cat)
    return (__c__, __r__, __s__, __trace__)

def category_moderators(cat):
    global __stack__
    if ('user' not in locals()):
        user = Cell(None)
    if ('new_html' not in locals()):
        new_html = Cell(None)
    if ('current_html' not in locals()):
        current_html = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___477 = is_admin()[0]
    ___478 = ___477
    __trace__.add_inputs(___478.inputs)
    __stack__.push()
    __stack__.add(___478.inputs, bot=True)
    users.adopt(__stack__.all())
    users.adopt(___478.inputs)
    name.adopt(__stack__.all())
    name.adopt(___478.inputs)
    current_html.adopt(__stack__.all())
    current_html.adopt(___478.inputs)
    new_html.adopt(__stack__.all())
    new_html.adopt(___478.inputs)
    if ___478:
        ___480 = meetup.sql(Cell('SELECT name FROM categories WHERE id = ?0'), Cell([cat]))
        name = ___480[Cell(0)][Cell(0)]
        name.adopt(__stack__.all())
        ___481 = meetup.sql(Cell('SELECT id, user_id, name FROM users'), Cell([cat]))
        users = ___481
        users.adopt(__stack__.all())
        ___482 = users
        for user in ___482:
            ___484 = meetup.sql(Cell('SELECT * FROM cat_mod WHERE category = ?0 AND moderator = ?1'), Cell([cat, user[Cell(0)]]))
            ___485 = db_len(___484)
            ___486 = Cell((___485 > Cell(0)))
            __stack__.push()
            __stack__.add(___486.inputs)
            current_html.adopt(__stack__.all())
            current_html.adopt(___486.inputs)
            new_html.adopt(__stack__.all())
            new_html.adopt(___486.inputs)
            if ___486:
                ___488 = db_str(user[Cell(1)])
                ___489 = db_str(cat)
                ___490 = db_str(user[Cell(0)])
                current_html = (current_html + ((((((((Cell('<tr><td>') + ___488) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td><a href="/2/delete_category_moderator/')) + ___489) + Cell('/')) + ___490) + Cell('">Remove moderator</a></td></tr>')))
                current_html.adopt(__stack__.all())
            else:
                ___491 = db_str(user[Cell(1)])
                ___492 = db_str(cat)
                ___493 = db_str(user[Cell(0)])
                new_html = (new_html + ((((((((Cell('<tr><td>') + ___491) + Cell('</td><td>')) + user[Cell(2)]) + Cell('</td><td><a href="/2/add_category_moderator/')) + ___492) + Cell('/')) + ___493) + Cell('">Make moderator</a></td></tr>')))
                new_html.adopt(__stack__.all())
            __stack__.pop()
        __k__ = [Cell('name'), Cell('current_html'), Cell('new_html'), Cell('cat')]
        ___494 = db_str(cat)
        ___495 = meetup.render(Cell('category_moderators.html'), Cell({
            __k__[0].value: name,
            __k__[1].value: current_html,
            __k__[2].value: new_html,
            __k__[3].value: ___494,
        }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
        __r__ = Cell(___495, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___496 = error_html(Cell('Illegal operation: Category moderators'))[0]
        ___497 = category(cat)[0]
        __r__ = Cell((___496 + ___497), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/category_moderators/<int:cat>')
def _category_moderators(cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('category_moderators', 'cat', cat)
    (__r__, __s__) = category_moderators(cat)
    return (__c__, __r__, __s__, __trace__)

def delete_category_moderator(cat, user):
    global __stack__
    ___498 = is_admin()[0]
    ___499 = ___498
    __trace__.add_inputs(___499.inputs)
    __stack__.push()
    __stack__.add(___499.inputs, bot=True)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___499.inputs)
    if ___499:
        ___501 = meetup.sql(Cell('DELETE FROM cat_mod WHERE category = ?0 AND moderator = ?1'), Cell([cat, user]))
        ___502 = category_moderators(cat)[0]
        __r__ = Cell(___502, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___503 = error_html(Cell('Illegal operation: Delete category moderator'))[0]
        ___504 = category(cat)[0]
        __r__ = Cell((___503 + ___504), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_category_moderator/<int:cat>/<int:user>')
def _delete_category_moderator(cat, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('delete_category_moderator', 'cat', cat)
    user = meetup.register('delete_category_moderator', 'user', user)
    (__r__, __s__) = delete_category_moderator(cat, user)
    return (__c__, __r__, __s__, __trace__)

def add_category_moderator(cat, user):
    global __stack__
    ___505 = is_admin()[0]
    ___506 = ___505
    __trace__.add_inputs(___506.inputs)
    __stack__.push()
    __stack__.add(___506.inputs, bot=True)
    meetup.add_sql_inputs('cat_mod', __stack__.all())
    meetup.add_sql_inputs('cat_mod', ___506.inputs)
    if ___506:
        ___508 = meetup.sql(Cell('INSERT INTO cat_mod (category, moderator) VALUES (?0, ?1)'), Cell([cat, user]))
        ___509 = category_moderators(cat)[0]
        __r__ = Cell(___509, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___510 = error_html(Cell('Illegal operation: Add category moderator'))[0]
        ___511 = category(cat)[0]
        __r__ = Cell((___510 + ___511), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_category_moderator/<int:cat>/<int:user>')
def _add_category_moderator(cat, user):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    cat = meetup.register('add_category_moderator', 'cat', cat)
    user = meetup.register('add_category_moderator', 'user', user)
    (__r__, __s__) = add_category_moderator(cat, user)
    return (__c__, __r__, __s__, __trace__)

def events():
    global __stack__
    ___512 = events_from(Cell(0))[0]
    __r__ = Cell(___512, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/events')
def _events():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = events()
    return (__c__, __r__, __s__, __trace__)

def events_from(start):
    global __stack__
    if ('event_html_' not in locals()):
        event_html_ = Cell(None)
    if ('events_html' not in locals()):
        events_html = Cell(None)
    if ('L' not in locals()):
        L = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    events_html = Cell('')
    events_html.adopt(__stack__.all())
    ___513 = meetup.me()
    ___514 = meetup.sql(Cell('SELECT events.* FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 ORDER BY events.id DESC LIMIT 5 OFFSET ?1'), Cell([___513, start]))
    events = ___514
    events.adopt(__stack__.all())
    ___515 = meetup.me()
    ___516 = meetup.sql(Cell('SELECT events.id FROM events LEFT JOIN friends ON events.owner = friends.friend_id WHERE friends.user_id = ?0 OR events.owner = ?0 LIMIT 6 OFFSET ?1'), Cell([___515, start]))
    ___517 = db_len(___516)
    L = ___517
    L.adopt(__stack__.all())
    ___518 = events
    __trace__.add_inputs(___518.inputs)
    for ev in ___518:
        ___520 = event_html(ev, Cell(True))[0]
        event_html_ = ___520
        event_html_.adopt(__stack__.all())
        events_html = (events_html + (event_html_ + Cell('<br>')))
        events_html.adopt(__stack__.all())
    ___521 = meetup.check(__c__, __trace__, Cell('Analytics'), events_html, Cell('evilanalytics.org'))
    ___522 = ___521
    __trace__.add_inputs(___522.inputs)
    __stack__.push()
    __stack__.add(___522.inputs, bot=True)
    if ___522:
        ___524 = meetup.send(__c__, __trace__, Cell('evilanalytics.org'), Cell('Analytics'), events_html)
    __stack__.pop()
    ___525 = Cell((Cell((start > Cell(0))).value or Cell((L > Cell(5))).value), inputs=(Cell((start > Cell(0))).inputs + Cell((L > Cell(5))).inputs))
    __stack__.push()
    __stack__.add(___525.inputs)
    events_html.adopt(__stack__.all())
    events_html.adopt(___525.inputs)
    if ___525:
        events_html = (Cell('  </ul>\n</nav>') + events_html)
        events_html.adopt(__stack__.all())
        ___527 = Cell((L > Cell(5)))
        __stack__.push()
        __stack__.add(___527.inputs)
        events_html.adopt(__stack__.all())
        events_html.adopt(___527.inputs)
        if ___527:
            ___529 = db_str((start + Cell(5)))
            ___530 = db_str((start + Cell(6)))
            events_html = (((((Cell('<li class="page-item"><a class="page-link" href="/2/events/') + ___529) + Cell('">Events ')) + ___530) + Cell('-...</a></li>')) + events_html)
            events_html.adopt(__stack__.all())
        __stack__.pop()
        ___531 = db_str((start + Cell(1)))
        ___532 = db_str((start + Cell(5)))
        events_html = (((((Cell('<li class="page-item active"><a class="page-link" href="#">Events ') + ___531) + Cell('-')) + ___532) + Cell('</a></li>')) + events_html)
        events_html.adopt(__stack__.all())
        ___533 = Cell((start > Cell(0)))
        __stack__.push()
        __stack__.add(___533.inputs)
        events_html.adopt(__stack__.all())
        events_html.adopt(___533.inputs)
        if ___533:
            ___535 = db_str((start - Cell(5)))
            ___536 = db_str((start - Cell(4)))
            ___537 = db_str(start)
            events_html = (((((((Cell('<li class="page-item"><a class="page-link" href="/2/events/') + ___535) + Cell('">Events ')) + ___536) + Cell('-')) + ___537) + Cell('</a></li>')) + events_html)
            events_html.adopt(__stack__.all())
        __stack__.pop()
        events_html = (Cell('<nav>\n  <ul class="pagination">') + events_html)
        events_html.adopt(__stack__.all())
    __stack__.pop()
    __k__ = [Cell('location_select'), Cell('events_html')]
    ___538 = location_select(Cell(0))[0]
    ___539 = meetup.render(Cell('events.html'), Cell({
        __k__[0].value: Cell((Cell('Service'), ___538)),
        __k__[1].value: Cell((Cell('Service'), events_html)),
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___539, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/events/<int:start>')
def _events_from(start):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    start = meetup.register('events_from', 'start', start)
    (__r__, __s__) = events_from(start)
    return (__c__, __r__, __s__, __trace__)

def add_event():
    global __stack__
    if ('owned' not in locals()):
        owned = Cell(None)
    if ('date' not in locals()):
        date = Cell(None)
    if ('level' not in locals()):
        level = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    ___540 = meetup.get('add_event', Cell('title'))
    title = ___540
    title.adopt(__stack__.all())
    ___541 = meetup.get('add_event', Cell('description'))
    description = ___541
    description.adopt(__stack__.all())
    ___542 = Cell((description == Cell('')))
    __stack__.push()
    __stack__.add(___542.inputs)
    description.adopt(__stack__.all())
    description.adopt(___542.inputs)
    if ___542:
        description = (title + Cell(' (no description provided)'))
        description.adopt(__stack__.all())
    __stack__.pop()
    ___544 = meetup.get('add_event', Cell('location'))
    ___545 = db_int(___544)
    location = ___545
    location.adopt(__stack__.all())
    ___546 = meetup.get('add_event', Cell('time'))
    time = ___546
    time.adopt(__stack__.all())
    ___547 = meetup.get('add_event', Cell('date'))
    date = ___547
    date.adopt(__stack__.all())
    ___548 = meetup.me()
    owner = ___548
    owner.adopt(__stack__.all())
    ___549 = meetup.me()
    ___550 = meetup.sql(Cell('SELECT level FROM users WHERE user_id = ?0'), Cell([___549]))
    level = ___550[Cell(0)][Cell(0)]
    level.adopt(__stack__.all())
    ___551 = meetup.me()
    ___552 = meetup.sql(Cell('SELECT * FROM events WHERE owner = ?0 LIMIT 3'), Cell([___551]))
    ___553 = db_len(___552)
    owned = ___553
    owned.adopt(__stack__.all())
    ___554 = Cell((Cell((level == Cell(1))).value and Cell((owned > Cell(2))).value), inputs=(Cell((level == Cell(1))).inputs + Cell((owned > Cell(2))).inputs))
    __trace__.add_inputs(___554.inputs)
    __stack__.push()
    __stack__.add(___554.inputs, bot=True)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___554.inputs)
    if ___554:
        ___556 = error_html(Cell('Illegal operation: Add event'))[0]
        ___557 = events()[0]
        __r__ = Cell((___556 + ___557), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___558 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([title, description, location, time, date, owner]))
        ___559 = meetup.redirect(Cell('Service'), Cell('events'))
        __r__ = Cell(___559, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_event')
def _add_event():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_event()
    return (__c__, __r__, __s__, __trace__)

def event(ev):
    global __stack__
    if ('attendee' not in locals()):
        attendee = Cell(None)
    if ('attendees' not in locals()):
        attendees = Cell(None)
    if ('cats_html' not in locals()):
        cats_html = Cell(None)
    if ('manager' not in locals()):
        manager = Cell(None)
    if ('i' not in locals()):
        i = Cell(None)
    if ('managers_html' not in locals()):
        managers_html = Cell(None)
    if ('cats' not in locals()):
        cats = Cell(None)
    if ('attendees_html' not in locals()):
        attendees_html = Cell(None)
    if ('requesters' not in locals()):
        requesters = Cell(None)
    if ('requesters_html' not in locals()):
        requesters_html = Cell(None)
    if ('cat' not in locals()):
        cat = Cell(None)
    if ('is_event_category' not in locals()):
        is_event_category = Cell(None)
    if ('managers' not in locals()):
        managers = Cell(None)
    if ('evt' not in locals()):
        evt = Cell(None)
    if ('requester' not in locals()):
        requester = Cell(None)
    managers_html = Cell('')
    managers_html.adopt(__stack__.all())
    attendees_html = Cell('')
    attendees_html.adopt(__stack__.all())
    requesters_html = Cell('')
    requesters_html.adopt(__stack__.all())
    cats_html = Cell('')
    cats_html.adopt(__stack__.all())
    ___560 = meetup.sql(Cell('SELECT * FROM events WHERE id = ?0'), Cell([ev]))
    evt = ___560[Cell(0)]
    evt.adopt(__stack__.all())
    ___561 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 2'), Cell([ev]))
    managers = ___561
    managers.adopt(__stack__.all())
    ___562 = managers
    for manager in ___562:
        manager = managers[i]
        manager.adopt(__stack__.all())
        ___564 = db_str(manager[Cell(0)])
        managers_html = (managers_html + ((((Cell('<tr><td>') + ___564) + Cell('</td><td>')) + manager[Cell(1)]) + Cell('</td>')))
        managers_html.adopt(__stack__.all())
        ___565 = meetup.me()
        ___566 = is_premium()[0]
        ___567 = Cell((Cell((evt[Cell(6)] == ___565)).value and ___566.value), inputs=(Cell((evt[Cell(6)] == ___565)).inputs + ___566.inputs))
        __stack__.push()
        __stack__.add(___567.inputs)
        managers_html.adopt(__stack__.all())
        managers_html.adopt(___567.inputs)
        if ___567:
            ___569 = db_str(ev)
            ___570 = db_str(manager[Cell(0)])
            managers_html = (managers_html + ((((Cell('<td><a href="/2/revoke_moderator/') + ___569) + Cell('/')) + ___570) + Cell('">Revoke</a></td></tr>')))
            managers_html.adopt(__stack__.all())
        else:
            managers_html = (managers_html + Cell('<td></td></tr>'))
            managers_html.adopt(__stack__.all())
        __stack__.pop()
    ___571 = is_attendee(ev)[0]
    ___572 = is_manager(ev)[0]
    ___573 = is_premium()[0]
    ___574 = Cell((___571.value or Cell((___572.value or ___573.value), inputs=(___572.inputs + ___573.inputs)).value), inputs=(___571.inputs + Cell((___572.value or ___573.value), inputs=(___572.inputs + ___573.inputs)).inputs))
    __stack__.push()
    __stack__.add(___574.inputs)
    attendees_html.adopt(__stack__.all())
    attendees_html.adopt(___574.inputs)
    attendees.adopt(__stack__.all())
    attendees.adopt(___574.inputs)
    if ___574:
        ___576 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN ev_att ON ev_att.attendee = users.user_id WHERE ev_att.event = ?0 AND ev_att.level = 1'), Cell([ev]))
        attendees = ___576
        attendees.adopt(__stack__.all())
        ___577 = attendees
        for attendee in ___577:
            ___579 = db_str(attendee[Cell(0)])
            attendees_html = (attendees_html + ((((Cell('<tr><td>') + ___579) + Cell('</td><td>')) + attendee[Cell(1)]) + Cell('</td>')))
            attendees_html.adopt(__stack__.all())
            ___580 = is_manager(ev)[0]
            ___581 = meetup.me()
            ___582 = is_premium()[0]
            ___583 = Cell((___580.value or Cell((Cell((evt[Cell(6)] == ___581)).value and ___582.value), inputs=(Cell((evt[Cell(6)] == ___581)).inputs + ___582.inputs)).value), inputs=(___580.inputs + Cell((Cell((evt[Cell(6)] == ___581)).value and ___582.value), inputs=(Cell((evt[Cell(6)] == ___581)).inputs + ___582.inputs)).inputs))
            __stack__.push()
            __stack__.add(___583.inputs)
            attendees_html.adopt(__stack__.all())
            attendees_html.adopt(___583.inputs)
            if ___583:
                ___585 = db_str(ev)
                ___586 = db_str(attendee[Cell(0)])
                attendees_html = (attendees_html + ((((Cell('<td><a href="/2/promote_attendee/') + ___585) + Cell('/')) + ___586) + Cell('">Promote to manager</a></td></tr>')))
                attendees_html.adopt(__stack__.all())
            else:
                attendees_html = (attendees_html + Cell('<td></td></tr>'))
                attendees_html.adopt(__stack__.all())
            __stack__.pop()
    __stack__.pop()
    ___587 = is_manager(ev)[0]
    ___588 = ___587
    __stack__.push()
    __stack__.add(___588.inputs)
    requesters.adopt(__stack__.all())
    requesters.adopt(___588.inputs)
    is_event_category.adopt(__stack__.all())
    is_event_category.adopt(___588.inputs)
    cats.adopt(__stack__.all())
    cats.adopt(___588.inputs)
    cats_html.adopt(__stack__.all())
    cats_html.adopt(___588.inputs)
    requesters_html.adopt(__stack__.all())
    requesters_html.adopt(___588.inputs)
    if ___588:
        ___590 = meetup.sql(Cell('SELECT users.user_id, users.name FROM users JOIN requests ON requests.requester = users.user_id WHERE requests.event = ?0'), Cell([ev]))
        requesters = ___590
        requesters.adopt(__stack__.all())
        ___591 = requesters
        for requester in ___591:
            ___593 = db_str(requester[Cell(0)])
            ___594 = db_str(requester[Cell(1)])
            ___595 = db_str(ev)
            ___596 = db_str(requester[Cell(0)])
            ___597 = db_str(ev)
            ___598 = db_str(requester[Cell(0)])
            requesters_html = (requesters_html + ((((((((((((Cell('<tr><td>') + ___593) + Cell('</td><td>')) + ___594) + Cell('</td><td><a href="/2/accept_request/')) + ___595) + Cell('/')) + ___596) + Cell('">Accept</a>&nbsp;<a href="/2/reject_request/')) + ___597) + Cell('/')) + ___598) + Cell('">Reject</a>&nbsp;</td></tr>')))
            requesters_html.adopt(__stack__.all())
        ___599 = meetup.me()
        ___600 = meetup.sql(Cell('SELECT categories.* FROM categories LEFT JOIN friends ON friends.friend_id = categories.owner WHERE friends.user_id = ?0 OR categories.owner = ?0'), Cell([___599]))
        cats = ___600
        cats.adopt(__stack__.all())
        ___601 = cats
        for cat in ___601:
            ___603 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE event = ?0 AND category = ?1'), Cell([ev, cat[Cell(0)]]))
            ___604 = db_len(___603)
            is_event_category = Cell((___604 > Cell(0)))
            is_event_category.adopt(__stack__.all())
            ___605 = is_event_category
            __stack__.push()
            __stack__.add(___605.inputs)
            cats_html.adopt(__stack__.all())
            cats_html.adopt(___605.inputs)
            if ___605:
                ___607 = db_str(cat[Cell(1)])
                ___608 = db_str(ev)
                ___609 = db_str(cat[Cell(0)])
                cats_html = (cats_html + ((((((Cell('<tr><td>') + ___607) + Cell(' (selected)</td><td><a href="/2/delete_event_category/')) + ___608) + Cell('/')) + ___609) + Cell('">Delete</td></tr>')))
                cats_html.adopt(__stack__.all())
            else:
                ___610 = db_str(cat[Cell(1)])
                ___611 = db_str(ev)
                ___612 = db_str(cat[Cell(0)])
                cats_html = (cats_html + ((((((Cell('<tr><td>') + ___610) + Cell('</td><td><a href="/2/add_event_category/')) + ___611) + Cell('/')) + ___612) + Cell('">Add</td></tr>')))
                cats_html.adopt(__stack__.all())
            __stack__.pop()
    __stack__.pop()
    __k__ = [Cell('event_html'), Cell('managers_html'), Cell('is_attendee_manager_or_premium'), Cell('attendees_html'), Cell('is_manager'), Cell('requesters_html'), Cell('cats_html'), Cell('is_moderator'), Cell('ev')]
    ___613 = event_html(evt, Cell(True))[0]
    ___614 = is_attendee(ev)[0]
    ___615 = is_manager(ev)[0]
    ___616 = is_premium()[0]
    ___617 = is_manager(ev)[0]
    ___618 = is_moderator(ev)[0]
    ___619 = db_str(ev)
    ___620 = meetup.render(Cell('event.html'), Cell({
        __k__[0].value: ___613,
        __k__[1].value: managers_html,
        __k__[2].value: Cell((___614.value or Cell((___615.value or ___616.value), inputs=(___615.inputs + ___616.inputs)).value), inputs=(___614.inputs + Cell((___615.value or ___616.value), inputs=(___615.inputs + ___616.inputs)).inputs)),
        __k__[3].value: attendees_html,
        __k__[4].value: ___617,
        __k__[5].value: requesters_html,
        __k__[6].value: cats_html,
        __k__[7].value: ___618,
        __k__[8].value: ___619,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs, __k__[7].inputs, __k__[8].inputs]))
    __r__ = Cell(___620, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/event/<int:ev>')
def _event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('event', 'ev', ev)
    (__r__, __s__) = event(ev)
    return (__c__, __r__, __s__, __trace__)

def add_event_category(ev, cat):
    global __stack__
    ___621 = is_manager(ev)[0]
    ___622 = ___621
    __trace__.add_inputs(___622.inputs)
    __stack__.push()
    __stack__.add(___622.inputs, bot=True)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___622.inputs)
    if ___622:
        ___624 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([ev, cat]))
        ___625 = event(ev)[0]
        __r__ = Cell(___625, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___626 = error_html(Cell('Illegal operation: Add event category'))[0]
        ___627 = event(ev)[0]
        __r__ = Cell((___626 + ___627), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/add_event_category/<int:ev>/<int:cat>')
def _add_event_category(ev, cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('add_event_category', 'ev', ev)
    cat = meetup.register('add_event_category', 'cat', cat)
    (__r__, __s__) = add_event_category(ev, cat)
    return (__c__, __r__, __s__, __trace__)

def delete_event_category(ev, cat):
    global __stack__
    ___628 = is_manager(ev)[0]
    ___629 = ___628
    __trace__.add_inputs(___629.inputs)
    __stack__.push()
    __stack__.add(___629.inputs, bot=True)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___629.inputs)
    if ___629:
        ___631 = meetup.sql(Cell('DELETE FROM ev_cat WHERE event = ?0 AND category = ?1'), Cell([ev, cat]))
        ___632 = event(ev)[0]
        __r__ = Cell(___632, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___633 = error_html(Cell('Illegal operation: Delete event category'))[0]
        ___634 = event(ev)[0]
        __r__ = Cell((___633 + ___634), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_event_category/<int:ev>/<int:cat>')
def _delete_event_category(ev, cat):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('delete_event_category', 'ev', ev)
    cat = meetup.register('delete_event_category', 'cat', cat)
    (__r__, __s__) = delete_event_category(ev, cat)
    return (__c__, __r__, __s__, __trace__)

def delete_event(ev):
    global __stack__
    ___635 = is_manager(ev)[0]
    ___636 = ___635
    __trace__.add_inputs(___636.inputs)
    __stack__.push()
    __stack__.add(___636.inputs, bot=True)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___636.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___636.inputs)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___636.inputs)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___636.inputs)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___636.inputs)
    if ___636:
        ___638 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev]))
        ___639 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0'), Cell([ev]))
        ___640 = meetup.sql(Cell('DELETE FROM ev_cat WHERE event = ?0'), Cell([ev]))
        ___641 = meetup.sql(Cell('DELETE FROM invitations WHERE event = ?0'), Cell([ev]))
        ___642 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0'), Cell([ev]))
        ___643 = events()[0]
        __r__ = Cell(___643, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___644 = error_html(Cell('Illegal operation: Delete event'))[0]
        ___645 = events()[0]
        __r__ = Cell((___644 + ___645), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_event/<int:ev>')
def _delete_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('delete_event', 'ev', ev)
    (__r__, __s__) = delete_event(ev)
    return (__c__, __r__, __s__, __trace__)

def update_event(ev):
    global __stack__
    if ('date' not in locals()):
        date = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('evt' not in locals()):
        evt = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    ___646 = meetup.sql(Cell('SELECT * FROM events WHERE id = ?0'), Cell([ev]))
    evt = ___646[Cell(0)]
    evt.adopt(__stack__.all())
    title = evt[Cell(1)]
    title.adopt(__stack__.all())
    description = evt[Cell(2)]
    description.adopt(__stack__.all())
    ___647 = Cell((evt[Cell(3)] > Cell(0)))
    __stack__.push()
    __stack__.add(___647.inputs)
    location.adopt(__stack__.all())
    location.adopt(___647.inputs)
    if ___647:
        ___649 = location_html(evt[Cell(3)])[0]
        location = ___649
        location.adopt(__stack__.all())
    else:
        location = Cell('')
        location.adopt(__stack__.all())
    __stack__.pop()
    time = evt[Cell(4)]
    time.adopt(__stack__.all())
    date = evt[Cell(5)]
    date.adopt(__stack__.all())
    ___650 = db_str(evt[Cell(6)])
    owner = ___650
    owner.adopt(__stack__.all())
    __k__ = [Cell('title'), Cell('description'), Cell('location_select'), Cell('time'), Cell('date'), Cell('owner'), Cell('ev')]
    ___651 = location_select(evt[Cell(3)])[0]
    ___652 = db_str(ev)
    ___653 = meetup.render(Cell('update_event.make'), Cell({
        __k__[0].value: title,
        __k__[1].value: description,
        __k__[2].value: ___651,
        __k__[3].value: time,
        __k__[4].value: date,
        __k__[5].value: owner,
        __k__[6].value: ___652,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs, __k__[4].inputs, __k__[5].inputs, __k__[6].inputs]))
    __r__ = Cell(___653, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_event/<int:ev>')
def _update_event(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('update_event', 'ev', ev)
    (__r__, __s__) = update_event(ev)
    return (__c__, __r__, __s__, __trace__)

def update_event_do():
    global __stack__
    if ('ev_att' not in locals()):
        ev_att = Cell(None)
    if ('ev_cat' not in locals()):
        ev_cat = Cell(None)
    if ('request' not in locals()):
        request = Cell(None)
    if ('ev_cats' not in locals()):
        ev_cats = Cell(None)
    if ('invitation' not in locals()):
        invitation = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('time' not in locals()):
        time = Cell(None)
    if ('title' not in locals()):
        title = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('invitations' not in locals()):
        invitations = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('requests' not in locals()):
        requests = Cell(None)
    if ('date' not in locals()):
        date = Cell(None)
    if ('description' not in locals()):
        description = Cell(None)
    if ('ev_atts' not in locals()):
        ev_atts = Cell(None)
    ___654 = meetup.get('update_event_do', Cell('id_'))
    ___655 = db_int(___654)
    id_ = ___655
    id_.adopt(__stack__.all())
    ___656 = is_manager(id_)[0]
    ___657 = is_moderator(id_)[0]
    ___658 = non(Cell((___656.value or ___657.value), inputs=(___656.inputs + ___657.inputs)))
    __trace__.add_inputs(___658.inputs)
    __stack__.push()
    __stack__.add(___658.inputs, bot=True)
    title.adopt(__stack__.all())
    title.adopt(___658.inputs)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___658.inputs)
    requests.adopt(__stack__.all())
    requests.adopt(___658.inputs)
    date.adopt(__stack__.all())
    date.adopt(___658.inputs)
    time.adopt(__stack__.all())
    time.adopt(___658.inputs)
    description.adopt(__stack__.all())
    description.adopt(___658.inputs)
    invitations.adopt(__stack__.all())
    invitations.adopt(___658.inputs)
    meetup.add_sql_inputs('ev_cat', __stack__.all())
    meetup.add_sql_inputs('ev_cat', ___658.inputs)
    ev_atts.adopt(__stack__.all())
    ev_atts.adopt(___658.inputs)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___658.inputs)
    ev_cats.adopt(__stack__.all())
    ev_cats.adopt(___658.inputs)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___658.inputs)
    owner.adopt(__stack__.all())
    owner.adopt(___658.inputs)
    location.adopt(__stack__.all())
    location.adopt(___658.inputs)
    new_id.adopt(__stack__.all())
    new_id.adopt(___658.inputs)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___658.inputs)
    if ___658:
        ___660 = error_html(Cell('Illegal operation: Update event'))[0]
        ___661 = event(id_)[0]
        __r__ = Cell((___660 + ___661), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___662 = meetup.get('update_event_do', Cell('title'))
        title = ___662
        title.adopt(__stack__.all())
        ___663 = meetup.get('update_event_do', Cell('description'))
        description = ___663
        description.adopt(__stack__.all())
        ___664 = meetup.get('update_event_do', Cell('location'))
        ___665 = db_int(___664)
        location = ___665
        location.adopt(__stack__.all())
        ___666 = meetup.get('update_event_do', Cell('time'))
        time = ___666
        time.adopt(__stack__.all())
        ___667 = meetup.get('update_event_do', Cell('date'))
        date = ___667
        date.adopt(__stack__.all())
        ___668 = meetup.get('update_event_do', Cell('owner'))
        ___669 = db_int(___668)
        owner = ___669
        owner.adopt(__stack__.all())
        ___670 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([id_]))
        ___671 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([title, description, location, time, date, owner]))
        new_id = ___671[Cell(0)]
        new_id.adopt(__stack__.all())
        ___672 = meetup.sql(Cell('SELECT * FROM ev_att WHERE event = ?0'), Cell([id_]))
        ev_atts = ___672
        ev_atts.adopt(__stack__.all())
        ___673 = ev_atts
        for ev_att in ___673:
            ___675 = meetup.sql(Cell('DELETE FROM ev_att WHERE id = ?0'), Cell([ev_att[Cell(0)]]))
            ___676 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, ?2)'), Cell([new_id, ev_att[Cell(2)], ev_att[Cell(3)]]))
        ___677 = meetup.sql(Cell('SELECT * FROM ev_cat WHERE event = ?0'), Cell([id_]))
        ev_cats = ___677
        ev_cats.adopt(__stack__.all())
        ___678 = ev_cats
        for ev_cat in ___678:
            ___680 = meetup.sql(Cell('DELETE FROM ev_cat WHERE id = ?0'), Cell([ev_cat[Cell(0)]]))
            ___681 = meetup.sql(Cell('INSERT INTO ev_cat (event, category) VALUES (?0, ?1)'), Cell([new_id, ev_cat[Cell(2)]]))
        ___682 = meetup.sql(Cell('SELECT * FROM invitations WHERE event = ?0'), Cell([id_]))
        invitations = ___682
        invitations.adopt(__stack__.all())
        ___683 = invitations
        for invitation in ___683:
            ___685 = meetup.sql(Cell('DELETE FROM invitations WHERE id = ?0'), Cell([invitation[Cell(0)]]))
            ___686 = meetup.sql(Cell('INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)'), Cell([invitation[Cell(1)], invitation[Cell(2)], new_id]))
        ___687 = meetup.sql(Cell('SELECT * FROM requests WHERE event = ?0'), Cell([id_]))
        requests = ___687
        requests.adopt(__stack__.all())
        ___688 = requests
        for request in ___688:
            ___690 = meetup.sql(Cell('DELETE FROM requests WHERE id = ?0'), Cell([request[Cell(0)]]))
            ___691 = meetup.sql(Cell('INSERT INTO requests (requester, event) VALUES (?0, ?1)'), Cell([request[Cell(1)], new_id]))
        ___692 = event(new_id)[0]
        __r__ = Cell(___692, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_event_do')
def _update_event_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_event_do()
    return (__c__, __r__, __s__, __trace__)

def promote_attendee(ev, att):
    global __stack__
    if ('evt' not in locals()):
        evt = Cell(None)
    ___693 = is_manager(ev)[0]
    ___694 = meetup.me()
    ___695 = is_premium()[0]
    ___696 = Cell((___693.value or Cell((Cell((evt[Cell(6)] == ___694)).value and ___695.value), inputs=(Cell((evt[Cell(6)] == ___694)).inputs + ___695.inputs)).value), inputs=(___693.inputs + Cell((Cell((evt[Cell(6)] == ___694)).value and ___695.value), inputs=(Cell((evt[Cell(6)] == ___694)).inputs + ___695.inputs)).inputs))
    __trace__.add_inputs(___696.inputs)
    __stack__.push()
    __stack__.add(___696.inputs, bot=True)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___696.inputs)
    if ___696:
        ___698 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, att]))
        ___699 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 2)'), Cell([ev, att]))
        ___700 = event(ev)[0]
        __r__ = Cell(___700, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___701 = error_html(Cell('Illegal operation: Promote attendee'))[0]
        ___702 = event(ev)[0]
        __r__ = Cell((___701 + ___702), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/promote_attendee/<int:ev>/<int:att>')
def _promote_attendee(ev, att):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('promote_attendee', 'ev', ev)
    att = meetup.register('promote_attendee', 'att', att)
    (__r__, __s__) = promote_attendee(ev, att)
    return (__c__, __r__, __s__, __trace__)

def revoke_moderator(ev, att):
    global __stack__
    if ('evt' not in locals()):
        evt = Cell(None)
    ___703 = is_manager(ev)[0]
    ___704 = meetup.me()
    ___705 = is_premium()[0]
    ___706 = Cell((___703.value or Cell((Cell((evt[Cell(6)] == ___704)).value and ___705.value), inputs=(Cell((evt[Cell(6)] == ___704)).inputs + ___705.inputs)).value), inputs=(___703.inputs + Cell((Cell((evt[Cell(6)] == ___704)).value and ___705.value), inputs=(Cell((evt[Cell(6)] == ___704)).inputs + ___705.inputs)).inputs))
    __trace__.add_inputs(___706.inputs)
    __stack__.push()
    __stack__.add(___706.inputs, bot=True)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___706.inputs)
    if ___706:
        ___708 = meetup.sql(Cell('DELETE FROM ev_att WHERE event = ?0 AND attendee = ?1'), Cell([ev, att]))
        ___709 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([ev, att]))
        ___710 = event(ev)[0]
        __r__ = Cell(___710, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___711 = error_html(Cell('Illegal operation: Revoke moderator'))[0]
        ___712 = event(ev)[0]
        __r__ = Cell((___711 + ___712), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/revoke_moderator/<int:ev>/<int:att>')
def _revoke_moderator(ev, att):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('revoke_moderator', 'ev', ev)
    att = meetup.register('revoke_moderator', 'att', att)
    (__r__, __s__) = revoke_moderator(ev, att)
    return (__c__, __r__, __s__, __trace__)

def accept_request(ev, req):
    global __stack__
    ___713 = is_manager(ev)[0]
    ___714 = ___713
    __trace__.add_inputs(___714.inputs)
    __stack__.push()
    __stack__.add(___714.inputs, bot=True)
    meetup.add_sql_inputs('ev_att', __stack__.all())
    meetup.add_sql_inputs('ev_att', ___714.inputs)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___714.inputs)
    if ___714:
        ___716 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0 AND requester = ?1'), Cell([ev, req]))
        ___717 = meetup.sql(Cell('INSERT INTO ev_att (event, attendee, level) VALUES (?0, ?1, 1)'), Cell([ev, req]))
        ___718 = event(ev)[0]
        __r__ = Cell(___718, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___719 = error_html(Cell('Illegal operation: Accept request'))[0]
        ___720 = event(ev)[0]
        __r__ = Cell((___719 + ___720), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/accept_request/<int:ev>/<int:req>')
def _accept_request(ev, req):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('accept_request', 'ev', ev)
    req = meetup.register('accept_request', 'req', req)
    (__r__, __s__) = accept_request(ev, req)
    return (__c__, __r__, __s__, __trace__)

def reject_request(ev, req):
    global __stack__
    ___721 = is_manager(ev)[0]
    ___722 = ___721
    __trace__.add_inputs(___722.inputs)
    __stack__.push()
    __stack__.add(___722.inputs, bot=True)
    meetup.add_sql_inputs('requests', __stack__.all())
    meetup.add_sql_inputs('requests', ___722.inputs)
    if ___722:
        ___724 = meetup.sql(Cell('DELETE FROM requests WHERE event = ?0 AND requester = ?1'), Cell([ev, req]))
        ___725 = event(ev)[0]
        __r__ = Cell(___725, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___726 = error_html(Cell('Illegal operation: Reject request'))[0]
        ___727 = event(ev)[0]
        __r__ = Cell((___726 + ___727), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/reject_request/<int:ev>/<int:req>')
def _reject_request(ev, req):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('reject_request', 'ev', ev)
    req = meetup.register('reject_request', 'req', req)
    (__r__, __s__) = reject_request(ev, req)
    return (__c__, __r__, __s__, __trace__)

def invite(ev):
    global __stack__
    if ('user_id' not in locals()):
        user_id = Cell(None)
    ___728 = is_manager(ev)[0]
    ___729 = ___728
    __trace__.add_inputs(___729.inputs)
    __stack__.push()
    __stack__.add(___729.inputs, bot=True)
    meetup.add_sql_inputs('invitations', __stack__.all())
    meetup.add_sql_inputs('invitations', ___729.inputs)
    user_id.adopt(__stack__.all())
    user_id.adopt(___729.inputs)
    if ___729:
        ___731 = meetup.get('invite', Cell('ID'))
        user_id = ___731
        user_id.adopt(__stack__.all())
        ___732 = meetup.me()
        ___733 = meetup.sql(Cell('INSERT INTO invitations (inviter, invitee, event) VALUES (?0, ?1, ?2)'), Cell([___732, user_id, ev]))
        ___734 = event(ev)[0]
        __r__ = Cell(___734, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___735 = error_html(Cell('Illegal operation: Invite'))[0]
        ___736 = event(ev)[0]
        __r__ = Cell((___735 + ___736), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/invite/<int:ev>')
def _invite(ev):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    ev = meetup.register('invite', 'ev', ev)
    (__r__, __s__) = invite(ev)
    return (__c__, __r__, __s__, __trace__)

def locations():
    global __stack__
    if ('table_html' not in locals()):
        table_html = Cell(None)
    if ('location' not in locals()):
        location = Cell(None)
    if ('locs' not in locals()):
        locs = Cell(None)
    table_html = Cell('')
    table_html.adopt(__stack__.all())
    ___737 = meetup.me()
    ___738 = meetup.sql(Cell('SELECT locations.* FROM friends LEFT JOIN locations ON friends.friend_id = locations.owner WHERE friends.user_id = ?0 OR locations.owner = ?0'), Cell([___737]))
    locs = ___738
    locs.adopt(__stack__.all())
    ___739 = locs
    for location in ___739:
        ___741 = meetup.check(__c__, __trace__, location[Cell(1)])
        ___742 = meetup.check(__c__, __trace__, location[Cell(2)])
        ___743 = meetup.check(__c__, __trace__, location[Cell(3)])
        ___744 = Cell((Cell((___741.value and ___742.value), inputs=(___741.inputs + ___742.inputs)).value and ___743.value), inputs=(Cell((___741.value and ___742.value), inputs=(___741.inputs + ___742.inputs)).inputs + ___743.inputs))
        __stack__.push()
        __stack__.add(___744.inputs)
        table_html.adopt(__stack__.all())
        table_html.adopt(___744.inputs)
        if ___744:
            ___746 = db_str(location[Cell(1)])
            ___747 = db_str(location[Cell(2)])
            table_html = (table_html + ((((Cell('<tr><td>') + ___746) + Cell('</td><td>')) + ___747) + Cell('</td>')))
            table_html.adopt(__stack__.all())
            ___748 = meetup.me()
            ___749 = Cell((location[Cell(3)] == ___748))
            __stack__.push()
            __stack__.add(___749.inputs)
            table_html.adopt(__stack__.all())
            table_html.adopt(___749.inputs)
            if ___749:
                ___751 = db_str(location[Cell(0)])
                ___752 = db_str(location[Cell(0)])
                table_html = (table_html + ((((Cell('<td><a href="/2/delete_location/') + ___751) + Cell('">Delete</a>&nbsp;<a href="/2/update_location/')) + ___752) + Cell('">Update</a></td></tr>')))
                table_html.adopt(__stack__.all())
            else:
                table_html = (table_html + Cell('<td></td></tr>'))
                table_html.adopt(__stack__.all())
            __stack__.pop()
        __stack__.pop()
    __k__ = [Cell('table_html'), Cell('is_admin')]
    ___753 = is_admin()[0]
    ___754 = meetup.render(Cell('locations.html'), Cell({
        __k__[0].value: table_html,
        __k__[1].value: ___753,
    }, inputs=[__k__[0].inputs, __k__[1].inputs]))
    __r__ = Cell(___754, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/locations')
def _locations():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = locations()
    return (__c__, __r__, __s__, __trace__)

def delete_location(location):
    global __stack__
    if ('usrs' not in locals()):
        usrs = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('evs' not in locals()):
        evs = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('usr' not in locals()):
        usr = Cell(None)
    ___755 = meetup.sql(Cell('SELECT owner FROM locations WHERE id = ?0'), Cell([location]))
    owner = ___755[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___756 = meetup.me()
    ___757 = Cell((owner != ___756))
    __trace__.add_inputs(___757.inputs)
    __stack__.push()
    __stack__.add(___757.inputs, bot=True)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___757.inputs)
    evs.adopt(__stack__.all())
    evs.adopt(___757.inputs)
    usrs.adopt(__stack__.all())
    usrs.adopt(___757.inputs)
    meetup.add_sql_inputs('locations', __stack__.all())
    meetup.add_sql_inputs('locations', ___757.inputs)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___757.inputs)
    if ___757:
        ___759 = error_html(Cell('Illegal operation: Delete location'))[0]
        ___760 = locations()[0]
        __r__ = Cell((___759 + ___760), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___761 = meetup.sql(Cell('DELETE FROM locations WHERE id = ?0'), Cell([location]))
        ___762 = meetup.sql(Cell('SELECT * FROM events WHERE location = ?0'), Cell([id_]))
        evs = ___762
        evs.adopt(__stack__.all())
        ___763 = evs
        for ev in ___763:
            ___765 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev[Cell(0)]]))
            ___766 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([ev[Cell(1)], ev[Cell(2)], Cell(0), ev[Cell(3)], ev[Cell(4)], ev[Cell(5)]]))
        ___767 = meetup.sql(Cell('SELECT * FROM users WHERE address = ?0'), Cell([id_]))
        usrs = ___767
        usrs.adopt(__stack__.all())
        ___768 = usrs
        for usr in ___768:
            ___770 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([usr[Cell(0)]]))
            ___771 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([usr[Cell(1)], usr[Cell(2)], usr[Cell(3)], Cell(0)]))
        ___772 = locations()[0]
        __r__ = Cell(___772, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/delete_location/<int:location>')
def _delete_location(location):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    location = meetup.register('delete_location', 'location', location)
    (__r__, __s__) = delete_location(location)
    return (__c__, __r__, __s__, __trace__)

def add_location():
    global __stack__
    if ('country' not in locals()):
        country = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___773 = meetup.get('add_location', Cell('country'))
    country = ___773
    country.adopt(__stack__.all())
    ___774 = meetup.get('add_location', Cell('name'))
    name = ___774
    name.adopt(__stack__.all())
    ___775 = meetup.me()
    ___776 = meetup.sql(Cell('INSERT INTO locations (country, name, owner) VALUES (?0, ?1, ?2)'), Cell([country, name, ___775]))
    ___777 = locations()[0]
    __r__ = Cell(___777, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/add_location')
def _add_location():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = add_location()
    return (__c__, __r__, __s__, __trace__)

def update_location(location):
    global __stack__
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('country' not in locals()):
        country = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('location_' not in locals()):
        location_ = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    ___778 = meetup.sql(Cell('SELECT * FROM locations WHERE id = ?0'), Cell([location]))
    location_ = ___778[Cell(0)]
    location_.adopt(__stack__.all())
    ___779 = db_str(location_[Cell(0)])
    id_ = ___779
    id_.adopt(__stack__.all())
    country = location_[Cell(1)]
    country.adopt(__stack__.all())
    name = location_[Cell(2)]
    name.adopt(__stack__.all())
    owner = location_[Cell(3)]
    owner.adopt(__stack__.all())
    __k__ = [Cell('id_'), Cell('country'), Cell('name'), Cell('owner')]
    ___780 = meetup.render(Cell('update_location.html'), Cell({
        __k__[0].value: id_,
        __k__[1].value: country,
        __k__[2].value: name,
        __k__[3].value: owner,
    }, inputs=[__k__[0].inputs, __k__[1].inputs, __k__[2].inputs, __k__[3].inputs]))
    __r__ = Cell(___780, adopt=__stack__.all())
    __s__ = __stack__.all()
    return (__r__, __s__)

@meetup.route('/update_location/<int:location>')
def _update_location(location):
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    location = meetup.register('update_location', 'location', location)
    (__r__, __s__) = update_location(location)
    return (__c__, __r__, __s__, __trace__)

def update_location_do():
    global __stack__
    if ('country' not in locals()):
        country = Cell(None)
    if ('usrs' not in locals()):
        usrs = Cell(None)
    if ('id_' not in locals()):
        id_ = Cell(None)
    if ('evs' not in locals()):
        evs = Cell(None)
    if ('ev' not in locals()):
        ev = Cell(None)
    if ('name' not in locals()):
        name = Cell(None)
    if ('owner' not in locals()):
        owner = Cell(None)
    if ('usr' not in locals()):
        usr = Cell(None)
    if ('new_id' not in locals()):
        new_id = Cell(None)
    ___781 = meetup.get('update_location_do', Cell('id_'))
    ___782 = db_int(___781)
    id_ = ___782
    id_.adopt(__stack__.all())
    ___783 = meetup.sql(Cell('SELECT owner FROM locations WHERE id = ?0'), Cell([id_]))
    owner = ___783[Cell(0)][Cell(0)]
    owner.adopt(__stack__.all())
    ___784 = meetup.me()
    ___785 = Cell((owner != ___784))
    __trace__.add_inputs(___785.inputs)
    __stack__.push()
    __stack__.add(___785.inputs, bot=True)
    meetup.add_sql_inputs('events', __stack__.all())
    meetup.add_sql_inputs('events', ___785.inputs)
    evs.adopt(__stack__.all())
    evs.adopt(___785.inputs)
    name.adopt(__stack__.all())
    name.adopt(___785.inputs)
    new_id.adopt(__stack__.all())
    new_id.adopt(___785.inputs)
    country.adopt(__stack__.all())
    country.adopt(___785.inputs)
    meetup.add_sql_inputs('locations', __stack__.all())
    meetup.add_sql_inputs('locations', ___785.inputs)
    usrs.adopt(__stack__.all())
    usrs.adopt(___785.inputs)
    meetup.add_sql_inputs('users', __stack__.all())
    meetup.add_sql_inputs('users', ___785.inputs)
    if ___785:
        ___787 = error_html(Cell('Illegal operation: Update location'))[0]
        ___788 = locations()[0]
        __r__ = Cell((___787 + ___788), adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    else:
        ___789 = meetup.get('update_location_do', Cell('country'))
        country = ___789
        country.adopt(__stack__.all())
        ___790 = meetup.get('update_location_do', Cell('name'))
        name = ___790
        name.adopt(__stack__.all())
        ___791 = meetup.sql(Cell('DELETE FROM locations WHERE id = ?0'), Cell([id_]))
        ___792 = meetup.sql(Cell('INSERT INTO locations (country, name) VALUES (?0, ?1)'), Cell([country, name]))
        new_id = ___792[Cell(0)]
        new_id.adopt(__stack__.all())
        ___793 = meetup.sql(Cell('SELECT * FROM events WHERE location = ?0'), Cell([id_]))
        evs = ___793
        evs.adopt(__stack__.all())
        ___794 = evs
        for ev in ___794:
            ___796 = meetup.sql(Cell('DELETE FROM events WHERE id = ?0'), Cell([ev[Cell(0)]]))
            ___797 = meetup.sql(Cell('INSERT INTO events (title, description, location, time, date, owner) VALUES (?0, ?1, ?2, ?3, ?4, ?5)'), Cell([ev[Cell(1)], ev[Cell(2)], new_id, ev[Cell(3)], ev[Cell(4)], ev[Cell(5)]]))
        ___798 = meetup.sql(Cell('SELECT * FROM users WHERE address = ?0'), Cell([id_]))
        usrs = ___798
        usrs.adopt(__stack__.all())
        ___799 = usrs
        for usr in ___799:
            ___801 = meetup.sql(Cell('DELETE FROM users WHERE id = ?0'), Cell([usr[Cell(0)]]))
            ___802 = meetup.sql(Cell('INSERT INTO users (user_id, name, level, address) VALUES (?0, ?1, ?2, ?3)'), Cell([usr[Cell(1)], usr[Cell(2)], usr[Cell(3)], new_id]))
        ___803 = locations()[0]
        __r__ = Cell(___803, adopt=__stack__.all())
        __s__ = __stack__.all()
        __stack__.pop()
        return (__r__, __s__)
    __stack__.pop()
    __s__ = __stack__.all()
    return (Cell(None), __s__, [], [])

@meetup.route('/update_location_do')
def _update_location_do():
    global __stack__, __trace__, __c__
    __stack__ = Stack()
    __trace__ = Cell(None)
    __c__ = Counter()
    (__r__, __s__) = update_location_do()
    return (__c__, __r__, __s__, __trace__)
