from flask import session
import sqlite3
from threading import RLock
from subprocess import Popen, PIPE, STDOUT
from settings import *
from databank.memory import Cell, Dag
#from databank.timing import timing
from databank.timing import Timer
import sqlite3
import os
import sys
import re
import time
import signal
import uuid

# Printing function

def _print_monitor(s, end='\n'):
    if is_set('verbose_monitor'):
        print(s, end=end)

# (v, u) -> "v:u"

def _str_of_label(w):
    return str(w[0]) + ":" + str(w[1])

# "v:u" -> (v, u)

def _label_of_str(s):
    s = s.split(":")
    return s[0], int(s[1])
    
# Predicates

class In:

    sig = "In(string,string,string)"
    
    def __init__(self, f, a, ut):
        self.f = f
        self.a = a
        self.ut = ut

    def __str__(self):
        return f"In(\"{self.f}\", {self.a}, \"{self.ut}\")"

class Out:

    sig = "Out(string,string,string)-"
    
    def __init__(self, u, p, q):
        self.u = u
        self.p = p
        self.q = q

    def __str__(self):
        return f"Out(\"{self.u}\", {self.p}, {self.q})"

class Itf:

    sig = "Itf(string,string)"
    
    def __init__(self, ut, q):
        self.ut = ut
        self.q = q

    def __str__(self):
        return f"Itf(\"{self.ut}\", {self.q})"

# Single-user monitor

class Monitor:
    
    def __init__(self, user_id, policy):
        self.user_id = user_id
        self.policy = policy
        self.policy_fn = POLICY.format(user_id)
        with open(self.policy_fn, "w") as f:
            f.write(policy)
        self.state_fn = STATE.format(user_id)
        self.sig_fn = SIG
        if os.path.isfile(self.state_fn):
            self.resume()
        else:
            cmd = [MONPOLY, "-formula", self.policy_fn, "-sig", self.sig_fn, "-enforce", "-nofilterrel"]
            self.proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True, preexec_fn=os.setpgrp)

    def tp(self):
        return int(time.time())

    def stop(self):
        print(f"Trying to kill {self.user_id}...", end=" ")
        self.save()
        self.proc.kill()
        print("ok!")

    ROK = re.compile(r"^\[Enforcer\] OK.")
    RSUPPRESS = re.compile(r"^\[Enforcer\] Suppress")
        
    def log(self, preds, check=False):
        if is_set("semipassive"):
            return {}
        pred_string = " ".join(list(map(str, preds)))
        req = f"@{self.tp()} {pred_string}{'?' if check else ';'}"
        _print_monitor(f"{self.user_id}→ {req}")
        self.proc.stdin.write(req)
        self.proc.stdin.flush()
        with Timer('../evaluation/time.json', 'monitoring'):
            out = self.proc.stdout.readline()
        _print_monitor(f"{self.user_id}← {out}", end="")
        if Monitor.ROK.match(out):
            return True
        elif Monitor.RSUPPRESS.match(out):
            out = self.proc.stdout.readline()
            _print_monitor(f"{self.user_id}← {out}", end="")
            assert(Monitor.ROK.match(out))
            return False
        else:
            assert(False)

    def save(self):
        req = f"> save_state {self.state_fn} <"
        _print_monitor(f"{self.user_id}→ {req}")
        self.proc.stdin.write(req)
        self.proc.stdin.flush()
        _print_monitor(f"{self.user_id}← {self.proc.stdout.readline()}", end="")

    def resume(self):
        cmd = [MONPOLY, "-formula", self.policy_fn, "-sig", self.sig_fn,
               "-load", self.state_fn, "-enforce", "-nofilterrel"]
        _print_monitor(f"{self.user_id}→ {' '.join(cmd)}")
        self.proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True, preexec_fn=os.setpgrp)
        out = self.proc.stdout.readline()
        _print_monitor(f"{self.user_id}← {out}", end="")


# Load monitors

monitors = {}

_db  = sqlite3.connect(FUNDAMENTAL)
_cur = _db.execute("SELECT id, policy FROM users")
_res = _cur.fetchall()
_cur.close()
_db.close()

for _r in _res:
    monitors[_r[0]] = Monitor(*_r)

with open(SIG, 'w') as f:
    f.write(In.sig + "\n")
    f.write(Itf.sig + "\n")
    f.write(Out.sig + "\n")

_monitor_files = [f for f in os.listdir(MONITOR_DIR) \
                  if f.split('-')[0] == "state" and int(f.split('-')[1]) not in monitors]
for _monitor_file in _monitor_files:
    os.remove(os.path.join(MONITOR_DIR, _monitor_file))

# Handle SIGINT

def save_all(signum, sigframe):
    for monitor in monitors.values():
        monitor.stop()
    os._exit(0)

signal.signal(signal.SIGINT, save_all)

# Monitor control functions (used by applications)

general_lock = RLock()

def _filter_labels(labels, id):
    return {l: u for (l, u) in labels.items() if u == id}

class Stop(Exception):
    pass

#@timing('../evaluation/time.json', 'monitoring')
def logreturn(u, i, p, d, t, check=False, auto=False):
    if auto:
        lock()
    out_u = str(u.value)
    out_i = i.next(inc=not check)
    res = Cell(True)
    if check:
        inputs_in_order = d.inputs_in_order()
    else:
        inputs_in_order = [d.collect()]
    for uts in inputs_in_order:
        uts_by_user = {}
        for (ut, user) in uts:
            if user not in uts_by_user:
                uts_by_user[user] = set()
            uts_by_user[user].add(ut)
        for (user, user_uts) in uts_by_user.items():
            monitor = monitors[user]
            preds = [Itf(ut, out_i) for ut in user_uts] + [Out(out_u, p, out_i)]
            b = monitor.log(preds, check=check)
            if not b:
                t.add_inputs(res.inputs)
                if auto:
                    commit()
                return Cell(False, inputs=res.inputs)
            else:
                res.inputs = res.inputs / Dag(singletons=[(ut, user) for ut in user_uts])
    if auto:
        commit()
    if not check:
        t.add_inputs(res.inputs)
    return Cell(True, inputs=t.inputs)

#@timing('../evaluation/time.json', 'monitoring')
def loginput(f, a, u, ts, v, auto=False):
    if auto:
        lock()
    ut = str(uuid.uuid4())
    monitors[u].log([In(f, a, ut)])
    if auto:
        commit()
    return Cell(v, inputs=Dag(singleton=(ut, u)))

# Testing only!
def loginput_backdoor(f, a, u):
    ut = str(uuid.uuid4())
    monitors[u].log([In(f, a, ut)])
    return ut

def lock():
    #for monitor in monitors.values():
    #monitor.save()
    general_lock.acquire()

def commit():
    general_lock.release()
