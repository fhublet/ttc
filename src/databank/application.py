from flask import Flask, request, session, render_template, url_for, redirect
from werkzeug.utils import secure_filename
import sqlite3
import datetime
import socket
import pickle
import time
from threading import Thread, get_ident, RLock
from copy import deepcopy
from Crypto.Cipher import AES
import os.path
import random
import string
import re

from databank.monitor import logreturn, loginput, lock, commit, Stop
from databank.memory import Cell, load, Dag
from databank.calls import Server, do_call, join_route, pad
#from databank.timing import timing
import databank.sql as sql
from databank.cfg import InputDependent
from settings import get_db, file_save, file_remove, file_exists, FUNDAMENTAL, is_set

class Cur:
    def __init__(self, db, lock, select_only=False):
        self.db = db
        self.lock = lock
        self.select_only = select_only
    def __enter__(self):
        if not self.select_only:
            self.lock.acquire()
        self.cur = self.db.cursor()
        return self.cur
    def __exit__(self, type, value, traceback):
        self.cur.close()
        if not self.select_only:
            self.db.commit()
            self.lock.release()

class Redirect:
    def __init__(self, purp, rule):
        self.purp = purp
        self.rule = rule

class Template:
    def __init__(self, template, args):
        self.template = template
        self.args     = args
            
## Application wrapper
            
class Application:

    def __init__(self, flask, server, id_, app_name, sp_IP, database, key, databank_side=True):
        self.flask = flask
        self.id_ = id_
        self.app_name = app_name
        if databank_side:
            self.server = server
        else:
            self.server = Server(databank_side=False)            
        self.server.register_app(id_, sp_IP, key)
        self.database = database
        self.aes = AES.new(bytes(key, 'utf8'), AES.MODE_EAX)
        self.databank_side = databank_side
        self.db = sqlite3.connect(database, check_same_thread=False)
        self.db.cursor().execute("PRAGMA synchronous = OFF")
        self.lock = RLock()
        self.sql_asts = {}

    def __del__(self):
        self.db.close()

    # Function decorators

    def callback(self, rule):
        def inner(f):
            def wrapper(*args, **kw):
                x = f(*args, **kw)
                return x.untaint()
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            self.server.register_callback(self.id_, rule, self, wrapper)
            return wrapper
        return inner
    
    def route(self, rule, **kwargs):
        def inner(f):
            def wrapper(*args, **kw):
                i, ret, stack, trace = f(*args, **kw)
                ret.adopt(stack)
                page = None
                if isinstance(ret.value, Redirect):
                    verdict = logreturn(self.me(), i, ret.value.purp, ret.inputs, trace, auto=True)
                    if not verdict.value:
                        page = render_template("violation.html")
                    else:
                        page = redirect(join_route(str(self.id_), ret.value.rule))
                elif isinstance(ret.value, Template):
                    final_args = {}
                    for arg_name, (purp, val, uts) in ret.value.args.items():
                        verdict = logreturn(self.me(), i, purp, uts, trace, auto=True)
                        if not verdict.value:
                            final_args[arg_name] = "<strong>Content suppressed to avoid a privacy violation.</strong>"
                        else:
                            final_args[arg_name] = val
                    page = render_template(os.path.join("collection", self.app_name, ret.value.template), **final_args)
                else:
                    if type(ret.value) is tuple and len(ret.value) == 2:
                        purp = ret.value[0].untaint_value()
                        val  = ret.value[1].untaint_value()
                        uts  = ret.inputs / ret.value.inputs / ret.value[0].inputs / ret.value[1].inputs
                        verdict = logreturn(self.me(), i, purp, uts, trace, auto=True)
                        if not verdict.value:
                            page = render_template("violation.html", **locals())
                        else:
                            page = val
                return page
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            return self.flask.route(join_route(self.id_, rule), **kwargs)(wrapper)
        return inner

    # Distant call

    def send(self, i, t, a, p, arg):
        verdict = logreturn(a, i, p.value, arg.all_inputs(), t, auto=True)
        if verdict.value:
            print(f"Sent {arg.value} to {a}!")

    # Check with monitor

    def check(self, i, t, p, arg, a=None):
        a = a or self.me()
        u=time.time()
        r = logreturn(a, i, p.value, arg.all_inputs(), t, check=True, auto=True)
        return r

    # Exposed user input methods

    def method(self):
        return Cell(request.method)

    def _generate_filename(self, file_):
        return ''.join(random.choices(string.ascii_letters, k=16)) + "_" + secure_filename(file_.filename)

    def post(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty():
            raise InputDependent("POST key")
        if key.value in request.files:
            file_ = request.files.get(key.value)
            value = self._generate_filename(file_)
            file_save(value, file_)
        else:
            value = request.form.get(key.value, None)
        return self.register(func_name, key.value, value)

    def getlist(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty(): # TODO: do this statically
            raise InputDependent("POST key")
        values = request.form.getlist(key.value, None)
        for value in range(len(values)):
            values[value] = self.register(func_name, key.value, values[value])
        return Cell(values)

    def get(self, func_name, key):
        if isinstance(key, Cell) and not key.inputs.is_empty():
            raise InputDependent("GET key")
        value = request.args.get(key.value, None)
        return self.register(func_name, key.value, value)

    def link(self, value):
        if type(value.value) is str and file_exists(value.value):
            return Cell(url_for('file_link', filename=value.value), inputs=value.inputs)
        return Cell(None, inputs=value.inputs)

    def remove(self, value):
        if type(value.value) is str and file_exists(value.value):
            file_remove(value.value)
        return Cell(None)

    # Session

    def get_session(self, key):
        if isinstance(key, Cell):
            key = key.value
        if key in session and key[:4] != "user":
            return load(session[key])
        else:
            return Cell(None)

    def pop_session(self, key):
        if isinstance(key, Cell):
            key = key.value
        if key in session and key[:4] != "user":
            session.pop(key, None)
        return Cell(None)

    def set_session(self, key, value):
        if isinstance(key, Cell):
            key = key.value
        session[key] = value.dump()
        return Cell(None)

    def add_session_inputs(self, key, inputs):
        if key in session:
            cell = load(session[key])
            cell.add_inputs(inputs)
            session[key] = cell.dump()
        else:
            session[key] = Cell(None, inputs=inputs).dump()
        return Cell(None)

    # extension: COOKIES

    def _me(self):
        try:
            return session['user_id']
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_id
    
    def me(self):
        value = self._me()
        return Cell(value)#, inputs={value: value})

    def name(self):
        try:
            value = session['user_name']
            return Cell(value)#, inputs={value: self._me()})
        except RuntimeError:
            # no active HTTP request → callback
            return Cell(self.callback_user_name)#, inputs={value: self._me()})

    # extension: access name and other personal information

    def now(self):
        return int(time.time())

    def now_utc(self):
        return datetime.datetime.utcfromtimestamp(self.now()).strftime('%Y-%m-%d @ %H:%M')
            
    # Database operations

    def db_cur(self, select_only=False):
        return Cur(self.db, self.lock, select_only=select_only)

    def _print_db(self, s):
        if is_set('verbose_db'):
            print(s)

    #@timing('../evaluation/time.json', 'database')
    def sql(self, query, args=Cell(None), stack=None):
        #print(args)
        if query.value in self.sql_asts:
            ast, where_vars, tables = deepcopy(self.sql_asts[query.value])
        else:
            ast = sql.parse(query.value)
            ast.table = f"{self.id_}_{ast.table}"
            if isinstance(ast, sql.Select):
                # read variables in WHERE clause
                where_vars = set()
                for join in ast.joins:
                    join.table = f"{self.id_}_{join.table}"
                    where_vars |= join.get_vars()
                if ast.where is not None:
                    where_vars |= ast.where.get_vars()
                if ast.order_by is not None:
                    where_vars |= ast.order_by.get_vars()
                # list used tables
                tables = {ast.table} | {join.table for join in ast.joins}
            else:
                where_vars, tables = set(), set()
            self.sql_asts[query.value] = deepcopy(ast), where_vars, tables
        if args is not None:
            ast.set_params(args)
        args_cell = Cell(None, inputs=args.all_inputs())
        if isinstance(ast, sql.Select):
            # retrieve results
            with self.db_cur(select_only=True) as cur:
                self._print_db(">> " + str(ast))
                cur = cur.execute(str(ast))
                results = cur.fetchall()
                self._print_db("<< " + repr(results))
                # retrieve ids
                ast_ids = deepcopy(ast)
                ast_ids.result_columns = sql.ResultColumns(
                    columns=[sql.Col(table=table, name="id") for table in tables])
                self._print_db(">> " + str(ast_ids))
                cur = cur.execute(str(ast_ids))
                indices = cur.fetchall()
                self._print_db("<< " + repr(indices))
                idx = {}
                inputs = {}
                # for each used table
                for t, table in enumerate(tables):
                    ids = [i[t] for i in indices if i[t] is not None]
                    # fill in index
                    idx[table] = {}
                    for j, id_ in enumerate(ids):
                        if id_ not in idx[table]:
                            idx[table][id_] = []
                        idx[table][id_].append(j)
                    # identify relevant vars
                    rel_vars = list(filter(lambda x: "." not in x or x.split('.')[0] == f'"{table}"',
                                                where_vars))
                    rel_vars = [var.split('.')[1] if '.' in var else var for var in rel_vars]
                    # retrieve table-level inputs
                    table_inputs_q = ("SELECT i.inputs FROM \"{}_inputs_\" AS i "
                                      "WHERE i.entry IS NULL").format(table)
                    self._print_db(">> " + table_inputs_q)
                    cur = cur.execute(table_inputs_q)
                    inputs[table] = Dag()
                    for i in cur.fetchall():
                        inputs[table] = inputs[table] + Dag(json=i[0])
                    # retrieve field-level inputs (from WHERE clauses)
                    field_inputs_q = ("SELECT i.inputs FROM \"{}_inputs_\" AS i "
                                      "WHERE i.field IN ({})") \
                                      .format(table,
                                              ", ".join(map(lambda x: "'{}'".format(x), rel_vars)))
                    self._print_db(">> " + field_inputs_q)
                    cur = cur.execute(field_inputs_q)
                    inputs2 = Dag()
                    for i in cur.fetchall():
                        inputs2 = inputs2 + Dag(json=i[0])
                    inputs[table] = inputs[table] / inputs2
                    # retrieve entry-level inputs (from entries)
                    entry_inputs_q = ("SELECT i.entry, i.field, i.inputs FROM \"{}_inputs_\" AS i "
                                      "WHERE i.entry IN ({})") \
                                      .format(table, ", ".join(map(str, list(set(ids)))))
                    self._print_db(">> " + entry_inputs_q)
                    cur = cur.execute(entry_inputs_q)
                    for i in cur.fetchall():
                        key = (table, i[0], i[1])
                        if key not in inputs:
                            inputs[key] = Dag()
                        inputs[key] = inputs[key] + Dag(json=i[2])
                    # show all inputs retrieved
                    self._print_db("<< " + repr(inputs))
                if ast.result_columns.type_ in ["star", "starred"]:
                    star_table = ast.table if ast.result_columns.type_ == "star" \
                        else str(ast.result_columns).split('.')[0].replace('"', '')
                    # read list of columns
                    col_q = "PRAGMA table_info(\"{}\")".format(star_table)
                    self._print_db(">> " + col_q)
                    cur = cur.execute(col_q)
                    columns = [n[1] for n in cur.fetchall()]
                    self._print_db("<< " + repr(columns))
                    col_idx = {star_table + '.' + col: i for (i, col) in enumerate(columns)}
                elif ast.result_columns.type_ == "countstar":
                    col_idx = {}
                else:
                    col_idx = {str(col): i for (i, col) in enumerate(ast.result_columns.columns)}
            # build answer
            cell = Cell([Cell(result) for result in results], inputs=args_cell.inputs)
            # add inputs
            for (key, the_inputs) in inputs.items():
                # add table-level inputs and inputs from columns in WHERE clause
                if type(key) is str:
                    cell.inputs = cell.inputs + the_inputs
                else:
                    table, entry, field = key
                    #print(table, entry, field, idx[table], col_idx)
                    if field is None and entry in idx and idx[entry] in cell.value:
                        # add row-level inputs
                        for j in idx[table][entry]:
                            cell.value[j].inputs += the_inputs
                    elif field is not None and entry in idx[table] \
                         and (f'"{table}".{field}' in col_idx or field in col_idx):
                        # add field-level inputs
                        for j in idx[table][entry]:
                            if field in col_idx:
                                cell.value[j].value[col_idx[field]].inputs += the_inputs
                            else:
                                cell.value[j].value[col_idx[f'"{table}".{field}']].inputs += the_inputs
            return cell
        elif isinstance(ast, sql.Insert):
            # insert entries
            # requires a trigger
            with self.db_cur() as cur:
                # delete content of new table (to avoid bugs)
                delete_new_q = "DELETE FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + delete_new_q)
                cur = cur.execute(delete_new_q)
                # execute insert query
                self._print_db(">> " + str(ast))
                cur = cur.execute(str(ast))
                # read new entries' ids
                new_q = "SELECT * FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + new_q)
                cur = cur.execute(new_q)
                entry_ids = [n[0] for n in cur.fetchall()]
                self._print_db("<< " + repr(entry_ids))
                # read list of columns
                col_q = "PRAGMA table_info(\"{}\")".format(ast.table)
                self._print_db(">> " + col_q)
                cur = cur.execute(col_q)
                columns = [n[1] for n in cur.fetchall()]
                self._print_db("<< " + repr(columns))
                # retrieve new entries
                new2_q = "SELECT * FROM \"{}\" WHERE id IN ({})".format(ast.table, ", ".join(map(str, entry_ids)))
                self._print_db(">> " + new2_q)
                cur = cur.execute(new2_q)
                entries = cur.fetchall()
                self._print_db("<< " + str(entries))
                # put inserted entries into memory cell
                ins_cells = {entry[0]: Cell({k: v for (k, v) in zip(columns[1:], entry[1:])},
                                            inputs=args.inputs)
                             for entry in entries}
                # set inputs
                if ast.column_list:
                    column_list = ast.column_list.columns
                else:
                    column_list = columns
                for entry in entries:
                    for (value, column) in zip(ast.values.values, column_list):
                        ins_cells[entry[0]].value[column].inputs += value.get_inputs()
                log_cell = Cell(None, inputs=args.inputs)
                # retrieve table-level inputs
                table_inputs_old_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                    .format(ast.table)
                old_inputs = cur.execute(table_inputs_old_q).fetchall()
                if old_inputs:
                    old_inputs = Dag(json=old_inputs[0][0])
                else:
                    old_inputs = Dag()
                # insert new table-level inputs (from stack)
                if stack:
                    new_inputs = stack.all()
                    if not new_inputs.is_empty():
                        table_inputs_old_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                            .format(ast.table)
                        old_inputs = cur.execute(table_inputs_old_q).fetchall()
                        if old_inputs:
                            new_inputs = old_inputs / new_inputs
                            table_inputs_q = "UPDATE \"{}_inputs_\" SET inputs = '{}' WHERE entry IS NULL AND field IS NULL" \
                                .format(ast.table, new_inputs.to_json())
                        else:
                            table_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs) VALUES (NULL, NULL, '{}')" \
                                .format(ast.table, new_inputs.to_json())
                        self._print_db(">> " + table_inputs_q)
                        cur = cur.execute(table_inputs_q)
                else:
                    new_inputs = old_inputs
                # for each new entry
                for (i, ins_cell) in ins_cells.items():
                    # insert row-level inputs
                    if not ins_cell.inputs.is_empty():
                        row_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs) VALUES ({}, NULL, '{}')" \
                            .format(ast.table, i, (ins_cell.inputs - new_inputs).to_json())
                        self._print_db(">> " + row_inputs_q)
                        cur = cur.execute(row_inputs_q)
                    # for each field
                    for (field, value) in ins_cell.value.items():
                        # insert field-level inputs
                        if not value.inputs.is_empty():
                            field_inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs) VALUES ({}, '{}', '{}')" \
                                .format(ast.table, i, field,
                                        ((value.inputs - ins_cell.inputs) - new_inputs).to_json())
                            self._print_db(">> " + field_inputs_q)
                            cur = cur.execute(field_inputs_q)
                delete_new_q = "DELETE FROM \"{}_new_\"".format(ast.table)
                self._print_db(">> " + delete_new_q)
                cur = cur.execute(delete_new_q)
                return Cell(entry_ids, inputs=log_cell.inputs)
        elif isinstance(ast, sql.Delete):
            with self.db_cur() as cur:
                # select ids to be deleted
                ast_ids = sql.Select(result_columns=sql.ResultColumns(
                    type_="list", columns=[sql.Var(name="id")]),
                                     table=ast.table,
                                     where=ast.where)
                self._print_db(">> " + str(ast_ids))
                cur = cur.execute(str(ast_ids))
                ids = [i[0] for i in cur.fetchall()]
                self._print_db("<< " + repr(ids))
                # select inputs to be deleted
                inputs_ids_q = "SELECT id FROM \"{}_inputs_\" WHERE entry IN ({})" \
                               .format(ast.table, ", ".join(map(str, ids)))
                self._print_db(">> " + inputs_ids_q)
                cur = cur.execute(inputs_ids_q)
                inputs_ids = [i[0] for i in cur.fetchall()]
                self._print_db("<< " + repr(inputs_ids))
                log_cell = Cell(None, inputs=args.all_inputs())
                # delete everything
                delete_q = "DELETE FROM \"{}\" WHERE id in ({})" \
                           .format(ast.table, ", ".join(map(str, ids)))
                self._print_db(">> " + delete_q)
                cur = cur.execute(delete_q)
                delete_inputs_q = "DELETE FROM \"{}_inputs_\" WHERE id in ({})" \
                                  .format(ast.table, ", ".join(map(str, inputs_ids)))
                self._print_db(">> " + delete_inputs_q)
                cur = cur.execute(delete_inputs_q)
                return Cell(ids, inputs=log_cell.all_inputs())
        else:
            assert(False)

    def add_sql_inputs(self, table, inputs):
        if not inputs:
            return
        table = f"{self.id_}_{table}"
        # insert input
        if not inputs.is_empty():
            with self.db_cur() as cur:
                old_inputs_q = "SELECT inputs FROM \"{}_inputs_\" WHERE entry IS NULL AND field IS NULL" \
                    .format(table)
                old_inputs = cur.execute(old_inputs_q).fetchall()
                if old_inputs:
                    inputs = Dag(json=old_inputs[0][0]) / inputs
                    inputs_q = "UPDATE \"{}_inputs_\" SET inputs = '{}' WHERE entry IS NULL AND field IS NULL" \
                        .format(table, inputs.to_json())
                else:
                    inputs_q = "INSERT INTO \"{}_inputs_\" (entry, field, inputs) VALUES (NULL, NULL, '{}')" \
                        .format(table, inputs.to_json())
                self._print_db(">> " + inputs_q)
                cur.execute(inputs_q)  
                
    # Register inputs
    
    def register(self, func_name, key, value):
        return loginput(str(self.id_) + "/" + func_name, key, self._me(), self.now(), value, auto=True)

    # Template operations

    def _parse_render_args(self, the_args):
        args = {}
        if type(the_args.value) is dict:
            for arg, v in the_args.value.items():
                if type(v.value) is tuple:
                    if len(v.value) == 2:
                        args[arg] = (v.value[0].untaint_value(),
                                     v.value[1].untaint(),
                                     the_args.inputs / v.inputs / v.value[0].inputs / v.value[1].all_inputs())
        return args

    def render(self, template, args=Cell({})):
        return Cell(Template(template.value, self._parse_render_args(args)))

    ## Redirect

    def redirect(self, purp, rule):
        return Cell(Redirect(purp.value, rule.value), inputs=purp.inputs / rule.inputs)

    ## Flash

    def flash(self, msg):
        return self.set_session('flash', msg)

    def unflash(self):
        c = self.get_session('flash')
        self.pop_session('flash')
        return c
    
## Passive application wrapper
            
class PassiveApplication:

    def __init__(self, flask, server, id_, app_name, sp_IP, database, key, databank_side=True):
        self.flask = flask
        self.id_ = id_
        self.app_name = app_name
        if databank_side:
            self.server = server
        else:
            self.server = Server(databank_side=False)            
        self.server.register_app(id_, sp_IP, key)
        self.database = database
        self.aes = AES.new(bytes(key, 'utf8'), AES.MODE_EAX)
        self.databank_side = databank_side
        self.db = sqlite3.connect(database, check_same_thread=False)
        self.db.cursor().execute("PRAGMA synchronous = OFF")
        self.lock = RLock()
        self.sql_asts = {}

    # Function decorators

    def callback(self, rule):
        def inner(f):
            def wrapper(*args, **kw):
                x = f(*args, **kw)
                return x
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            self.server.register_callback(self.id_, rule, self, wrapper)
            return wrapper
        return inner
    
    def route(self, rule, **kwargs):
        def inner(f):
            def wrapper(*args, **kw):
                return f(*args, **kw)
            wrapper.__name__ = str(self.id_) + "__" + f.__name__
            return self.flask.route(join_route(self.id_, rule), **kwargs)(wrapper)
        return inner

    # Distant call

    def call(self, addr, function, *args, stack=None, **kwargs):
        dump = pickle.dumps((function, args.untaint(), kwargs.untaint()))
        msg = self.aes.encrypt(pad(dump))
        ret = do_call(msg, addr, databank_side=self.databank_side) 
        data = pickle.loads(self.aes.decrypt(ret).strip())
        return data

    # Check with monitor

    def check(self, p, arg):
        return True

    # Exposed user input methods

    def method(self):
        return request.method
    
    def post(self, key):
        if key in request.files:
            file_ = request.files.get(key)
            value = self._generate_filename(file_)
            file_Save(value, file_)
        else:
            value = request.form.get(key, None)
        return value

    def get(self, key):
        return request.args.get(key, None)

    def link(self, value):
        if type(value) is str and file_exists(value):
            return url_for('file_link', filename=value)
        return None

    def remove(self, value):
        if type(value) is str and file_exists(value):
            file_remove(value)
        return None

    # Session

    def get_session(self, key):
        if key in session and key[:4] != "user":
            cell = session[key]
            return Cell(json=cell)
        else:
            return None

    def pop_session(self, key):
        if key in session and key[:4] != "user":
            session.pop(key, None)
        return None

    def set_session(self, key, value):
        session[key] = value.dump()
        return None

    # extension: COOKIES

    def _me(self):
        try:
            return session['user_id']
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_id
    
    def me(self):
        value = self._me()
        return value

    def name(self):
        try:
            value = session['user_name']
            return value
        except RuntimeError:
            # no active HTTP request → callback
            return self.callback_user_name

    # extension: access name and other personal information

    def now(self):
        return int(time.time())

    def now_utc(self):
        return datetime.utcfromtimestamp(self.now()).strftime('%Y-%m-%d @ %H:%M')
            
    # Database operations

    def db_cur(self, select_only=False):
        return Cur(self.db, self.lock, select_only=select_only)

    def _print_db(self, s):
        if is_set('verbose_db'):
            print(s)

    #@timing('../evaluation/time.json', 'database')
    def sql(self, query, args=None, stack=None, assigned=None):
        if query in self.sql_asts:
            ast, select_only = deepcopy(self.sql_asts[query])
        else:
            ast = sql.parse(query)
            ast.table = f"{self.id_}_{ast.table}"
            if isinstance(ast, sql.Select):
                for join in ast.joins:
                    join.table = f"{self.id_}_{join.table}"
                select_only = True
            else:
                select_only = False
            self.sql_asts[query] = deepcopy(ast), select_only
        t2 = time.time()
        if args is not None:
            ast.set_params(args)
        with self.db_cur(select_only=select_only) as cur:
            self._print_db(">> " + str(ast))
            cur = cur.execute(str(ast))
            results = cur.fetchall()
            self._print_db("<< " + repr(results))
        t3 = time.time()
        return results

    # Template operations
    
    def render(self, template, args):
        return render_template(template, **args)

    # Redirect
    
    def redirect(self, purp, rule):
        return redirect(rule)

    ## Flash

    def flash(self, msg):
        return self.set_session('flash', msg)

    def unflash(self):
        return self.get_session('flash')

# TODO: remove files when they are no longer needed
