theory TTC
  imports Main TaggedValues Traces TTCWhile 
begin

section \<open>TTC programs and states\<close>

record 'a prog_f =
  Code :: 'a
  Out_y :: var
  Out_u :: user
  Out_p :: prp
  Time :: ts

type_synonym 'a prog = "fn \<Rightarrow> 'a prog_f list"

datatype step_type = Taint | Track | Control

datatype ('a, 'b) S\<^sub>I = InterpNull | Interp 'a 'b usr checker
datatype S\<^sub>W = Taint | Track usr fn ts nat | Control usr fn ts nat 
type_synonym S\<^sub>E = "if_event trace"
type_synonym S\<^sub>M = mem

type_synonym ('a, 'b) S = "S\<^sub>W \<times> ('a, 'b) S\<^sub>I \<times> S\<^sub>E \<times> S\<^sub>M"

datatype Time' = Taint' ts | Track' usr fn ts nat | Control' usr fn ts nat

section \<open>Indistinguishability of states\<close>

inductive ind_S :: "('a, 'b) S \<Rightarrow> ut \<Rightarrow> ('a, 'b) S \<Rightarrow> bool" ("_ \<approx>\<^sup>S\<^sub>_ _") where
  "s\<^sub>W = s\<^sub>W' \<Longrightarrow> s\<^sub>I = s\<^sub>I' \<Longrightarrow> s\<^sub>E \<approx>\<^sup>\<sigma> s\<^sub>E' \<Longrightarrow> s\<^sub>M \<approx>\<^sup>m\<^sub>l s\<^sub>M' \<Longrightarrow> (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<approx>\<^sup>S\<^sub>l (s\<^sub>W', s\<^sub>I', s\<^sub>E', s\<^sub>M')"

lemma ind_S_ind_mem: "(s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<approx>\<^sup>S\<^sub>l (s\<^sub>W', s\<^sub>I', s\<^sub>E', s\<^sub>M') \<Longrightarrow> s\<^sub>M \<approx>\<^sup>m\<^sub>l s\<^sub>M'"
  by (cases rule: ind_S.cases) auto

lemma reflp_ind_S: "x \<approx>\<^sup>S\<^sub>l x"
  by (cases x) (simp add: ind_S.intros equivp_reflp[OF equiv_ind_if_trace] ind_mem_refl)

lemma equiv_ind_S: "equivp (\<lambda>x y. ind_S x l y)"
proof (rule equivpI; unfold reflp_def symp_def transp_def; intro allI impI)
  fix x
  show "x \<approx>\<^sup>S\<^sub>l x"
    by (cases x) (simp add: ind_S.intros equivp_reflp[OF equiv_ind_if_trace] ind_mem_refl)
next
  fix x y
  assume "x \<approx>\<^sup>S\<^sub>l y"
  thus "y \<approx>\<^sup>S\<^sub>l x"
    by (cases x; cases y) 
       (simp add: ind_S.intros equivp_symp[OF equiv_ind_if_trace] ind_mem_sym)
next
  fix x y z 
  assume "x \<approx>\<^sup>S\<^sub>l y" "y \<approx>\<^sup>S\<^sub>l z"
  thus "x \<approx>\<^sup>S\<^sub>l z"
    by (cases x; cases y; cases z; simp)
       (erule ind_S.cases; erule ind_S.cases; rule ind_S.intros; auto;
        use ind_mem_trans equivp_transp[OF equiv_ind_if_trace] in metis)+
qed

fun sum_from0 :: "(nat \<Rightarrow> nat) \<Rightarrow> nat  \<Rightarrow> nat" where
  "sum_from0 f 0 = f 0"
| "sum_from0 f i = f i + sum_from0 f (i-1)"

section \<open>Locale PL\<close>

locale PL =
  fixes code_steps :: "'a \<Rightarrow> 'b \<Rightarrow> mem \<Rightarrow> checker \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> mem \<Rightarrow> bool" ("_, _, _ \<down>\<^sup>*\<^sub>_ _, _, _" 10) 
    and \<pi> :: "'a prog"
    and \<E> :: "if_event trace \<Rightarrow> ts \<Rightarrow> if_event set \<Rightarrow> if_event set"
    and P :: "if_event pro"
    and pc\<^sub>0 :: 'b
    and \<epsilon> :: 'a
  assumes PL_indep_past: "indep_past P"
      and PL_enforcer:   "enforcer \<E>"
      and PL_lang:       "lang \<E> = P"
      and PL_Itf_mono:   "Itf_mono P"
      and PL_pos_time:   "\<forall>f. \<forall>i \<in> {0..<length (\<pi> f)}. Time ((\<pi> f) ! i) > 0"
      and PL_nonempty_f: "\<forall>f. length (\<pi> f) > 0"
      and PL_ni:         "\<And>f i m\<^sub>1 m\<^sub>2 pc\<^sub>1 pc\<^sub>2 m'\<^sub>1 m'\<^sub>2. i \<in> {0..<length (\<pi> f)} \<Longrightarrow> m\<^sub>1 \<approx>\<^sup>m\<^sub>l m\<^sub>2
                                  \<Longrightarrow> ((Code ((\<pi> f) ! i)), pc\<^sub>0, m\<^sub>1 \<down>\<^sup>*\<^sub>\<gamma> \<epsilon>, pc\<^sub>1, m'\<^sub>1) 
                                  \<Longrightarrow> ((Code ((\<pi> f) ! i)), pc\<^sub>0, m\<^sub>2 \<down>\<^sup>*\<^sub>\<gamma> \<epsilon>, pc\<^sub>2, m'\<^sub>2) \<Longrightarrow> m'\<^sub>1 \<approx>\<^sup>m\<^sub>l m'\<^sub>2"
      and PL_deterministic: "\<And>f s\<^sub>M \<gamma> i pc'\<^sub>1 s'\<^sub>M\<^sub>1 pc'\<^sub>2 s'\<^sub>M\<^sub>2. i \<in> {0..<length (\<pi> f)} 
                              \<Longrightarrow> ((Code ((\<pi> f) ! i)), pc\<^sub>0, s\<^sub>M \<down>\<^sup>*\<^sub>\<gamma> \<epsilon>, pc'\<^sub>1, s'\<^sub>M\<^sub>1)
                              \<Longrightarrow> ((Code ((\<pi> f) ! i)), pc\<^sub>0, s\<^sub>M \<down>\<^sup>*\<^sub>\<gamma> \<epsilon>, pc'\<^sub>2, s'\<^sub>M\<^sub>2)
                              \<Longrightarrow> s'\<^sub>M\<^sub>1 = s'\<^sub>M\<^sub>2"

context PL
begin

subsection \<open>TTC semantics\<close>

fun next\<^sub>W :: "fn \<Rightarrow> usr \<Rightarrow> ts \<Rightarrow> nat \<Rightarrow> S\<^sub>W" where
  "next\<^sub>W f u \<tau> i = (if i \<ge> length (\<pi> f)-1 then Taint else Track u f (\<tau> + Time ((\<pi> f) ! (i+1))) (i+1))"    

fun next_time :: "fn \<Rightarrow> ts \<Rightarrow> nat \<Rightarrow> (ts \<times> nat)" where
  "next_time f \<tau> i = (if i \<ge> length (\<pi> f)-1 then (\<tau>, 4) else (\<tau> + Time ((\<pi> f) ! (i+1)), 2))" 
                                             
fun the_usr :: "usr \<Rightarrow> user \<Rightarrow> usr" where
  "the_usr _ (UUsr u) = u"
| "the_usr u UMe = u"

fun c :: "fn \<Rightarrow> usr \<Rightarrow> if_event trace \<Rightarrow> ts \<Rightarrow> checker" where
  "c f u \<sigma> \<tau>' l u' p = ((\<E> \<sigma> \<tau>' {IOut f (the_usr u u') p \<tau>', IItf l \<tau>'}) = {})"

fun init\<^sub>I :: "fn \<Rightarrow> usr \<Rightarrow> if_event trace \<Rightarrow> ts \<Rightarrow> nat \<Rightarrow> ('a, 'b) S\<^sub>I" where
  "init\<^sub>I f u \<sigma> \<tau>' i = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u (c f u \<sigma> \<tau>')"

fun next\<^sub>I :: "fn \<Rightarrow> usr \<Rightarrow> if_event trace \<Rightarrow> ts \<Rightarrow> nat \<Rightarrow> ('a, 'b) S\<^sub>I" where
  "next\<^sub>I f u \<sigma> \<tau> i = (if i \<ge> length (\<pi> f)-1 then InterpNull else init\<^sub>I f u \<sigma> (\<tau> + Time (\<pi> f ! (i+1))) (i+1))"

fun N :: "fn \<Rightarrow> usr \<Rightarrow> prp \<Rightarrow> ts \<Rightarrow> history \<Rightarrow> if_event set" where
  "N f u p \<tau> \<alpha> = { IOut f u p \<tau> } \<union> { IItf l \<tau> | l. l \<in> uts \<alpha> }"

definition S\<^sub>0 :: "('a, 'b) S" where
  "S\<^sub>0 = (Taint, InterpNull, [], (\<lambda>x. \<langle>0, []\<rangle>))"

fun fun_idx :: "('a, 'b) S \<Rightarrow> (fn \<times> nat) option" where 
  "fun_idx (s\<^sub>W, _, _, _) = (case s\<^sub>W of Taint \<Rightarrow> None | Track _ f _ i \<Rightarrow> Some (f, i) | Control _ f _ i \<Rightarrow> Some (f, i))"

inductive ttc :: "('a, 'b) S \<Rightarrow> io_label \<Rightarrow> ('a, 'b) S \<Rightarrow> bool" ("_ \<midarrow>_\<rightarrow> _" 10) where
  taint: 
    "\<tau>' = \<tau> + Time (\<pi> f ! 0) 
    \<Longrightarrow> s'\<^sub>E = s\<^sub>E @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})]
    \<Longrightarrow> (Taint, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<midarrow>Ii u f A \<tau>\<rightarrow> 
        (Track u f \<tau>' 0, init\<^sub>I f u s'\<^sub>E \<tau>' 0, s'\<^sub>E, (\<lambda>x. if x \<in> dom A then \<langle>the (A x), [{(\<tau>, x)}]\<rangle> else s\<^sub>M x))"
| track:
    "co, pc\<^sub>0, s\<^sub>M \<down>\<^sup>*\<^sub>\<gamma> \<epsilon>, pc', s'\<^sub>M
    \<Longrightarrow> s\<^sub>I = Interp co pc u \<gamma>
    \<Longrightarrow> (Track u f \<tau> i, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<midarrow>Bb\<rightarrow> (Control u f \<tau> i, s\<^sub>I, s\<^sub>E, s'\<^sub>M)"
| ctrl_accept:
    "u' = the_usr u (Out_u (\<pi> f ! i))
    \<Longrightarrow> p = Out_p (\<pi> f ! i)
    \<Longrightarrow> s\<^sub>M (Out_y (\<pi> f ! i)) = \<langle>v, \<alpha>\<rangle>
    \<Longrightarrow> b = \<E> s\<^sub>E \<tau> (N f u' p \<tau> \<alpha>)
    \<Longrightarrow> s'\<^sub>E = s\<^sub>E @ [(\<tau>, N f u' p \<tau> \<alpha>)]
    \<Longrightarrow> b = {}
    \<Longrightarrow> (Control u f \<tau> i, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<midarrow>Oo f u' p v \<tau>\<rightarrow> (next\<^sub>W f u \<tau> i, next\<^sub>I f u s'\<^sub>E \<tau> i, s'\<^sub>E, s\<^sub>M)"
| ctrl_reject:
    "u' = the_usr u (Out_u (\<pi> f ! i))
    \<Longrightarrow> p = Out_p (\<pi> f ! i)
    \<Longrightarrow> s\<^sub>M (Out_y (\<pi> f ! i)) = \<langle>v, \<alpha>\<rangle>
    \<Longrightarrow> b = \<E> s\<^sub>E \<tau> (N f u' p \<tau> \<alpha>)
    \<Longrightarrow> s'\<^sub>E = s\<^sub>E @ [(\<tau>, N f u' p \<tau> \<alpha>)]
    \<Longrightarrow> b \<noteq> {}
    \<Longrightarrow> \<sigma>' = s\<^sub>E @ [(\<tau>, {})]
    \<Longrightarrow> (Control u f \<tau> i, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<midarrow>Bb\<rightarrow> (next\<^sub>W f u \<tau> i, next\<^sub>I f u \<sigma>' \<tau> i, \<sigma>', s\<^sub>M)"

subsection \<open>Well-formedness\<close>

definition sum_times :: "fn \<Rightarrow> nat \<Rightarrow> nat" where
  "sum_times f = sum_from0 (\<lambda>i. Time ((\<pi> f) ! i))"

definition wf_in_seq2 :: "in_seq \<Rightarrow> bool" where
  "wf_in_seq2 I = (\<forall>i \<in> {0..<length I-1}. \<forall>u f A \<tau> u' f' A' \<tau>'. I ! i = (u, f, A, \<tau>) 
                      \<longrightarrow> I ! (i+1) = (u', f', A', \<tau>') 
                      \<longrightarrow> \<tau>' > \<tau> + sum_times f (length (\<pi> f)-1))"

lemma wf_in_seq2_tail: "wf_in_seq2 (a # T) \<Longrightarrow> wf_in_seq2 T"
  apply(unfold wf_in_seq2_def, intro ballI allI)
  subgoal for i
    by (erule ballE[where x="Suc i"]) auto
  done

fun valid_code :: "('a, 'b) S \<Rightarrow> bool" where
  "valid_code (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) = 
    (\<forall>u f \<tau> i. (s\<^sub>W = Track u f \<tau> i \<or> s\<^sub>W = Control u f \<tau> i) 
               \<longrightarrow> i < length (\<pi> f) \<and> (\<exists>c. s\<^sub>I = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u c))"

subsection \<open>TTC fulfills lg\<close>

lemma no_way_back: "S \<midarrow>e\<rightarrow> S' \<Longrightarrow> S' \<noteq> S\<^sub>0"
  by (cases rule: ttc.cases) (auto simp add: S\<^sub>0_def)

lemma same_ts_fn_wf_in_seq2: 
  "length I = length I' 
   \<Longrightarrow> \<forall>i \<in> {0..<length I}. tuple_ts (I ! i) = tuple_ts (I' ! i) 
                          \<and> tuple_fn (I ! i) = tuple_fn (I' ! i)
   \<Longrightarrow> wf_in_seq2 I \<Longrightarrow> wf_in_seq2 I'"
  apply(unfold wf_in_seq2_def)
  apply(rule ballI)
  subgoal for i
    apply(rotate_tac -2, erule ballE[where x=i]) 
    subgoal
      apply(intro allI impI)
      apply(cases "I ! i"; cases "I ! (i+1)"; auto)
      apply(subgoal_tac "tuple_ts (I ! i) = tuple_ts (I' ! i)")
      apply(subgoal_tac "tuple_fn (I ! i) = tuple_fn (I' ! i)")
      apply(subgoal_tac "tuple_ts (I ! (Suc i)) = tuple_ts (I' ! (Suc i))")
      apply simp      
      apply(erule ballE[where x="Suc i"]; auto)
      by (erule ballE[where x=i]; auto)+
    by simp
  done

lemma ind_in_seq_same_ts_fn: 
  "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> \<forall>i \<in> {0..<length I}. tuple_ts (I ! i) = tuple_ts (I' ! i) 
                                   \<and> tuple_fn (I ! i) = tuple_fn (I' ! i)"
  apply(induct rule: ind_in_seq.induct; simp)
  subgoal for T l T' A A' \<tau> a u f
    apply(rule ballI)
    subgoal for i
      by (cases i) auto
    done
  done

lemma ind_wf_in_seq2: "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> wf_in_seq2 I \<Longrightarrow> wf_in_seq2 I'"
  using same_ts_fn_wf_in_seq2 ind_in_seq_same_ts_fn ind_in_seq_same_ts by blast

lemma valid_step: "valid_code S \<Longrightarrow> S \<midarrow>e\<rightarrow> S' \<Longrightarrow> valid_code S'"
  by (erule ttc.cases; auto) (use PL_nonempty_f in blast)+

lemma valid_S\<^sub>0: "valid_code S\<^sub>0"
  by (auto simp add: S\<^sub>0_def)

lemma determ: "valid_code S \<Longrightarrow> S \<midarrow>e'\<rightarrow> S' \<Longrightarrow> S \<midarrow>e''\<rightarrow> S'' \<Longrightarrow> S' \<noteq> S'' \<or> e' \<noteq> e'' \<Longrightarrow> 
                      \<exists>u' f' A' \<tau>' u'' f'' A'' \<tau>''. 
                         e' = Ii u' f' A' \<tau>' \<and> e'' = Ii u'' f'' A'' \<tau>'' 
                         \<and> (u', f', A', \<tau>') \<noteq> (u'', f'', A'', \<tau>'')"
  by (frule valid_step[where S'=S'],  assumption, 
      frule valid_step[where S'=S''], assumption, 
      erule ttc.cases; erule ttc.cases)
     (use PL_deterministic in auto)

interpretation ttc_lg: lg S\<^sub>0 ttc wf_in_seq2 valid_code
  using determ no_way_back ind_wf_in_seq2 valid_step valid_S\<^sub>0 unfolding lg_def by blast 

subsection \<open>TODO\<close>

lemma P_snoc: "\<sigma> \<in> P \<Longrightarrow> D \<inter> sup_ev = {} \<Longrightarrow> \<sigma> @ [(\<tau>, D)] \<in> P"
  apply(fold PL_lang)
  apply(unfold lang_def)
  apply auto
  apply(cases "\<E> \<sigma> \<tau> D = {}")
  apply(erule in_lang.intros(2))
  apply assumption
  using PL_enforcer unfolding enforcer_def by force

lemma P_snoc2: "\<sigma> \<in> P \<Longrightarrow> \<E> \<sigma> \<tau> D = {} \<Longrightarrow> \<sigma> @ [(\<tau>, D)] \<in> P"
  apply(fold PL_lang)
  apply(unfold lang_def)
  apply auto
  apply(erule in_lang.intros(2))
  by assumption

lemma ind_enforcer: "\<sigma> \<in> P \<Longrightarrow> \<sigma>' \<in> P \<Longrightarrow> \<sigma> \<approx>\<^sup>\<sigma> \<sigma>' \<Longrightarrow> (\<E> \<sigma> \<tau> D = {}) = (\<E> \<sigma>' \<tau> D = {})"
  apply(insert lang_empty_eq[OF PL_lang, of \<sigma> \<tau> D])
  apply(erule meta_impE)
  unfolding sup_ev_def apply blast
  apply(insert lang_empty_eq[OF PL_lang, of \<sigma>' \<tau> D])
  apply(erule meta_impE)
  unfolding sup_ev_def apply blast
  apply simp
  apply(insert PL_indep_past[unfolded indep_past_def, rule_format, of "\<sigma> @ [(\<tau>, D)]" "\<sigma>' @ [(\<tau>, D)]"])
  apply(elim meta_impE)
  apply(erule ind_if_trace.cases; simp)
  apply simp
  by assumption

lemma ind_c_eq: "\<sigma> \<in> P \<Longrightarrow> \<sigma>' \<in> P \<Longrightarrow> \<sigma> \<approx>\<^sup>\<sigma> \<sigma>' \<Longrightarrow> A \<approx>\<^sup>p\<^sub>l A' 
       \<Longrightarrow> c f u (\<sigma> @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})]) 
         = c f u (\<sigma>' @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A'})])"
  apply(rule ext)+
  apply simp
  subgoal for \<tau>' l u' p
    apply(insert lang_empty_eq[OF PL_lang, of "(\<sigma> @ [(\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A})])" 
                                              \<tau>' "{IOut f (the_usr u u') p \<tau>', IItf l \<tau>'}"])
    apply(erule meta_impE, erule P_snoc)
    unfolding sup_ev_def apply blast
    apply(insert lang_empty_eq[OF PL_lang, of "(\<sigma>' @ [(\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A'})])" 
                                              \<tau>' "{IOut f (the_usr u u') p \<tau>', IItf l \<tau>'}"])
    apply(erule meta_impE, erule P_snoc)
    unfolding sup_ev_def apply blast
    apply(subgoal_tac "length \<sigma> = length \<sigma>'" "dom A = dom A'")
    apply(elim ssubst)
    apply(rule PL_indep_past[unfolded indep_past_def, rule_format, 
      of "(\<sigma> @ [(\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A'})]) @ [(\<tau>', {IOut f (the_usr u u') p \<tau>', IItf l \<tau>'})]"
         "(\<sigma>'@ [(\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A'})]) @ [(\<tau>', {IOut f (the_usr u u') p \<tau>', IItf l \<tau>'})]"])
    apply(erule ind_if_trace.cases; simp)
    apply(unfold butlast_snoc)
    apply(erule ind_if_trace_snoc)
    apply(rule equivp_reflp[OF equiv_ind_part_mem])
    apply(erule ind_part_mem.cases; simp)
    by (erule ind_if_trace.cases; simp)
  done

lemma ind_c_eq2: "\<sigma> @ [(\<tau>, D)] \<in> P \<Longrightarrow> \<sigma>' @ [(\<tau>, D')] \<in> P \<Longrightarrow> \<sigma> \<approx>\<^sup>\<sigma> \<sigma>' 
       \<Longrightarrow> {IIn u f l | u f l. True} \<inter> D = {} \<Longrightarrow> {IIn u f l | u f l. True} \<inter> D' = {}
       \<Longrightarrow> c f u (\<sigma> @ [(\<tau>, D)]) = c f u (\<sigma>' @ [(\<tau>, D')])"
  apply(rule ext)+
  apply simp
  subgoal for \<tau>' l u' p
    apply(insert lang_empty_eq[OF PL_lang, of "(\<sigma> @ [(\<tau>, D)])" 
                                              \<tau>' "{IOut f (the_usr u u') p \<tau>', IItf l \<tau>'}"])
    apply(erule meta_impE, assumption)
    apply(insert lang_empty_eq[OF PL_lang, of "(\<sigma>' @ [(\<tau>, D')])" 
                                              \<tau>' "{IOut f (the_usr u u') p \<tau>', IItf l \<tau>'}"])
    apply(erule meta_impE, assumption)
    apply(subgoal_tac "length \<sigma> = length \<sigma>'")
    apply(rotate_tac -2, erule ssubst, rotate_tac -1, erule ssubst)
    apply(rule PL_indep_past[unfolded indep_past_def, rule_format, 
      of "(\<sigma> @ [(\<tau>, D)]) @ [(\<tau>', {IOut f (the_usr u u') p \<tau>', IItf l \<tau>'})]"
         "(\<sigma>' @ [(\<tau>, D')]) @ [(\<tau>', {IOut f (the_usr u u') p \<tau>', IItf l \<tau>'})]"])
    apply(erule ind_if_trace.cases; simp)
    apply(unfold butlast_snoc)
    apply(subgoal_tac "{uu. \<exists>u f a b. uu = IIn u f (a, b)} = {IIn u f l | u f l. True}")
    apply(rule ind_if_trace_snoc2)
    apply assumption
    apply metis
    apply metis
    apply auto[1]
    by (erule ind_if_trace.cases; simp)
  done

lemma ind_in_seq_ts: "((u, f, A, \<tau>) # T) \<approx>\<^sup>I\<^sub>(\<tau>', a) ((u, f, A', \<tau>) # T') \<Longrightarrow> \<tau> \<noteq> \<tau>' \<Longrightarrow> A = A'"
  by (erule ind_in_seq.cases; auto)

fun happy :: "('a, 'b) S \<Rightarrow> bool" where
  "happy (_, _, \<sigma>, _) = (\<sigma> \<in> P)"

inductive right_code :: "('a, 'b) S \<Rightarrow> bool" where
  right_code_null: "fun_idx S = None \<Longrightarrow> S = (_, InterpNull, _, _) \<Longrightarrow> right_code S"
| right_code_match: "fun_idx S = Some (f, i) \<Longrightarrow> i \<in> {0..<length (\<pi> f)} \<Longrightarrow> S = (_ , Interp (Code ((\<pi> f) ! i)) _ _ _, _, _) \<Longrightarrow> right_code S"

lemma right_code_S\<^sub>0: "right_code S\<^sub>0"
  apply(unfold S\<^sub>0_def)
  apply(rule right_code_null)
  by auto

lemma happy_S\<^sub>0: "happy S\<^sub>0"
  using PL_lang in_lang.intros(1) lang_def S\<^sub>0_def
  by (metis happy.simps mem_Collect_eq)


lemma A20a:
  "ttc_lg.wf_in_seq' I
 \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'
 \<Longrightarrow> happy S \<and> right_code S"
  apply(induct rule: ttc_lg.ttrans_induct)
  apply(simp add: happy_S\<^sub>0 right_code_S\<^sub>0)
  subgoal for I \<sigma> S' u f A \<tau> T S''
    apply(erule ttc.cases; simp)
    apply(erule conjE, rule conjI)
    apply(erule enforcer_IIn[OF PL_enforcer PL_lang[THEN sym]], auto)
    using right_code.intros PL_nonempty_f by auto
  subgoal for I \<sigma> S' I' S''
    apply(erule ttc.cases; auto; erule right_code.cases)
    using PL_enforcer PL_lang enforcer_IIn right_code.intros by auto
  subgoal for I \<sigma> S' I' f u p d \<tau> S''
    apply(erule ttc.cases; auto; erule right_code.cases)
    apply(erule enforcer_IIn[OF PL_enforcer PL_lang[THEN sym]], auto)
    using P_snoc2 apply blast
    apply(rule right_code_null; simp)
    using P_snoc2 apply blast
    by (rule right_code_match; simp)
  done


lemma ttrans_empty: "I = [] \<Longrightarrow> S = S\<^sub>0 \<Longrightarrow> ttc_lg.ttrans S I \<sigma> S' I' \<Longrightarrow> S' = S \<and> \<sigma> = [] \<and> I' = []"
  apply(rotate_tac 2, induct rule: ttc_lg.ttrans.induct)
  apply blast
  using ttc_lg.ttrans_empty apply blast
  subgoal for S I \<sigma> S' I' S''
    apply(subgoal_tac "S' = S\<^sub>0")
    apply(erule ttc.cases; auto simp add: S\<^sub>0_def)
    by force
  subgoal for S I \<sigma> S' I' f u p d \<tau> S''
    apply(subgoal_tac "S' = S\<^sub>0")
    apply(erule ttc.cases; auto simp add: S\<^sub>0_def)
    by force
  done

lemma ttrans_S\<^sub>0: "S = S\<^sub>0 \<Longrightarrow> ttc_lg.ttrans S I \<sigma> S I' \<Longrightarrow> \<sigma> = [] \<and> I' = I"
  apply(rotate_tac 1, induct rule: ttc_lg.ttrans.induct)
  apply blast
  by (erule ttc.cases; auto simp add: S\<^sub>0_def)+

lemma ttrans_right_code: "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma> S' I' \<Longrightarrow> right_code S'"
  apply(induct rule: ttc_lg.ttrans_induct)
  apply(rule right_code_S\<^sub>0)
  apply(erule right_code.cases; erule ttc.cases; auto)
  apply(rule right_code_match)
  using PL_nonempty_f apply auto[3]
  apply(erule right_code.cases; erule ttc.cases; auto)
  apply(rule right_code_match)
  using PL_nonempty_f apply auto[3]
  apply(rule right_code_null; simp)
  apply(rule right_code_match)
  using PL_nonempty_f apply auto[3]
  apply(erule right_code.cases; erule ttc.cases; auto)
  apply(rule right_code_null; simp)
  apply(rule right_code_match)
  using PL_nonempty_f by auto

  
lemma same_label: "S \<midarrow>e\<^sub>1\<rightarrow> S\<^sub>1 \<Longrightarrow> S \<midarrow>e\<^sub>2\<rightarrow> S\<^sub>2 
                      \<Longrightarrow> (\<forall>u f A \<tau>. e\<^sub>1 \<noteq> Ii u f A \<tau>) \<or> (\<forall>u f A \<tau>. e\<^sub>2 \<noteq> Ii u f A \<tau>) \<Longrightarrow> e\<^sub>1 = e\<^sub>2"
  by (erule ttc.cases; erule ttc.cases; auto)


lemma list_find_index: "I = H @ x # T \<Longrightarrow> I ! (length H) = x"
  by auto

lemma wf_in_seq_ord: "wf_in_seq I \<Longrightarrow> i < i' \<Longrightarrow> i' < length I 
                      \<Longrightarrow> I ! i = (u, f, A, \<tau>) \<Longrightarrow> I ! i' = (u', f', A', \<tau>') 
                      \<Longrightarrow> \<tau> < \<tau>'"
  apply(induct "i' - i" arbitrary: i i' u f A \<tau> u' f' A' \<tau>')
  apply simp
  subgoal for j i i' u f A \<tau> u' f' A' \<tau>'
    apply(cases "I ! (i'-1)")
    subgoal for u'' f'' A'' \<tau>''
      apply(cases j)
      apply(unfold wf_in_seq_def)
      apply(erule ballE[where x=i']; force)
      apply(erule meta_allE[where x=i], erule meta_allE[where x="i'-1"])
      apply(elim meta_allE meta_impE)
      apply simp
      apply assumption
      apply(rule dual_order.strict_trans2[where b="j+i-1"]; force)
      apply simp
      apply assumption+
      by (erule ballE[where x=i']; simp)
    done
  done

lemma wf_in_seq_ord2: "wf_in_seq I \<Longrightarrow> i < length I \<Longrightarrow> i' < length I 
                      \<Longrightarrow> I ! i = (u, f, A, \<tau>) \<Longrightarrow> I ! i' = (u', f', A', \<tau>) 
                      \<Longrightarrow> i = i'"
  using wf_in_seq_ord by (metis nat_neq_iff)

lemma conc_length: "H @ T = H' @ T' \<Longrightarrow> length H = length H' \<Longrightarrow> H = H' \<and> T = T'"
  by simp

lemma wf_in_seq_dec: "wf_in_seq I \<Longrightarrow> I = H @ (u, f, A, \<tau>) # T \<Longrightarrow> I = H' @ (u', f', A', \<tau>) # T' 
                      \<Longrightarrow> H = H' \<and> (u, f, A) = (u', f', A') \<and> T = T'"
  apply(subgoal_tac "I ! (length H) = (u, f, A, \<tau>)" "I ! (length H') = (u', f', A', \<tau>)")
  apply(subgoal_tac "length H = length H'")
  using conc_length apply blast
  apply(rule wf_in_seq_ord2; simp)
  apply(metis add_Suc_right length_Cons length_append less_add_same_cancel1 zero_less_Suc)
  apply simp
  apply(metis nth_append_length)
  by auto


lemma sum_times_0: "Time (\<pi> f ! 0) = sum_times f 0"
  using sum_times_def sum_from0.simps(1) by auto

lemma sum_times_Suc: "sum_times f i + Time (\<pi> f ! Suc i) = sum_times f (Suc i)"
  using sum_times_def sum_from0.simps(2) by auto

lemma sum_times_noteq_0: "i \<in> {0..<length (\<pi> f)} \<Longrightarrow> sum_times f i \<noteq> 0"
  apply(cases i)
  using sum_times_0 PL_pos_time apply(metis less_numeral_extra(3))
  subgoal for j
    apply simp
    apply(fold sum_times_Suc)
    using PL_pos_time by simp
  done

lemma times_not_eq2: "i\<^sub>2 \<in> {0..<length (\<pi> f)} \<Longrightarrow> i\<^sub>1 < i\<^sub>2 \<Longrightarrow> sum_times f i\<^sub>1 < sum_times f i\<^sub>2"
  apply(induct "i\<^sub>2 - i\<^sub>1" arbitrary: i\<^sub>1 i\<^sub>2)
  apply simp
  subgoal for j i\<^sub>1 i\<^sub>2
    apply(cases j)
    apply(subgoal_tac "i\<^sub>2 = Suc i\<^sub>1"; simp)
    unfolding sum_times_def apply(simp add: PL_pos_time sum_times_Suc)
    subgoal for j
      apply(rule dual_order.strict_trans[where b="sum_from0 (\<lambda>i. Time (\<pi> f ! i)) (i\<^sub>2-1)"])
      apply(cases i\<^sub>2; simp)
      using PL_pos_time atLeastLessThan_iff by force+
    done
  done

lemma times_not_eq': "\<tau>\<^sub>1 + sum_times f\<^sub>1 i\<^sub>1 < \<tau>\<^sub>2  \<Longrightarrow> \<tau>\<^sub>1 + sum_times f\<^sub>1 i\<^sub>1 < \<tau>\<^sub>2 + sum_times f\<^sub>2 i\<^sub>2"
  by auto



lemma wf_in_seq'_alt: 
    "ttc_lg.wf_in_seq' I 
\<longleftrightarrow> (\<forall>i j k. i < j \<longrightarrow> j < length I \<longrightarrow> k < length (\<pi> (tuple_fn (I ! i))) 
             \<longrightarrow> tuple_ts (I ! i) + sum_times (tuple_fn (I ! i)) k  < tuple_ts (I ! j))"
  apply(intro iffI allI impI)
  subgoal for i j k
    apply(rule less_le_trans[where y="tuple_ts (I ! (i+1))"])
    apply(unfold ttc_lg.wf_in_seq'_def wf_in_seq2_def, erule conjE)[1]
    apply(erule ballE[where x=i]; simp)
    apply(cases "I ! i"; cases "I ! (Suc i)"; auto)
    subgoal for u f A \<tau> u' f' A' \<tau>'
      apply(rule le_less_trans[where y="\<tau> + sum_times f (length (\<pi> f)-1)"]; simp)
      apply(cases "k < length (\<pi> f) - 1")
      apply(rule less_imp_le_nat)      
      apply(rule times_not_eq2)
      using PL_nonempty_f apply force+
      by (metis Nat.lessE One_nat_def diff_Suc_1 dual_order.refl)
    by (simp add: ttc_lg.wf_in_seq'_le)
  apply(unfold ttc_lg.wf_in_seq'_def, rule conjI)
  apply(unfold wf_in_seq_alt; auto)
  subgoal for i j
    apply(erule allE[where x=i], erule allE[where x=j], elim impE; simp?)
    apply(erule allE[where x=0])
    using PL_nonempty_f by auto
  apply(unfold wf_in_seq2_def, intro ballI allI)
  subgoal for i u f A \<tau> u' f' A' \<tau>'
    apply(erule allE[where x=i], erule allE[where x="i+1"], erule allE[where x="length (\<pi> f)-1"])
    using PL_nonempty_f by auto
  done

lemma wf_in_seq'_spec:
  "ttc_lg.wf_in_seq' I 
   \<Longrightarrow> i < j
   \<Longrightarrow> j < length I
   \<Longrightarrow> k < length (\<pi> (tuple_fn (I ! i))) 
   \<Longrightarrow> tuple_ts (I ! i) + sum_times (tuple_fn (I ! i)) k  < tuple_ts (I ! j)"
  by (auto simp add: wf_in_seq'_alt)

lemma wf_in_seq'_spec2:
  "ttc_lg.wf_in_seq' I 
   \<Longrightarrow> i \<le> j 
   \<Longrightarrow> i = j \<longrightarrow> k < k'
   \<Longrightarrow> j < length I
   \<Longrightarrow> I ! i = (u, f, A, \<tau>)
   \<Longrightarrow> I ! j = (u', f', A', \<tau>')
   \<Longrightarrow> k < length (\<pi> f) 
   \<Longrightarrow> k' < length (\<pi> f')
   \<Longrightarrow> \<tau> + sum_times f k  < \<tau>' + sum_times f' k'"
  apply(cases "i < j")
  apply(rule less_trans[where y=\<tau>'])
  using wf_in_seq'_spec apply fastforce
  using sum_times_noteq_0 apply fastforce
  apply(subgoal_tac "i = j"; simp)
  apply(rule times_not_eq2)
  using PL_nonempty_f apply fastforce
  by assumption

lemma wf_in_seq'_spec3:
  "ttc_lg.wf_in_seq' I 
   \<Longrightarrow> i < j 
   \<Longrightarrow> j < length I
   \<Longrightarrow> I ! i = (u, f, A, \<tau>)
   \<Longrightarrow> I ! j = (u', f', A', \<tau>')
   \<Longrightarrow> k < length (\<pi> f) 
   \<Longrightarrow> \<tau> + sum_times f k  < \<tau>'"
  apply(cases "i < j")
  using wf_in_seq'_spec sum_times_noteq_0 by fastforce+

lemma wf_in_seq'_spec4:
  "ttc_lg.wf_in_seq' I 
   \<Longrightarrow> i < j 
   \<Longrightarrow> j < length I
   \<Longrightarrow> I ! i = (u, f, A, \<tau>)
   \<Longrightarrow> I ! j = (u', f', A', \<tau>')
   \<Longrightarrow> \<tau>  < \<tau>'"
  apply(drule wf_in_seq'_spec3[where i=i and j=j and k=0]; assumption?)
  using PL_nonempty_f apply blast
  by force

subsection \<open>Time\<close>

fun time :: "('a, 'b) S \<Rightarrow> ts \<times> nat" where 
  "time (s\<^sub>W, _, s\<^sub>E, _) = (case s\<^sub>W of Taint \<Rightarrow> (case rev s\<^sub>E of [] \<Rightarrow> (0, 4) 
                                                           | (\<tau>, _) # _ \<Rightarrow> (\<tau>, 4))
                                   | Track _ _ \<tau> _ \<Rightarrow> (\<tau>, 2) 
                                   | Control _ _ \<tau> _ \<Rightarrow> (\<tau>, 3))"

fun time' :: "('a, 'b) S \<Rightarrow> Time'" where 
  "time' (s\<^sub>W, _, s\<^sub>E, _) = (case s\<^sub>W of Taint \<Rightarrow> (case rev s\<^sub>E of [] \<Rightarrow> Taint' 0 
                                                            | (\<tau>, _) # _ \<Rightarrow> Taint' \<tau>)
                                   | Track u f \<tau> i \<Rightarrow> Track' u f \<tau> i 
                                   | Control u f \<tau> i \<Rightarrow> Control' u f \<tau> i)"

inductive ord_time :: "ts \<times> nat \<Rightarrow> ts \<times> nat \<Rightarrow> bool" (infix "\<prec>" 50) where
  "\<tau> < \<tau>' \<Longrightarrow> (\<tau>, i) \<prec> (\<tau>', i')"
| "i < i' \<Longrightarrow> (\<tau>, i) \<prec> (\<tau>, i')"

inductive orde_time :: "ts \<times> nat \<Rightarrow> ts \<times> nat \<Rightarrow> bool" (infix "\<preceq>" 50) where
  "\<tau> = \<tau>' \<Longrightarrow> \<tau> \<preceq> \<tau>"
| "\<tau> \<prec> \<tau>' \<Longrightarrow> \<tau> \<preceq> \<tau>'"

lemma ord_time_not_eq: "a \<prec> b \<Longrightarrow> a \<noteq> b"
  by (cases rule: ord_time.cases) auto

lemma ord_time_asym: "a \<prec> b \<Longrightarrow> \<not> (b \<prec> a)"
  by (cases rule: ord_time.cases) (simp add: ord_time.simps)+

lemma orde_time_alt_intro:  "\<tau> \<le> \<tau>' \<Longrightarrow> (\<tau>, i) \<preceq> (\<tau>', i)"  "i \<le> i' \<Longrightarrow> (\<tau>, i) \<preceq> (\<tau>, i')"
  by (cases "\<tau> = \<tau>'"; auto simp add: orde_time.intros ord_time.intros)
     (cases "i = i'"; auto simp add: orde_time.intros ord_time.intros)

lemma ord_time_trans: "x \<prec> y \<Longrightarrow> y \<prec> z \<Longrightarrow> x \<prec> z"
  by (erule ord_time.cases; erule ord_time.cases; simp add: ord_time.intros)

lemma ord_orde_time_trans: "x \<prec> y \<Longrightarrow> y \<preceq> z \<Longrightarrow> x \<prec> z"
  by (erule ord_time.cases; erule orde_time.cases; (erule ord_time.cases)?; simp add: ord_time.intros)

lemma orde_time_trans: "x \<preceq> y \<Longrightarrow> y \<preceq> z \<Longrightarrow> x \<preceq> z"
  by (erule orde_time.cases; erule orde_time.cases; simp add: orde_time.intros ord_time_trans)

fun the_f :: "in_seq \<Rightarrow> nat \<Rightarrow> Time' \<times> in_seq" where
  "the_f I 0 = (Taint' 0, I)"
| "the_f I (Suc i) = (case (the_f I i) of 
                        (Taint' \<tau>,         ((u, f, A, \<tau>')#T)) \<Rightarrow> (Track' u f (\<tau>' + Time (\<pi> f ! 0)) 0,  T )
                      | (Taint' \<tau>,         []               ) \<Rightarrow> (Taint' (\<tau>+1),     [])
                      | (Track' u f \<tau> i,   I                ) \<Rightarrow> (Control' u f \<tau> i, I )
                      | (Control' u f \<tau> i, I                ) 
                        \<Rightarrow> (if i = length (\<pi> f)-1 
                             then (Taint' \<tau>, I) 
                             else (Track' u f (\<tau> + Time (\<pi> f ! (i+1))) (i+1), I)))"

fun proj_f :: "Time' \<times> in_seq \<Rightarrow> ts \<times> nat" where
  "proj_f (Taint' \<tau>, _) = (\<tau>, 4)"
| "proj_f (Track' u f \<tau> i, _) = (\<tau>, 2)"
| "proj_f (Control' u f \<tau> i, _) = (\<tau>, 3)"

abbreviation "ff I i \<equiv> proj_f (the_f I i)"

lemma the_f_conc: "the_f I i = (\<tau>, I') \<Longrightarrow> \<exists>H. I = H @ I'"
  apply(induct i arbitrary: \<tau> I'; simp)
  subgoal for i \<tau> I'
    by (cases "the_f I i"; auto split: Time'.splits list.splits if_splits)
  done

lemma the_f_sum_times: 
  "ttc_lg.wf_in_seq' I \<Longrightarrow> the_f I i = (\<tau>, I') \<Longrightarrow>
    (\<forall>u f \<tau>' i. \<tau> = Track' u f \<tau>' i \<or> \<tau> = Control' u f \<tau>' i \<longrightarrow> i < length (\<pi> f) 
     \<and> (\<exists>H A \<tau>\<^sub>0. I = H @ (u, f, A, \<tau>\<^sub>0) # I' \<and> \<tau>' = \<tau>\<^sub>0 + sum_times f i))
  \<and> (\<forall>\<tau>'. (\<tau> = Taint' \<tau>' \<and> \<tau>' > 0) 
       \<longrightarrow> (\<exists>u f H A \<tau>\<^sub>0. I = H @ (u, f, A, \<tau>\<^sub>0) # I' \<and> \<tau>' = \<tau>\<^sub>0 + sum_times f (length (\<pi> f)-1))
         \<or> (I' = [] \<and> ((I = []) \<or> (\<tau>' > tuple_ts (I ! (length I - 1))))))
  \<and> (\<forall>\<tau>'. (\<tau> = Taint' 0 \<longrightarrow> I = I'))"
proof(induct i arbitrary: \<tau> I')
  case 0
  then show ?case by auto
next
  case (Suc i)
  obtain \<tau>' I'' where \<tau>': "the_f I i = (\<tau>', I'')" by fastforce
  show ?case
  proof(cases \<tau>')
    case Taint'
    thus ?thesis using PL_nonempty_f sum_times_0 \<tau>' Suc by (auto split: list.splits)
  next
    case Track'
    thus ?thesis using \<tau>' Suc by auto
  next
    case Control'
    thus ?thesis using sum_times_noteq_0 sum_times_Suc \<tau>' Suc by (force split: list.splits if_splits)
  qed
qed

lemma the_f1: "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> the_f I i = (time' S, I')"
  apply(induct rule: ttc_lg.count_induct; simp add: S\<^sub>0_def; erule ttc.cases; auto split: list.splits)
  using S\<^sub>0_def ttc_lg.count_valid_code by fastforce+
  
lemma f0: "the_f I i = (time' S, I') \<Longrightarrow> ff I i = time S"
  by (cases S; auto split: S\<^sub>W.splits list.splits)

lemma f1: "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> ff I i = time S"
  using the_f1 f0 by blast

lemma the_f2: "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> fst (the_f I i) = fst (the_f I' i) \<and> snd (the_f I i) \<approx>\<^sup>I\<^sub>l snd (the_f I' i)"
  apply(induct arbitrary: i rule: ind_in_seq.induct, simp)
  apply(rule equivp_reflp[OF equiv_ind_in_seq])
  subgoal for T l T' A A' \<tau> a u f i
    apply(induct i arbitrary: T T' A A' \<tau> a u f; simp)
    apply(erule ind_in_seq.intros(2); assumption)
    subgoal for i T T' A A' \<tau> a u f
      apply(erule meta_allE[where x=T], erule meta_allE[where x=T'])
      apply(erule meta_allE[where x=A], erule meta_allE[where x=A'])
      apply(erule meta_allE[where x=\<tau>], erule meta_allE[where x=a])
      apply(erule meta_allE[where x=u], erule meta_allE[where x=f], elim meta_impE; simp?)
      apply(cases "the_f ((u, f, A, \<tau>) # T) i"; cases "the_f ((u, f, A', \<tau>) # T') i")
      subgoal for a I a' I'
        apply(cases a; cases a'; simp)
        apply(cases I; cases I'; simp)
        using ind_in_seq.simps apply blast
        using ind_in_seq.simps apply blast
        subgoal for x b c b' c'
          apply(cases "b # c" l "b' # c'" rule: ind_in_seq.cases; simp)
          by (cases b) (simp add: equivp_reflp[OF equiv_ind_in_seq])
        done
      done
    done
  done

lemma f2: "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> ff I i = ff I' i"
  apply(drule the_f2[where i=i])
  apply(cases "the_f I i"; cases "the_f I' i"; auto)
  subgoal for a
    by (cases a) auto
  done

lemma f3: "ttc_lg.wf_in_seq' I \<Longrightarrow> ff I i\<^sub>1 \<prec> ff I (i\<^sub>1+1)"
  apply(cases "the_f I i\<^sub>1")
  apply( auto split: Time'.splits list.splits simp add: ord_time.intros)
  subgoal for \<tau> u f A \<tau>'
    apply(cases \<tau>)
    apply(drule the_f_sum_times, simp)
    apply(rule ord_time.intros(1))
    using PL_pos_time PL_nonempty_f apply simp
    apply(frule the_f_sum_times, simp)
    apply(elim exE conjE disjE; auto)
    apply(rule ord_time.intros(1))
    subgoal for _ u'' f'' H'' A'' \<tau>\<^sub>0
      apply(subgoal_tac "\<tau>\<^sub>0 + sum_times f'' (length (\<pi> f'') - Suc 0) < \<tau>'", simp)
      apply(erule wf_in_seq'_spec3[where i="length H''" and j="length H''+1"]; (simp add: nth_append)?)
      using PL_nonempty_f by force
    done
  subgoal for u f \<tau>' i
    apply(frule the_f_sum_times; simp)
    apply(elim disjE exE conjE)
    apply(rule ord_time.intros(1))
    using PL_pos_time by auto
  done

lemma f3':  "ttc_lg.wf_in_seq' I \<Longrightarrow> (\<forall>i\<^sub>1. ff I i\<^sub>1 \<prec> ff I (i\<^sub>1+1))"
  using f3 by auto

lemma find_f_times: 
  "\<exists>f. (\<forall>I \<sigma> S I' i. ttc_lg.wf_in_seq' I \<longrightarrow> ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<longrightarrow> f I i = time S) 
     \<and> (\<forall>I l I' i. (I \<approx>\<^sup>I\<^sub>l I') \<longrightarrow> f I i = f I' i) 
     \<and> (\<forall>I i\<^sub>1. ttc_lg.wf_in_seq' I \<longrightarrow> f I i\<^sub>1 \<prec> f I (i\<^sub>1+1))"
  by (rule exI[where x=ff]) (use f1 f2 f3 in auto)

lemma ord_time_mono: "\<forall>i. f I i \<prec> f I (i+1) \<Longrightarrow> (i::nat) < j \<Longrightarrow> f I i \<prec> f I j"
  apply(induct "j-i" arbitrary: i j; simp)
  subgoal for \<delta> i j
    apply(cases \<delta>)
    apply(rotate_tac 2, erule meta_allE[where x=i])
    apply(metis One_nat_def Suc_eq_plus1 le_add_diff_inverse less_or_eq_imp_le)
    apply(rule ord_time_trans[where y="f I (Suc i)"])
    by force+
  done

lemma ord_time_inj: "\<forall>i. f I i \<prec> f I (i+1) \<Longrightarrow> (i::nat) = j \<longleftrightarrow> f I i = f I j"
  apply(rule iffI)
  apply simp
  apply(cases "i < j")
  apply(drule ord_time_mono, assumption, drule ord_time_not_eq, simp)
  apply(cases "j < i")
  apply(drule ord_time_mono, assumption, drule ord_time_not_eq, simp)
  by simp

lemma ord_time_mono': "\<forall>i. f I i \<prec> f I (i+1) \<Longrightarrow> f I (i::nat) \<prec> f I j \<Longrightarrow> i < j"
  apply(cases "i = j")
  using ord_time_not_eq apply blast
  apply(cases "i < j")
  apply assumption
  by (drule ord_time_mono[of f _ j i]) (auto simp add: ord_time_asym)

lemma find_f_times': 
  "\<exists>f. (\<forall>I \<sigma> S I' i. ttc_lg.wf_in_seq' I \<longrightarrow> ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<longrightarrow> f I i = time S) 
     \<and> (\<forall>I l I' i. (I \<approx>\<^sup>I\<^sub>l I') \<longrightarrow> f I i = f I' i) 
     \<and> (\<forall>I i\<^sub>1 i\<^sub>2. ttc_lg.wf_in_seq' I \<longrightarrow> f I i\<^sub>1 = f I i\<^sub>2 \<longrightarrow> i\<^sub>1 = i\<^sub>2)"
  apply(insert find_f_times)
  apply(erule exE)
  subgoal for f
    apply(rule exI[where x=f])
    apply(intro conjI)
    apply(erule conjunct1)
    apply(drule conjunct2)
    apply(erule conjunct1)
    apply(rule allI)
    subgoal for I
      apply(drule conjunct2)+
      apply(erule allE[where x=I])
      apply(intro allI impI)
      subgoal for i\<^sub>1 i\<^sub>2
        apply(cases "i\<^sub>1 < i\<^sub>2")
        apply(subgoal_tac "f I i\<^sub>1 \<prec> f I i\<^sub>2")
        using ord_time_not_eq apply metis
        using ord_time_mono apply metis
        apply(cases "i\<^sub>2 < i\<^sub>1")
        apply(subgoal_tac "f I i\<^sub>2 \<prec> f I i\<^sub>1")
        using ord_time_not_eq apply metis
        using ord_time_mono apply metis
        by simp
      done
    done
  done

subsection \<open>TODO2\<close>

lemma count_time': 
  assumes "ttc_lg.wf_in_seq' I" 
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i"
      and "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i"      
      and "I \<approx>\<^sup>I\<^sub>l I'"
    shows "time' S\<^sub>1 = time' S\<^sub>2 \<and> (I\<^sub>1 \<approx>\<^sup>I\<^sub>l I\<^sub>2)"
proof -
  obtain f where f:
    "\<And>\<sigma> S I' i. ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> f I i = time S"
    "\<And>i. I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> f I i = f I' i"
    using find_f_times' assms(1) by metis
  from assms show ?thesis
  proof(induct arbitrary: I' S\<^sub>2 I\<^sub>2 \<sigma>\<^sub>2 rule: ttc_lg.count_induct)
    case (count_nop_case I)
    then show ?case by (auto simp add: ttc_lg.count_0)
  next
    case (count_in_case I \<sigma> S' u f A \<tau> T i S'' I' S\<^sub>2 I\<^sub>2)
    from count_in_case(5) 
    show ?case
    proof(cases rule: ttc_lg.count.cases)
      case count_nop
      thus ?thesis by simp
    next
      case (count_in \<sigma>' S\<^sub>1 u' f' A' \<tau>' i')
      hence "i = i'" by simp
      hence "((u, f, A, \<tau>) # T) \<approx>\<^sup>I\<^sub>l ((u', f', A', \<tau>') # I\<^sub>2)" using count_in_case(3,6) count_in(3) by auto
      hence "u = u'" "f = f'" "A \<approx>\<^sup>p\<^sub>l A'" "\<tau> = \<tau>'" "T \<approx>\<^sup>I\<^sub>l I\<^sub>2" using ind_in_seq_head by blast+
      moreover have "time' S'' = time' S\<^sub>2"
        using count_in_case(4) apply(cases rule: ttc.cases)
        using count_in(4) by (cases rule: ttc.cases) (auto simp add: \<open>u = u'\<close> \<open>f = f'\<close> \<open>\<tau> = \<tau>'\<close>)
      ultimately show ?thesis by simp
    next
      case (count_none S\<^sub>1 i')
      hence "i = i'" by simp
      have "time' S' = time' S\<^sub>1" using count_in_case(3,6) count_none(2) \<open>i = i'\<close> by auto
      show ?thesis 
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_in_case(4) apply(cases rule: ttc.cases)
        using count_none(3) apply(cases rule: ttc.cases)
        subgoal for _ _ s\<^sub>E
          by (cases "rev s\<^sub>E") auto
        subgoal for _ _ s\<^sub>E
          by (cases "rev s\<^sub>E") auto
        done
    next
      case (count_out \<sigma>' S\<^sub>1 i' f' u' p' d' \<tau>')
      hence "i = i'" by simp
      have "time' S' = time' S\<^sub>1" using count_in_case(3,6) count_out(3) \<open>i = i'\<close> by auto
      show ?thesis 
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_in_case(4) apply(cases rule: ttc.cases)
        using count_out(4) apply(cases rule: ttc.cases)
        subgoal for _ _ s\<^sub>E
          by (cases "rev s\<^sub>E") auto
        done
    qed
  next
    case (count_none_case I \<sigma> S' T i S'' I' S\<^sub>2 I\<^sub>2)
    from count_none_case(5) 
    show ?case
    proof(cases rule: ttc_lg.count.cases)
      case count_nop
      thus ?thesis by simp
    next
      case (count_in \<sigma>' S\<^sub>1 u' f' A' \<tau>' i')
      hence "i = i'" by simp
      have "time' S' = time' S\<^sub>1" using count_none_case(3,6) count_in(3) \<open>i = i'\<close> by auto
      thus ?thesis         
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_none_case(4) apply(cases rule: ttc.cases)
        using count_in(4) apply(cases rule: ttc.cases; auto)
        subgoal for  _ _ _ _ _ _ _ _ _ _ _ s\<^sub>E'
          by (cases "rev s\<^sub>E'") auto
        using count_in(4) apply(cases rule: ttc.cases; auto)
        subgoal for _ _ _ _ _ _ _ _ _ s\<^sub>E'
          by (cases "rev s\<^sub>E'") auto
        done
    next
      case (count_none S\<^sub>1 i')
      hence "i = i'" by simp
      hence "time' S' = time' S\<^sub>1" using count_none_case(3,6) count_none(2) by auto
      from \<open>i = i'\<close> have "T \<approx>\<^sup>I\<^sub>l I\<^sub>2" using count_none_case(3,6) count_none(2) by auto
      moreover have "time' S'' = time' S\<^sub>2"
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_none_case(4) apply(cases rule: ttc.cases)
        using count_none(3) by (cases rule: ttc.cases; auto)+
      ultimately show ?thesis by simp
    next
      case (count_out \<sigma>' S\<^sub>1 i' f' u' p' d' \<tau>')
      hence "i = i'" by simp
      hence "time' S' = time' S\<^sub>1" using count_none_case(3,6) count_out(3) by auto
      from \<open>i = i'\<close> have "T \<approx>\<^sup>I\<^sub>l I\<^sub>2" using count_none_case(3,6) count_out(3) by auto
      moreover have "time' S'' = time' S\<^sub>2"
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_none_case(4) apply(cases rule: ttc.cases)
        using count_out(4) by (cases rule: ttc.cases; auto)+
      ultimately show ?thesis by simp
    qed
  next
    case (count_out_case I S' \<sigma> I' i f u p d \<tau> S'' I\<^sub>1 S\<^sub>2 I\<^sub>2)
    from count_out_case(5) 
    show ?case
    proof(cases rule: ttc_lg.count.cases)
      case count_nop
      then show ?thesis by simp
    next
      case (count_in \<sigma>' S\<^sub>1 u' f' A' \<tau>' i')
      hence "i = i'" by simp
      have "time' S' = time' S\<^sub>1" using count_out_case(3,6) count_in(3) \<open>i = i'\<close> by auto
      thus ?thesis         
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_out_case(4) apply(cases rule: ttc.cases)
        using count_in(4) apply(cases rule: ttc.cases; auto)
        subgoal for _ _ _ _ _ _ s\<^sub>E'
          by (cases "rev s\<^sub>E'") auto
        done
    next
      case (count_none S\<^sub>1 i')
      hence "i = i'" by simp
      have "time' S' = time' S\<^sub>1" using count_out_case(3,6) count_none(2) \<open>i = i'\<close> by auto
      from \<open>i = i'\<close> have "I' \<approx>\<^sup>I\<^sub>l I\<^sub>2" using count_out_case(3,6) count_none(2) by auto
      moreover have "time' S'' = time' S\<^sub>2"       
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_out_case(4) apply(cases rule: ttc.cases)
        using count_none(3) by (cases rule: ttc.cases) auto
      ultimately show ?thesis by simp
    next
      case (count_out \<sigma>' S\<^sub>1 i' f' u' p' d' \<tau>')
      hence "i = i'" by simp
      hence "time' S' = time' S\<^sub>1" using count_out_case(3,6) count_out(3) by auto
      from \<open>i = i'\<close> have "I' \<approx>\<^sup>I\<^sub>l I\<^sub>2" using count_out_case(3,6) count_out(3) by auto
      moreover have "time' S'' = time' S\<^sub>2"
        apply(insert \<open>time' S' = time' S\<^sub>1\<close>)
        using count_out_case(4) apply(cases rule: ttc.cases)
        using count_out(4) by (cases rule: ttc.cases) auto
      ultimately show ?thesis by simp
    qed
  qed
qed

lemma count_ind_in_seq: 
  assumes "ttc_lg.wf_in_seq' I" 
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i"
      and "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i"      
      and "I \<approx>\<^sup>I\<^sub>l I'"
    shows "I\<^sub>1 \<approx>\<^sup>I\<^sub>l I\<^sub>2"
  using count_time'[OF assms] by blast

lemma ttrans_count_time:
  "ttc_lg.wf_in_seq' I
   \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1
   \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2
   \<Longrightarrow> time S\<^sub>1 = time S\<^sub>2
   \<Longrightarrow> S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
  apply(insert find_f_times')
  apply(erule exE)
  subgoal for f
    apply(rule ttc_lg.ttrans_count_fun[where f=f and g=time])
    using ttc_lg.wf_in_seq'_def by blast+
  done

lemma ttrans_sum_times: 
 "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma> S I' 
  \<Longrightarrow> time' S = Track' u f \<tau> i \<or> time' S = Control' u f \<tau> i
  \<Longrightarrow> i < length (\<pi> f) \<and> (\<exists>H j A \<tau>\<^sub>0. I = H @ I' \<and> length H > 0 \<and> H ! (length H-1) = (u, f, A, \<tau>\<^sub>0) \<and> \<tau> = \<tau>\<^sub>0 + sum_times f i)"
  apply(induct arbitrary: u f \<tau> i rule: ttc_lg.ttrans_induct)
  apply(simp add: S\<^sub>0_def)
  apply(erule ttc.cases; auto)
  using PL_nonempty_f apply blast
  apply(drule ttc_lg.ttrans_conc, assumption; erule exE)
  subgoal for I \<sigma> f A \<tau> T u s\<^sub>E s\<^sub>I s\<^sub>M H
    apply(cases "rev s\<^sub>E"; auto)
    by (auto simp add: sum_times_def)
  by (erule ttc.cases; auto split: if_splits simp add: sum_times_Suc)+

lemma ttrans_sum_times': 
 "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma> S I' 
  \<Longrightarrow> time' S = Track' u f \<tau> i \<or> time' S = Control' u f \<tau> i
  \<Longrightarrow> i < length (\<pi> f) \<and> (\<exists>j A \<tau>\<^sub>0. j < length I \<and> I ! j = (u, f, A, \<tau>\<^sub>0) \<and> \<tau> = \<tau>\<^sub>0 + sum_times f i)"
  apply(drule ttrans_sum_times, assumption+)
  apply(elim conjE exE; simp)
  subgoal for H 
    by (rule exI[where x="length H-1"]) (simp add: trans_less_add1  nth_append)
  done

lemma ttrans_Oo_not_in_seq: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
      and "S \<midarrow>Oo f u p v \<tau>\<rightarrow> S\<^sub>1" 
    shows "\<forall>i \<in> {0..<length I}. tuple_ts (I ! i) \<noteq> \<tau>"
proof -
  from assms(3)
  obtain u' i\<^sub>0 where i\<^sub>0: "time' S = Control' u' f \<tau> i\<^sub>0" 
    by (cases rule: ttc.cases) auto
  then obtain j A \<tau>\<^sub>0 
    where 0: "i\<^sub>0 < length (\<pi> f) \<and> j < length I \<and> I ! j = (u', f, A, \<tau>\<^sub>0) \<and> \<tau> = \<tau>\<^sub>0 + sum_times f i\<^sub>0"
    using ttrans_sum_times'[OF assms(1,2)] by metis
  moreover have "\<forall>i \<in> {0..<length I}. tuple_ts (I ! i) \<noteq> tuple_ts (I ! j) + sum_times f i\<^sub>0"
  proof (rule ballI)
    fix j'
    assume "j' \<in> {0..<length I}"
    show "tuple_ts (I ! j') \<noteq> tuple_ts (I ! j) + sum_times f i\<^sub>0"
    proof(cases "j' \<le> j")
      case t: True
      show ?thesis
      proof(cases "j' = j")
        case True
        thus ?thesis by (simp add: calculation sum_times_noteq_0)
      next
        case False
        hence "j' < j" using t by simp
        hence "tuple_ts (I ! j') < tuple_ts (I ! j)" 
          using assms(1) 0 ttc_lg.wf_in_seq'_less by blast
        moreover have "tuple_ts (I ! j) < tuple_ts (I ! j) + sum_times f i\<^sub>0"
          using 0 sum_times_noteq_0 by auto
        ultimately show ?thesis by simp
      qed
    next
      case False
      hence "j < j'" by simp
      have "tuple_ts (I ! j) + sum_times f i\<^sub>0 \<le> tuple_ts (I ! j) + sum_times f ((length (\<pi> f)) - 1)"
        apply(cases "i\<^sub>0 < length (\<pi> f) - 1"; simp add: less_imp_le times_not_eq2)
        by (metis 0 PL_nonempty_f Suc_pred le_eq_less_or_eq linorder_neqE_nat not_less_eq)
      moreover have "tuple_ts (I ! j) + sum_times f ((length (\<pi> f)) - 1) < tuple_ts (I ! (Suc j))"
        using assms(1) apply(auto simp add: ttc_lg.wf_in_seq'_def wf_in_seq2_def)
        apply(erule ballE[where x=j])
        apply(cases "I ! j"; simp; cases "I ! Suc j"; simp add: 0)
        using \<open>j < j'\<close>  \<open>j' \<in> {0..<length I}\<close> by auto
      moreover have "tuple_ts (I ! (Suc j)) \<le> tuple_ts (I ! j')"
        using ttc_lg.wf_in_seq'_le assms(1) \<open>j < j'\<close> \<open>j' \<in> {0..<length I}\<close> by auto
      ultimately show ?thesis by simp
    qed
  qed
  ultimately show ?thesis by auto
qed


lemma ttrans_\<sigma>: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
      and "i < length \<sigma>"
    shows "\<exists>H i\<^sub>0 f' j' u' A' \<tau>'. 
              I = H @ I' \<and> i\<^sub>0 < length H \<and> H ! i\<^sub>0 = (u', f', A', \<tau>') \<and> j' \<le> length (\<pi> f')
              \<and> (j' = 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>') \<and> (j' > 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>' + sum_times f' (j'-1))
              \<and> (snd (time S) \<in> {2, 3} \<longrightarrow> 
                  (\<exists>u f A \<tau> j. j < length (\<pi> f) \<and> length H > 0 \<and> last H = (u, f, A, \<tau>) \<and> \<tau> \<ge> \<tau>' \<and> (\<tau> = \<tau>' \<longrightarrow> j \<ge> j') 
                        \<and> time' S \<in> {Track' u f (\<tau> + sum_times f j) j, 
                                      Control' u f (\<tau> + sum_times f j) j}))
              \<and> (snd (time S) \<notin> {2, 3} 
                  \<longrightarrow> (H = [] \<longrightarrow> time' S = Taint' 0) 
                    \<and> (H \<noteq> [] \<longrightarrow> (\<exists>u f A \<tau>. last H = (u, f, A, \<tau>) \<and> time' S = Taint' (\<tau> + sum_times f (length (\<pi> f) -1)))))"
  using assms apply(induct rule: ttc_lg.ttrans_induct)
  apply(simp add: S\<^sub>0_def)
  apply(erule ttc.cases; auto)
  subgoal for I \<sigma> u f A T \<tau> s\<^sub>E s\<^sub>I s\<^sub>M
    apply(frule ttc_lg.ttrans_conc, assumption)
    apply(erule exE)
    subgoal for H
      apply(rule exI[where x="H @ [(u, f, A, \<tau>)]"])
      apply(cases "i = length \<sigma>"; simp)
      subgoal
        apply(rule exI[where x="length H"]; auto)
        using PL_nonempty_f by (auto simp add: ttc_lg.In_labels_def sum_times_0)
      subgoal
        apply(erule meta_impE)
        apply(smt append_self_conv length_append length_append_singleton less_SucE ttc_lg.In_labels_def)
        apply(elim exE conjE)
        subgoal for i\<^sub>0 f' j u' A' \<tau>'
          apply(rule exI[where x=i\<^sub>0])
          apply(intro exI[where x=A'] exI[where x=\<tau>'] conjI, simp)
          apply(subgoal_tac "(\<sigma> @ ttc_lg.In_labels u f A \<tau>) ! i = \<sigma> ! i" "\<tau>' < \<tau>")
          apply(rule exI[where x=f'], rule exI[where x=j], rule exI[where x=u'], 
                rule exI[where x=A'], rule exI[where x=\<tau>'], intro conjI)
          using sum_times_0 PL_nonempty_f apply(auto simp add: nth_append length_append_singleton less_SucE)[8]
          apply(unfold wf_in_seq_alt ttc_lg.wf_in_seq'_def)
          apply(erule conjE, erule allE[where x=i\<^sub>0], erule allE[where x="length H"])
          apply(simp add: nth_append)
          by (smt append_Nil2 length_append length_append_singleton less_SucE nth_append ttc_lg.In_labels_def)
        done
      done
    done
  apply(erule ttc.cases; simp)
  subgoal for I \<sigma> S' I' S'' u' u f i' p s\<^sub>M v \<alpha> b s\<^sub>E \<tau> s'\<^sub>E \<sigma>' s\<^sub>I
    apply(elim exE conjE; clarsimp)
    subgoal for H i\<^sub>0 f' j' u'' A \<tau>' A' \<tau>''
      apply(cases "length (\<pi> f) - 1 \<le> i'"; simp)
      apply(rule exI[where x=i\<^sub>0]; simp; rule exI[where x=j']; simp)
      apply(metis One_nat_def PL.PL_nonempty_f PL_axioms Suc_diff_1 le_less_Suc_eq)
      apply(rule exI[where x=i\<^sub>0]; simp; rule exI[where x=j']; simp)
      using le_Suc_eq sum_times_Suc by auto
    done
  apply(erule ttc.cases; simp)
  subgoal for I \<sigma> S' I' f u d \<tau> S'' u' u'' i' p' s\<^sub>M \<alpha> b s\<^sub>E s'\<^sub>E s\<^sub>I
    apply(frule ttc_lg.ttrans_conc, assumption)
    apply(erule exE)
    subgoal for H
      apply(cases "i = length \<sigma>"; simp)
      subgoal
        apply(cases "length (\<pi> f) - 1 \<le> i'"; simp)
        apply(drule ttrans_sum_times, assumption, rule disjI2, simp)
        apply(elim conjE exE)
        subgoal for H' 
          apply(rule exI[where x="length H'-1"], rule conjI, simp, elim exE)
          subgoal for j A \<tau>\<^sub>0
            apply(rule exI[where x=f], rule exI[where x="i'+1"], rule exI[where x=u''], 
                  rule exI[where x=A], rule exI[where x=\<tau>\<^sub>0]; simp)
            apply(rule exI[where x=u''], rule exI[where x=f], rule exI[where x=A], 
                  rule exI[where x=\<tau>\<^sub>0]; simp)
            by (smt One_nat_def PL_nonempty_f Suc_pred last_conv_nth le_less_Suc_eq)
          done
        apply(drule ttrans_sum_times, assumption, rule disjI2, simp)
        apply(elim conjE exE)
        subgoal for H' 
          apply(rule exI[where x="length H'-1"], rule conjI, simp, elim exE)
          subgoal for j A \<tau>\<^sub>0
            apply(rule exI[where x=f], rule exI[where x="i'+1"], rule exI[where x=u''], 
                  rule exI[where x=A], rule exI[where x=\<tau>\<^sub>0]; simp)
            apply(rule exI[where x=u''], rule exI[where x=f]; auto)
            by (rule exI[where x=A], rule exI[where x=\<tau>\<^sub>0]; simp add: sum_times_Suc last_conv_nth)
          done
        done
      subgoal
        apply(elim exE conjE; clarsimp)
        subgoal for i\<^sub>0 f' j' u''' A \<tau>' A' \<tau>''
          apply(cases "length (\<pi> f) - 1 \<le> i'"; simp)
          subgoal
            apply(rule exI[where x=i\<^sub>0], rule conjI, simp)
            apply(cases "j' = 0"; simp)
            by (rule exI[where x=j']; simp add: nth_append last_conv_nth;
                smt One_nat_def PL_nonempty_f Suc_pred last_conv_nth le_less_Suc_eq)+
          subgoal
            apply(rule exI[where x=i\<^sub>0], rule conjI, simp)
            apply(cases "j' = 0"; simp)
            apply(rule exI[where x=0]; simp add: sum_times_Suc nth_append)
            apply(rule exI[where x=j']; simp add: sum_times_Suc nth_append)
            by force
          done
        done
      done
    done
  done

fun time_of_time' :: "Time' \<Rightarrow> ts \<times> nat" where
  "time_of_time' (Taint' \<tau>) = (\<tau>, 4)"
| "time_of_time' (Track' u f \<tau> i) = (\<tau>, 2)"
| "time_of_time' (Control' u f \<tau> i) = (\<tau>, 3)"

lemma time_of_time': "time S = time_of_time' (time' S)"
  apply(cases S)
  subgoal for s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M
    apply(cases s\<^sub>W; auto)
    by (cases "rev s\<^sub>E") auto
  done

lemma snd_time_in: "snd (time S) \<in> {2, 3, 4}"
  apply(cases S; auto)
  subgoal for s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M
    apply(cases s\<^sub>W; auto)  
    by (cases "rev s\<^sub>E") auto
  done

lemma ttrans_\<sigma>_less_time:
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
      and "i < length \<sigma>"
    shows "(fst (\<sigma> ! i), 4) \<preceq> time S"
proof -
  obtain H i\<^sub>0 f' j' u' A' \<tau>' where H:
     "I = H @ I' \<and> i\<^sub>0 < length H \<and> H ! i\<^sub>0 = (u', f', A', \<tau>') \<and> j' \<le> length (\<pi> f')              
      \<and> (j' = 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>') \<and> (j' > 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>' + sum_times f' (j'-1))
     \<and> (snd (time S) \<in> {2, 3} 
           \<longrightarrow> (\<exists>u f A \<tau> j. j < length (\<pi> f) \<and> length H > 0 \<and> last H = (u, f, A, \<tau>) \<and> \<tau> \<ge> \<tau>' \<and> (\<tau> = \<tau>' \<longrightarrow> j \<ge> j') 
                            \<and> time' S \<in> {Track' u f (\<tau> + sum_times f j) j, 
                                          Control' u f (\<tau> + sum_times f j) j}))
      \<and> (snd (time S) \<notin> {2, 3} 
           \<longrightarrow> (H = [] \<longrightarrow> time' S = Taint' 0) 
             \<and> (H \<noteq> [] \<longrightarrow> (\<exists>u f A \<tau>. last H = (u, f, A, \<tau>) \<and> time' S = Taint' (\<tau> + sum_times f (length (\<pi> f) -1)))))"
    apply(insert ttrans_\<sigma>[OF assms])
    apply(elim exE)
    subgoal for H i\<^sub>0 f' j' u' A' \<tau>'
      by (erule meta_allE[where x=H], erule meta_allE[where x=i\<^sub>0], erule meta_allE[where x=u'],
            erule meta_allE[where x=f'], erule meta_allE[where x=A'], erule meta_allE[where x=\<tau>'],
            erule meta_allE[where x=j']; elim meta_impE conjE, intro conjI; assumption)
    done
  show ?thesis
  proof(cases "snd (time S) \<in> {2, 3}")
    case True
    then obtain u f A \<tau> j where u:
      "j < length (\<pi> f) \<and> length H > 0 \<and> last H = (u, f, A, \<tau>) \<and> \<tau> \<ge> \<tau>' \<and> (\<tau> = \<tau>' \<longrightarrow> j \<ge> j')
       \<and> time' S \<in> {Track' u f (\<tau> + sum_times f j) j, Control' u f (\<tau> + sum_times f j) j}"
      using H by metis
    hence 0: "last H = H ! (length H - 1)" by (auto simp add: last_conv_nth)
    from u obtain i where "time S = (\<tau> + sum_times f j, i)" "i \<in> {2, 3}"
      apply(cases "time' S = Track' u f (\<tau> + sum_times f j) j")
      using time_of_time' by auto
    moreover have "j' > 0 \<Longrightarrow> \<tau>' + sum_times f' (j'-1) < \<tau> + sum_times f j"
      apply(cases "i\<^sub>0 = length H-1")
      subgoal
        apply(subgoal_tac "\<tau> = \<tau>'" "f = f'"; simp)
        apply(rule times_not_eq2)
        using H u 0 by force+
      subgoal
        apply(rule times_not_eq')
        apply(rule wf_in_seq'_spec3[OF assms(1), of i\<^sub>0 "length H-1" u' _ A' _ u f A])
        using H u 0 apply(force, force)
        using H u 0 apply(auto simp add: nth_append)[2]
        using H by force
      done
    moreover have "j' = 0 \<Longrightarrow> \<tau>' < \<tau> + sum_times f j"
      using u sum_times_noteq_0 by (cases "\<tau> = \<tau>'") auto
    ultimately show ?thesis
      using H by (cases "j' = 0") (auto simp add: orde_time.intros ord_time.intros)
  next
    case False
    hence "(H = [] \<and> time' S = Taint' 0) 
         \<or> (H \<noteq> [] \<and> (\<exists>u f A \<tau>. last H = (u, f, A, \<tau>) \<and> time' S = Taint' (\<tau> + sum_times f (length (\<pi> f) -1))))" 
      using H by auto
    thus ?thesis
    proof
      assume "H = [] \<and> time' S = Taint' 0"
      thus "(fst (\<sigma> ! i), 4) \<preceq> time S" using H by auto
    next
      assume a: "H \<noteq> [] \<and> (\<exists>u f A \<tau>. last H = (u, f, A, \<tau>) \<and> time' S = Taint' (\<tau> + sum_times f (length (\<pi> f) -1)))"
      then obtain u f A \<tau> where u: "last H = (u, f, A, \<tau>) \<and> time' S = Taint' (\<tau> + sum_times f (length (\<pi> f) -1))"
        by metis
      from a have 0: "last H = H ! (length H - 1)" by (auto simp add: last_conv_nth)
      moreover have "j' > 0 \<Longrightarrow> \<tau>' + sum_times f' (j'-1) \<le> \<tau> + sum_times f (length (\<pi> f) -1)"
        apply(cases "i\<^sub>0 = length H-1")
        subgoal
          apply(subgoal_tac "\<tau> = \<tau>'" "f = f'"; simp)
          apply(cases "j' = length (\<pi> f')", simp)
          apply(rule less_imp_le)
          using times_not_eq2 PL_nonempty_f H u 0 by force+
        subgoal
          apply(rule less_imp_le, rule times_not_eq')
          apply(rule wf_in_seq'_spec3[OF assms(1), of i\<^sub>0 "length H-1" u' _ A' _ u f A])
          using H u 0 apply(force, force)
          using H u 0 apply(auto simp add: nth_append)[2]
          using H by force
        done
      moreover have "j' = 0 \<Longrightarrow> \<tau>' \<le> \<tau> + sum_times f (length (\<pi> f) - 1)"
        apply(cases "i\<^sub>0 = length H-1")
        subgoal
          apply(subgoal_tac "\<tau> = \<tau>'" "f = f'"; simp)
          apply(cases "j' = length (\<pi> f')", simp)
          using times_not_eq2 PL_nonempty_f H u 0 by force+
        subgoal
          apply(rule less_imp_le)
          apply(subgoal_tac "\<tau>' < \<tau>", force)
          apply(rule wf_in_seq'_spec4[OF assms(1), of i\<^sub>0 "length H-1" u' _ A' _ u f A])
          using H apply force          
          using H u 0 by (auto simp add: nth_append)
        done
      ultimately show ?thesis
        using H u time_of_time' by (cases "j' = 0") (auto simp add: orde_time_alt_intro)
    qed
  qed
qed


lemma count_fun_time:
  assumes "ttc_lg.wf_in_seq' I"
      and "I \<approx>\<^sup>I\<^sub>l I'"
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1"
      and "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
      and "time S\<^sub>1 \<preceq> time S\<^sub>2"
    shows "i\<^sub>1 \<le> i\<^sub>2"
proof -
  have "ttc_lg.wf_in_seq' I'" using assms(1,2) using ttc_lg.ind_wf_in_seq' by blast
  then obtain f where f:
    "\<And>\<sigma> S I'' i. ttc_lg.count S\<^sub>0 I \<sigma> S I'' i \<Longrightarrow> f I i = time S"
    "\<And>\<sigma> S I'' i. ttc_lg.count S\<^sub>0 I' \<sigma> S I'' i \<Longrightarrow> f I' i = time S"
    "\<And>i. f I i = f I' i"
    "\<forall>i. f I i \<prec> f I (i+1)"
  using find_f_times assms(1,2,3) by smt
  from assms(5) show ?thesis
  proof(cases rule: orde_time.cases)
    case (1 \<tau>')
    hence "i\<^sub>1 = i\<^sub>2" 
      using ord_time_inj[of f I, OF f(4), of i\<^sub>1 i\<^sub>2] f(1)[OF assms(3)] f(2)[OF assms(4)] f(3)[of i\<^sub>2] 
      by smt
    thus ?thesis by simp
  next
    case 2
    hence "f I i\<^sub>1 \<prec> f I' i\<^sub>2" using f(1,2) assms(3,4) by smt
    hence "f I i\<^sub>1 \<prec> f I i\<^sub>2" using f(3) by smt
    thus ?thesis using ord_time_mono'[of f I, OF f(4)] by force
  qed
qed

lemma ttrans_time_step_less:
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S I\<^sub>1"
      and "S \<midarrow>Oo f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau>\<^sub>1\<rightarrow> S\<^sub>1"
      and "time S \<prec> time S\<^sub>2"
    shows "time S\<^sub>1 \<preceq> time S\<^sub>2"
proof -
  obtain f where f:
    "\<And>\<sigma> S I'' i. ttc_lg.count S\<^sub>0 I \<sigma> S I'' i \<Longrightarrow> f I i = time S"
    "\<forall>i. f I i \<prec> f I (i+1)"
  using find_f_times assms(1) by smt
  obtain i\<^sub>1 where i\<^sub>1: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S I\<^sub>1 i\<^sub>1" 
    using ttc_lg.count_ttrans[OF assms(1)] assms(3) by metis
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
    using ttc_lg.count_ttrans[OF assms(1)] assms(2) by metis
  have "i\<^sub>1+1 \<le> i\<^sub>2" using assms(5) i\<^sub>1 i\<^sub>2 f(1) ord_time_mono'[of f, OF f(2), of i\<^sub>1 i\<^sub>2] by simp
  moreover have "ttc_lg.count S\<^sub>0 I (\<sigma>\<^sub>1 @ [(\<tau>\<^sub>1, {Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1})]) S\<^sub>1 I\<^sub>1 (i\<^sub>1+1)"
    using ttc_lg.count_out i\<^sub>1 assms(4) by auto
  ultimately show "time S\<^sub>1 \<preceq> time S\<^sub>2"
    apply(cases "i\<^sub>1+1 = i\<^sub>2")
    using ttc_lg.ttrans_determ i\<^sub>2 orde_time.intros(1) apply blast
    using i\<^sub>2 f(1) ord_time_mono[of f, OF f(2), of "i\<^sub>1+1" i\<^sub>2] by (simp add: orde_time.simps)
qed

lemma ttrans_Oo_time:
  assumes "ttc_lg.wf_in_seq' I'"
      and "ttc_lg.ttrans S\<^sub>0 I' \<sigma>'' S\<^sub>2 I'\<^sub>1"
      and "\<sigma>'' = \<sigma>' @ [(\<tau>\<^sub>2, D')]"
      and "I \<approx>\<^sup>I\<^sub>l I'"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I\<^sub>1"
      and "S \<midarrow>Oo f u p v \<tau>\<rightarrow> S\<^sub>1"
      and "\<tau>\<^sub>2 \<ge> \<tau>"
    shows "\<exists>\<sigma>\<^sub>0 S' I''. ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>0 S' I'' \<and> time S' = (\<tau>, 3)"
proof -
  obtain f where f:
    "\<And>I \<sigma> S I' i. ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> f I i = time S"
    "\<And>i. f I i = f I' i"
    using find_f_times' assms(1,4) by metis
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I' \<sigma>'' S\<^sub>2 I'\<^sub>1 i\<^sub>2"
    using ttc_lg.count_ttrans[OF assms(1)] assms(2) by metis
  have "ttc_lg.wf_in_seq' I" 
    using assms(1,4) ttc_lg.ind_wf_in_seq' equivp_symp[OF equiv_ind_in_seq] by metis
  obtain i\<^sub>1 where i\<^sub>1: "ttc_lg.count S\<^sub>0 I \<sigma> S I\<^sub>1 i\<^sub>1"
    using ttc_lg.count_ttrans[OF \<open>ttc_lg.wf_in_seq' I\<close>] assms(5) by metis
  have "time S = (\<tau>, 3)"
    using assms(6) by (cases rule: ttc.cases) auto
  have "(\<tau>\<^sub>2, 3) \<preceq> time S\<^sub>2"
    apply(rule orde_time_trans[where y="(\<tau>\<^sub>2, 4)"])
    apply(simp add: orde_time_alt_intro)
    using ttrans_\<sigma>_less_time assms(1-3) by fastforce
  have "time S \<preceq> time S\<^sub>2"
    apply(rule orde_time_trans[where y="(\<tau>\<^sub>2, 3)"])
    using \<open>time S = (\<tau>, 3)\<close> assms(7) apply(cases "\<tau> = \<tau>\<^sub>2"; simp add: orde_time.intros ord_time.intros)
    using \<open>(\<tau>\<^sub>2, 3) \<preceq> time S\<^sub>2\<close> by assumption
  hence "i\<^sub>1 \<le> i\<^sub>2" using count_fun_time[OF \<open>ttc_lg.wf_in_seq' I\<close> assms(4) i\<^sub>1 i\<^sub>2] by simp
  then obtain \<sigma>\<^sub>0 S' I'' where \<sigma>\<^sub>0: "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>0 S' I'' i\<^sub>1"
    using ttc_lg.ttrans_count_le[OF assms(1) i\<^sub>2] by force
  have "time S' = (\<tau>, 3)"
    using \<open>time S = (\<tau>, 3)\<close> f(1)[OF assms(1) \<sigma>\<^sub>0] f(1)[OF \<open>ttc_lg.wf_in_seq' I\<close> i\<^sub>1] f(2) by auto
  thus ?thesis using \<sigma>\<^sub>0 ttc_lg.count_ttrans assms(1) \<open>ttc_lg.wf_in_seq' I\<close> by metis
qed



lemma count_\<sigma>_conc: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1"
      and "i\<^sub>1 < i\<^sub>2"
    shows "\<exists>\<sigma>. \<sigma>\<^sub>2 = \<sigma>\<^sub>1 @ \<sigma>"
  using assms apply(induct rule: ttc_lg.count_induct; auto)
  by (smt append.assoc append_self_conv less_SucE ttc_lg.ttrans_determ)+

lemma ttrans_\<sigma>_conc: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1"
      and "time S\<^sub>1 \<preceq> time S\<^sub>2"
    shows "\<exists>\<sigma>. \<sigma>\<^sub>2 = \<sigma>\<^sub>1 @ \<sigma>"
proof -
  obtain i\<^sub>1 where i\<^sub>1: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1" 
    using assms(3) ttc_lg.count_ttrans[OF assms(1)] by metis
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
    using assms(2) ttc_lg.count_ttrans[OF assms(1)] by metis
  from assms(4)
  show ?thesis
  proof (cases rule: orde_time.cases)
    case (1 \<tau>)
    hence "\<sigma>\<^sub>1 = \<sigma>\<^sub>2" using ttrans_count_time[OF assms(1,3,2)] by simp
    thus ?thesis by simp
  next
    case 2
    hence "i\<^sub>1 \<le> i\<^sub>2" 
      using ord_time_mono'[of ff I, OF f3'[OF assms(1)], of i\<^sub>1 i\<^sub>2] 
      using f1[OF assms(1) i\<^sub>1] f1[OF assms(1) i\<^sub>2] by auto
    thus ?thesis
      apply(cases "i\<^sub>1 < i\<^sub>2")
      apply(erule count_\<sigma>_conc[OF assms(1) i\<^sub>2 i\<^sub>1])
      using ttrans_count_time[OF assms(1,3,2)] f1[OF assms(1) i\<^sub>1] f1[OF assms(1) i\<^sub>2]  by simp
  qed
qed


lemma ttrans_\<sigma>_less:
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
      and "i < length \<sigma>"
      and "i' < length I'"
    shows "fst (\<sigma> ! i) < tuple_ts (I' ! i')"
proof -
  obtain H i\<^sub>0 u' f' A' \<tau>' j' where
    H: "I = H @ I' \<and> i\<^sub>0 < length H \<and> H ! i\<^sub>0 = (u', f', A', \<tau>') \<and> j' \<le> length (\<pi> f')
              \<and> (j' = 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>') \<and> (j' > 0 \<longrightarrow> fst (\<sigma> ! i) = \<tau>' + sum_times f' (j'-1))
              \<and> (snd (time S) \<in> {2, 3} \<longrightarrow> 
                  (\<exists>u f A \<tau> i\<^sub>1 j. j < length (\<pi> f) \<and> length H > 0 \<and> last H = (u, f, A, \<tau>) \<and> \<tau> \<ge> \<tau>' \<and> (\<tau> = \<tau>' \<longrightarrow> j \<ge> j') 
                        \<and> time' S \<in> {Track' u f (\<tau> + sum_times f j) j, 
                                      Control' u f (\<tau> + sum_times f j) j}))"
    using ttrans_\<sigma>[OF assms(1-3)] by force
  moreover have "tuple_ts (I ! i\<^sub>0) < tuple_ts (I ! (length H+i'))"
    using assms(1)[unfolded ttc_lg.wf_in_seq'_def wf_in_seq_alt, THEN conjunct1, rule_format, of i\<^sub>0 "length H + i'"]
    using H assms(4) by auto
  moreover have "tuple_ts (I ! i\<^sub>0) + sum_times f' (j'-1) < tuple_ts (I ! (i'+length H))"
    apply(subgoal_tac "tuple_fn (I ! i\<^sub>0) = f'" "j'-1 < length (\<pi> f')")
    apply(rule subst, assumption)
    apply(rule wf_in_seq'_spec)
    using H assms(1,4) apply auto[4]
    apply(smt H One_nat_def PL_nonempty_f diff_Suc_less le_eq_less_or_eq less_imp_diff_less)
    using H by (auto simp add: nth_append)
  ultimately show ?thesis using H by (cases "j' = 0") (auto simp add: nth_append)
qed

lemma ttrans_\<sigma>_mono:
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
    shows "\<forall>i j. i < j \<longrightarrow> j < length \<sigma> \<longrightarrow> fst (\<sigma> ! i) < fst (\<sigma> ! j)"
  using assms apply(induct rule: ttc_lg.ttrans_induct; simp)
  subgoal for I \<sigma>' S' u f A \<tau> T S''
    apply(intro allI impI)
    subgoal for i j
      apply(cases "j < length \<sigma>'")
      apply(erule allE[where x=i], erule allE[where x=j])
      apply(simp add: nth_append)
      apply(subgoal_tac "j = length \<sigma>'")
      apply(subgoal_tac "fst (\<sigma>' ! i) < \<tau>")
      apply(auto simp add: ttc_lg.In_labels_def nth_append)[1]
      apply(frule ttc_lg.ttrans_conc, assumption)
      apply(erule exE)
      subgoal for H
        using ttrans_\<sigma>_less[of I \<sigma>' S' "(u, f, A, \<tau>) # T", rule_format, of i 0] by auto
      by (smt append_self_conv length_append length_append_singleton linorder_neqE_nat not_less_eq 
              ttc_lg.In_labels_def)        
    done
  subgoal for I \<sigma>' S' I' f u p d \<tau> S''
    apply(intro allI impI)
    subgoal for i j
      apply(cases "j < length \<sigma>'")
      apply(erule allE[where x=i], erule allE[where x=j])
      apply(simp add: nth_append)
      apply(subgoal_tac "j = length \<sigma>'")
      apply(subgoal_tac "fst (\<sigma>' ! i) < \<tau>")
      apply(auto simp add: ttc_lg.In_labels_def nth_append)[1]
      apply(erule ttc.cases; auto)
      apply(frule ttrans_\<sigma>; assumption?)
      subgoal for u'' i'' s\<^sub>M \<alpha> s\<^sub>E s\<^sub>I
        apply(elim exE conjE; simp)+
        subgoal for H i\<^sub>0 f' j' u' A' \<tau>' u f A \<tau>'' 
          apply(cases "j' = 0"; simp)
          apply(metis atLeastLessThan_iff le_antisym less_add_same_cancel1 less_eq_nat.simps(1) linorder_not_less sum_times_noteq_0 trans_less_add1)
          apply(rule wf_in_seq'_spec2[where I="H@I'" and i=i\<^sub>0 and j="length H-1"], simp)
          apply(unfold ttc_lg.wf_in_seq'_def[where I="H @ I'"] wf_in_seq_alt, drule conjunct1)
          apply(rotate_tac -1, erule allE[where x="length H-1"], rotate_tac -1, erule allE[where x=i\<^sub>0])
          by (auto simp add: nth_append last_conv_nth)
        done
      by auto
    done
  done

lemma ttrans_\<sigma>_\<tau>: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I (\<sigma>\<^sub>1 @ [(\<tau>, D\<^sub>1)]) S\<^sub>1 I\<^sub>1"
      and "ttc_lg.ttrans S\<^sub>0 I (\<sigma>\<^sub>2 @ [(\<tau>, D\<^sub>2)]) S\<^sub>2 I\<^sub>2"
    shows "\<sigma>\<^sub>1 = \<sigma>\<^sub>2 \<and> D\<^sub>1 = D\<^sub>2"
proof -
  obtain i\<^sub>1 where i\<^sub>1: "ttc_lg.count S\<^sub>0 I (\<sigma>\<^sub>1 @ [(\<tau>, D\<^sub>1)]) S\<^sub>1 I\<^sub>1 i\<^sub>1"
    using ttc_lg.count_ttrans assms(1,2) by metis
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I (\<sigma>\<^sub>2 @ [(\<tau>, D\<^sub>2)]) S\<^sub>2 I\<^sub>2 i\<^sub>2"
    using ttc_lg.count_ttrans assms(1,3) by metis
  show ?thesis
  proof (cases "i\<^sub>1 \<le> i\<^sub>2")
    case t: True
    show ?thesis
    proof (cases "i\<^sub>1 < i\<^sub>2")
      case True
      then obtain \<sigma> where \<sigma>: "\<sigma>\<^sub>2 @ [(\<tau>, D\<^sub>2)] = \<sigma>\<^sub>1 @ [(\<tau>, D\<^sub>1)] @ \<sigma>"
        using count_\<sigma>_conc[OF assms(1) i\<^sub>2 i\<^sub>1] by auto
      thus ?thesis 
        apply(cases "length \<sigma>\<^sub>1 = length \<sigma>\<^sub>2"; simp?)
        apply(cases "length \<sigma>\<^sub>1 < length \<sigma>\<^sub>2")
        apply(insert ttrans_\<sigma>_mono[OF assms(1,3), rule_format, of "length \<sigma>\<^sub>1" "length \<sigma>\<^sub>2"])[1]
        apply(smt fst_conv length_append_singleton lessI nat_less_le nth_append_length)
        apply(insert ttrans_\<sigma>_mono[OF assms(1,3), rule_format, of "length \<sigma>\<^sub>2" "length \<sigma>\<^sub>1"])
        by (metis append.right_neutral conc_length fst_conv le_add1 length_append linorder_not_less 
                  list.distinct(1) nat_neq_iff nth_append_length)
    next
      case False
      hence "i\<^sub>1 = i\<^sub>2" using t by simp
      thus ?thesis using ttc_lg.ttrans_determ[of I "\<sigma>\<^sub>1 @ [(\<tau>, D\<^sub>1)]" S\<^sub>1 I\<^sub>1 i\<^sub>1 "\<sigma>\<^sub>2 @ [(\<tau>, D\<^sub>2)]"] i\<^sub>1 i\<^sub>2 by auto
    qed
  next
    case False
    then obtain \<sigma> where \<sigma>: "\<sigma>\<^sub>1 @ [(\<tau>, D\<^sub>1)] = \<sigma>\<^sub>2 @ [(\<tau>, D\<^sub>2)] @ \<sigma>"
      using count_\<sigma>_conc[OF assms(1) i\<^sub>1 i\<^sub>2] by auto
    thus ?thesis 
      apply(cases "length \<sigma>\<^sub>1 = length \<sigma>\<^sub>2"; simp?)
      apply(cases "length \<sigma>\<^sub>1 < length \<sigma>\<^sub>2")
      apply(insert ttrans_\<sigma>_mono[OF assms(1,3), rule_format, of "length \<sigma>\<^sub>1" "length \<sigma>\<^sub>2"])[1]
      apply(smt Suc_leI diff_add_inverse diff_is_0_eq' length_Cons length_append 
                length_append_singleton nat.simps(3))
      apply(insert ttrans_\<sigma>_mono[OF assms(1,3), rule_format, of "length \<sigma>\<^sub>2" "length \<sigma>\<^sub>1"])
      by (metis PL.ttrans_\<sigma>_mono PL_axioms assms(1,2) fst_conv length_append_singleton lessI 
                nat_neq_iff nth_append_length)
  qed
qed

lemma ttrans_Oo_same_\<tau>: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1"
      and "S\<^sub>1 \<midarrow>Oo f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau>\<rightarrow> S'\<^sub>1"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2" 
      and "S\<^sub>2 \<midarrow>Oo f\<^sub>2 u\<^sub>2 p\<^sub>2 v\<^sub>2 \<tau>\<rightarrow> S'\<^sub>2" 
    shows "(S\<^sub>1, I\<^sub>1, f\<^sub>1, u\<^sub>1, p\<^sub>1, v\<^sub>1, S'\<^sub>1) = (S\<^sub>2, I\<^sub>2, f\<^sub>2, u\<^sub>2, p\<^sub>2, v\<^sub>2, S'\<^sub>2)" 
proof -
  obtain f where f:
    "\<And>\<sigma> S I' i. ttc_lg.count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> f I i = time S"
    "\<And>i\<^sub>1 i\<^sub>2. f I i\<^sub>1 = f I i\<^sub>2 \<Longrightarrow> i\<^sub>1 = i\<^sub>2"
    using find_f_times' assms(1) by metis
  obtain i\<^sub>1 where "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1"
    using ttc_lg.count_ttrans[OF assms(1)] using assms(2) by metis
  hence time\<^sub>1: "f I i\<^sub>1 = time S\<^sub>1" using f(1) by auto
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
    using ttc_lg.count_ttrans[OF assms(1)] using assms(4) by metis
  hence time\<^sub>2: "f I i\<^sub>2 = time S\<^sub>2" using f(1) by auto
  have "time S\<^sub>1 = (\<tau>, 3)"
    using assms(3) by (cases rule: ttc.cases) simp
  moreover have "time S\<^sub>2 = (\<tau>, 3)"
    using assms(5) by (cases rule: ttc.cases) simp
  ultimately have time: "time S\<^sub>1 = time S\<^sub>2"
    by auto
  have "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2" 
    by (rule ttrans_count_time[OF assms(1,2,4) time])
  moreover have "valid_code S\<^sub>1" "valid_code S\<^sub>2"
    using assms(2,4) by (auto simp add: ttc_lg.ttrans_valid_code)
  ultimately show ?thesis
    apply(cases "S'\<^sub>1 \<noteq> S'\<^sub>2 \<or> Oo f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau> \<noteq> Oo f\<^sub>2 u\<^sub>2 p\<^sub>2 v\<^sub>2 \<tau>")
    using determ assms(3,5) by blast+
qed




lemma happy: "ttc_lg.wf_in_seq' I \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) I' \<Longrightarrow> s\<^sub>E \<in> P"
  using A20a by auto

lemma Track_interpreter_state: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.count S\<^sub>0 I \<sigma> (Track u f \<tau> i, s\<^sub>I, s\<^sub>E, s\<^sub>M) I' i\<^sub>1"
  obtains \<gamma> where "s\<^sub>I = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u \<gamma> \<and> i < length (\<pi> f)"
  using assms(2) apply(cases rule: ttc_lg.count.cases)
  apply(auto simp add: assms S\<^sub>0_def)
  apply(erule ttc.cases; auto)
  using PL_nonempty_f apply blast
  apply(erule ttc.cases; simp)
  subgoal for a aa ab ia u' ua fa iaa p v \<alpha> ba \<tau>' s'\<^sub>E \<sigma>'
    by (cases "length (\<pi> fa) - 1 \<le> iaa"; simp)
  apply(erule ttc.cases; simp)
  subgoal for \<sigma>' a aa ab ia fa ua d \<tau>' u' uaa iaa pa \<alpha> ba s'\<^sub>E
    by (cases "length (\<pi> fa) - 1 \<le> iaa"; simp)
  done

lemma Control_interpreter_state: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.count S\<^sub>0 I \<sigma> (Control u f \<tau> i, s\<^sub>I, s\<^sub>E, s\<^sub>M) I' i\<^sub>1"
  obtains \<gamma> where "s\<^sub>I = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u \<gamma> \<and> i < length (\<pi> f)"
  using assms(2) apply(cases rule: ttc_lg.count.cases)
  apply(auto simp add: assms S\<^sub>0_def)
  apply(erule ttc.cases; auto)
  apply(erule ttc.cases; auto)
  apply(erule Track_interpreter_state[OF assms(1), unfolded S\<^sub>0_def]; auto)
  subgoal for aa ab ia ua fa iaa v \<alpha> \<tau>' x
    by (cases "iaa \<ge> length (\<pi> fa) - 1"; simp)
  apply(erule ttc.cases; auto)
  subgoal for \<sigma> aa ab ia fa d \<tau>' ua iaa \<alpha>
    by (cases "iaa \<ge> length (\<pi> fa) - 1"; simp)
  done

subsection \<open>Obtaining indistinguishable states\<close>

lemma same_count_indistinguishable: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i" 
      and "ttc_lg.wf_in_seq' I'"
      and "I \<approx>\<^sup>I\<^sub>l I'"
      and "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i" 
    shows "S\<^sub>1 \<approx>\<^sup>S\<^sub>l S\<^sub>2"
proof -
  show ?thesis
  using assms
  proof (induct arbitrary: S\<^sub>2 I\<^sub>2 \<sigma>\<^sub>2 rule: ttc_lg.count_induct)
    case (count_nop_case I)
    hence "S\<^sub>2 = S\<^sub>0" using ttc_lg.count_0' by metis
    thus ?case by (simp add: equivp_reflp[OF equiv_ind_S])
  next
    case (count_in_case I \<sigma>\<^sub>1 S\<^sub>1' u\<^sub>1 f\<^sub>1 A\<^sub>1 \<tau>\<^sub>1 T\<^sub>1 i\<^sub>1 S\<^sub>1 S\<^sub>2 T\<^sub>2)
    hence \<sigma>\<^sub>1: "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1' ((u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # T\<^sub>1)"
      using ttc_lg.count_ttrans count_in_case(1,2) by blast
    from count_in_case(7)
    show ?case
    proof (cases rule: ttc_lg.count.cases)
      case count_nop
      then show ?thesis by auto
    next
      case (count_in \<sigma>\<^sub>2 S\<^sub>2' u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_in_case(3,5,6) by auto
      hence \<sigma>\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2' ((u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # T\<^sub>2)"
        using ttc_lg.count_ttrans count_in_case(5) count_in(2,3) by blast
      have "((u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # T\<^sub>1) \<approx>\<^sup>I\<^sub>l ((u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # T\<^sub>2)"
        using count_in(2,3) count_in_case(1,2,6,5) by (simp add: count_ind_in_seq)
      hence 0: "u\<^sub>1 = u\<^sub>2 \<and> f\<^sub>1 = f\<^sub>2 \<and> \<tau>\<^sub>1 = \<tau>\<^sub>2 \<and> (A\<^sub>1 \<approx>\<^sup>p\<^sub>l A\<^sub>2)" "A\<^sub>1 \<noteq> A\<^sub>2 \<longrightarrow> (\<exists>a. l = (\<tau>\<^sub>1, a))"
        by (auto, cases rule: ind_in_seq.cases; auto simp add: ind_part_mem.intros(1))+
      from \<open>S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'\<close> show ?thesis
        apply(cases rule: ind_S.cases)
        apply(rule ttc.cases[OF count_in_case(4)]; rule ttc.cases[OF count_in(4)]; clarify)
        apply(rule ind_S.intros)
        using 0(1) apply auto
        apply(rule fun_cong)
        apply(rule ind_c_eq)
        using \<sigma>\<^sub>1 \<sigma>\<^sub>2 count_in_case(1,5) apply (auto simp add: happy)[4] 
        apply(rule ind_if_trace_snoc; simp)
        apply(rule ind_mem.intros; simp)
        apply(cases "A\<^sub>1 = A\<^sub>2")
        apply(simp add: ind_tv_refl ind_mem_ind_tv)
        apply(drule 0(2)[rule_format], erule exE)
        apply(erule ind_part_mem.cases)
        apply(auto simp add: ind_tv_refl ind_mem_ind_tv)
        by (rule ind_tv_tag, rule ind_history_refl, simp)+
    next
      case (count_none S\<^sub>2' i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_in_case(3,5,6) by auto
      hence False
        apply(cases rule: ind_S.cases)
        by (rule ttc.cases[OF count_in_case(4)]; rule ttc.cases[OF count_none(3)]) auto
      thus ?thesis by (rule FalseE)
    next
      case (count_out \<sigma>\<^sub>2 S\<^sub>2' i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_in_case(3,5,6) by auto
      hence False
        apply(cases rule: ind_S.cases)
        by (rule ttc.cases[OF count_in_case(4)]; rule ttc.cases[OF count_out(4)]) auto
      thus ?thesis by (rule FalseE)
    qed
  next 
    case (count_none_case I \<sigma>\<^sub>1' S\<^sub>1' I\<^sub>1' i\<^sub>1 S\<^sub>1)
    obtain \<sigma>\<^sub>1 where \<sigma>\<^sub>1: "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1' I\<^sub>1'"
      using ttc_lg.count_ttrans count_none_case(1,2) by blast
    from count_none_case(7)
    show ?case
    proof (cases rule: ttc_lg.count.cases)
      case count_nop
      then show ?thesis by auto
    next
      case (count_in \<sigma>\<^sub>2 S\<^sub>2' u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_none_case(3,5,6) by auto
      hence False
        apply(cases rule: ind_S.cases)
        by (rule ttc.cases[OF count_none_case(4)]; rule ttc.cases[OF count_in(4)]) auto
      thus ?thesis by (rule FalseE)
    next
      case (count_none S\<^sub>2' i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_none_case(3,5,6) by auto
      obtain \<sigma>\<^sub>2 where \<sigma>\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2' I\<^sub>2"
        using ttc_lg.count_ttrans count_none_case(5) count_none(1,2) by blast
      from \<open>S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'\<close> show ?thesis
        apply(cases rule: ind_S.cases; auto)
        subgoal for s\<^sub>W' s\<^sub>I' s\<^sub>E\<^sub>1' s\<^sub>E\<^sub>2' s\<^sub>M\<^sub>1' s\<^sub>M\<^sub>2'
        proof -
          assume assms: "S\<^sub>1' = (s\<^sub>W', s\<^sub>I', s\<^sub>E\<^sub>1', s\<^sub>M\<^sub>1')" "S\<^sub>2' = (s\<^sub>W', s\<^sub>I', s\<^sub>E\<^sub>2', s\<^sub>M\<^sub>2')"
                        "s\<^sub>E\<^sub>1' \<approx>\<^sup>\<sigma> s\<^sub>E\<^sub>2'" "s\<^sub>M\<^sub>1' \<approx>\<^sup>m\<^sub>l s\<^sub>M\<^sub>2'"
          show "S\<^sub>1 \<approx>\<^sup>S\<^sub>l S\<^sub>2"
          proof (cases s\<^sub>W')
            case Taint
            show ?thesis
              using count_none_case(4) by (cases rule: ttc.cases) (auto simp add: Taint assms(1))
          next
            case (Track u f \<tau> i)
            obtain \<gamma> where \<gamma>: "s\<^sub>I' = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u \<gamma>" "i < length (\<pi> f)"
              apply(rule Track_interpreter_state[OF count_none_case(1)])
              using count_none_case(2)[unfolded assms(1) Track] by auto
            show ?thesis
              apply(rule ttc.cases[OF count_none_case(4)]; rule ttc.cases[OF count_none(3)]; clarify)
              using Track assms apply auto
              apply(rule ind_S.intros)
              apply(rule refl)+
              apply assumption
              apply(rule PL_ni[where i=i and f=f])
              using \<gamma>(2) apply force
              using assms(4) apply assumption
              using \<gamma>(1) by force+
          next
            case (Control u f \<tau> i)
            obtain \<gamma> where \<gamma>: "s\<^sub>I' = Interp (Code (\<pi> f ! i)) pc\<^sub>0 u \<gamma>" "i < length (\<pi> f)"
              apply(rule Control_interpreter_state[OF count_none_case(1)])
              using count_none_case(2)[unfolded assms(1) Control] by auto
            show ?thesis
              apply(rule ttc.cases[OF count_none_case(4)]; rule ttc.cases[OF count_none(3)]; clarify)
              using Control assms apply auto
              apply(rule ind_S.intros)
              using assms(1,2) apply blast+
              using assms(1,2) ind_if_trace_snoc2[OF assms(3)] apply blast
              using assms(4) apply assumption
              apply(rule ind_S.intros)
              apply(rule refl)+
              apply auto[1]
              apply(rule fun_cong)
              apply(rule ind_c_eq2)
              using \<sigma>\<^sub>1 \<sigma>\<^sub>2 assms(3) count_none_case(1,5) unfolding assms(1,2) 
              apply(auto simp add: happy P_snoc)
              using assms(1,2) ind_if_trace_snoc2[OF assms(3)] by blast
          qed
        qed
      done
    next
      case (count_out \<sigma>\<^sub>2 S\<^sub>2' i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_none_case(3,5,6) by auto
      obtain \<sigma>\<^sub>2 where \<sigma>\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2' I\<^sub>2"
        using ttc_lg.count_ttrans count_none_case(5) count_out(2,3) by blast
      from \<open>S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'\<close> show ?thesis
        apply(cases rule: ind_S.cases; auto)
        apply(rule ttc.cases[OF count_none_case(4)]; rule ttc.cases[OF count_out(4)]; clarify)
        apply(rule ind_S.intros)
        apply(rule refl)
        apply clarsimp
        apply(rule fun_cong)
        apply(rule ind_c_eq2)
        apply(rule P_snoc)
        apply(rule happy[OF count_none_case(1)])
        using P_snoc2 \<sigma>\<^sub>1  \<sigma>\<^sub>2 assms(3) happy apply auto[6]
        apply(rule ind_if_trace_snoc2)
        using assms(3) by auto
    qed
  next
    case (count_out_case I S\<^sub>1' \<sigma>\<^sub>1 I\<^sub>1' i\<^sub>1 f\<^sub>1 u\<^sub>1 p\<^sub>1 d\<^sub>1 \<tau>\<^sub>1 S\<^sub>1)
    obtain \<sigma>\<^sub>1 where \<sigma>\<^sub>1: "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1' I\<^sub>1'"
      using ttc_lg.count_ttrans count_out_case(1,2) by blast
    from count_out_case(7)
    show ?case
    proof (cases rule: ttc_lg.count.cases)
      case count_nop
      then show ?thesis by auto
    next
      case (count_in \<sigma>\<^sub>2 S\<^sub>2' u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_out_case(3,5,6) by auto
      hence False
        apply(cases rule: ind_S.cases)
        by (rule ttc.cases[OF count_out_case(4)]; rule ttc.cases[OF count_in(4)]) auto
      thus ?thesis by (rule FalseE)
    next
      case (count_none S\<^sub>2' i\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_out_case(3,5,6) by auto      
      obtain \<sigma>\<^sub>2 where \<sigma>\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2' I\<^sub>2"
        using ttc_lg.count_ttrans count_out_case(5) count_none(1,2) by blast
      from \<open>S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'\<close> show ?thesis
        apply(cases rule: ind_S.cases; auto)
        apply(rule ttc.cases[OF count_out_case(4)]; rule ttc.cases[OF count_none(3)]; clarify)
        apply(rule ind_S.intros)
        apply(rule refl)
        apply clarsimp
        apply(rule fun_cong)
        apply(rule ind_c_eq2)
        apply(rule P_snoc2)
        apply(rule happy[OF count_out_case(1)])
        using P_snoc2 \<sigma>\<^sub>1 \<sigma>\<^sub>2 assms(3) happy apply auto[2]
        apply(rule P_snoc)
        apply(rule happy[OF count_out_case(5)])
        using P_snoc2 \<sigma>\<^sub>1 \<sigma>\<^sub>2 assms(3) happy apply auto[5]
        apply(rule ind_if_trace_snoc2)
        using assms(3) by auto
    next
      case (count_out \<sigma>\<^sub>2 S\<^sub>2' i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'" using count_out_case(3,5,6) by auto
      obtain \<sigma>\<^sub>2 where \<sigma>\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2' I\<^sub>2"
        using ttc_lg.count_ttrans count_out_case(5) count_out(2,3) by blast
      from \<open>S\<^sub>1' \<approx>\<^sup>S\<^sub>l S\<^sub>2'\<close> show ?thesis
        apply(cases rule: ind_S.cases; auto)
        apply(rule ttc.cases[OF count_out_case(4)]; rule ttc.cases[OF count_out(4)]; clarify)
        apply(rule ind_S.intros)
        apply(rule refl)
        apply clarsimp
        apply(rule fun_cong)
        apply(rule ind_c_eq2)
        using P_snoc2 \<sigma>\<^sub>1 \<sigma>\<^sub>2 assms(3) count_out_case(1) happy apply auto[5]
        apply(rule ind_if_trace_snoc2)
        using assms(3) by auto
    qed
  qed
qed

lemma same_time_indistinguishable: 
  assumes "ttc_lg.wf_in_seq' I"
      and "ttc_lg.wf_in_seq' I'"
      and "I \<approx>\<^sup>I\<^sub>l I'"
      and "ttc_lg.ttrans S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1" 
      and "ttc_lg.ttrans S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2" 
      and "time S\<^sub>1 = time S\<^sub>2"
    shows "S\<^sub>1 \<approx>\<^sup>S\<^sub>l S\<^sub>2"
proof -
  obtain f where f:
    "\<And>i. ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i \<Longrightarrow> f I i = time S\<^sub>1"
    "\<And>i. ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i \<Longrightarrow> f I' i = time S\<^sub>2"
    "\<And>i. f I i = f I' i"
    "\<And>i\<^sub>1 i\<^sub>2. f I i\<^sub>1 = f I i\<^sub>2 \<Longrightarrow> i\<^sub>1 = i\<^sub>2"
    using find_f_times' assms(1-3) by smt
  obtain i\<^sub>1 where i\<^sub>1: "ttc_lg.count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1"
    using ttc_lg.count_ttrans[OF assms(1)] using assms(4) by metis
  hence time\<^sub>1: "f I i\<^sub>1 = time S\<^sub>1" using f(1) by auto
  obtain i\<^sub>2 where i\<^sub>2: "ttc_lg.count S\<^sub>0 I' \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2"
    using ttc_lg.count_ttrans[OF assms(2)] using assms(5) by metis
  hence time\<^sub>2: "f I' i\<^sub>2 = time S\<^sub>2" using f(2) by auto
  have "i\<^sub>1 = i\<^sub>2" 
    by (rule f(4)) (use time\<^sub>1 time\<^sub>2 f(2-3) assms(6) in auto)
  thus ?thesis using assms(1-3) i\<^sub>1 i\<^sub>2 by (simp add: same_count_indistinguishable)
qed

subsection \<open>Approximation\<close>

lemma approx: 
  "ttc_lg.wf_in_seq' I 
   \<Longrightarrow> ttc_lg.ttrans S\<^sub>0 I \<sigma> S' I' 
   \<Longrightarrow> S' = (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) 
   \<Longrightarrow> ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E"
  proof (induct arbitrary: s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M rule: ttc_lg.ttrans_induct)
    case (ttrans_nop I)
    hence "ttc_lg.\<Delta> I [] = []" by (simp add: ttc_lg.\<Delta>_def)
    moreover have "s\<^sub>E = []" using ttrans_nop(2) by (simp add: S\<^sub>0_def)
    ultimately show ?case by (auto simp add: approx_TTC.intros)
  next
    case (ttrans_in I \<sigma> S u f A \<tau> T S' s\<^sub>W' s\<^sub>I' s\<^sub>E' s\<^sub>M')
    obtain s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M where S: "S = (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M)" using time.cases by blast
    hence "ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E" using ttrans_in(3) by auto
    hence "ttc_lg.\<Delta> I (\<sigma> @ ttc_lg.In_labels u f A \<tau>) \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (s\<^sub>E @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})])"
    proof(cases "A \<noteq> Map.empty")
      case True
      then obtain a where "a \<in> dom A" by fastforce
      thus ?thesis
        apply(auto simp add: ttc_lg.\<Delta>_def ttc_lg.In_labels_def)
        apply(rule approx_TTC_snoc3; auto?)
        using \<open>ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E\<close> apply(simp add: ttc_lg.\<Delta>_def)
        subgoal for v
          apply(rule exI[where x="ttc_lg.\<rho> I (In u f a v) \<tau>"], simp)
          by (rule exI[where x="In u f a v"]) auto
        by (metis domI singletonI ttc_lg.\<rho>.simps(1))
    next
      case False
      thus ?thesis 
        apply(auto simp add: ttc_lg.\<Delta>_def ttc_lg.In_labels_def)
        apply(rule approx_TTC_snoc2; auto?)
        using \<open>ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E\<close> by (auto simp add: ttc_lg.\<Delta>_def)
    qed
    moreover have "s\<^sub>E' = s\<^sub>E @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})]"
      using ttrans_in(4) by (cases rule: ttc.cases) (auto simp add: ttrans_in(5) S)
    ultimately show ?case by blast
  next
    case (ttrans_none I \<sigma> S I' S' s\<^sub>W' s\<^sub>I' s\<^sub>E' s\<^sub>M')
    obtain s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M where S: "S = (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M)" using time.cases by blast
    hence "ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E" using ttrans_none(3) by auto
    from ttrans_none(4)
    show ?case
      apply(cases rule: ttc.cases; simp add: ttrans_none S)
      apply(rule approx_TTC_snoc2)
      using \<open>ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C s\<^sub>E\<close> by assumption
  next
    case (ttrans_out I \<sigma> S I' f u' p d \<tau> S' s\<^sub>W' s\<^sub>I' s\<^sub>E' s\<^sub>M')
    obtain s\<^sub>W s\<^sub>I s\<^sub>E s\<^sub>M where S: "S = (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M)" using time.cases by blast
    obtain u i \<alpha> where uiv\<alpha>: 
      "s\<^sub>W = Control u f \<tau> i"       "the_usr u (Out_u (\<pi> f ! i)) = u'" 
      "\<E> s\<^sub>E \<tau> (N f u' p \<tau> \<alpha>) = {}" "s\<^sub>M (Out_y (\<pi> f ! i)) = \<langle>d, \<alpha>\<rangle>"
      "p = Out_p (\<pi> f ! i)"
      using ttrans_out(4) by (cases rule: ttc.cases) (auto simp add: ttrans_out(5) S)
    from ttrans_out(4) have "time S = (\<tau>, 3)" by (cases rule: ttc.cases) auto
    from ttrans_out(4) have "s\<^sub>E' = s\<^sub>E @ [(\<tau>, N f u' p \<tau> \<alpha>)]" 
      by (cases rule: ttc.cases) (auto simp add: uiv\<alpha>(1,4) ttrans_out(5) S)
    moreover have "ttc_lg.\<Delta> I (\<sigma> @ [(\<tau>, {Out f u' p d})]) \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (s\<^sub>E @ [(\<tau>, N f u' p \<tau> \<alpha>)])"
      apply(auto simp add: ttc_lg.\<Delta>_def ttc_lg.In_labels_def simp del: uts.simps)
      apply(rule approx_TTC_snoc3; (auto simp del: uts.simps)?)
      using ttrans_out(3)[OF S] apply(simp add: ttc_lg.\<Delta>_def del: uts.simps)
      subgoal for \<tau>' a
      proof -
        assume "ttc_lg.interf (\<tau>', a) I \<tau>"
        then obtain H' T' v' f' A' d' s S\<^sub>1 I\<^sub>1 f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau>\<^sub>1 S\<^sub>2 \<sigma>'\<^sub>1 \<tau>\<^sub>2 D D'\<^sub>1 S'\<^sub>1 I'\<^sub>1 I'\<^sub>2 where
          interf: "I = H' @ (v', f', A', \<tau>') # T'"            "a \<in> dom A'" 
                  "I'\<^sub>2 = H' @ (v', f', A'(a \<mapsto> d'), \<tau>') # T'" "ttc_lg.ttrans S\<^sub>0 I s S\<^sub>1 I\<^sub>1" 
                  "S\<^sub>1 \<midarrow>Oo f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau>\<rightarrow> S\<^sub>2" "ttc_lg.ttrans S\<^sub>0 I'\<^sub>2 (\<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]) S'\<^sub>1 I'\<^sub>1" "\<tau>\<^sub>2 \<ge> \<tau>"                                   
                  "\<forall>i \<le> length \<sigma>'\<^sub>1. (\<forall>D'\<^sub>1. (\<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]) ! i = (\<tau>, D'\<^sub>1) \<longrightarrow> Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<notin> D'\<^sub>1)"
          by (cases rule: ttc_lg.interf.cases) auto
        have e: "(S\<^sub>1, I\<^sub>1, f\<^sub>1, u\<^sub>1, p\<^sub>1, v\<^sub>1, S\<^sub>2) = (S, I', f, u', p, d, S')"
          using ttrans_Oo_same_\<tau>[OF ttrans_out(1)] interf(4,5) ttrans_out(2,4) by blast
        have "I \<approx>\<^sup>I\<^sub>(\<tau>', a) I'\<^sub>2" using ind_in_seq_dec interf(1-3) by blast
        hence "ttc_lg.wf_in_seq' I'\<^sub>2" using ttrans_out(1) ttc_lg.ind_wf_in_seq' by blast
        obtain S'\<^sub>2 I'\<^sub>1 \<sigma>'\<^sub>2 where I'\<^sub>1: "ttc_lg.ttrans S\<^sub>0 I'\<^sub>2 \<sigma>'\<^sub>2 S'\<^sub>2 I'\<^sub>1" "time S'\<^sub>2 = (\<tau>, 3)"
          using ttrans_Oo_time[OF \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close> interf(6) refl \<open>I \<approx>\<^sup>I\<^sub>(\<tau>', a) I'\<^sub>2\<close> interf(4,5,7)]
          by blast
        hence "S \<approx>\<^sup>S\<^sub>(\<tau>', a) S'\<^sub>2"
          using same_time_indistinguishable
                 [OF ttrans_out(1) \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close> \<open>I \<approx>\<^sup>I\<^sub>(\<tau>', a) I'\<^sub>2\<close> interf(4)]
          using \<open>time S = (\<tau>, 3)\<close> e by auto
        then obtain s\<^sub>W\<^sub>2 s\<^sub>I\<^sub>2 s\<^sub>E\<^sub>2 s\<^sub>M\<^sub>2 j where j: "S'\<^sub>2 = (s\<^sub>W\<^sub>2, s\<^sub>I\<^sub>2, s\<^sub>E\<^sub>2, s\<^sub>M\<^sub>2)" "s\<^sub>W\<^sub>2 = Control u f \<tau> i"
          using S uiv\<alpha>(1) by (cases rule: ind_S.cases) auto
        have "time S = time S'\<^sub>2" using \<open>time S = (\<tau>, 3)\<close> \<open>time S'\<^sub>2 = (\<tau>, 3)\<close> by simp
        obtain s\<^sub>W'' s\<^sub>I'' s\<^sub>E'' s\<^sub>M'' where S'\<^sub>2: "S'\<^sub>2 = (s\<^sub>W'', s\<^sub>I'', s\<^sub>E'', s\<^sub>M'')" using time.cases by blast
        have ind: "s\<^sub>M (Out_y (\<pi> f ! i)) \<approx>\<^sub>(\<tau>', a) s\<^sub>M'' (Out_y (\<pi> f ! i))"
          using \<open>S \<approx>\<^sup>S\<^sub>(\<tau>', a) S'\<^sub>2\<close>  S'\<^sub>2 S ind_mem_ind_tv by (cases rule: ind_S.cases) auto
        obtain d' \<alpha>' where d': "s\<^sub>M'' (Out_y (\<pi> f ! i)) = \<langle>d', \<alpha>'\<rangle>" using tv.exhaust by blast
        have "\<langle>d, \<alpha>\<rangle> \<noteq> \<langle>d', \<alpha>'\<rangle>"
        proof (rule ccontr, simp)
          assume e': "d = d' \<and> \<alpha> = \<alpha>'"
          let ?s\<^sub>E = "s\<^sub>E'' @ [(\<tau>, N f u' p \<tau> \<alpha>')]"
          obtain S'\<^sub>3 where S'\<^sub>3: "S'\<^sub>2 \<midarrow>Oo f u' p d \<tau>\<rightarrow> S'\<^sub>3"
            apply(erule meta_allE[where x="(next\<^sub>W f u \<tau> i, next\<^sub>I f u ?s\<^sub>E \<tau> i, ?s\<^sub>E, s\<^sub>M'')"])
            apply(erule meta_impE)
            using S'\<^sub>2 j apply(auto simp del: next\<^sub>W.simps next\<^sub>I.simps N.simps)
            apply(rule ctrl_accept)
            using uiv\<alpha>(2,5) d' e' apply(auto simp del: N.simps)[5]
            using ind_enforcer[where \<sigma>=s\<^sub>E] apply(rule subst)
            using S ttrans_out(1,2) \<open>ttc_lg.ttrans S\<^sub>0 I'\<^sub>2 \<sigma>'\<^sub>2 S'\<^sub>2 I'\<^sub>1\<close> \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close>
               apply(auto simp add: happy)
            using \<open>S \<approx>\<^sup>S\<^sub>(\<tau>', a) S'\<^sub>2\<close> apply(cases rule: ind_S.cases; auto simp add: j(1) S)
            using uiv\<alpha>(3,5) e' by auto
          hence I'\<^sub>2: "ttc_lg.ttrans S\<^sub>0 I'\<^sub>2 (\<sigma>'\<^sub>2 @ [(\<tau>, {Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1})]) S'\<^sub>3 I'\<^sub>1"
            using ttc_lg.ttrans.io_out \<open>ttc_lg.ttrans S\<^sub>0 I'\<^sub>2 \<sigma>'\<^sub>2 S'\<^sub>2 I'\<^sub>1\<close> e by blast
          then obtain \<sigma>'' where \<sigma>'': "\<sigma>'\<^sub>2 @ [(\<tau>, {Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1})] @ \<sigma>'' = \<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]"
            apply(cases "\<tau> = \<tau>\<^sub>2")
            using ttrans_\<sigma>_\<tau>[OF \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close> I'\<^sub>2] interf(6) apply simp
            apply(subgoal_tac "time S'\<^sub>3 \<preceq> time S'\<^sub>1")
            using ttrans_\<sigma>_conc[OF \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close>] interf(6) apply(smt append.assoc)
            proof -
              have "(\<tau>\<^sub>2, 4) \<preceq> time S'\<^sub>1"
                using ttrans_\<sigma>_less_time[OF \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close> interf(6), of "length \<sigma>'\<^sub>1"] 
                by auto
              moreover assume a: "\<tau> \<noteq> \<tau>\<^sub>2"
              ultimately have "time S'\<^sub>2 \<prec> time S'\<^sub>1"
                using \<open>time S'\<^sub>2 = (\<tau>, 3)\<close> \<open>\<tau>\<^sub>2 \<ge> \<tau>\<close> 
                by (auto simp add: ord_time.intros(1) ord_orde_time_trans[where y="(\<tau>\<^sub>2, 4)"] )
              thus "time S'\<^sub>3 \<preceq> time S'\<^sub>1" 
                using ttrans_time_step_less[OF \<open>ttc_lg.wf_in_seq' I'\<^sub>2\<close> interf(6) I'\<^sub>1(1) S'\<^sub>3] by simp
            qed
          moreover have "length \<sigma>'\<^sub>2 \<le> length \<sigma>'\<^sub>1"
            apply(subgoal_tac "length \<sigma>'\<^sub>2 + 1 + length \<sigma>'' = length \<sigma>'\<^sub>1 + 1", simp)
            by (metis Suc_eq_plus1 \<sigma>'' add.commute append_Cons append_Nil length_Cons length_append)
          moreover have "(\<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]) ! length \<sigma>'\<^sub>2 = (\<tau>, {Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1})"
            using \<sigma>'' append_Cons list_find_index by metis
          ultimately show False using interf(8)[rule_format, of "length \<sigma>'\<^sub>2"] by auto
        qed
        thus "(\<tau>', a) \<in> uts \<alpha>" using ind d' uiv\<alpha>(4) ind_tv_noteq_uts by auto
      qed
      done
    ultimately show ?case by blast
  qed

subsection \<open>Main theorem\<close>

lemma main: "ttc_lg.enforces P"
proof(rule ttc_lg.enforces_approx[where approx=approx_TTC 
                                    and \<Pi>="{P | P. Itf_mono P}" 
                                    and t="\<lambda>(_, _, \<sigma>, _). \<sigma>"])
  fix \<sigma> \<sigma>'
  assume "P \<in> {P |P. Itf_mono P}" "\<sigma>' \<in> P" "\<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>'"
  thus "\<sigma> \<in> P" using Itf_mono_def by blast
next
  fix I \<sigma> S I'
  assume "P \<in> {P |P. Itf_mono P}" "ttc_lg.wf_in_seq' I" "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
  thus "ttc_lg.\<Delta> I \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (case S of (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<Rightarrow> s\<^sub>E)"
    using approx by (cases S) auto
next
  fix I \<sigma> S I'
  assume "P \<in> {P |P. Itf_mono P}" "ttc_lg.wf_in_seq' I" "ttc_lg.ttrans S\<^sub>0 I \<sigma> S I'"
  thus "(case S of (s\<^sub>W, s\<^sub>I, s\<^sub>E, s\<^sub>M) \<Rightarrow> s\<^sub>E) \<in> P"
    using happy by (cases S) auto
next
  show "P \<in> {P |P. Itf_mono P}" using PL_Itf_mono by auto
qed

end

section \<open>Locale TTCWhile_PL\<close>

locale TTCWhile_PL = ctxt +
  fixes code_steps :: "code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> checker \<Rightarrow> code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> bool" 
    and \<pi> :: "code prog"
    and \<E> :: "if_event trace \<Rightarrow> ts \<Rightarrow> if_event set \<Rightarrow> if_event set"
    and P :: "if_event pro"
    and pc\<^sub>0 :: pc
    and \<epsilon> :: code
  assumes PL_indep_past: "indep_past P"
      and PL_enforcer:   "enforcer \<E>"
      and PL_lang:       "lang \<E> = P"
      and PL_Itf_mono:   "Itf_mono P"
      and PL_nonempty_f: "\<forall>f. length (\<pi> f) > 0"
      and PL_pos_time:   "\<forall>f. \<forall>i \<in> {0..<length (\<pi> f)}. Time ((\<pi> f) ! i) > 0"
      and PL_wf_code:    "\<forall>f. \<forall>i \<in> {0..<length (\<pi> f)}. wf_code (Code ((\<pi> f) ! i)) 
                                                     \<and> pops (Code ((\<pi> f) ! i)) = 0"
      and WhileTTC_code_steps: "code_steps = steps" 
      and WhileTTC_pc\<^sub>0:  "pc\<^sub>0 = []"
      and WhileTTC_\<epsilon>:    "\<epsilon> = []"
                                                 
sublocale TTCWhile_PL \<subseteq> PL
proof (unfold_locales)
  show "indep_past P" by (rule PL_indep_past)
next
  show "enforcer \<E>"   by (rule PL_enforcer)
next
  show "lang \<E> = P"   by (rule PL_lang)
next
  show "Itf_mono P"   by (rule PL_Itf_mono)
next
  show "\<forall>f. \<forall>i\<in>{0..<length (\<pi> f)}. 0 < Time (\<pi> f ! i)" by (rule PL_pos_time)
next
  show "\<forall>f. 0 < length (\<pi> f)" by (rule PL_nonempty_f)
next
  fix l \<gamma> f i m\<^sub>1 m\<^sub>2 pc\<^sub>1 pc\<^sub>2 m'\<^sub>1 m'\<^sub>2
  assume "i \<in> {0..<length (\<pi> f)}" "m\<^sub>1 \<approx>\<^sup>m\<^sub>l m\<^sub>2"
         "code_steps (Code (\<pi> f ! i)) pc\<^sub>0 m\<^sub>1 \<gamma> \<epsilon> pc\<^sub>1 m'\<^sub>1"
         "code_steps (Code (\<pi> f ! i)) pc\<^sub>0 m\<^sub>2 \<gamma> \<epsilon> pc\<^sub>2 m'\<^sub>2"
  thus "m'\<^sub>1 \<approx>\<^sup>m\<^sub>l m'\<^sub>2"
    using ni[of "Code (\<pi> f ! i)" pc\<^sub>0 m\<^sub>1 \<gamma> m\<^sub>2  _ \<epsilon> pc\<^sub>1 _ \<epsilon> pc\<^sub>2]
          terminates'_def PL_wf_code WhileTTC_\<epsilon> WhileTTC_pc\<^sub>0 WhileTTC_code_steps
    by (metis list.size(3))
next
  fix f s\<^sub>M \<gamma> i pc'\<^sub>1 s'\<^sub>M\<^sub>1 pc'\<^sub>2 s'\<^sub>M\<^sub>2
  assume "i \<in> {0..<length (\<pi> f)}"
         "code_steps (Code (\<pi> f ! i)) pc\<^sub>0 s\<^sub>M \<gamma> \<epsilon> pc'\<^sub>1 s'\<^sub>M\<^sub>1"
         "code_steps (Code (\<pi> f ! i)) pc\<^sub>0 s\<^sub>M \<gamma> \<epsilon> pc'\<^sub>2 s'\<^sub>M\<^sub>2"
  thus "s'\<^sub>M\<^sub>1 = s'\<^sub>M\<^sub>2"
    using loop_terminates[of "Code (\<pi> f ! i)" pc\<^sub>0 s\<^sub>M \<gamma> \<epsilon> pc'\<^sub>1] 
          steps_deterministic[THEN conjunct2]
          WhileTTC_code_steps WhileTTC_\<epsilon> 
    by auto
qed

end