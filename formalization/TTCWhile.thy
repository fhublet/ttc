theory TTCWhile
  imports Main TaggedValues
begin

section \<open>Basic types\<close>

type_synonym binop = string
datatype expr = ECst val | EVar var | EBinop expr binop expr

datatype stmt = SAss var expr | SWhile var "stmt list" | SCheck var var user prp | SPop
type_synonym code = "stmt list"

section \<open>LHS\<close>

fun lhs :: "code \<Rightarrow> var set" where
  "lhs [] = {}"
| "lhs ((SAss x e) # t) = {x} \<union> lhs t"
| "lhs ((SWhile x b) # t) = lhs b \<union> lhs t"
| "lhs ((SCheck x y u p) # t) = {x} \<union> lhs t"
| "lhs (SPop # t) = lhs t"

lemma lhs_conc: "lhs (\<alpha> @ \<beta>) = lhs \<alpha> \<union> lhs \<beta>"
proof (induct \<alpha> arbitrary: \<beta>)
  case Nil
  thus ?case by simp
next
  case (Cons a \<alpha>)
  thus ?case by (cases a) auto
qed

section \<open>Pop\<close>

fun pops :: "code \<Rightarrow> nat" where
  "pops [] = 0"
| "pops (SPop # t) = 1 + pops t"
| "pops (h # t) = pops t"

lemma pops_conc: "pops (\<alpha> @ \<beta>) = pops \<alpha> + pops \<beta>"
proof (induct \<alpha> arbitrary: \<beta>)
  case Nil
  thus ?case by simp
next
  case (Cons a \<alpha>)
  thus ?case by (cases a) auto
qed

section \<open>Well-formedness\<close>

fun wf_code :: "code \<Rightarrow> bool" where
  "wf_code [] = True"
| "wf_code ((SWhile x b) # t) = (pops b = 0 \<and> wf_code b \<and> wf_code t)"
| "wf_code (h # t) = wf_code t"

lemma wf_code_conc: "wf_code \<alpha> \<Longrightarrow> wf_code \<beta> \<Longrightarrow> wf_code (\<alpha> @ \<beta>)"
proof (induct \<alpha> arbitrary: \<beta>)
  case Nil
  thus ?case by simp
next
  case (Cons a \<alpha>)
  thus ?case by (cases a) auto
qed

locale ctxt =
  fixes \<Omega> :: "binop \<Rightarrow> val \<Rightarrow> val \<Rightarrow> val"

context ctxt
begin

section \<open>Expression evaluation\<close>

inductive eval :: "expr \<Rightarrow> mem \<Rightarrow> tv \<Rightarrow> bool"  ("\<lbrakk>_\<rbrakk>(_) = _" 10) where
  ecst:   "\<lbrakk>ECst k\<rbrakk>(m) = \<langle>k, []\<rangle>"
| evar:   "\<lbrakk>EVar x\<rbrakk>(m) = m x"
| ebinop: "\<lbrakk>e\<^sub>1\<rbrakk>(m) = \<langle>v\<^sub>1, \<alpha>\<^sub>1\<rangle> \<Longrightarrow> \<lbrakk>e\<^sub>2\<rbrakk>(m) = \<langle>v\<^sub>2, \<alpha>\<^sub>2\<rangle> \<Longrightarrow> \<lbrakk>EBinop e\<^sub>1 \<omega> e\<^sub>2\<rbrakk>(m) = \<langle>\<Omega> \<omega> v\<^sub>1 v\<^sub>2, \<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2\<rangle>"

lemma eval_ex: "\<exists>!w. \<lbrakk>e\<rbrakk>(m) = w"
proof(induct e)
  case (ECst k)
  hence "\<lbrakk>ECst k\<rbrakk>(m) = \<langle>k, []\<rangle>" by (rule ecst)
  moreover have "\<And>w. \<lbrakk>ECst k\<rbrakk>m = w \<Longrightarrow> w = \<langle>k, []\<rangle>" by (erule eval.cases) auto
  ultimately show ?case by (simp add: ex1I)
next
  case (EVar x)
  hence "\<lbrakk>EVar x\<rbrakk>(m) = m x" by (rule evar)
  moreover have "\<And>w. \<lbrakk>EVar x\<rbrakk>m = w \<Longrightarrow> w = m x" by (erule eval.cases) auto
  ultimately show ?case by (rule ex1I, simp)
next
  case (EBinop e\<^sub>1 \<omega> e\<^sub>2)
  then obtain v\<^sub>1 \<alpha>\<^sub>1 v\<^sub>2 \<alpha>\<^sub>2 where v: "\<lbrakk>e\<^sub>1\<rbrakk>(m) = \<langle>v\<^sub>1,\<alpha>\<^sub>1\<rangle>" 
                                   "\<lbrakk>e\<^sub>2\<rbrakk>(m) = \<langle>v\<^sub>2,\<alpha>\<^sub>2\<rangle>"
                                   "\<And>w. \<lbrakk>e\<^sub>1\<rbrakk>(m) = w \<Longrightarrow> w = \<langle>v\<^sub>1,\<alpha>\<^sub>1\<rangle>"
                                   "\<And>w. \<lbrakk>e\<^sub>2\<rbrakk>(m) = w \<Longrightarrow> w = \<langle>v\<^sub>2,\<alpha>\<^sub>2\<rangle>" 
    by (metis tv.exhaust)
  from v(1-2) have "\<lbrakk>EBinop e\<^sub>1 \<omega> e\<^sub>2\<rbrakk>(m) = \<langle>\<Omega> \<omega> v\<^sub>1 v\<^sub>2, \<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2\<rangle>" by (rule ebinop)
  moreover have "\<And>w. \<lbrakk>EBinop e\<^sub>1 \<omega> e\<^sub>2\<rbrakk>m = w \<Longrightarrow> w = \<langle>\<Omega> \<omega> v\<^sub>1 v\<^sub>2, \<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2\<rangle>"
    by (erule eval.cases) (use v(3-4) in auto)
  ultimately show ?case by (rule ex1I, simp)
qed

lemma eval_ind: "m \<approx>\<^sup>m\<^sub>l m' \<Longrightarrow> \<lbrakk>e\<rbrakk>(m) = w \<Longrightarrow> \<lbrakk>e\<rbrakk>(m') = w' \<Longrightarrow> w \<approx>\<^sub>l w'"
proof(induct e arbitrary: w w'; erule eval.cases; erule eval.cases; 
      auto simp del: history_union'.simps)
  fix k
  show "\<langle>k, []\<rangle> \<approx>\<^sub>l \<langle>k, []\<rangle>" by (rule ind_tv_refl)
next
  fix x
  assume "m \<approx>\<^sup>m\<^sub>l m'"
  thus "m x \<approx>\<^sub>l m' x" by (cases rule: ind_mem.cases) auto
next
  fix \<omega> e\<^sub>2 e\<^sub>1 v\<^sub>1 \<alpha>\<^sub>1 v\<^sub>2 \<alpha>\<^sub>2 v\<^sub>1' \<alpha>\<^sub>1' v\<^sub>2' \<alpha>\<^sub>2'
  assume assms: "\<And>w w'. \<lbrakk>e\<^sub>1\<rbrakk>m = w \<Longrightarrow> \<lbrakk>e\<^sub>1\<rbrakk>m' = w' \<Longrightarrow> w \<approx>\<^sub>l w'"
                "\<And>w w'. \<lbrakk>e\<^sub>2\<rbrakk>m = w \<Longrightarrow> \<lbrakk>e\<^sub>2\<rbrakk>m' = w' \<Longrightarrow> w \<approx>\<^sub>l w'"
                "m \<approx>\<^sup>m\<^sub>l m'" 
                "\<lbrakk>e\<^sub>1\<rbrakk>m = \<langle>v\<^sub>1,\<alpha>\<^sub>1\<rangle>" "\<lbrakk>e\<^sub>2\<rbrakk>m = \<langle>v\<^sub>2,\<alpha>\<^sub>2\<rangle>" "\<lbrakk>e\<^sub>1\<rbrakk>m' = \<langle>v\<^sub>1',\<alpha>\<^sub>1'\<rangle>" "\<lbrakk>e\<^sub>2\<rbrakk>m' = \<langle>v\<^sub>2',\<alpha>\<^sub>2'\<rangle>"
  hence ind: "\<langle>v\<^sub>1,\<alpha>\<^sub>1\<rangle> \<approx>\<^sub>l \<langle>v\<^sub>1',\<alpha>\<^sub>1'\<rangle>" "\<langle>v\<^sub>2,\<alpha>\<^sub>2\<rangle> \<approx>\<^sub>l \<langle>v\<^sub>2',\<alpha>\<^sub>2'\<rangle>" by auto
  then show "\<langle>\<Omega> \<omega> v\<^sub>1 v\<^sub>2, \<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2\<rangle> \<approx>\<^sub>l \<langle>\<Omega> \<omega> v\<^sub>1' v\<^sub>2', \<alpha>\<^sub>1' \<UU>\<UU> \<alpha>\<^sub>2'\<rangle>"
  proof(cases "l \<in> uts \<alpha>\<^sub>1 \<inter> uts \<alpha>\<^sub>1' \<or> l \<in> uts \<alpha>\<^sub>2 \<inter> uts \<alpha>\<^sub>2'")
    case True
    thus ?thesis 
      by (metis Int_iff Un_iff ind ind_history_union' ind_tv_ind_history ind_tv_tag uts_union')
  next
    case False
    hence "\<langle>v\<^sub>1, \<alpha>\<^sub>1\<rangle> = \<langle>v\<^sub>1', \<alpha>\<^sub>1\<rangle>" "\<langle>v\<^sub>2, \<alpha>\<^sub>2\<rangle> = \<langle>v\<^sub>2', \<alpha>\<^sub>2'\<rangle>"
      using ind ind_tv_noteq_uts by blast+
    hence "\<langle>\<Omega> \<omega> v\<^sub>1 v\<^sub>2, \<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2\<rangle> = \<langle>\<Omega> \<omega> v\<^sub>1' v\<^sub>2', \<alpha>\<^sub>1' \<UU>\<UU> \<alpha>\<^sub>2'\<rangle>" 
      using False ind(1) ind_tv_noteq_uts by blast
    thus ?thesis using ind_tv_refl by auto
  qed
qed

section \<open>Statement evaluation\<close>

inductive step :: "code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> checker \<Rightarrow> code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> bool" 
   ("_, _, _ \<down>\<^sub>_ _, _, _" 10) where
  assign:      "\<lbrakk>e\<rbrakk>(m) = \<langle>v, \<alpha>\<rangle> \<Longrightarrow> (SAss x e) # \<pi>, pc, m \<down>\<^sub>c \<pi>, pc, m(x := \<langle>v, (all pc) \<CC>\<CC> \<alpha>\<rangle>)"
| while_true:  "m x = \<langle>v, \<alpha>\<rangle> \<Longrightarrow> v \<noteq> 0 
                \<Longrightarrow> (SWhile x b) # \<pi>, pc, m \<down>\<^sub>c b @ (SWhile x b) # SPop # \<pi>, 
                     \<alpha> # pc, sanitize' m (lhs b) (all pc \<CC>\<CC> \<alpha>)"
| while_false: "m x = \<langle>0, \<alpha>\<rangle> \<Longrightarrow> (SWhile x b) # \<pi>, pc, m \<down>\<^sub>c SPop # \<pi>, 
                                 \<alpha> # pc, sanitize' m (lhs b) (all pc \<CC>\<CC> \<alpha>)"
| pop:         "SPop # \<pi>, \<alpha> # pc, m \<down>\<^sub>c \<pi>, pc, m"
| check:       "m y = \<langle>v, \<alpha>\<rangle> \<Longrightarrow> \<kappa> c u p \<alpha> = \<langle>v', \<alpha>'\<rangle> 
                \<Longrightarrow> (SCheck x y u p) # \<pi>, pc, m \<down>\<^sub>c \<pi>, pc, m(x := \<langle>v', (all pc) \<CC>\<CC> \<alpha>'\<rangle>)"

inductive steps :: "code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> checker \<Rightarrow> code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> bool" 
   ("_, _, _ \<down>\<^sup>*\<^sub>_ _, _, _" 10) where
  steps_base: "(\<pi>, pc, m) = (\<pi>', pc', m') \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m'"
| steps_step: "\<pi>, pc, m \<down>\<^sub>c \<pi>', pc', m' \<Longrightarrow> \<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m'' 
               \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m''"

lemma steps_trans: "\<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m' \<Longrightarrow> \<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m'' 
                    \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m''"
  by (induct rule: steps.induct) (use ctxt.steps_step in blast)+

subsection \<open>Determinism\<close>

lemma deterministic: 
  "\<pi>, pc, m \<down>\<^sub>c \<pi>\<^sub>1, pc\<^sub>1, m\<^sub>1 \<Longrightarrow> \<pi>, pc, m \<down>\<^sub>c \<pi>\<^sub>2, pc\<^sub>2, m\<^sub>2 \<Longrightarrow> \<pi>\<^sub>1 = \<pi>\<^sub>2 \<and> pc\<^sub>1 = pc\<^sub>2 \<and> m\<^sub>1 = m\<^sub>2"
  apply(cases \<pi>)
  apply(erule step.cases; auto)
  subgoal for s t
    apply(cases s; erule step.cases; erule step.cases; auto)
    using eval_ex by blast
  done

lemma steps_deterministic: 
  assumes "\<pi>, pc\<^sub>0, m \<down>\<^sup>*\<^sub>c \<pi>', pc\<^sub>1, m\<^sub>1"
      and "\<pi>, pc\<^sub>0, m \<down>\<^sup>*\<^sub>c \<pi>', pc\<^sub>2, m\<^sub>2"
      and "\<pi>' = []"
    shows "pc\<^sub>1 = pc\<^sub>2 \<and> m\<^sub>1 = m\<^sub>2"
  apply(insert assms)
  apply(induct arbitrary: pc\<^sub>2 m\<^sub>2 rule: steps.induct)
  apply(erule steps.cases; simp; erule step.cases; simp)
  subgoal for \<pi> pc m c \<pi>\<^sub>1 pc\<^sub>1 m\<^sub>1 \<pi>' pc'\<^sub>1 m'\<^sub>1 pc'\<^sub>2 m'\<^sub>2
    apply(rotate_tac 3)
    apply(erule steps.cases)
    apply(erule meta_allE[where x=pc'\<^sub>2], erule meta_allE[where x=m'\<^sub>2])
    apply(erule step.cases; simp)
    apply(drule deterministic)
    by blast+
  done

subsection \<open>Termination\<close>

definition terminates :: "code \<Rightarrow> bool" where
  "terminates \<pi> \<equiv> \<forall>m c. \<exists>pc' m'. \<pi>, [], m \<down>\<^sup>*\<^sub>c [], pc', m'"

definition terminates' :: "code \<Rightarrow> pc \<Rightarrow> mem \<Rightarrow> checker \<Rightarrow> bool" where
  "terminates' \<pi> pc m c \<equiv> \<exists>pc' m'. \<pi>, pc, m \<down>\<^sup>*\<^sub>c [], pc', m'"

lemma step_terminates: "terminates' \<pi> pc m c \<Longrightarrow> \<pi>, pc, m \<down>\<^sub>c \<pi>', pc', m' \<Longrightarrow> terminates' \<pi>' pc' m' c"
  apply(unfold terminates'_def)
  apply(elim exE)
  subgoal for pc'' m''
    apply(erule steps.cases; auto)
    apply(erule step.cases; auto)
    subgoal for \<pi>\<^sub>2 pc\<^sub>2 m\<^sub>2
      apply(drule deterministic[of \<pi> pc m c \<pi>' pc' m' \<pi>\<^sub>2 pc\<^sub>2 m\<^sub>2])
      by auto
    done
  done

lemma steps_terminates: "terminates' \<pi> pc m c \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m' \<Longrightarrow> terminates' \<pi>' pc' m' c"
  by (rotate_tac 1, induct rule: steps.induct) (use step_terminates in blast)+

subsection \<open>Progress\<close>

lemma progress: "\<pi> \<noteq> [] \<Longrightarrow> pops \<pi> = length pc \<Longrightarrow> \<exists>\<pi>' pc' m'. \<pi>, pc, m \<down>\<^sub>c \<pi>', pc', m'"
  apply(cases \<pi>)
  apply blast
  subgoal for s t
    apply(cases s)
    subgoal for x e
      apply(insert eval_ex[of e m])
      apply(erule ex1E)
      subgoal for w
        by (cases w) (metis assign)
      done
    subgoal for x b
      apply(cases "m x")
      subgoal for v \<alpha>
        by (cases "v \<noteq> 0") (use while_true while_false in metis)+
      done
    subgoal for x y u p
      apply(cases "m y")
      subgoal for v \<alpha>
        by (cases "\<kappa> c u p \<alpha>") (metis check)
      done
    subgoal
      by (cases pc; simp) (metis pop)
    done
  done

subsection \<open>Decomposition into elementary steps\<close>

lemma step_pops: "wf_code \<pi>  \<Longrightarrow> pops \<pi> = length pc \<Longrightarrow> \<pi>, pc, m \<down>\<^sub>c \<pi>', pc', m'
                  \<Longrightarrow> wf_code \<pi>' \<and> pops \<pi>' = length pc'"
  by (cases rule: step.cases) (auto simp add: pops_conc wf_code_conc)

lemma steps_decompose: "wf_code \<pi> \<Longrightarrow> pops \<pi> = length pc \<Longrightarrow> (\<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m') \<longleftrightarrow>
                        (\<exists>k \<Pi> PC M. (\<forall>i \<in> {0..<k::nat}. \<Pi> i, PC i, M i \<down>\<^sub>c \<Pi> (i+1), PC (i+1), M (i+1)) \<and>
                        (\<Pi> 0, PC 0, M 0) = (\<pi>, pc, m) \<and> (\<Pi> k, PC k, M k) = (\<pi>', pc', m'))"
  apply(rule iffI)
  apply(rotate_tac 2, induct rule: steps.induct)
  subgoal for \<pi> pc m \<pi>' pc' m'
    by (intro exI[where x=0] exI[where x="\<lambda>x. \<pi>"] exI[where x="\<lambda>x. pc"] exI[where x="\<lambda>x. m"] conjI) auto
  subgoal for \<pi> pc m c \<pi>' pc' m' \<pi>'' pc'' m''
    apply(elim meta_impE exE)
    using step_pops apply blast+
    subgoal for k \<Pi> PC M
      apply(intro exI[where x="Suc k"] exI[where x="\<lambda>x. case x of 0 \<Rightarrow> \<pi> | Suc i \<Rightarrow> \<Pi> i"]
                  exI[where x="\<lambda>x. case x of 0 \<Rightarrow> pc | Suc i \<Rightarrow> PC i"]
                  exI[where x="\<lambda>x. case x of 0 \<Rightarrow> m | Suc i \<Rightarrow> M i"])
      apply auto
      subgoal for i
        by (cases i) auto
      done
    done
  apply(erule exE)
  subgoal for k
    apply(induct k arbitrary: \<pi> pc m c)
    apply(rule steps_base)
    apply blast
    subgoal for k \<pi> pc m c
      apply(subgoal_tac "\<pi> \<noteq> []")
      apply(drule progress[of \<pi> pc m c])
      apply assumption
      apply(rotate_tac -1, erule exE, rotate_tac -1, erule exE, rotate_tac -1, erule exE)
      subgoal for \<pi>'' pc'' m''
        apply(rule steps_step)
        apply assumption
        apply(elim meta_allE[where x=\<pi>''] meta_allE[where x=pc''] meta_allE[where x=m''] meta_allE[where x=c] exE)
        subgoal for \<Pi> PC M
          apply(erule meta_impE)
          using step_pops apply blast
          apply(erule meta_impE)
          using step_pops apply blast
          apply(erule meta_impE)
          apply(intro exI[where x="\<lambda>x. \<Pi> (Suc x)"] exI[where x="\<lambda>x. PC (Suc x)"] exI[where x="\<lambda>x. M (Suc x)"])
          apply simp
          apply(elim conjE ballE[where x=0])
          using deterministic  by auto
        done
      apply(elim exE conjE ballE[where x=0])
      subgoal for \<Pi> PC M
        by (erule step.cases; auto)
      by simp
    done
  done

subsection \<open>Convergence\<close>

lemma convergence: "\<pi>, pc, m \<down>\<^sub>c \<pi>\<^sub>1, pc\<^sub>1, m\<^sub>1 \<Longrightarrow> (\<pi>, pc, m) \<noteq> (\<pi>\<^sub>2, pc\<^sub>2, m\<^sub>2) 
                   \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>\<^sub>2, pc\<^sub>2, m\<^sub>2 \<Longrightarrow> \<pi>\<^sub>1, pc\<^sub>1, m\<^sub>1 \<down>\<^sup>*\<^sub>c \<pi>\<^sub>2, pc\<^sub>2, m\<^sub>2"
  by (erule steps.cases; simp) (use deterministic in blast)

lemma convergence2: "\<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>\<^sub>1, pc\<^sub>1, m\<^sub>1 \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c [], pc\<^sub>2, m\<^sub>2 
                     \<Longrightarrow> \<pi>\<^sub>1, pc\<^sub>1, m\<^sub>1 \<down>\<^sup>*\<^sub>c [], pc\<^sub>2, m\<^sub>2"
  by (induct rule: steps.induct, blast, erule steps.cases)
     ((erule step.cases; auto), use deterministic in blast)

subsection \<open>Noninterference\<close>

lemma leq_neg_Suc: "a \<le> b \<Longrightarrow> \<not> (Suc a \<le> b) \<Longrightarrow> a = b"
  by auto

lemma loop_terminates: 
  assumes "\<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m'"
      and "\<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>, pc, m" 
      and "terminates' \<pi> pc m c" 
      and "wf_code \<pi>" "pops \<pi> = length pc" 
      and "wf_code \<pi>'" "pops \<pi>' = length pc'"
  shows "(\<pi>, pc, m) = (\<pi>', pc', m')"
proof -
  obtain pc\<^sub>1 m\<^sub>1 k\<^sub>1 \<Pi>\<^sub>1 PC\<^sub>1 M\<^sub>1 where 
     1: "\<forall>i \<in> {0..<k\<^sub>1::nat}. \<Pi>\<^sub>1 i, PC\<^sub>1 i, M\<^sub>1 i \<down>\<^sub>c \<Pi>\<^sub>1 (i+1), PC\<^sub>1 (i+1), M\<^sub>1 (i+1)"
        "(\<Pi>\<^sub>1 0, PC\<^sub>1 0, M\<^sub>1 0) = (\<pi>, pc, m)" "(\<Pi>\<^sub>1 k\<^sub>1, PC\<^sub>1 k\<^sub>1, M\<^sub>1 k\<^sub>1) = ([], pc\<^sub>1, m\<^sub>1)"
    using steps_decompose assms terminates'_def by (smt (z3))
  obtain pc\<^sub>2 m\<^sub>2 k\<^sub>2 \<Pi>\<^sub>2 PC\<^sub>2 M\<^sub>2 where 
     2: "\<forall>i \<in> {0..<k\<^sub>2::nat}. \<Pi>\<^sub>2 i, PC\<^sub>2 i, M\<^sub>2 i \<down>\<^sub>c \<Pi>\<^sub>2 (i+1), PC\<^sub>2 (i+1), M\<^sub>2 (i+1)"
        "(\<Pi>\<^sub>2 0, PC\<^sub>2 0, M\<^sub>2 0) = (\<pi>, pc, m)" "(\<Pi>\<^sub>2 k\<^sub>2, PC\<^sub>2 k\<^sub>2, M\<^sub>2 k\<^sub>2) = (\<pi>', pc', m')"
    using steps_decompose assms by (smt (z3))
  show ?thesis
  proof(cases "\<pi>' = []")
    case True
    show ?thesis 
      by (rule steps.cases[OF assms(2)], blast) (erule step.cases; auto simp add: True)
  next
    case False
    have 4: "\<forall>i \<in> {0..k\<^sub>2}. i \<le> k\<^sub>1 \<and> (\<Pi>\<^sub>1 i, PC\<^sub>1 i, M\<^sub>1 i) = (\<Pi>\<^sub>2 i, PC\<^sub>2 i, M\<^sub>2 i)"
    proof(rule ballI)
      fix i 
      assume i: "i \<in> {0..k\<^sub>2}"
      then show "i \<le> k\<^sub>1 \<and> (\<Pi>\<^sub>1 i, PC\<^sub>1 i, M\<^sub>1 i) = (\<Pi>\<^sub>2 i, PC\<^sub>2 i, M\<^sub>2 i)"
      proof(induct i)
        case 0
        thus ?case using 1(2) 2(2) by simp
      next
        case (Suc i)
        show ?case
        proof(rule context_conjI)
          show "Suc i \<le> k\<^sub>1"
          proof(rule ccontr)
            assume "\<not> Suc i \<le> k\<^sub>1"
            hence "i = k\<^sub>1" using Suc by simp
            hence "[], PC\<^sub>2 i, M\<^sub>2 i \<down>\<^sub>c \<Pi>\<^sub>2 (i+1), PC\<^sub>2 (i+1), M\<^sub>2 (i+1)" 
              using 1(3) 2(1)[THEN ballE, of i] Suc by auto
            thus False by (rule step.cases) auto
          qed
        next
          assume "Suc i \<le> k\<^sub>1"
          hence 5: "i < k\<^sub>1" "i < k\<^sub>2" using Suc by auto
          show "(\<Pi>\<^sub>1 (Suc i), PC\<^sub>1 (Suc i), M\<^sub>1 (Suc i)) = (\<Pi>\<^sub>2 (Suc i), PC\<^sub>2 (Suc i), M\<^sub>2 (Suc i))"
            by (simp, rule deterministic)
               (rule 1(1)[THEN ballE, of i] 2(1)[THEN ballE, of i]; use 5 Suc in force)+
        qed
      qed
    qed
    hence 5: "k\<^sub>2 \<le> k\<^sub>1 \<and> (\<Pi>\<^sub>1 k\<^sub>2, PC\<^sub>1 k\<^sub>2, M\<^sub>1 k\<^sub>2) = (\<pi>', pc', m')" using 2(3) by auto
    obtain pc\<^sub>3 m\<^sub>3 k\<^sub>3 \<Pi>\<^sub>3 PC\<^sub>3 M\<^sub>3 where 
      3: "\<forall>i \<in> {0..<k\<^sub>3::nat}. \<Pi>\<^sub>3 i, PC\<^sub>3 i, M\<^sub>3 i \<down>\<^sub>c \<Pi>\<^sub>3 (i+1), PC\<^sub>3 (i+1), M\<^sub>3 (i+1)"
      "(\<Pi>\<^sub>3 0, PC\<^sub>3 0, M\<^sub>3 0) = (\<pi>', pc', m')" "(\<Pi>\<^sub>3 k\<^sub>3, PC\<^sub>3 k\<^sub>3, M\<^sub>3 k\<^sub>3) = (\<pi>, pc, m)"
      using steps_decompose assms by (smt (z3))
    have "\<forall>p. (\<forall>i \<in> {0..k\<^sub>3}. k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+i \<le> k\<^sub>1
             \<and> (\<Pi>\<^sub>3 i, PC\<^sub>3 i, M\<^sub>3 i) = (\<Pi>\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+i), PC\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+i), M\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+i)))
            \<and> (\<forall>i \<in> {0..k\<^sub>2}. k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+k\<^sub>3+i \<le> k\<^sub>1
             \<and> (\<Pi>\<^sub>2 i, PC\<^sub>2 i, M\<^sub>2 i) = (\<Pi>\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+k\<^sub>3+i), PC\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+k\<^sub>3+i), M\<^sub>1 (k\<^sub>2+p*(k\<^sub>2+k\<^sub>3)+k\<^sub>3+i)))"
      apply(rule allI)
      subgoal for p
        apply(induct p)
        apply(rule context_conjI)
        apply(rule ballI)
        subgoal for i
          apply(induct i)
          using 5 3(2) apply simp
          subgoal for i
            apply simp
            apply(rule context_conjI)
            apply(rule ccontr)
            apply(subgoal_tac "k\<^sub>2 + i = k\<^sub>1")
            apply(rule 3(1)[THEN ballE, of i])
            using 1(3) apply simp
            apply(erule step.cases; auto)
            apply force
            apply(rule leq_neg_Suc; simp)
            apply(elim conjE)
            apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + i"])
            apply(rule 3(1)[THEN ballE, of i])
            using deterministic apply simp
            by force+            
          done
        apply(rule ballI)
        subgoal for i
          apply(induct i)
          apply(erule ballE[where x=k\<^sub>3])
          apply simp
          using 2(2) 3(3) apply simp
          apply fastforce
          subgoal for i
            apply simp
            apply(rule context_conjI)
            apply(rule ccontr)
            apply(subgoal_tac "k\<^sub>2 + k\<^sub>3 + i = k\<^sub>1")
            apply(rule 2(1)[THEN ballE, of i])
            using 1(3) apply simp
            apply(erule step.cases; auto)
            apply force
            apply(rule leq_neg_Suc; simp)
            apply(elim conjE)
            apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + k\<^sub>3 + i"])
            apply(rule 2(1)[THEN ballE, of i])
            using deterministic apply simp
            by force+
          done
        subgoal for p
          apply(elim conjE)
          apply(rule context_conjI)
          apply(rule ballI)
          subgoal for i
            apply(induct i)
            apply(rotate_tac 1)
            apply(erule ballE[where x=k\<^sub>2])
            using 2(3) 3(2) apply (auto; metis add.assoc add.commute)[1]
            apply fastforce
            subgoal for i
              apply simp
              apply(rule context_conjI)
              apply(rule ccontr)
              apply(subgoal_tac "k\<^sub>2 + (k\<^sub>2 + k\<^sub>3 + p * (k\<^sub>2 + k\<^sub>3)) + i = k\<^sub>1")
              apply(rule 3(1)[THEN ballE, of i])
              using 1(3) apply simp
              apply(erule step.cases; auto)
              apply force
              apply(rule leq_neg_Suc; simp)
              apply(elim conjE)
              apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + p * (k\<^sub>2 + k\<^sub>3) + i"])
              apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + (k\<^sub>2 + k\<^sub>3 + p * (k\<^sub>2 + k\<^sub>3)) + i"])
              using deterministic apply simp
              by force+
            done
          apply(rule ballI)
          subgoal for i
            apply(induct i)
            apply(rotate_tac 2, erule ballE[where x=k\<^sub>3])
            apply simp
            using 2(2) 3(3) apply simp
            subgoal for i
              apply simp
              apply(rule context_conjI)
              apply(rule ccontr)
              apply(subgoal_tac "k\<^sub>2 + (k\<^sub>2 + k\<^sub>3 + p * (k\<^sub>2 + k\<^sub>3)) + k\<^sub>3 + i = k\<^sub>1")
              apply(rule 2(1)[THEN ballE, of i])
              using 1(3) apply simp
              apply(erule step.cases; auto)
              apply force
              apply(rule leq_neg_Suc; simp)
              apply(elim conjE)
              apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + p * (k\<^sub>2 + k\<^sub>3) + k\<^sub>3 + i"])
              apply(rule 1(1)[THEN ballE, of "k\<^sub>2 + (k\<^sub>2 + k\<^sub>3 + p * (k\<^sub>2 + k\<^sub>3)) + k\<^sub>3 + i"])
              using deterministic apply simp
              by force+
            done
          done
        done
      done
    hence 6: "\<forall>p. k\<^sub>2+p*(k\<^sub>2+k\<^sub>3) \<le> k\<^sub>1" by force
    have "k\<^sub>2+k\<^sub>3 = 0"
    proof(rule ccontr)
      assume "k\<^sub>2+k\<^sub>3 \<noteq> 0"
      hence "k\<^sub>2+k\<^sub>3 \<ge> 1" by auto
      moreover have "k\<^sub>1 > 0" 
        using 6 calculation mult_is_0 by fastforce
      ultimately have "k\<^sub>2 + (2*k\<^sub>1)*(k\<^sub>2 + k\<^sub>3) > k\<^sub>1"
        by (simp add: \<open>0 < k\<^sub>1\<close> mult.commute trans_less_add2)
      moreover have "k\<^sub>2 + (2*k\<^sub>1)*(k\<^sub>2 + k\<^sub>3) \<le> k\<^sub>1"
        using 6[THEN allE[where x="2*k\<^sub>1"]] by blast
      ultimately show False by auto
    qed
    hence "k\<^sub>2 = 0" by simp
    thus ?thesis using 2(2,3) by simp
  qed
qed

lemma not_equal_steps: "terminates' \<pi> pc m c \<Longrightarrow> wf_code \<pi> \<Longrightarrow> pops \<pi> = length pc \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m'
                        \<Longrightarrow> \<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m'' \<Longrightarrow> (\<pi>', pc', m') \<noteq> (\<pi>'', pc'', m'')
                        \<Longrightarrow> (\<pi>, pc, m) \<noteq> (\<pi>'', pc'', m'')"
  apply(rotate_tac 3)
  apply(induct rule: steps.induct)
  apply blast
  apply(elim meta_impE)
  apply assumption+
  apply(erule step_terminates)
  apply assumption
  using step_pops apply blast+
  using loop_terminates ctxt.steps_step  
  by (metis (mono_tags, lifting) step_pops steps_base steps_trans)

lemma steps_induct: 
  "wf_code \<pi> \<Longrightarrow> pops \<pi> = length pc \<Longrightarrow> \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m' \<Longrightarrow> terminates' \<pi> pc m c \<Longrightarrow>
  (\<And>\<pi> pc m \<pi>' pc' m' c. (\<pi>, pc, m) = (\<pi>', pc', m') \<Longrightarrow> P \<pi> pc m \<pi>' pc' m' c) \<Longrightarrow>
  (\<And>\<pi> pc m \<pi>'' pc'' m'' c.
      (\<And>\<pi>' pc' m'. \<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m' \<Longrightarrow> (\<pi>, pc, m) \<noteq> (\<pi>', pc', m') \<Longrightarrow>
        \<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m'' \<Longrightarrow> P \<pi>' pc' m' \<pi>'' pc'' m'' c) \<Longrightarrow>
       P \<pi> pc m \<pi>'' pc'' m'' c) \<Longrightarrow>
  P \<pi> pc m \<pi>' pc' m' c"
  apply(insert steps.induct[where P="\<lambda>\<pi> pc m c \<pi>'' pc'' m''. \<forall>\<pi>' pc' m'. wf_code \<pi> \<longrightarrow> pops \<pi> = length pc \<longrightarrow> terminates' \<pi> pc m c \<longrightarrow>
    (\<pi>, pc, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m') \<longrightarrow> (\<pi>', pc', m' \<down>\<^sup>*\<^sub>c \<pi>'', pc'', m'') \<longrightarrow> P \<pi>' pc' m' \<pi>'' pc'' m'' c", 
        of \<pi> pc m c \<pi>' pc' m'])
  apply(elim meta_impE)
  apply assumption
  apply clarify
  subgoal for \<pi>\<^sub>1 pc\<^sub>1 m\<^sub>1 \<pi>\<^sub>2 pc\<^sub>2 m\<^sub>2 c \<pi>\<^sub>3 pc\<^sub>3 m\<^sub>3
    apply(cases "(\<pi>\<^sub>2, pc\<^sub>2, m\<^sub>2) = (\<pi>\<^sub>3, pc\<^sub>3, m\<^sub>3)")
    apply blast
    using not_equal_steps by metis
  apply clarify
  subgoal for \<pi>\<^sub>1 pc\<^sub>1 m\<^sub>1 c \<pi>\<^sub>2 pc\<^sub>2 m\<^sub>2 \<pi>\<^sub>3 pc\<^sub>3 m\<^sub>3 \<pi>\<^sub>4 pc\<^sub>4 m\<^sub>4
    apply(rotate_tac 5)  
    apply(erule meta_allE[where x=\<pi>\<^sub>4], rotate_tac -1, erule meta_allE[where x=pc\<^sub>4], rotate_tac -1,
          erule meta_allE[where x=m\<^sub>4], rotate_tac -1, erule meta_allE[where x=\<pi>\<^sub>3],  rotate_tac -1,
          erule meta_allE[where x=pc\<^sub>3],rotate_tac -1, erule meta_allE[where x=m\<^sub>3], rotate_tac -1,
          erule meta_allE[where x=c], erule meta_impE)
    subgoal for \<pi>'' pc'' m''
      apply(elim allE[where x=\<pi>''] allE[where x=pc''] allE[where x=m''] impE)
      apply(meson step_pops)+
      apply(erule step_terminates; assumption)
      subgoal
        apply(rule convergence[of \<pi>\<^sub>1 pc\<^sub>1 m\<^sub>1])
        apply assumption
        apply(erule not_equal_steps; assumption)
        by (rule steps_trans; assumption)
      by assumption+
    by assumption
  apply(elim allE[where x=\<pi>] allE[where x=pc] allE[where x=m] impE)
  using steps_base by simp_all

lemma A89: "l \<in> uts \<beta> \<Longrightarrow> \<langle>v, (\<alpha> \<CC>\<CC> \<beta>) \<CC>\<CC> \<gamma>\<rangle> \<approx>\<^sub>l \<langle>v', (\<alpha> \<CC>\<CC> \<beta>) \<CC>\<CC> \<gamma>'\<rangle>"
  using conc'_assoc ind_tv_conc' ind_history_refl ind_tv_conc2' by auto

lemma div_branch:
  "l \<in> uts \<alpha> 
   \<Longrightarrow> \<pi>\<^sub>0 = b @ SPop # \<pi> 
   \<Longrightarrow> pc\<^sub>0 = \<beta> @ \<alpha> # pc 
   \<Longrightarrow> wf_code b
   \<Longrightarrow> pops b = length \<beta>
   \<Longrightarrow> \<pi>' = []
   \<Longrightarrow> m = sanitize' m\<^sub>0 (lhs b) (all (\<alpha> # pc))
   \<Longrightarrow> \<pi>\<^sub>0, pc\<^sub>0, m \<down>\<^sup>*\<^sub>c \<pi>', pc', m'
   \<Longrightarrow> \<exists>m'. (\<pi>\<^sub>0, pc\<^sub>0, m \<down>\<^sup>*\<^sub>c \<pi>, pc, m') \<and> m \<approx>\<^sup>m\<^sub>l m'"
  apply(rotate_tac -1)
  apply(induct arbitrary: b \<beta> m\<^sub>0 rule: steps.induct; 
        auto simp del: uts.simps sanitize.simps history_union'.simps history_conc'.simps)
  apply(erule step.cases; auto simp del: uts.simps sanitize.simps history_union'.simps history_conc'.simps)
  subgoal for pc'' m'' b \<beta> m\<^sub>0 e v \<alpha>' x \<pi>'
    apply(cases b; auto simp del: uts.simps sanitize.simps history_union'.simps history_conc'.simps)
    subgoal for t
      apply(erule meta_allE[where x=t])
      apply(erule meta_allE[where x=\<beta>])
      apply(erule meta_allE[where x="if x \<in> lhs t 
                                     then m\<^sub>0(x := \<langle>v, (all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>) 
                                     else m\<^sub>0(x := \<langle>v, (((all pc) \<CC>\<CC> \<alpha>) \<CC>\<CC> (all \<beta>)) \<CC>\<CC> \<alpha>'\<rangle>)"])
      apply(elim meta_impE)
      apply blast+
      using all_Cons all_conc' conc'_assoc apply(auto split: if_splits)[1]
      apply(erule exE)
      subgoal for m'
        apply(elim conjE)
        apply(intro exI[where x=m'] conjI)
        apply(rule steps_step, erule assign)
        apply(subgoal_tac "(\<lambda>y. (if y \<in> lhs t 
                                 then case ((if x \<in> lhs t
                                             then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                             else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y)
                                      of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                 else ((if x \<in> lhs t
                                        then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                        else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y)))
                         = (\<lambda>xa. if xa = x \<or> xa \<in> lhs t
                                 then case m\<^sub>0 xa 
                                      of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                 else m\<^sub>0 xa) (x := \<langle>v, all (\<beta> @ \<alpha> # pc) \<CC>\<CC> \<alpha>'\<rangle>)")
        apply(auto simp add: conc'_assoc all_conc' simp del: history_conc'.simps)
        apply(rule ind_mem_trans[
                                 where y="sanitize' (if x \<in> lhs t 
                                                     then m\<^sub>0(x := \<langle>v, (all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)
                                                     else m\<^sub>0(x := \<langle>v, (((all pc) \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>))
                                          (lhs t) (all pc \<CC>\<CC> \<alpha>)"])
        using A89 conc'_assoc ind_mem_intro
        apply(simp add: ind_tv_refl split: if_split tv.splits)[1]
        apply(subgoal_tac "(\<lambda>y. (if y \<in> lhs t 
                                   then case ((if x \<in> lhs t
                                              then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                              else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y)
                                        of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                   else ((if x \<in> lhs t
                                         then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                         else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y)))
                             = sanitize' (if x \<in> lhs t 
                                          then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                          else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>))
                                  (lhs t) (all pc \<CC>\<CC> \<alpha>)")
        by (simp, auto simp add: conc'_assoc simp del: history_conc'.simps)
      done
    done
  subgoal for pc'' m'' b \<beta> m\<^sub>0 x v \<alpha>' b' \<pi>'
    apply(cases b; auto simp del: uts.simps sanitize.simps)
    subgoal for t
      apply(erule meta_allE[where x="b' @ SWhile x b' # SPop # t"])
      apply(erule meta_allE[where x="\<alpha>' # \<beta>"])
      apply(erule meta_allE[where x="sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>)"])
      apply(elim meta_impE)
      using wf_code_conc apply fastforce+
      using pops_conc apply fastforce
      using all_Cons all_conc' lhs_conc conc'_assoc apply (auto split: if_splits tv.splits)[1]
      apply(erule exE)
      subgoal for m'
        apply(elim conjE)
        apply(intro exI[where x=m'] conjI)
        apply(rule steps_step, (rule while_true; assumption))
        apply(subgoal_tac "(\<lambda>y. (if y \<in> lhs (b' @ SWhile x b' # SPop # t)
                                   then case sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>) y 
                                        of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                   else sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>) y))
                           = sanitize' (\<lambda>x. if x \<in> lhs b' \<or> x \<in> lhs t
                                            then case m\<^sub>0 x of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                            else m\<^sub>0 x) (lhs b') (all (\<beta> @ \<alpha> # pc) \<CC>\<CC> \<alpha>')")
        apply simp
        using all_Cons all_conc' lhs_conc conc'_assoc apply (auto split: if_splits tv.splits)[1]
        apply(rule ind_mem_trans[ 
                                 where y="sanitize' (sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                                                   (lhs (b' @ SWhile x b' # SPop # t)) (all pc \<CC>\<CC> \<alpha>)"])
        using all_Cons all_conc' lhs_conc A89 ind_tv_refl ind_mem_intro
        apply (auto simp del: history_conc'.simps split: tv.splits)[1]
        apply(subgoal_tac "(\<lambda>y. if y \<in> lhs (b' @ SWhile x b' # SPop # t)
                                then case sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>) y 
                                     of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                else sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>) y) 
                         = sanitize' (sanitize' m\<^sub>0 (lhs b') (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                                     (lhs (b' @ SWhile x b' # SPop # t)) (all pc \<CC>\<CC> \<alpha>)")
        by (auto simp add: conc'_assoc simp del: history_conc'.simps)
      done
    done
  subgoal for pc'' m'' b \<beta> m\<^sub>0 x \<alpha>' b' \<pi>'
    apply(cases b; auto simp del: uts.simps sanitize.simps)
    subgoal for t
      apply(erule meta_allE[where x="SPop # t"])
      apply(erule meta_allE[where x="\<alpha>' # \<beta>"])
      apply(erule meta_allE[where x="sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                                              (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>))"])
      apply(elim meta_impE)
      apply fastforce+
      using all_Cons all_conc' lhs_conc conc'_assoc apply(auto split: if_splits tv.splits)[1]
      apply(erule exE)
      subgoal for m'
        apply(elim conjE)
        apply(intro exI[where x=m'] conjI)
        apply(rule steps_step, (rule while_false; assumption))
        apply(subgoal_tac "(\<lambda>x. if x \<in> lhs (SPop # t)
          then case sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                              (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)) x
               of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
          else sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                         (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)) x) 
             = sanitize' (\<lambda>x. if x \<in> lhs b' \<or> x \<in> lhs t
                              then case m\<^sub>0 x of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                              else m\<^sub>0 x) (lhs b') (all (\<beta> @ \<alpha> # pc) \<CC>\<CC> \<alpha>')")
        apply simp
        using all_Cons all_conc' lhs_conc conc'_assoc apply(auto split: if_splits tv.splits)[1]
        apply(rule ind_mem_trans[ 
                                 where y="sanitize' (sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                                                    (lhs b' - lhs t)  ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)))
                                          (lhs (SPop # t)) (all pc \<CC>\<CC> \<alpha>)"])
        using A89 ind_tv_refl ind_mem_intro     
        apply(auto simp add: conc'_assoc simp del: history_conc'.simps uts.simps split: tv.splits)[1]
        apply(subgoal_tac "(\<lambda>x. if x \<in> lhs (SPop # t)
   then case sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                        (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)) x 
        of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
   else sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                  (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)) x) 
                         = sanitize' (sanitize' (sanitize' m\<^sub>0 (lhs b' \<inter> lhs t) (((all \<beta> \<CC>\<CC> \<alpha>') \<CC>\<CC> all pc) \<CC>\<CC> \<alpha>))
                                                (lhs b' - lhs t) ((((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>') \<CC>\<CC> (all pc \<CC>\<CC> \<alpha>)))
                                     (lhs (SPop # t)) (all pc \<CC>\<CC> \<alpha>)")
        by (auto simp add: conc'_assoc simp del: history_conc'.simps)
      done
    done
  subgoal for pc' m'' b \<beta> m\<^sub>0 \<pi>' \<alpha>' pc''
    apply(cases b; auto simp del: uts.simps sanitize.simps)
    apply(intro exI[where x=m\<^sub>0] conjI)
    using steps_step pop steps_base apply fastforce
    using ind_mem_refl apply fastforce
    subgoal for t
      apply(erule meta_allE[where x=t])
      apply(erule meta_allE[where x="tl \<beta>"])
      apply(erule meta_allE[where x=m\<^sub>0])
      apply(elim meta_impE; clarify?)
      apply(metis list.sel(3) list.size(3) nat.distinct(1) tl_append2)
      apply force
      subgoal for m'
        apply(intro exI[where x=m'] conjI)
        apply(rule steps_step, rule pop)
        apply(smt length_0_conv list.sel(3) nat.distinct(1) tl_append2)
        by blast
      done
    done
  subgoal for pc'' m'' b \<beta> m\<^sub>0 y v' \<alpha>'' c u p v \<alpha>' x \<pi>'
    apply(cases b; auto simp del: uts.simps sanitize.simps)
    subgoal for t
      apply(erule meta_allE[where x=t])
      apply(erule meta_allE[where x=\<beta>])
      apply(erule meta_allE[where x="if x \<in> lhs t 
                                     then m\<^sub>0(x := \<langle>v, (all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>) 
                                     else m\<^sub>0(x := \<langle>v, (((all pc) \<CC>\<CC> \<alpha>) \<CC>\<CC> (all \<beta>)) \<CC>\<CC> \<alpha>'\<rangle>)"])
      apply(elim meta_impE)
      using all_Cons all_conc' conc'_assoc apply auto
      subgoal for m'
        apply(intro exI[where x=m'] conjI)
        apply(rule steps_step, rule check; assumption?)
        apply(subgoal_tac "(\<lambda>y. if y \<in> lhs t
                                then case (if x \<in> lhs t
                                           then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                           else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y 
                                     of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                                else (if x \<in> lhs t
                                      then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                      else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>)) y)
                        = (\<lambda>y. if y = x \<or> y \<in> lhs t
                               then case m\<^sub>0 y of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, (all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> \<beta>\<rangle>
                               else m\<^sub>0 y) (x := \<langle>v, all (\<beta> @ \<alpha> # pc) \<CC>\<CC> \<alpha>'\<rangle>)")
        using all_Cons all_conc' conc'_assoc apply (auto split: if_splits)[2]
        apply(rule ind_mem_trans[ 
                                 where y="sanitize' (if x \<in> lhs t 
                                                     then m\<^sub>0(x := \<langle>v, all \<beta> \<CC>\<CC> \<alpha>'\<rangle>)
                                                     else m\<^sub>0(x := \<langle>v, ((all pc \<CC>\<CC> \<alpha>) \<CC>\<CC> all \<beta>) \<CC>\<CC> \<alpha>'\<rangle>))
                                          (lhs t) (all pc \<CC>\<CC> \<alpha>)"])
        using A89 conc'_assoc ind_mem_intro
        by (auto simp add: ind_tv_refl conc'_assoc simp del: history_conc'.simps 
                 split: if_splits tv.splits)
      done
    done
  done

lemma ni:
  assumes ni_term:   "terminates' \<pi> pc m\<^sub>1 c" "terminates' \<pi> pc m\<^sub>2 c"
      and ni_wf_code:"wf_code \<pi>"
      and ni_pops:   "pops \<pi> =  length pc"
      and ni_ind:    "m\<^sub>1 \<approx>\<^sup>m\<^sub>l m\<^sub>2"
      and ni_steps1: "\<pi>, pc, m\<^sub>1 \<down>\<^sup>*\<^sub>c \<pi>\<^sub>1, pc\<^sub>1, m'\<^sub>1" "\<pi>\<^sub>1 = []"
      and ni_steps2: "\<pi>, pc, m\<^sub>2 \<down>\<^sup>*\<^sub>c \<pi>\<^sub>2, pc\<^sub>2, m'\<^sub>2" "\<pi>\<^sub>2 = []"
    shows "m'\<^sub>1 \<approx>\<^sup>m\<^sub>l m'\<^sub>2"
  using ni_steps1 ni_steps2 ni_ind ni_pops ni_wf_code ni_term 
  apply(induct \<pi> pc m\<^sub>1 \<pi>\<^sub>1 pc\<^sub>1 m'\<^sub>1 c arbitrary: m\<^sub>2 \<pi>\<^sub>2 pc\<^sub>2 m'\<^sub>2 rule: steps_induct)
  apply(rule ni_wf_code ni_pops ni_steps1 ni_term)+
  subgoal 1 using convergence2 ctxt.not_equal_steps steps_base by (smt old.prod.inject)
  subgoal for \<pi> pc m\<^sub>1 \<pi>'' pc\<^sub>1  m'\<^sub>1 c m\<^sub>2 \<pi>\<^sub>2 pc\<^sub>2 m'\<^sub>2
    apply(cases \<pi>)
    apply(smt 1 convergence2 ctxt.not_equal_steps steps_base)
    subgoal for s t
      apply(cases s)
      subgoal for x e
        apply(erule steps.cases; auto; erule step.cases; auto simp del: history_conc'.simps)
        subgoal for v\<^sub>1 \<alpha>\<^sub>1
          apply(insert eval_ex[of e m\<^sub>2])
          apply(erule ex1E)
          subgoal for w
            apply(cases w)
            subgoal for v\<^sub>2 \<alpha>\<^sub>2
              apply(erule meta_allE[where x=t])
              apply(erule meta_allE[where x=pc])
              apply(erule meta_allE[where x="m\<^sub>1 (x := \<langle>v\<^sub>1, all pc \<CC>\<CC> \<alpha>\<^sub>1\<rangle>)"])
              apply(erule meta_allE[where x="m\<^sub>2 (x := \<langle>v\<^sub>2, all pc \<CC>\<CC> \<alpha>\<^sub>2\<rangle>)"])
              apply(erule meta_allE[where x="[]"])
              apply(erule meta_allE[where x=pc\<^sub>2])
              apply(erule meta_allE[where x=m'\<^sub>2])
              apply(elim meta_impE; assumption?)
              apply(rule steps_step; use assign steps_base in blast)
              apply(metis not_Cons_self2)
              apply(erule steps.cases; clarify; erule step.cases; blast)
              using ind_mem_update ind_tv_conc' eval_ind ind_history_refl step_terminates assign
              by metis+
            done
          done
        done
      subgoal for x b
        apply(cases "m\<^sub>1 x = m\<^sub>2 x")
        subgoal
          apply(cases "m\<^sub>1 x"; cases "m\<^sub>2 x"; simp)
          subgoal for v \<alpha>
            apply(cases "v \<noteq> 0")
            subgoal
              apply(erule meta_allE[where x="b @ SWhile x b # SPop # t"])
              apply(erule meta_allE[where x="\<alpha> # pc"])
              apply(erule meta_allE[where x="sanitize' m\<^sub>1 (lhs b) (all pc \<CC>\<CC> \<alpha>)"])
              apply(erule meta_allE[where x="sanitize' m\<^sub>2 (lhs b) (all pc \<CC>\<CC> \<alpha>)"])
              apply(erule meta_allE[where x="[]"])
              apply(erule meta_allE[where x=pc\<^sub>2])
              apply(erule meta_allE[where x=m'\<^sub>2])
              apply(elim meta_impE; clarify)
              apply(rule steps_step, use while_true in blast)
              using steps_base apply blast
              apply fastforce
              apply(erule steps.cases; auto; erule step.cases; auto)
              apply(rotate_tac 2, erule steps.cases; auto; erule step.cases; auto)
              using ind_history_refl ind_mem_sanitize' apply blast
              using pops_conc wf_code.simps wf_code_conc apply force+
              using step_terminates[where m=m\<^sub>1] step_terminates[where m=m\<^sub>2] while_true by blast+
            subgoal
              apply(erule meta_allE[where x="SPop # t"])
              apply(erule meta_allE[where x="\<alpha> # pc"])
              apply(erule meta_allE[where x="sanitize' m\<^sub>1 (lhs b) (all pc \<CC>\<CC> \<alpha>)"])
              apply(erule meta_allE[where x="sanitize' m\<^sub>2 (lhs b) (all pc \<CC>\<CC> \<alpha>)"])
              apply(erule meta_allE[where x="[]"])
              apply(erule meta_allE[where x=pc\<^sub>2])
              apply(erule meta_allE[where x=m'\<^sub>2])
              apply(elim meta_impE)
              apply(rule steps_step, use while_false in blast)
              using steps_base apply blast
              apply fastforce
              apply(erule steps.cases; auto; erule step.cases; auto)
              apply(rotate_tac 2, erule steps.cases; auto; erule step.cases; auto)
              using ind_history_refl ind_mem_sanitize' apply blast+
              using wf_code.simps apply force+
              using step_terminates[where m=m\<^sub>1] step_terminates[where m=m\<^sub>2] while_false by blast+
            done
          done
        apply(cases "m\<^sub>1 x"; cases "m\<^sub>2 x"; simp)
        subgoal for v\<^sub>1 \<alpha>\<^sub>1 v\<^sub>2 \<alpha>\<^sub>2 
          apply(insert div_branch[of l \<alpha>\<^sub>1 "if v\<^sub>1 \<noteq> 0 then b @ SWhile x b # SPop # t else SPop # t"
                                     "if v\<^sub>1 \<noteq> 0 then b @ [SWhile x b] else []" 
                                     t "\<alpha>\<^sub>1 # pc" "[]" pc "[]" "sanitize' m\<^sub>1 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>1)"
                                     "if v\<^sub>1 = 0 then sanitize' m\<^sub>1 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>1) else m\<^sub>1" c pc\<^sub>1 m'\<^sub>1])
          apply(elim meta_impE)
          apply(rule IntD1[where B="uts \<alpha>\<^sub>2"], rule ind_tv_noteq_uts[of v\<^sub>1 _ _ v\<^sub>2],
                metis ind_mem.cases, blast)
          apply fastforce+
          using wf_code.simps wf_code_conc apply force
          apply(auto simp add: pops_conc lhs_conc)[3]
          apply(erule steps.cases; auto; erule step.cases; auto)
          apply(insert div_branch[of l \<alpha>\<^sub>2 "if v\<^sub>2 \<noteq> 0 then b @ SWhile x b # SPop # t else SPop # t"
                                     "if v\<^sub>2 \<noteq> 0 then b @ [SWhile x b] else []" 
                                     t "\<alpha>\<^sub>2 # pc" "[]" pc "[]" "sanitize' m\<^sub>2 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>2)"
                                     "if v\<^sub>2 = 0 then sanitize' m\<^sub>2 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>2) else m\<^sub>2" c pc\<^sub>2 m'\<^sub>2])
          apply(elim meta_impE)
          apply(rule IntD2[where A="uts \<alpha>\<^sub>1"], rule ind_tv_noteq_uts[of v\<^sub>1 _ _ v\<^sub>2],
                metis ind_mem.cases)
          apply fastforce+
          using wf_code.simps wf_code_conc apply force
          apply(auto simp add: pops_conc lhs_conc)[3]
          apply(rotate_tac 3)
          apply(erule steps.cases; auto; erule step.cases; auto)
          apply(elim exE conjE)
          subgoal for m''\<^sub>1 m''\<^sub>2
            apply(erule meta_allE[where x=t])
            apply(erule meta_allE[where x=pc])
            apply(erule meta_allE[where x=m''\<^sub>1])
            apply(erule meta_allE[where x=m''\<^sub>2])
            apply(erule meta_allE[where x="[]"])
            apply(erule meta_allE[where x=pc\<^sub>2])
            apply(erule meta_allE[where x=m'\<^sub>2])
            apply(elim meta_impE; clarify?)
            apply(cases "v\<^sub>1 \<noteq> 0", rule steps_step;
                  use while_true steps_step while_false in fastforce)
            apply fastforce
            apply(rule convergence2[of "SWhile x b # t" pc m\<^sub>1],
                  cases "v\<^sub>1 \<noteq> 0", rule steps_step;
                  use steps_step while_false  while_true in fastforce)
            apply(rule convergence2[of "SWhile x b # t" pc m\<^sub>2], 
                  cases "v\<^sub>2 \<noteq> 0", rule steps_step;
                  use steps_step while_false  while_true in fastforce)
            using ind_mem_trans[where y="sanitize' m\<^sub>1 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>1)"]
                  ind_mem_trans[where y="sanitize' m\<^sub>2 (lhs b) (all pc \<CC>\<CC> \<alpha>\<^sub>2)"]
                  ind_mem_sym ind_history_refl
                  ind_mem_sanitize' ind_history_conc' ind_mem.cases ind_tv_ind_history 
            apply metis
            apply(erule steps_terminates[of "SWhile x b # t" pc m\<^sub>1], 
                  cases "v\<^sub>1 \<noteq> 0", rule steps_step, rule while_true;
                  (use steps_step while_false in fastforce)+)
            by (erule steps_terminates[of "SWhile x b # t" pc m\<^sub>2], 
                cases "v\<^sub>2 \<noteq> 0", rule steps_step, rule while_true) 
               (use steps_step while_false in fastforce)+
          done
        done
      apply(erule steps.cases; auto; erule step.cases; auto simp del: history_conc'.simps)
      subgoal for x u p y v\<^sub>1 \<alpha>\<^sub>1 v'\<^sub>1 \<alpha>'\<^sub>1
        apply(cases "m\<^sub>2 y")
        subgoal for v\<^sub>2 \<alpha>\<^sub>2
          apply(cases "\<kappa> c u p \<alpha>\<^sub>2")
          subgoal for v'\<^sub>2 \<alpha>'\<^sub>2
            apply(erule meta_allE[where x=t])
            apply(erule meta_allE[where x=pc])
            apply(erule meta_allE[where x="m\<^sub>1 (x := \<langle>v'\<^sub>1, all pc \<CC>\<CC> \<alpha>'\<^sub>1\<rangle>)"])
            apply(erule meta_allE[where x="m\<^sub>2 (x := \<langle>v'\<^sub>2, all pc \<CC>\<CC> \<alpha>'\<^sub>2\<rangle>)"])
            apply(erule meta_allE[where x="[]"])
            apply(erule meta_allE[where x=pc\<^sub>2])
            apply(erule meta_allE[where x=m'\<^sub>2])
            apply(elim meta_impE; clarify?)
            using steps_step check steps_base apply metis
            apply(metis not_Cons_self2)
            apply(erule steps.cases; clarify; erule step.cases; fastforce)
            apply(rule ind_mem_update, assumption, rule ind_tv_conc';
                  use ind_history_\<kappa> ind_mem.cases ind_tv_ind_history ind_history_refl in metis)
            using step_terminates check by fastforce+
          done
        done
      subgoal
        apply(erule steps.cases; auto; erule step.cases; auto)
        subgoal for \<alpha> pc
          apply(erule meta_allE[where x=t])
          apply(erule meta_allE[where x="pc"])
          apply(erule meta_allE[where x="m\<^sub>1"])
          apply(erule meta_allE[where x="m\<^sub>2"])
          apply(erule meta_allE[where x="[]"])
          apply(erule meta_allE[where x=pc\<^sub>2])
          apply(erule meta_allE[where x=m'\<^sub>2])
          apply(elim meta_impE; clarify?)
          using steps_step pop steps_base apply metis
          apply fastforce
          apply(erule steps.cases; auto; erule step.cases; auto)
          using step_terminates pop by fastforce+
        done
      done
    done
  done

end
end