theory TaggedValues
  imports Main
begin

section \<open>Basic types\<close>

type_synonym ts = nat
type_synonym var = string
type_synonym ut = "nat \<times> var"
type_synonym uts = "ut set"
type_synonym val = int
type_synonym usr = string
type_synonym prp = string
datatype user = UUsr usr | UMe
type_synonym checker = "ut \<Rightarrow> user \<Rightarrow> prp \<Rightarrow> bool"

text \<open>Histories\<close>
type_synonym history = "uts list"

text \<open>Tagged values\<close>
datatype tv = TV val history ("\<langle>_, _\<rangle>" 1000) 

text \<open>Memory states\<close>
type_synonym mem = "string \<Rightarrow> tv"

text \<open>Program counter\<close>
type_synonym pc = "history list"

section \<open>Helper lemmas\<close>

lemma union_base: "List.fold (\<union>) \<alpha> A = List.fold (\<union>) \<alpha> {} \<union> A"
  by (induct \<alpha> arbitrary: A, simp) (metis fold_simps(2) sup.assoc sup.commute)

lemma filter_map_filter_map: 
   "filter (\<lambda>a. a \<noteq> {}) (map (\<lambda>a. a - h) (filter (\<lambda>a. a \<noteq> {}) (map (\<lambda>a. a - h') \<alpha>)))
  = filter (\<lambda>a. a \<noteq> {}) (map (\<lambda>a. a - (h' \<union> h)) \<alpha>)" 
  by (induct \<alpha> arbitrary: h h') auto

section \<open>Operations on histories\<close>

subsection \<open>Flattening\<close>

fun uts :: "history \<Rightarrow> uts" where
  "uts \<alpha> = List.fold (\<union>) \<alpha> {}"

lemma uts_Cons: "uts (h # t) = h \<union> uts t"
  by (metis fold_simps(2) sup_bot.right_neutral sup_commute union_base uts.simps)

lemma uts_head: "h \<subseteq> uts (h # t)"
  using uts_Cons by blast

lemma uts_tail: "uts t \<subseteq> uts (h # t)"
  using uts_Cons by blast


subsection \<open>Normalization\<close>

fun \<NN>' :: "history \<Rightarrow> uts \<Rightarrow> history" where
  "\<NN>' [] _ = []"
| "\<NN>' (h # t) h' = (if h \<subseteq> h' then (\<NN>' t (h \<union> h')) else (h - h') # (\<NN>' t (h \<union> h')))"

fun \<NN> :: "history \<Rightarrow> history" where
 "\<NN> \<alpha> = \<NN>' \<alpha> {}"

lemma uts_normalize': "uts (\<NN>' \<alpha> h) = uts \<alpha> - h"
proof(induction \<alpha> arbitrary: h)
  case Nil
  thus ?case by simp
next
  case (Cons h t h')
  show ?case using Cons.IH union_base[where A="h-h'"] union_base[where A="h"] by auto 
qed

lemma uts_normalize: "uts (\<NN> \<alpha>) = uts \<alpha>"
  using uts_normalize' by simp

lemma normalized'_proj: "b \<subseteq> a \<Longrightarrow> \<NN>' (\<NN>' \<alpha> a) b = \<NN>' \<alpha> a"
proof (induction \<alpha> arbitrary: a b)
  case Nil
  thus ?case by simp
next
  case (Cons a \<alpha> b c)
  thus ?case using Cons.IH[of "a - b \<union> c" "a \<union> b"] by (simp, blast)
qed

lemma normalized'_proj2: "a \<subseteq> b \<Longrightarrow> \<NN>' (\<NN>' \<alpha> a) b = \<NN>' \<alpha> b"
proof (induction \<alpha> arbitrary: a b)
  case Nil
  thus ?case by simp
next
  case (Cons a \<alpha> b c)
  from Cons.prems have "a \<union> c = a - b \<union> c" by auto
  moreover from Cons.prems have "a \<union> b \<subseteq> a - b \<union> c" by auto
  ultimately show ?case using Cons.IH by (auto simp add: sup.absorb2)
qed

lemma normalized_proj: "\<NN> (\<NN> \<alpha>) = \<NN> \<alpha>"
  by (induct \<alpha>) (auto simp add: normalized'_proj)

lemma normalized'_append: "a \<subseteq> b \<Longrightarrow> \<NN>' (\<NN>' \<alpha> a @ \<beta>) b = \<NN>' (\<NN>' (\<alpha> @ \<beta>) a) b"
proof(induction \<alpha> arbitrary: a b)
  case Nil
  thus ?case using normalized'_proj2 by fastforce
next
  case (Cons a \<alpha> b c)
  thus ?case using Cons.IH[of "a \<union> b" "a - b \<union> c"] by (simp, blast)
qed 

lemma normalized'_append2: "\<NN>' (\<NN>' (\<alpha> @ \<beta>) a @ \<gamma>) a = \<NN>' (\<alpha> @ \<NN>' (\<beta> @ \<gamma>) {}) a"
  by (induct \<alpha> arbitrary: a \<beta> \<gamma>) (auto simp add: normalized'_proj2 normalized'_append Un_absorb1)

subsection \<open>Concatenation and pairwise union\<close>

fun history_conc :: "history \<Rightarrow> history \<Rightarrow> history" (infix "\<CC>" 100) where
  "\<alpha> \<CC> \<alpha>' = \<alpha> @ \<alpha>'" 

lemma uts_conc: "uts (\<alpha> \<CC> \<beta>) = uts \<alpha> \<union> uts \<beta>"
  by (induct \<alpha>, auto) (metis Un_iff union_base)+
 
lemma uts_conc1: "uts \<alpha>' \<subseteq> uts (\<alpha> \<CC> \<alpha>')"
  using uts_tail by auto

lemma uts_conc2: "uts \<alpha> \<subseteq> uts (\<alpha> \<CC> \<alpha>')"
  using uts_head by (induct \<alpha>) auto

fun history_union :: "history \<Rightarrow> history \<Rightarrow> history" (infix "\<UU>" 100) where
  "[] \<UU> \<alpha>' = \<alpha>'"
| "\<alpha> \<UU> []  = \<alpha>"
| "(h # t) \<UU> (h' # t') = (h \<union> h') # (t \<UU> t')"

lemma uts_union: "uts (\<alpha> \<UU> \<beta>) = uts \<alpha> \<union> uts \<beta>"
proof (induct \<alpha> arbitrary: \<beta>)
  case Nil
  thus ?case by simp
next
  case (Cons h t \<beta>')
  thus ?case 
    by (induct \<beta>', simp) 
       (metis fold_simps(2) history_union.simps(3) sup.assoc sup_left_commute union_base uts.simps)
qed

fun history_conc' :: "history \<Rightarrow> history \<Rightarrow> history" (infix "\<CC>\<CC>" 100) where
  "\<alpha> \<CC>\<CC> \<alpha>' = \<NN> (\<alpha> \<CC> \<alpha>')"

lemma uts_conc': "uts (\<alpha> \<CC>\<CC> \<beta>) = uts \<alpha> \<union> uts \<beta>"
  using uts_conc uts_normalize by simp

lemma uts_conc1': "uts \<alpha>' \<subseteq> uts (\<alpha> \<CC>\<CC> \<alpha>')"
  using uts_conc1 uts_normalize by simp

lemma uts_conc2': "uts \<alpha> \<subseteq> uts (\<alpha> \<CC>\<CC> \<alpha>')"
  using uts_conc2 uts_normalize by simp

lemma conc'_normalized: "\<NN> (\<alpha> \<CC>\<CC> \<alpha>') = \<alpha> \<CC>\<CC> \<alpha>'"
  using normalized_proj by simp

lemma conc'_assoc: "(\<alpha> \<CC>\<CC> \<beta>) \<CC>\<CC> \<gamma> = \<alpha> \<CC>\<CC> (\<beta> \<CC>\<CC> \<gamma>)"
  using normalized'_append normalized'_append2 by auto

fun history_union' :: "history \<Rightarrow> history \<Rightarrow> history" (infix "\<UU>\<UU>" 100) where
  "\<alpha> \<UU>\<UU> \<alpha>' = \<NN> (\<alpha> \<UU> \<alpha>')"

lemma uts_union': "uts (\<alpha> \<UU>\<UU> \<beta>) = uts \<alpha> \<union> uts \<beta>"
  using uts_union uts_normalize by simp

section \<open>Operations on memory states\<close>

fun sanitize :: "mem \<Rightarrow> var set \<Rightarrow> history \<Rightarrow> mem" where
  "sanitize m b \<alpha> = (\<lambda>x. if x \<in> b then (case m x of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, \<alpha> \<CC> \<beta>\<rangle>) else m x)"

fun sanitize' :: "mem \<Rightarrow> var set \<Rightarrow> history \<Rightarrow> mem" where
  "sanitize' m b \<alpha> = (\<lambda>x. if x \<in> b then (case m x of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, \<alpha> \<CC>\<CC> \<beta>\<rangle>) else m x)"

section \<open>Operations on program counters\<close>

fun all :: "pc \<Rightarrow> history" where
  "all [] = []"
| "all (h#t) = (all t) \<CC>\<CC> h"

lemma all_normalized: "\<NN> (all \<alpha>) = all \<alpha>"
  by (induct \<alpha>, simp) (simp add: conc'_normalized del: history_conc'.simps \<NN>.simps)

lemma all_conc': "all (\<alpha> @ \<beta>) = (all \<beta>) \<CC>\<CC> (all \<alpha>)"
proof(induct \<alpha> arbitrary: \<beta>)
  case Nil
  thus ?case 
    by (auto split: list.splits simp add: conc'_normalized all_normalized simp del: \<NN>.simps)
next
  case Cons
  thus ?case 
    by (auto split: list.splits simp add: conc'_assoc simp del: history_conc'.simps \<NN>.simps)
qed

lemma all_Cons: "all (h # t) = all t \<CC>\<CC> h"
  by auto

section \<open>Indistinguishability relations\<close>

subsection \<open>Indistinguishability of histories\<close>

inductive ind_history :: "history \<Rightarrow> ut \<Rightarrow> history \<Rightarrow> bool"  ("_ =\<^sub>_ _") where
  ind_history_empty: "[] =\<^sub>l []"
| ind_history_tag:   "l \<in> h \<Longrightarrow> (h # t) =\<^sub>l (h # t')"
| ind_history_eq:    "t =\<^sub>l t' \<Longrightarrow> (h # t) =\<^sub>l (h # t')"

lemma ind_history_noteq_uts: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> \<alpha> \<noteq> \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
  by (induct rule: ind_history.induct) (use uts_head uts_tail in blast)+

lemma ind_history_head: "(h # t) =\<^sub>l (h' # t') \<Longrightarrow> h = h'"
  by (cases rule: ind_history.cases) auto

lemma ind_history_refl: "x =\<^sub>l x"
  by (induct x) (auto simp add: ind_history_empty ind_history_eq)

lemma ind_history_sym: "x =\<^sub>l y \<Longrightarrow> y =\<^sub>l x"
  by (induct rule: ind_history.induct) (auto simp add: ind_history.intros)

lemma ind_history_trans: "x =\<^sub>l y \<Longrightarrow> y =\<^sub>l z \<Longrightarrow> x =\<^sub>l z"
proof(induct arbitrary: z rule: ind_history.induct)
  case (ind_history_empty l)
  thus ?case by assumption
next
  case (ind_history_tag l h t t' z)
  thus ?case by (cases z; simp) (erule ind_history.cases; simp add: ind_history.intros)+
next
  case (ind_history_eq t l t' h z)
  thus ?case by (cases z; simp) (erule ind_history.cases; simp add: ind_history.intros)+
qed

lemma ind_history_tag_left: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
proof(induct rule: ind_history.induct)
  case ind_history_empty
  thus ?case by simp
next
  case ind_history_tag
  thus ?case using union_base by force
next
  case ind_history_eq
  thus ?case by (metis IntI ind_history.ind_history_eq ind_history_noteq_uts)
qed 

lemma ind_history_tag_right: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
  using ind_history_tag_left ind_history_sym by blast

lemma ind_history_Un_alt: "\<NN>' \<alpha> (h \<union> h') = filter (\<lambda>a. a \<noteq> {}) (map (\<lambda>a. a - h') (\<NN>' \<alpha> h))"
  by (induct \<alpha> arbitrary: h h') (auto simp add: filter_map_filter_map)

lemma ind_history_filter_empty: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (filter (\<lambda>a. a \<noteq> {}) \<alpha>) =\<^sub>l (filter (\<lambda>a. a \<noteq> {}) \<alpha>')"
  by (induct rule: ind_history.induct) (auto simp add: ind_history.intros)

lemma ind_history_map_sub: "l \<notin> h' \<Longrightarrow> \<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (map (\<lambda>a. a - h') \<alpha>) =\<^sub>l (map (\<lambda>a. a - h') \<alpha>')"
  by (rotate_tac 1, induct arbitrary: h' rule: ind_history.induct) (auto simp add: ind_history.intros)

lemma ind_history_Un: "(\<NN>' \<alpha> h) =\<^sub>l (\<NN>' \<alpha>' h) \<Longrightarrow> l \<notin> h' \<Longrightarrow> (\<NN>' \<alpha> (h \<union> h')) =\<^sub>l (\<NN>' \<alpha>' (h \<union> h'))"
  by (simp add: ind_history_Un_alt ind_history_filter_empty ind_history_map_sub)

lemma ind_history_normalize': "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<notin> h \<Longrightarrow> (\<NN>' \<alpha> h) =\<^sub>l (\<NN>' \<alpha>' h)"
proof(induction arbitrary: h rule: ind_history.induct)
  case ind_history_empty
  thus ?case by (simp add: ind_history.intros)
next
  case ind_history_tag
  thus ?case by (auto simp add: ind_history.intros)
next
  case (ind_history_eq t l t' h' h)
  thus ?case 
  proof(cases "l \<in> h'")
    case True
    thus ?thesis using True ind_history_eq.prems by (auto simp add: ind_history_tag)
  next
    case False
    hence "(\<NN>' t (h' \<union> h)) =\<^sub>l (\<NN>' t' (h' \<union> h))" 
      using ind_history_eq.IH[of "h' \<union> h"] ind_history_eq.prems by simp
    thus ?thesis by (auto simp add: ind_history.intros)
  qed
qed

lemma ind_history_normalize: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (\<NN> \<alpha>) =\<^sub>l (\<NN> \<alpha>')"
  by (simp add: ind_history_normalize')

lemma ind_history_conc: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> \<beta> =\<^sub>l \<beta>' \<Longrightarrow> (\<beta> \<CC> \<alpha>) =\<^sub>l (\<beta>' \<CC> \<alpha>')"
proof(induction \<beta> arbitrary: \<beta>')
  case Nil
  from Nil.prems(2) show ?case by (cases rule: ind_history.cases) (simp add: Nil.prems(1))
next
  case c: (Cons h t \<beta>')
  thus ?case
  proof(cases \<beta>')
    case Nil
    thus ?thesis using ind_history.cases c.prems(2) by blast
  next
    case (Cons h' t')
    show ?thesis
      using c.prems(2) 
      by (cases rule: ind_history.cases) (use c.IH c.prems(1) ind_history.intros in auto)
  qed
qed

lemma ind_history_conc2: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>' \<Longrightarrow> (\<alpha> \<CC> \<beta>) =\<^sub>l (\<alpha>' \<CC> \<beta>')"
proof (induction \<alpha> arbitrary: \<alpha>')
  case Nil
  thus ?case by fastforce
next
  case c: (Cons h t \<alpha>')
  then show ?case
  proof (cases \<alpha>')
    case Nil
    thus ?thesis using c by fastforce
  next
    case (Cons h' t')
    thus ?thesis
    proof(cases "l \<in> h")
      case True
      moreover from c.prems(1) have "h = h'" using True Cons by (cases rule: ind_history.cases) auto
      ultimately show ?thesis using Cons by (auto simp add: ind_history_tag)
    next
      case False
      moreover from c.prems(1) have "h = h' \<and> (t =\<^sub>l t')" 
        by (cases rule: ind_history.cases) (auto simp add: Cons False)
      moreover have "l \<in> uts t \<inter> uts t'" 
        using c.prems(2) unfolding Cons uts_Cons using False calculation by auto
      ultimately show ?thesis using Cons c.IH by (auto simp add: ind_history_eq)
    qed
  qed
qed

lemma ind_history_conc': "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> \<beta> =\<^sub>l \<beta>' \<Longrightarrow> (\<beta> \<CC>\<CC> \<alpha>) =\<^sub>l (\<beta>' \<CC>\<CC> \<alpha>')"
  using ind_history_conc ind_history_normalize by simp

lemma ind_history_append: "\<beta> =\<^sub>l \<beta>' \<Longrightarrow> \<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (\<beta> @ \<alpha>) =\<^sub>l (\<beta>' @ \<alpha>')"
  by (metis history_conc.simps ind_history_conc)

lemma ind_history_conc2': "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>' \<Longrightarrow> (\<alpha> \<CC>\<CC> \<beta>) =\<^sub>l (\<alpha>' \<CC>\<CC> \<beta>')"
  using ind_history_conc2 ind_history_normalize by simp

lemma ind_history_union: "\<alpha>\<^sub>1 =\<^sub>l \<alpha>'\<^sub>1 \<Longrightarrow> \<alpha>\<^sub>2 =\<^sub>l \<alpha>'\<^sub>2 \<Longrightarrow> (\<alpha>\<^sub>1 \<UU> \<alpha>\<^sub>2) =\<^sub>l (\<alpha>'\<^sub>1 \<UU> \<alpha>'\<^sub>2)"
proof(induction \<alpha>\<^sub>1 arbitrary: \<alpha>\<^sub>2 \<alpha>'\<^sub>1 \<alpha>'\<^sub>2)
  case Nil
  from Nil.prems(1) show ?case by (cases rule: ind_history.cases) (auto simp add: Nil.prems(2))
next
  case c: (Cons h\<^sub>1 t\<^sub>1 \<alpha>\<^sub>2 \<alpha>'\<^sub>1 \<alpha>'\<^sub>2)
  from c.prems(1) obtain h'\<^sub>1 t'\<^sub>1 where 1: "\<alpha>'\<^sub>1 = h'\<^sub>1 # t'\<^sub>1" 
    by (cases rule: ind_history.cases) auto
  from c show ?case
  proof(induct \<alpha>\<^sub>2)
    case Nil
    from Nil.prems(3) show ?case 
      by (cases rule: ind_history.cases; cases \<alpha>'\<^sub>1) (use c.prems(1) in auto)
  next
    case (Cons h\<^sub>2 t\<^sub>2)
    from Cons.prems(3) obtain h'\<^sub>2 t'\<^sub>2 where 2: "\<alpha>'\<^sub>2 = h'\<^sub>2 # t'\<^sub>2" 
      by (cases rule: ind_history.cases) auto
    show ?case
    proof(cases "l \<in> h\<^sub>1 \<or> l \<in> h\<^sub>2")
      case True
      hence "l \<in> h'\<^sub>1 \<or> l \<in> h'\<^sub>2" using Cons.prems(2,3) 1 2 ind_history.cases by blast
      thus ?thesis 
        using ind_history_tag Cons.prems(2,3) 1 2 
        using ind_history_head[of h\<^sub>1 t\<^sub>1 l h'\<^sub>1 t'\<^sub>1] ind_history_head[of h\<^sub>2 t\<^sub>2 l h'\<^sub>2 t'\<^sub>2] by auto
    next
      case False
      hence "t\<^sub>1 =\<^sub>l t'\<^sub>1" "t\<^sub>2 =\<^sub>l t'\<^sub>2" using Cons.prems(2,3) 1 2 ind_history.cases by blast+
      thus ?thesis 
        using ind_history_eq 1 2 Cons.prems(3) c.IH c.prems(1)
        using ind_history_head[of h\<^sub>1 t\<^sub>1 l h'\<^sub>1 t'\<^sub>1] ind_history_head[of h\<^sub>2 t\<^sub>2 l h'\<^sub>2 t'\<^sub>2] by auto
    qed
  qed
qed

lemma ind_history_union': "\<alpha>\<^sub>1 =\<^sub>l \<alpha>'\<^sub>1 \<Longrightarrow> \<alpha>\<^sub>2 =\<^sub>l \<alpha>'\<^sub>2 \<Longrightarrow> (\<alpha>\<^sub>1 \<UU>\<UU> \<alpha>\<^sub>2) =\<^sub>l (\<alpha>'\<^sub>1 \<UU>\<UU> \<alpha>'\<^sub>2)"
  using ind_history_union ind_history_normalize by simp

subsection \<open>Indistinguishability of tagged values\<close>

inductive ind_tv :: "tv \<Rightarrow> ut \<Rightarrow> tv \<Rightarrow> bool" ("_ \<approx>\<^sub>_ _") where
  ind_tv_tag: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>' \<Longrightarrow> \<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle>"
| ind_tv_eq:  "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> \<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v, \<alpha>'\<rangle>"

lemma ind_tv_ind_history: "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle> \<Longrightarrow> \<alpha> =\<^sub>l \<alpha>'"
  by (cases rule: ind_tv.cases) auto

lemma ind_tv_noteq_uts: "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle> \<Longrightarrow> \<langle>v, \<alpha>\<rangle> \<noteq> \<langle>v', \<alpha>'\<rangle> \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
  by (erule ind_tv.cases) (use ind_history_noteq_uts in blast)+

lemma ind_tv_tag_left: "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v, \<alpha>'\<rangle> \<Longrightarrow> l \<in> uts \<alpha> \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
  using ind_history_tag_left ind_tv_ind_history by blast

lemma ind_tv_tag_right: "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v, \<alpha>'\<rangle> \<Longrightarrow> l \<in> uts \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>'"
  using ind_history_tag_right ind_tv_ind_history by blast

lemma ind_tv_refl: "x \<approx>\<^sub>l x"
  by (cases x) (simp add: ind_tv_eq ind_history_refl)

lemma ind_tv_sym: "x \<approx>\<^sub>l y \<Longrightarrow> y \<approx>\<^sub>l x"
  by (induct rule: ind_tv.induct) (auto simp add: ind_tv.intros ind_history_sym)

lemma ind_tv_trans: "x \<approx>\<^sub>l y \<Longrightarrow> y \<approx>\<^sub>l z \<Longrightarrow> x \<approx>\<^sub>l z"
proof -
  assume assms: "x \<approx>\<^sub>l y" "y \<approx>\<^sub>l z"
  obtain v \<alpha> v' \<alpha>' v'' \<alpha>'' where 0: "x = \<langle>v, \<alpha>\<rangle>" "y = \<langle>v', \<alpha>'\<rangle>" "z = \<langle>v'', \<alpha>''\<rangle>" 
    by (metis tv.exhaust)
  show ?thesis
  proof(cases "v = v''")
    case True
    thus ?thesis by (metis assms 0 ind_tv_ind_history ind_history_trans ind_tv_eq)
  next
    case False
    hence "v \<noteq> v' \<or> v' \<noteq> v''" by simp
    hence "l \<in> uts \<alpha> \<inter> uts \<alpha>' \<or> l \<in> uts \<alpha>' \<and> l \<in> uts \<alpha>''"
      using ind_tv_noteq_uts assms 0 ind_tv_ind_history by blast
    hence "l \<in> uts \<alpha> \<inter> uts \<alpha>''"
      using ind_tv_tag_left ind_tv_tag_right ind_tv_noteq_uts assms 0 by blast
    moreover have "\<alpha> =\<^sub>l \<alpha>''" by (metis ind_tv_ind_history ind_history_trans assms 0)
    ultimately show ?thesis using 0 by (simp add: ind_tv_tag)
  qed
qed

lemma ind_tv_conc: 
  assumes "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle>"
      and "\<beta> =\<^sub>l \<beta>'"
    shows "\<langle>v, \<beta> \<CC> \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<beta>' \<CC> \<alpha>'\<rangle>"
proof (cases "l \<in> uts (\<beta> \<CC> \<alpha>) \<inter> uts (\<beta>' \<CC> \<alpha>')")
  case True
  thus ?thesis using ind_tv_tag ind_tv_ind_history ind_history_conc assms by blast
next
  case False
  hence "l \<notin> uts \<alpha> \<inter> uts \<alpha>'" using ind_history_tag_left uts_conc by auto
  hence "\<langle>v, \<alpha>\<rangle> = \<langle>v', \<alpha>'\<rangle>" using assms(1) ind_tv_noteq_uts by metis
  hence "v = v'" by auto
  thus ?thesis using ind_history_conc ind_tv_ind_history assms by (auto simp add: ind_tv_eq)
qed

lemma ind_tv_conc': 
  assumes "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle>"
      and "\<beta> =\<^sub>l \<beta>'"
    shows "\<langle>v, \<beta> \<CC>\<CC> \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<beta>' \<CC>\<CC> \<alpha>'\<rangle>"
proof (cases "l \<in> uts (\<beta> \<CC>\<CC> \<alpha>) \<inter> uts (\<beta>' \<CC>\<CC> \<alpha>')")
  case True
  thus ?thesis using ind_tv_tag ind_tv_ind_history ind_history_conc' assms by blast
next
  case False
  hence "l \<notin> uts \<alpha> \<inter> uts \<alpha>'" using ind_history_tag_left uts_conc' by auto
  hence "\<langle>v, \<alpha>\<rangle> = \<langle>v', \<alpha>'\<rangle>" using assms(1) ind_tv_noteq_uts by metis
  hence "v = v'" by auto
  thus ?thesis using ind_history_conc' ind_tv_ind_history assms by (auto simp add: ind_tv_eq)
qed

lemma ind_tv_conc2: 
  "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>' \<Longrightarrow> \<langle>v, \<alpha> \<CC> \<beta>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>' \<CC> \<beta>'\<rangle>"
  using ind_tv_tag ind_history_conc2 uts_conc by auto

lemma ind_tv_conc2': "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> l \<in> uts \<alpha> \<inter> uts \<alpha>' \<Longrightarrow> \<langle>v, \<alpha> \<CC>\<CC> \<beta>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>' \<CC>\<CC> \<beta>'\<rangle>"
  using ind_history_conc2' ind_tv_tag uts_conc' by auto

lemma ind_tv_Cons: "\<langle>v, \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', \<alpha>'\<rangle>  \<Longrightarrow> \<langle>v, h # \<alpha>\<rangle> \<approx>\<^sub>l \<langle>v', h # \<alpha>'\<rangle>"
  using ind_tv_conc ind_history_empty ind_history_eq 
  by (metis append_Cons history_conc.simps append_Nil)

subsection \<open>Indistinguishability of memory states\<close>

inductive ind_mem :: "mem \<Rightarrow> ut \<Rightarrow> mem \<Rightarrow> bool" ("_ \<approx>\<^sup>m\<^sub>_ _") where
  ind_mem_intro: "\<forall>v. ((m v) \<approx>\<^sub>l (m' v)) \<Longrightarrow> m \<approx>\<^sup>m\<^sub>l m'"

lemma ind_mem_ind_tv: "m \<approx>\<^sup>m\<^sub>l m' \<Longrightarrow> (m v) \<approx>\<^sub>l (m' v)"
  using ind_mem.cases by blast

lemma ind_mem_refl: "x \<approx>\<^sup>m\<^sub>l x"
  by (simp add: ind_tv_refl ind_mem_intro)

lemma ind_mem_sym: "x \<approx>\<^sup>m\<^sub>l y \<Longrightarrow> y \<approx>\<^sup>m\<^sub>l x"
  by (cases rule: ind_mem.cases) (auto simp add: ind_tv_sym ind_mem_intro)

lemma ind_mem_trans: "x \<approx>\<^sup>m\<^sub>l y \<Longrightarrow> y \<approx>\<^sup>m\<^sub>l z \<Longrightarrow> x \<approx>\<^sup>m\<^sub>l z"
  by (cases  x l y rule: ind_mem.cases; cases y l z rule: ind_mem.cases)
     (metis ind_tv_trans ind_mem_intro)+

lemma ind_mem_update: "m \<approx>\<^sup>m\<^sub>l m' \<Longrightarrow> w \<approx>\<^sub>l w' \<Longrightarrow> (m(x := w)) \<approx>\<^sup>m\<^sub>l (m'(x := w'))"
  using ind_mem_intro by (simp add: ind_mem_ind_tv)

lemma ind_mem_sanitize: "m \<approx>\<^sup>m\<^sub>l m' \<Longrightarrow> \<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (sanitize m b \<alpha>) \<approx>\<^sup>m\<^sub>l (sanitize m' b \<alpha>')"
  by (intro ind_mem_intro allI; auto split: tv.split) 
     (use ind_tv_conc ind_mem_ind_tv history_conc.elims in metis)+

lemma ind_mem_sanitize': "m \<approx>\<^sup>m\<^sub>l m' \<Longrightarrow> \<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (sanitize' m b \<alpha>) \<approx>\<^sup>m\<^sub>l (sanitize' m' b \<alpha>')"
  by (intro ind_mem_intro allI; auto split: tv.split) 
     (use \<NN>.simps history_conc'.simps history_conc.simps ind_mem_ind_tv ind_tv_conc' in metis)+

section \<open>Checking\<close>

fun \<kappa> :: "checker \<Rightarrow> user \<Rightarrow> prp \<Rightarrow> history  \<Rightarrow> tv" where
  "\<kappa> c u p [] = \<langle>1, []\<rangle>"
| "\<kappa> c u p (h # t) = (if (\<forall>l \<in> h. c l u p) then (case \<kappa> c u p t of \<langle>v, \<beta>\<rangle> \<Rightarrow> \<langle>v, h # \<beta>\<rangle>) else \<langle>0, []\<rangle>)"

lemma ind_history_\<kappa>: "\<alpha> =\<^sub>l \<alpha>' \<Longrightarrow> (\<kappa> c u p \<alpha>) \<approx>\<^sub>l (\<kappa> c u p \<alpha>')"
proof(induct rule: ind_history.induct)
  case (ind_history_empty l)
  thus ?case by (simp add: ind_tv.intros ind_history.intros)
next
  case (ind_history_tag l h t t')
  thus ?case 
    by (auto simp add: ind_history.intros ind_tv.intros split: tv.splits)
       (use ind_history.ind_history_tag ind_history_tag_right ind_tv_tag subset_iff uts_head in metis)
next
  case (ind_history_eq t l t' h)
  thus ?case by (auto simp add: ind_tv_Cons ind_tv.intros ind_history.intros split: tv.splits)
qed

lemma \<kappa>_accept: "\<kappa> c u p \<alpha> = \<langle>v, \<beta>\<rangle> \<Longrightarrow> l \<in> uts \<beta> \<Longrightarrow> c l u p"
proof (induct \<alpha> arbitrary: v \<beta>)
  case Nil
  then show ?case by simp
next
  case (Cons a \<alpha> v \<beta>)
  then show ?case 
    by (cases "\<forall>l\<in>a. c l u p"; cases "\<kappa> c u p \<alpha>") (auto, drule subst[OF union_base], blast)
qed

end 