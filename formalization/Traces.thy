theory Traces
  imports Main TaggedValues
begin

section \<open>Labels and events\<close>

type_synonym fn = string

datatype io_label = Ii usr fn "var \<rightharpoonup> val" ts | Oo fn usr prp val ts | Bb
datatype io_event = In usr fn var val | Out fn usr prp val
datatype if_event = IIn usr fn ut | IOut fn usr prp ts | IItf ut ts

section \<open>Input sequences\<close>

type_synonym in_tuple = "usr \<times> fn \<times> (var \<rightharpoonup> val) \<times> ts"
type_synonym in_seq = "in_tuple list"

fun tuple_ts where "tuple_ts (u, f, A, \<tau>) = \<tau>"
fun tuple_fn where "tuple_fn (u, f, A, \<tau>) = f"

definition wf_in_seq :: "in_tuple list \<Rightarrow> bool" where
  "wf_in_seq I = (\<forall>i \<in> {1..<length I}. tuple_ts (I ! (i-1)) < tuple_ts (I ! i))"

lemma wf_in_seq_spec: "wf_in_seq I \<Longrightarrow> i \<ge> 1 \<Longrightarrow> i < length I \<Longrightarrow> tuple_ts (I ! (i-1)) < tuple_ts (I ! i)"
  unfolding wf_in_seq_def by simp

lemma wf_in_seq_alt: "wf_in_seq I \<longleftrightarrow> (\<forall>i j. i < j \<longrightarrow> j < length I \<longrightarrow> tuple_ts (I ! i) < tuple_ts (I ! j))"
proof(intro iffI allI impI)
  fix i j
  assume assms: "wf_in_seq I" "i < j" "j < length I"
  thus "tuple_ts (I ! i) < tuple_ts (I ! j)"
    proof(induction "j-i" arbitrary: j)
      case 0
      thus ?case by simp
    next
      case (Suc \<delta> j)
      hence "tuple_ts (I ! i) \<le> tuple_ts (I ! (j - 1))"
        by (cases \<delta>, auto) (erule meta_allE[where x="j-1"]; elim meta_impE; auto)
      moreover have "tuple_ts (I ! (j - 1)) < tuple_ts (I ! j)"
        using Suc(3-5) using wf_in_seq_spec by auto
      ultimately show ?case by (auto simp add: le_less_trans)
    qed
next
  assume "\<forall>i j. i < j \<longrightarrow> j < length I \<longrightarrow> tuple_ts (I ! i) < tuple_ts (I ! j)"
  thus "wf_in_seq I" unfolding wf_in_seq_def by auto
qed


inductive ind_part_mem :: "(var \<rightharpoonup> val) \<Rightarrow> ut \<Rightarrow> (var \<rightharpoonup> val) \<Rightarrow> bool" ("_ \<approx>\<^sup>p\<^sub>_ _") where 
  "A \<approx>\<^sup>p\<^sub>l A"
| "A = B(a \<mapsto> v) \<Longrightarrow> A' = B(a \<mapsto> v') \<Longrightarrow> l = (\<tau>, a) \<Longrightarrow> A \<approx>\<^sup>p\<^sub>l A'"

lemma ind_part_mem_dom: "A \<approx>\<^sup>p\<^sub>l A' \<Longrightarrow> dom A = dom A'"
  by (cases rule: ind_part_mem.cases) auto

inductive ind_in_seq :: "in_seq \<Rightarrow> ut \<Rightarrow> in_seq \<Rightarrow> bool" ("_ \<approx>\<^sup>I\<^sub>_ _") where 
  "I \<approx>\<^sup>I\<^sub>l I"
| "T \<approx>\<^sup>I\<^sub>l T' \<Longrightarrow> A \<approx>\<^sup>p\<^sub>l A' \<Longrightarrow> A \<noteq> A' \<longrightarrow> l = (\<tau>, a) \<Longrightarrow> (u, f, A, \<tau>) # T \<approx>\<^sup>I\<^sub>l (u, f, A', \<tau>) # T'"

lemma same_ts_wf_in_seq: 
  "length I = length I' \<Longrightarrow> \<forall>i \<in> {0..<length I}. tuple_ts (I ! i) = tuple_ts (I' ! i) \<Longrightarrow> 
   wf_in_seq I \<Longrightarrow> wf_in_seq I'"
  using wf_in_seq_alt by force

lemma ind_in_seq_same_ts:
  "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> length I = length I' \<and> (\<forall>i \<in> {0..<length I}. tuple_ts (I ! i) = tuple_ts (I' ! i))"
  by (induct rule: ind_in_seq.induct) (use less_Suc_eq_0_disj in fastforce)+

lemma ind_wf_in_seq: "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> wf_in_seq I \<Longrightarrow> wf_in_seq I'"
  using ind_in_seq_same_ts same_ts_wf_in_seq by blast

lemma ind_in_seq_head_cases:
  "(u, f, A, \<tau>) # T \<approx>\<^sup>I\<^sub>l (u', f', A', \<tau>') # T' 
\<Longrightarrow> u = u' \<and> f = f' \<and>  A \<approx>\<^sup>p\<^sub>l A' \<and> (A \<noteq> A' \<longrightarrow> (\<exists>a. l = (\<tau>, a))) \<and> \<tau> = \<tau>'"
  by (cases rule: ind_in_seq.cases) (auto simp add: ind_part_mem.intros(1))

lemma ind_in_seq_dec: "a \<in> dom A \<Longrightarrow> (H @ (u, f, A, \<tau>) # T) \<approx>\<^sup>I\<^sub>(\<tau>, a) (H @ (u, f, A(a \<mapsto> v'), \<tau>) # T)"
  by (induct H; auto, (rule ind_in_seq.intros; (auto simp add: ind_in_seq.intros)?; rule ind_part_mem.intros; auto)+)

lemma equiv_ind_part_mem: "equivp (\<lambda>x y. ind_part_mem x l y)"
proof (rule equivpI; unfold reflp_def symp_def transp_def; intro allI impI)
  fix x 
  show "x \<approx>\<^sup>p\<^sub>l x" by (rule ind_part_mem.intros(1))
next
  fix x y 
  assume "x \<approx>\<^sup>p\<^sub>l y"
  thus "y \<approx>\<^sup>p\<^sub>l x" 
    by (cases rule: ind_part_mem.cases) 
       (auto simp add: ind_part_mem.intros)
next
  fix x y z
  assume assms: "x \<approx>\<^sup>p\<^sub>l y" "y \<approx>\<^sup>p\<^sub>l z"
  from assms(1) show "x \<approx>\<^sup>p\<^sub>l z" 
  proof(cases)
    case 1
    thus ?thesis using assms(2) by simp
  next
    case (2 B a v v' \<tau>)
    from assms(2) show ?thesis
    proof(cases)
      case 1
      thus ?thesis using assms(1) by simp
    next
      case 3: (2 B' a' w w' \<tau>')
      thus ?thesis using 2 
        by (auto simp add: ind_part_mem.cases ind_part_mem.intros) 
           (metis fun_upd_upd ind_part_mem.intros(2))
    qed
  qed
qed

lemma equiv_ind_in_seq: "equivp (\<lambda>x y. ind_in_seq x l y)"
proof (rule equivpI; unfold reflp_def symp_def transp_def; intro allI impI)
  fix x
  show "x \<approx>\<^sup>I\<^sub>l x" by (rule ind_in_seq.intros(1))
next
  fix x y
  assume "x \<approx>\<^sup>I\<^sub>l y"
  thus "y \<approx>\<^sup>I\<^sub>l x"
    by (induct rule: ind_in_seq.induct)
       (use ind_in_seq.intros equivp_symp[OF equiv_ind_part_mem] in blast)+
next
  fix x y z
  show "x \<approx>\<^sup>I\<^sub>l y \<Longrightarrow> y \<approx>\<^sup>I\<^sub>l z \<Longrightarrow> x \<approx>\<^sup>I\<^sub>l z"
  proof (induct arbitrary: z rule: ind_in_seq.induct; auto)
    fix T a b T' A' \<tau> u f z
    assume assms': "T \<approx>\<^sup>I\<^sub>(a, b) T'" "\<And>z. T' \<approx>\<^sup>I\<^sub>(a, b) z \<Longrightarrow> T \<approx>\<^sup>I\<^sub>(a, b) z"
                   "A' \<approx>\<^sup>p\<^sub>(a, b) A'" "(u, f, A', \<tau>) # T' \<approx>\<^sup>I\<^sub>(a, b) z" 
                   "\<not> (u, f, A', \<tau>) # T \<approx>\<^sup>I\<^sub>(a, b) z"
    show False
    proof (cases z)
      case Nil
      thus ?thesis using assms'(4) ind_in_seq_same_ts by force
    next
      case (Cons h t)
      from assms'(4) show ?thesis
        by (cases rule: ind_in_seq.cases) 
           (use assms'(1,2,3,5) ind_in_seq.intros(2) in blast)+
    qed
  next
    fix T T' A A' \<tau> a u f z
    assume assms': "T \<approx>\<^sup>I\<^sub>(\<tau>, a) T'" "\<And>z. T' \<approx>\<^sup>I\<^sub>(\<tau>, a) z \<Longrightarrow> T \<approx>\<^sup>I\<^sub>(\<tau>, a) z"
                   "A \<approx>\<^sup>p\<^sub>(\<tau>, a) A'" "(u, f, A', \<tau>) # T' \<approx>\<^sup>I\<^sub>(\<tau>, a) z" 
    show "(u, f, A, \<tau>) # T \<approx>\<^sup>I\<^sub>(\<tau>, a) z"
    proof (cases z)
      case Nil
      thus ?thesis using assms'(4) ind_in_seq_same_ts by force
    next
      case (Cons h t)
      from assms'(4) show ?thesis
        by (cases rule: ind_in_seq.cases) 
           (use assms'(1-3) equiv_ind_part_mem equivp_transp ind_in_seq.intros(2) in fastforce)+
    qed
  qed
qed

lemma ind_in_seq_head: "(u, f, A, \<tau>) # T \<approx>\<^sup>I\<^sub>l I'
                        \<Longrightarrow> \<exists>A' T'. ((T \<approx>\<^sup>I\<^sub>l T') \<and> (A \<approx>\<^sup>p\<^sub>l A') \<and> I' = (u, f, A', \<tau>) # T')"
  by (cases rule: ind_in_seq.cases) 
     (auto simp add: equivp_reflp[OF equiv_ind_in_seq] equivp_reflp[OF equiv_ind_part_mem])

lemma ind_in_seq_conc: "B \<approx>\<^sup>I\<^sub>l B' \<Longrightarrow> A @ B \<approx>\<^sup>I\<^sub>l A @ B'"
proof(induct A)
  case Nil
  thus ?case using ind_in_seq.intros(1) by fastforce
next
  case (Cons h t)
  thus ?case by (cases h) (auto simp add: ind_in_seq.intros(2) equivp_reflp[OF equiv_ind_part_mem])
qed

section \<open>Traces\<close>

type_synonym 'a trace = "(ts \<times> 'a set) list"
type_synonym 'a pro   = "'a trace set"

definition sup_ev where "sup_ev = {IOut f u p \<tau> | f u p \<tau>. True}"

definition wf_trace :: "'a trace \<Rightarrow> bool" where
    "wf_trace \<sigma>
\<longleftrightarrow> (\<forall>i \<in> {0..<length \<sigma>-1}. \<forall>\<tau> \<tau>' D D'. \<sigma> ! i = (\<tau>, D) \<longrightarrow> \<sigma> ! (i+1) = (\<tau>', D') \<longrightarrow> \<tau> < \<tau>')"

inductive ind_if_trace :: "if_event trace \<Rightarrow> if_event trace \<Rightarrow> bool" ("_ \<approx>\<^sup>\<sigma> _") where
  "length \<sigma> = length \<sigma>'
   \<Longrightarrow> \<forall>i \<in> {0..<length \<sigma>}.
         case (\<sigma> ! i, \<sigma>' ! i) of ((\<tau>, D), (\<tau>', D')) \<Rightarrow>
           \<tau> = \<tau>' \<and> {IIn u f l | u f l. True} \<inter> D = {IIn u f l | u f l. True} \<inter> D'
   \<Longrightarrow> \<sigma> \<approx>\<^sup>\<sigma> \<sigma>'"


lemma equiv_ind_if_trace: "equivp ind_if_trace"
proof (rule equivpI; unfold reflp_def symp_def transp_def; intro allI impI)
  fix x
  show "x \<approx>\<^sup>\<sigma> x"
    by (rule ind_if_trace.intros) auto
next
  fix x y 
  assume "x \<approx>\<^sup>\<sigma> y"
  thus "y \<approx>\<^sup>\<sigma> x"
  proof cases
    case 1
    thus ?thesis by (simp add: ind_if_trace.intros prod.case_eq_if)
  qed 
next
  fix x y z
  assume assms: "x \<approx>\<^sup>\<sigma> y" "y \<approx>\<^sup>\<sigma> z"
  from assms(1) show "x \<approx>\<^sup>\<sigma> z"
  proof cases
    case 1
    from assms(2) show ?thesis
    proof cases
      case "1'": 1
      thus ?thesis using 1 by (simp add: ind_if_trace.intros prod.case_eq_if)
    qed
  qed
qed

lemma ind_if_trace_snoc: 
  assumes "\<sigma> \<approx>\<^sup>\<sigma> \<sigma>'"
      and "A \<approx>\<^sup>p\<^sub>l A'" 
    shows "\<sigma> @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})] \<approx>\<^sup>\<sigma> \<sigma>' @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A'})]"
  using assms(1)
  proof cases
    case 1
    let ?\<sigma>  = "\<sigma>  @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A})]"
    let ?\<sigma>' = "\<sigma>' @ [(\<tau>, {IIn u f (\<tau>, a) | a. a \<in> dom A'})]"
    have "length ?\<sigma> = length ?\<sigma>'" using 1(1) by auto
    moreover have "\<forall>i\<in>{0..<length ?\<sigma>}.
     case (?\<sigma> ! i, ?\<sigma>' ! i) of
       ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> {IIn u f l |u f l. True} \<inter> D = {IIn u f l |u f l. True} \<inter> D'"
    proof (rule ballI)
      fix i
      assume i: "i \<in> {0..<length ?\<sigma>}"
      show "case (?\<sigma> ! i, ?\<sigma>' ! i) of
       ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> {IIn u f l |u f l. True} \<inter> D = {IIn u f l |u f l. True} \<inter> D'"
      proof (cases "i < length \<sigma>")
        case True
        thus ?thesis using 1 by (simp add: nth_append)
      next
        case False
        hence "i = length \<sigma>" using i by auto
        hence 2: "?\<sigma>  ! i = (\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A})"
                 "?\<sigma>' ! i = (\<tau>, {IIn u f (\<tau>, a) |a. a \<in> dom A'})"
          by (simp, simp add: 1(1) \<open>i = length \<sigma>\<close>)
        show ?thesis using assms(2) by (cases rule: ind_part_mem.cases; use 2 in auto)+
      qed
    qed
    ultimately show ?thesis by (rule ind_if_trace.intros)
  qed


lemma ind_if_trace_snoc2: 
  assumes "\<sigma> \<approx>\<^sup>\<sigma> \<sigma>'"
      and "{IIn u f l | u f l. True} \<inter> D = {}"
      and "{IIn u f l | u f l. True} \<inter> D' = {}"
    shows "\<sigma> @ [(\<tau>, D)] \<approx>\<^sup>\<sigma> \<sigma>' @ [(\<tau>, D')]"
  using assms(1)
  proof cases
    case 1
    let ?\<sigma>  = "\<sigma>  @ [(\<tau>, D)]"
    let ?\<sigma>' = "\<sigma>' @ [(\<tau>, D')]"
    have "length ?\<sigma> = length ?\<sigma>'" using 1(1) by auto
    moreover have "\<forall>i\<in>{0..<length ?\<sigma>}.
     case (?\<sigma> ! i, ?\<sigma>' ! i) of
       ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> {IIn u f l |u f l. True} \<inter> D = {IIn u f l |u f l. True} \<inter> D'"
    proof (rule ballI)
      fix i
      assume i: "i \<in> {0..<length ?\<sigma>}"
      show "case (?\<sigma> ! i, ?\<sigma>' ! i) of
       ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> {IIn u f l |u f l. True} \<inter> D = {IIn u f l |u f l. True} \<inter> D'"
      proof (cases "i < length \<sigma>")
        case True
        thus ?thesis using 1 by (simp add: nth_append)
      next
        case False
        hence "i = length \<sigma>" using i by auto
        hence 2: "?\<sigma>  ! i = (\<tau>, D)" "?\<sigma>' ! i = (\<tau>, D')" by (simp, simp add: 1(1) \<open>i = length \<sigma>\<close>)
        thus ?thesis using assms(2-3) by simp
      qed
    qed
    ultimately show ?thesis by (rule ind_if_trace.intros)
  qed

section \<open>Enforcers\<close>

inductive in_lang :: "if_event trace \<Rightarrow> (if_event trace \<Rightarrow> ts \<Rightarrow> if_event set \<Rightarrow> if_event set) \<Rightarrow> bool" ("_ \<in> \<L>(_)") where
  "[] \<in> \<L>(E)"
| "\<sigma> \<in> \<L>(E) \<Longrightarrow> E \<sigma> \<tau> D = {} \<Longrightarrow> \<sigma> @ [(\<tau>, D)] \<in> \<L>(E)"

definition lang :: "(if_event trace \<Rightarrow> ts \<Rightarrow> if_event set \<Rightarrow> if_event set) \<Rightarrow> if_event pro" where
  "lang E = {\<sigma> | \<sigma>. \<sigma> \<in> \<L>(E)}"

definition enforcer :: "(if_event trace \<Rightarrow> ts \<Rightarrow> if_event set \<Rightarrow> if_event set) \<Rightarrow> bool" where
  "enforcer E = (\<forall>\<sigma> \<tau> D D'. E \<sigma> \<tau> D = D' \<longrightarrow> D' \<subseteq> D \<inter> sup_ev)"

lemma enforcer_empty: "enforcer E \<Longrightarrow> P = lang E \<Longrightarrow> \<sigma> \<in> P \<Longrightarrow> E \<sigma> \<tau> D = {} \<longleftrightarrow> \<sigma> @ [(\<tau>, D)] \<in> P"
  by (rule iffI; simp add: lang_def in_lang.intros(2); erule in_lang.cases) auto

lemma enforcer_IIn: 
  assumes "enforcer E" "P = lang E" "\<sigma> \<in> P" "\<forall>e \<in> D. \<exists>u f l. e = IIn u f l"
  shows "\<sigma> @ [(\<tau>, D)] \<in> P"
proof -
  have "D \<inter> sup_ev = {}" using assms(4) unfolding sup_ev_def by auto
  hence "E \<sigma> \<tau> D = {}" using assms(1)[unfolded enforcer_def, rule_format, of \<sigma> \<tau> D] by auto
  thus ?thesis using assms(2,3) unfolding lang_def by (auto simp add: in_lang.intros(2))
qed

fun remove_empty :: "'a trace \<Rightarrow> 'a trace" where
  "remove_empty \<sigma> = filter (\<lambda>(\<tau>, D). D \<noteq> {}) \<sigma>"

lemma remove_empty_snoc: "remove_empty (\<sigma> @ [(\<tau>, {})]) = remove_empty \<sigma>"
  by auto


lemma lang_empty_eq: "lang E = P \<Longrightarrow> \<sigma>' \<in> P \<Longrightarrow> E \<sigma>' \<tau> D = {} \<longleftrightarrow> \<sigma>' @ [(\<tau>, D)] \<in> P"
  by (auto simp add: lang_def in_lang.intros(2)) (erule in_lang.cases; simp)

section \<open>Approximation\<close>

inductive approx_TTC :: "if_event trace \<Rightarrow> if_event trace \<Rightarrow> bool" (infix "\<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C" 100) where
  "length \<sigma> = length (remove_empty \<sigma>') 
   \<Longrightarrow> \<forall>i \<in> {0..<length \<sigma>}. case (\<sigma> ! i, (remove_empty \<sigma>') ! i) of ((\<tau>, D), (\<tau>', D')) 
                               \<Rightarrow> \<tau> = \<tau>' \<and> D \<subseteq> D' \<and> D' - D \<subseteq> {IItf ii oo | ii oo. True}
   \<Longrightarrow> \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>'"


lemma approx_TTC_snoc2: "\<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>' \<Longrightarrow> \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (\<sigma>' @ [(\<tau>, {})])"
  by (erule approx_TTC.cases, rule approx_TTC.intros; simp)

lemma approx_TTC_snoc3: 
  assumes "\<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>'"
      and "D \<noteq> {}"
      and "D \<subseteq> D'"
      and "D' - D \<subseteq> {IItf ii oo | ii oo. True}" 
    shows "(\<sigma> @ [(\<tau>, D)]) \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (\<sigma>' @ [(\<tau>, D')])"
proof -
  from assms(1) have "length \<sigma> = length (remove_empty \<sigma>')" by (cases rule: approx_TTC.cases) auto
  moreover have "D' \<noteq> {}" using assms(2-3) by auto
  ultimately have "length (\<sigma> @ [(\<tau>, D)]) = length (remove_empty (\<sigma>' @ [(\<tau>, D')]))"
    using assms(2) by (auto split: if_splits)
  moreover have "\<forall>i\<in>{0..<length (\<sigma> @ [(\<tau>, D)])}.
   case ((\<sigma> @ [(\<tau>, D)]) ! i, remove_empty (\<sigma>' @ [(\<tau>, D')]) ! i) of
   ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> D \<subseteq> D' \<and> D' - D \<subseteq> {IItf ii oo |ii oo. True}"
  proof(rule ballI)
    fix i
    assume a: "i \<in> {0..<length (\<sigma> @ [(\<tau>, D)])}"
    show "case ((\<sigma> @ [(\<tau>, D)]) ! i, remove_empty (\<sigma>' @ [(\<tau>, D')]) ! i) of
           ((\<tau>, D), \<tau>', D') \<Rightarrow> \<tau> = \<tau>' \<and> D \<subseteq> D' \<and> D' - D \<subseteq> {IItf ii oo |ii oo. True}"
    proof(cases "i < length \<sigma>")
      case True
      hence "(\<sigma> @ [(\<tau>, D)]) ! i = \<sigma> ! i" by (simp add: nth_append)
      have "remove_empty (\<sigma>' @ [(\<tau>, D')]) ! i = remove_empty \<sigma>' ! i"
        using True \<open>length \<sigma> = length (remove_empty \<sigma>')\<close> by (auto simp add: nth_append)
      show ?thesis
        by (rule approx_TTC.cases[OF assms(1)], clarify)
           (metis (mono_tags, lifting) \<open>(\<sigma> @ [(\<tau>, D)]) ! i = \<sigma> ! i\<close> 
                                       \<open>remove_empty (\<sigma>' @ [(\<tau>, D')]) ! i = remove_empty \<sigma>' ! i\<close>
                                       True atLeastLessThan_iff le0 split_conv)
    next
      case False
      hence "i = length \<sigma>" using a by simp
      hence "(\<sigma> @ [(\<tau>, D)]) ! i = (\<tau>, D)" by simp
      moreover have "(remove_empty (\<sigma>' @ [(\<tau>, D')])) ! i = (\<tau>, D')"
        using \<open>length \<sigma> = length (remove_empty \<sigma>')\<close> \<open>i = length \<sigma>\<close> \<open>D' \<noteq> {}\<close> by auto
      ultimately show ?thesis using assms(3-4) by simp
    qed
  qed
  ultimately show ?thesis using approx_TTC.intros by blast
qed

lemma approx_TTC_snoc: "\<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>' \<Longrightarrow> D \<noteq> {} \<Longrightarrow> (\<sigma> @ [(\<tau>, D)]) \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C (\<sigma>' @ [(\<tau>, D)])"
  using approx_TTC_snoc3 by simp

definition Itf_mono :: "if_event pro \<Rightarrow> bool" where
  "Itf_mono P = (\<forall>\<sigma> \<sigma>'. \<sigma>' \<in> P \<longrightarrow> \<sigma> \<sqsubseteq>\<^sub>T\<^sub>T\<^sub>C \<sigma>' \<longrightarrow> \<sigma> \<in> P)"

definition indep_past :: "if_event pro \<Rightarrow> bool" where
  "indep_past P = 
     (\<forall>\<sigma> \<sigma>'. length \<sigma> = length \<sigma>'
             \<longrightarrow> butlast \<sigma> \<approx>\<^sup>\<sigma> butlast \<sigma>'
             \<longrightarrow> (\<sigma> \<in> P \<longleftrightarrow> \<sigma>' \<in> P))"

section \<open>Locale lg\<close>

locale lg =
  fixes S\<^sub>0 :: 'a
    and trans :: "'a \<Rightarrow> io_label \<Rightarrow> 'a \<Rightarrow> bool" ("_ \<midarrow>_\<rightarrow> _" 1000)
    and wf_in_seq2 :: "in_seq \<Rightarrow> bool"
    and valid_code :: "'a \<Rightarrow> bool"
  assumes determ: "valid_code S \<Longrightarrow> S \<midarrow>e'\<rightarrow> S' \<Longrightarrow> S \<midarrow>e''\<rightarrow> S'' \<Longrightarrow> S' \<noteq> S'' \<or> e' \<noteq> e'' \<Longrightarrow> 
                      \<exists>u' f' A' \<tau>' u'' f'' A'' \<tau>''. 
                         e' = Ii u' f' A' \<tau>' \<and> e'' = Ii u'' f'' A'' \<tau>'' 
                         \<and> (u', f', A', \<tau>') \<noteq> (u'', f'', A'', \<tau>'')"
      and no_way_back: "S \<midarrow>e\<rightarrow> S' \<Longrightarrow> S' \<noteq> S\<^sub>0"
      and ind_wf_in_seq2: "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> wf_in_seq2 I \<Longrightarrow> wf_in_seq2 I'"
      and valid_step: "valid_code S \<Longrightarrow> S \<midarrow>e\<rightarrow> S' \<Longrightarrow> valid_code S'"
      and valid_S\<^sub>0: "valid_code S\<^sub>0"

context lg
begin

subsection \<open>Extended well-formedness of input sequences\<close>

definition wf_in_seq' :: "in_seq \<Rightarrow> bool" where
  "wf_in_seq' I = (wf_in_seq I \<and> wf_in_seq2 I)"

lemma ind_wf_in_seq': "I \<approx>\<^sup>I\<^sub>l I' \<Longrightarrow> wf_in_seq' I \<Longrightarrow> wf_in_seq' I'"
  unfolding wf_in_seq'_def using ind_wf_in_seq ind_wf_in_seq2 by blast

lemma wf_in_seq'_less: "wf_in_seq' I \<Longrightarrow> i < j \<Longrightarrow> j < length I \<Longrightarrow> tuple_ts (I ! i) < tuple_ts (I ! j)"
  using wf_in_seq'_def wf_in_seq_alt by blast

lemma wf_in_seq'_le: "wf_in_seq' I \<Longrightarrow> i \<le> j \<Longrightarrow> j < length I \<Longrightarrow> tuple_ts (I ! i) \<le> tuple_ts (I ! j)"
  using wf_in_seq'_less by (cases "i = j") (auto simp add: nat_less_le)

subsection \<open>Transition relations (ttrans and count)\<close>

definition In_labels where 
  "In_labels u f A \<tau> = (if A \<noteq> Map.empty then [(\<tau>, {In u f a d | a d. a \<in> dom A \<and> A a = Some d})] else [])"

inductive ttrans :: "'a \<Rightarrow> in_seq \<Rightarrow> io_event trace \<Rightarrow> 'a \<Rightarrow> in_seq \<Rightarrow> bool" ("_, _ \<Midarrow>_\<Rightarrow> _, _" 10) where
  io_nop: "S = S' \<Longrightarrow> I = I' \<Longrightarrow> \<sigma> = [] \<Longrightarrow> S, I \<Midarrow>\<sigma>\<Rightarrow> S', I'"
| io_in: "S, I \<Midarrow>\<sigma>\<Rightarrow> S', (u, f, A, \<tau>)#T
    \<Longrightarrow> S' \<midarrow>Ii u f A \<tau>\<rightarrow> S''
    \<Longrightarrow> S, I \<Midarrow>\<sigma> @ (In_labels u f A \<tau>)\<Rightarrow> S'', T"
| io_none: "S, I \<Midarrow>\<sigma>\<Rightarrow> S', I'
    \<Longrightarrow> S' \<midarrow>Bb\<rightarrow> S''
    \<Longrightarrow> S, I \<Midarrow>\<sigma>\<Rightarrow> S'', I'"
| io_out: "S, I \<Midarrow>\<sigma>\<Rightarrow> S', I'
    \<Longrightarrow> S' \<midarrow>Oo f u p d \<tau>\<rightarrow> S''
    \<Longrightarrow> S, I \<Midarrow>\<sigma> @ [(\<tau>, {Out f u p d})]\<Rightarrow> S'', I'"

inductive count :: "'a \<Rightarrow> in_seq \<Rightarrow> io_event trace \<Rightarrow> 'a  \<Rightarrow> in_seq \<Rightarrow> nat \<Rightarrow> bool" where
  count_nop: "count S I [] S I 0"
| count_in: "count S I \<sigma> S' ((u, f, A, \<tau>)#T) i
    \<Longrightarrow> S' \<midarrow>Ii u f A \<tau>\<rightarrow> S''
    \<Longrightarrow> count S I (\<sigma> @ (In_labels u f A \<tau>)) S'' T (i+1)"
| count_none: "count S I \<sigma> S' I' i
    \<Longrightarrow> S' \<midarrow>Bb\<rightarrow> S''
    \<Longrightarrow> count S I \<sigma> S'' I' (i+1)"
| count_out: "count S I \<sigma> S' I' i 
    \<Longrightarrow> S' \<midarrow>Oo f u p d \<tau>\<rightarrow> S''
    \<Longrightarrow> count S I (\<sigma> @ [(\<tau>, {Out f u p d})]) S'' I' (i+1)"

lemma count_0: "count S I \<sigma> S' I' 0 \<longleftrightarrow> S = S' \<and> I = I' \<and> \<sigma> = []"
  by (rule iffI, erule count.cases; simp add: count_nop)

lemma count_0': "count S\<^sub>0 I \<sigma> S' I' i \<Longrightarrow> i = 0 \<longleftrightarrow> S' = S\<^sub>0 \<and> I = I' \<and> \<sigma> = []"
  by (rule iffI, simp add: count_0, erule count.cases, assumption) (use no_way_back in blast)+

lemma subst_allE: "P t \<Longrightarrow> (\<And>s. s = t \<Longrightarrow> P s \<Longrightarrow> Q) \<Longrightarrow> Q"
  by simp

lemma ttrans_induct [consumes 2, case_names ttrans_nop ttrans_in ttrans_none ttrans_out]:
  "wf_in_seq' I \<Longrightarrow> S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S, I'
       \<Longrightarrow> (\<And>I. (wf_in_seq' I \<Longrightarrow> P I [] S\<^sub>0 I))
       \<Longrightarrow> (\<And>I \<sigma> S' u f A \<tau> T S''.
              (wf_in_seq' I 
               \<Longrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', (u, f, A, \<tau>)#T)
               \<Longrightarrow> P I \<sigma> S' ((u, f, A, \<tau>)#T) 
               \<Longrightarrow> (S' \<midarrow>Ii u f A \<tau>\<rightarrow> S'')
               \<Longrightarrow> P I (\<sigma> @ (In_labels u f A \<tau>)) S'' T))
       \<Longrightarrow> (\<And>I \<sigma> S' I' S''. 
              (wf_in_seq' I
               \<Longrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I')
               \<Longrightarrow> P I \<sigma> S' I'
               \<Longrightarrow> (S' \<midarrow>Bb\<rightarrow> S'')
               \<Longrightarrow> P I \<sigma> S'' I'))
       \<Longrightarrow> (\<And>I \<sigma> S' I' f u p d \<tau> S''. 
              (wf_in_seq' I
               \<Longrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I')
               \<Longrightarrow> P I \<sigma> S' I'
               \<Longrightarrow> (S' \<midarrow>Oo f u p d \<tau>\<rightarrow> S'')
               \<Longrightarrow> P I (\<sigma> @ [(\<tau>, {Out f u p d})]) S'' I'))
       \<Longrightarrow> P I \<sigma> S I'"
  apply(erule rev_mp)
  apply(erule subst_allE[of "\<lambda>x. (x, I \<Midarrow>\<sigma>\<Rightarrow> S, I')" S\<^sub>0])
  subgoal for S
    apply(rotate_tac -1, induct rule: ttrans.induct)
    unfolding wf_in_seq'_def by fastforce+
  done

lemma count_induct [consumes 2, case_names count_nop_case count_in_case count_none_case count_out_case]:
  "wf_in_seq' I \<Longrightarrow> count S\<^sub>0 I \<sigma> S I' i
       \<Longrightarrow> (\<And>I. (wf_in_seq' I \<Longrightarrow> P S\<^sub>0 I [] I 0))
       \<Longrightarrow> (\<And>I \<sigma> S' u f A \<tau> T i S''.
              (wf_in_seq' I 
               \<Longrightarrow> count S\<^sub>0 I \<sigma> S' ((u, f, A, \<tau>)#T) i
               \<Longrightarrow> P S' I \<sigma> ((u, f, A, \<tau>)#T) i
               \<Longrightarrow> (S' \<midarrow>Ii u f A \<tau>\<rightarrow> S'')
               \<Longrightarrow> P S'' I (\<sigma> @ (In_labels u f A \<tau>)) T (i+1)))
       \<Longrightarrow> (\<And>I \<sigma> S' I' i S''. 
              (wf_in_seq' I
               \<Longrightarrow> count S\<^sub>0 I \<sigma> S' I' i
               \<Longrightarrow> P S' I \<sigma> I' i
               \<Longrightarrow> (S' \<midarrow>Bb\<rightarrow> S'')
               \<Longrightarrow> P S'' I \<sigma> I' (i+1)))
       \<Longrightarrow> (\<And>I S' \<sigma> I' i f u p d \<tau> S''. 
              (wf_in_seq' I
               \<Longrightarrow> count S\<^sub>0 I \<sigma> S' I' i
               \<Longrightarrow> P S' I \<sigma> I' i
               \<Longrightarrow> (S' \<midarrow>Oo f u p d \<tau>\<rightarrow> S'')
               \<Longrightarrow> P S'' I (\<sigma> @ [(\<tau>, {Out f u p d})]) I' (i+1)))
       \<Longrightarrow> P S I \<sigma> I' i"
  apply(erule rev_mp)
  apply(erule subst_allE[of "\<lambda>x. count x I \<sigma> S I' i" S\<^sub>0])
  subgoal for S
    apply(rotate_tac -1, induct rule: count.induct)
    by fastforce+
  done

lemma count_ttrans: 
  assumes "wf_in_seq' I"
    shows "(\<exists>i. count S\<^sub>0 I \<sigma> S' I' i) \<longleftrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I')"
proof (rule iffI)
  show "(\<exists>i. count S\<^sub>0 I \<sigma> S' I' i) \<Longrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I')"
  proof (erule exE)
    fix i
    note assms(1)
    moreover assume "count S\<^sub>0 I \<sigma> S' I' i"
    ultimately show "S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I'"
      by (induct rule: count_induct) (auto simp add: ttrans.intros)
  qed
next
  note assms(1)
  thus "S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I' \<Longrightarrow> (\<exists>i. count S\<^sub>0 I \<sigma> S' I' i)"
    by (induct rule: ttrans_induct) (use count.intros in metis)+
qed

lemma count_valid_code: "count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> valid_code S"
proof (induction i arbitrary: I \<sigma> S I')
  case 0
  thus ?case by (cases rule: count.cases) (auto simp add: valid_S\<^sub>0)
next
  case (Suc i)
  from Suc.prems show ?case 
    by (cases rule: count.cases) (metis Suc.IH valid_step add_diff_cancel_right' diff_Suc_1)+
qed

lemma ttrans_valid_code0: "ttrans S I \<sigma> S' I' \<Longrightarrow> S = S\<^sub>0 \<Longrightarrow> valid_code S'"
  by (induction rule: ttrans.induct) (use valid_S\<^sub>0 valid_step in metis)+

lemma ttrans_valid_code: "ttrans S\<^sub>0 I \<sigma> S I' \<Longrightarrow> valid_code S"
  using ttrans_valid_code0 by blast

lemma ttrans_determ: 
  assumes "count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i"
      and "count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i"
    shows "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
using assms
proof(induction i arbitrary: S\<^sub>1 I\<^sub>1 S\<^sub>2 I\<^sub>2 \<sigma>\<^sub>1 \<sigma>\<^sub>2 rule: nat.induct)
  case zero
  from zero.prems(1) have "S\<^sub>1 = S\<^sub>0 \<and> I\<^sub>1 = I \<and> \<sigma>\<^sub>1 = []" by (cases rule: count.cases) auto
  moreover from zero.prems(2) have "S\<^sub>2 = S\<^sub>0 \<and> I\<^sub>2 = I \<and> \<sigma>\<^sub>2 = []" by (cases rule: count.cases) auto
  ultimately show ?case by simp    
next
  case (Suc j S\<^sub>1' I\<^sub>1 S\<^sub>2' I\<^sub>2 \<sigma>\<^sub>1' \<sigma>\<^sub>2')
  show ?case using Suc.prems(1)
  proof(cases rule: count.cases)
    case (count_in \<sigma>\<^sub>1 S\<^sub>1 u\<^sub>1 f\<^sub>1 A\<^sub>1 \<tau>\<^sub>1 i\<^sub>1)
    show ?thesis using Suc.prems(2)
    proof(cases rule: count.cases)
      case count_in': (count_in \<sigma>\<^sub>2 S\<^sub>2 u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> (u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1 = (u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 "(u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1" \<sigma>\<^sub>2 S\<^sub>2 "(u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2"]
        using count_in(2,3) by auto
      thus ?thesis using determ count_valid_code count_in(1,3,4) count_in'(1,4) by blast
    next
      case (count_none S\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> (u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1 =  I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2'"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 "(u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1" \<sigma>\<^sub>2' S\<^sub>2 I\<^sub>2]
        using count_in(2,3) by auto
      thus ?thesis using determ count_valid_code count_in(1,3,4) count_none(3) by blast
    next
      case (count_out \<sigma>\<^sub>2 S\<^sub>2 i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> (u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1 =  I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 "(u\<^sub>1, f\<^sub>1, A\<^sub>1, \<tau>\<^sub>1) # I\<^sub>1" \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2]
        using count_in(2,3) by auto
      thus ?thesis using determ count_valid_code count_in(1,3,4) count_out(1,4) by blast
    qed
  next
    case (count_none S\<^sub>1 i\<^sub>1)
    show ?thesis using Suc.prems(2)
    proof(cases rule: count.cases)
      case (count_in \<sigma>\<^sub>2 S\<^sub>2 u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = (u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2 \<and> \<sigma>\<^sub>1' = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1' S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2 S\<^sub>2 "(u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2"]
        using count_none(1,2) by auto
      thus ?thesis using determ count_valid_code count_none(3) count_in(1,3,4) by blast
    next
      case count_none': (count_none S\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1' = \<sigma>\<^sub>2'"
        using Suc.IH[of \<sigma>\<^sub>1' S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2' S\<^sub>2 I\<^sub>2]
        using count_none(1,2) by auto
      thus ?thesis using determ count_valid_code count_none(3) count_none'(2,3) by blast
    next
      case (count_out \<sigma>\<^sub>2 S\<^sub>2 i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1' = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1' S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2]
        using count_none(1,2) by auto
      thus ?thesis using determ count_valid_code count_none(3) count_out(1,3,4) by blast
    qed
  next
    case (count_out \<sigma>\<^sub>1 S\<^sub>1 i\<^sub>1 f\<^sub>1 u\<^sub>1 p\<^sub>1 d\<^sub>1 \<tau>\<^sub>1)
    show ?thesis using Suc.prems(2)
    proof(cases rule: count.cases)
      case (count_in \<sigma>\<^sub>2 S\<^sub>2 u\<^sub>2 f\<^sub>2 A\<^sub>2 \<tau>\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = (u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2 S\<^sub>2 "(u\<^sub>2, f\<^sub>2, A\<^sub>2, \<tau>\<^sub>2) # I\<^sub>2"]
        using count_out(2,3) by auto
      thus ?thesis using determ count_valid_code count_out(4) count_in(1,3,4) by blast
    next
      case (count_none S\<^sub>2 i\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 =  I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2'"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2' S\<^sub>2 I\<^sub>2]
        using count_out(2,3) by auto
      thus ?thesis using determ count_valid_code count_out(4) count_none(2,3) by blast
    next
      case count_out': (count_out \<sigma>\<^sub>2 S\<^sub>2 i\<^sub>2 f\<^sub>2 u\<^sub>2 p\<^sub>2 d\<^sub>2 \<tau>\<^sub>2)
      hence "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
        using Suc.IH[of \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2]
        using count_out(2,3) by auto
      thus ?thesis using determ count_valid_code count_out(1,4) count_out'(1,3,4) by blast
    qed
  qed
qed

lemma ttrans_conc: "wf_in_seq' I \<Longrightarrow> S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S, I' \<Longrightarrow> \<exists>H. I = H @ I'"
  by (induct rule: ttrans_induct) auto

lemma ttrans_count_fun: 
  assumes "wf_in_seq' I"
   and "\<And>\<sigma> S I' i. count S\<^sub>0 I \<sigma> S I' i \<Longrightarrow> f I i = g S"
   and "\<And>i\<^sub>1 i\<^sub>2. f I i\<^sub>1 = f I i\<^sub>2 \<Longrightarrow> i\<^sub>1 = i\<^sub>2"
   and "S\<^sub>0, I \<Midarrow>\<sigma>\<^sub>1\<Rightarrow> S\<^sub>1, I\<^sub>1"
   and "S\<^sub>0, I \<Midarrow>\<sigma>\<^sub>2\<Rightarrow> S\<^sub>2, I\<^sub>2"
   and "g S\<^sub>1 = g S\<^sub>2"
 shows "S\<^sub>1 = S\<^sub>2 \<and> I\<^sub>1 = I\<^sub>2 \<and> \<sigma>\<^sub>1 = \<sigma>\<^sub>2"
proof -
  obtain i\<^sub>1 where i\<^sub>1: "count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1" 
    using assms(1,4) count_ttrans[of I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1, THEN iffD2] by auto
  moreover obtain i\<^sub>2 where i\<^sub>2: "count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2" 
    using assms(1,5) count_ttrans[of I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2, THEN iffD2] by auto
  ultimately have "i\<^sub>1 = i\<^sub>2" using assms(3,6) by (simp add: assms(2))
  thus ?thesis using i\<^sub>1 i\<^sub>2 ttrans_determ by auto
qed

lemma ttrans_count_le:
  "wf_in_seq' I
   \<Longrightarrow> count S\<^sub>0 I \<sigma>\<^sub>2 S\<^sub>2 I\<^sub>2 i\<^sub>2
   \<Longrightarrow> i\<^sub>1 \<le> i\<^sub>2
   \<Longrightarrow> \<exists>\<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1. count S\<^sub>0 I \<sigma>\<^sub>1 S\<^sub>1 I\<^sub>1 i\<^sub>1"
proof(induction arbitrary: i\<^sub>1 rule: count_induct)
  case (count_nop_case I i\<^sub>1)
  hence "i\<^sub>1 = 0" by simp
  moreover have "count S\<^sub>0 I [] S\<^sub>0 I 0" by (rule count_nop)
  ultimately show ?case by blast
next
  case (count_in_case I \<sigma> S\<^sub>2 u f A \<tau> T i S\<^sub>2' i\<^sub>1)
  show ?case
  proof (cases "i\<^sub>1 = Suc i")
    case True
    hence "count S\<^sub>0 I (\<sigma> @ In_labels u f A \<tau>) S\<^sub>2' T i\<^sub>1"
      using count_in Suc_eq_plus1 count_in_case.hyps(2,3) by metis
    thus ?thesis by blast
  next
    case False
    thus ?thesis using count_in_case.IH[of i\<^sub>1] count_in_case.prems by auto
  qed
next
  case (count_none_case I \<sigma> S\<^sub>2 I' i S\<^sub>2' i\<^sub>1)
  show ?case
  proof (cases "i\<^sub>1 = Suc i")
    case True
    hence "count S\<^sub>0 I \<sigma> S\<^sub>2' I' i\<^sub>1"
      using count_none Suc_eq_plus1 count_none_case.hyps(2,3) by metis
    thus ?thesis by blast
  next
    case False
    thus ?thesis using count_none_case.IH[of i\<^sub>1] count_none_case.prems by auto
  qed
next
  case (count_out_case I S\<^sub>2 \<sigma> I' i f u p d \<tau> S\<^sub>2' i\<^sub>1)
  show ?case
  proof (cases "i\<^sub>1 = Suc i")
    case True
    hence "count S\<^sub>0 I (\<sigma> @ [(\<tau>, {Out f u p d})]) S\<^sub>2' I' i\<^sub>1"
      using count_out Suc_eq_plus1 count_out_case.hyps(2,3) by metis
    thus ?thesis by blast
  next
    case False
    thus ?thesis using count_out_case.IH[of i\<^sub>1] count_out_case.prems by auto
  qed
qed

lemma ttrans_empty: "S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S', I' \<Longrightarrow> I = [] \<longrightarrow> I' = []"
  by (induct rule: ttrans.induct; blast)

subsection \<open>Interference\<close>

inductive interf :: "ut \<Rightarrow> in_seq \<Rightarrow> ts \<Rightarrow> bool" ("_ \<leadsto>\<^sub>_ _" 1) where
  "I = H @ (u, f, A, \<tau>) # T
   \<Longrightarrow> a \<in> dom A
   \<Longrightarrow> I' = H @ (u, f, A(a \<mapsto> d), \<tau>) # T
   \<Longrightarrow> S\<^sub>0, I \<Midarrow>s\<Rightarrow> S\<^sub>1, I\<^sub>1
   \<Longrightarrow> S\<^sub>1 \<midarrow>Oo f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<tau>\<^sub>1\<rightarrow> S\<^sub>2 
   \<Longrightarrow> S\<^sub>0, I' \<Midarrow>\<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]\<Rightarrow> S'\<^sub>1, I'\<^sub>1
   \<Longrightarrow> \<tau>\<^sub>2 \<ge> \<tau>\<^sub>1
   \<Longrightarrow> \<forall>i \<le> length \<sigma>'\<^sub>1. (\<forall>D'\<^sub>1. (\<sigma>'\<^sub>1 @ [(\<tau>\<^sub>2, D)]) ! i = (\<tau>\<^sub>1, D'\<^sub>1) \<longrightarrow> Out f\<^sub>1 u\<^sub>1 p\<^sub>1 v\<^sub>1 \<notin> D'\<^sub>1)
   \<Longrightarrow> (\<tau>, a) \<leadsto>\<^sub>I \<tau>\<^sub>1"

subsection \<open>Enforcement\<close>

fun \<rho> :: "in_seq \<Rightarrow> io_event \<Rightarrow> ts \<Rightarrow> if_event set" where
  "\<rho> I (In u f a d) \<tau> = {IIn u f (\<tau>, a)}"
| "\<rho> I (Out f u p d) \<tau> = {IOut f u p \<tau>} \<union> {IItf (\<tau>', a) \<tau> | \<tau>' a. (\<tau>', a) \<leadsto>\<^sub>I \<tau>}"

definition \<Delta> :: "in_seq \<Rightarrow> io_event trace \<Rightarrow> if_event trace" where
  "\<Delta> I \<sigma> = map (\<lambda>(\<tau>, D). (\<tau>, \<Union>{\<rho> I e \<tau> | e. e \<in> D})) \<sigma>"

definition enforces :: "if_event pro \<Rightarrow> bool" where
  "enforces P = (\<forall>I \<sigma> S I'. wf_in_seq' I \<longrightarrow> (S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S, I') \<longrightarrow> \<Delta> I \<sigma> \<in> P)"

lemma enforces_approx:
  fixes approx :: "if_event trace \<Rightarrow> if_event trace \<Rightarrow> bool" (infix "\<sqsubseteq>" 100)
  assumes mono:   "\<And>\<sigma> \<sigma>'. P \<in> \<Pi> \<Longrightarrow> \<sigma>' \<in> P \<Longrightarrow> \<sigma> \<sqsubseteq> \<sigma>' \<Longrightarrow> \<sigma> \<in> P"
      and approx: "\<And>I \<sigma> S I'. P \<in> \<Pi> \<Longrightarrow> wf_in_seq' I \<Longrightarrow>  S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S, I' \<Longrightarrow> \<Delta> I \<sigma> \<sqsubseteq> t S"
      and happy:  "\<And>I \<sigma> S I'. P \<in> \<Pi> \<Longrightarrow> wf_in_seq' I \<Longrightarrow> S\<^sub>0, I \<Midarrow>\<sigma>\<Rightarrow> S, I' \<Longrightarrow> t S \<in> P"
  shows "P \<in> \<Pi> \<Longrightarrow> enforces P"
  using assms unfolding enforces_def by metis

end


end