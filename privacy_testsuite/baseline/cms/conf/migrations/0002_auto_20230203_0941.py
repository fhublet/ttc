# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='paper',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='paper',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='papercoauthor',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='papercoauthor',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='paperpcconflict',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='paperpcconflict',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='paperversion',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='paperversion',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='review',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='review',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='reviewassignment',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='reviewassignment',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='userpcconflict',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='userpcconflict',
            name='jeeves_vars',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='jeeves_id',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='jeeves_vars',
        ),
        migrations.AlterField(
            model_name='comment',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='paper',
            name='accepted',
            field=models.NullBooleanField(),
        ),
        migrations.AlterField(
            model_name='paper',
            name='author',
            field=models.ForeignKey(to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='papercoauthor',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='paperpcconflict',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='paperpcconflict',
            name='pc',
            field=models.ForeignKey(to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='paperversion',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='review',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='review',
            name='reviewer',
            field=models.ForeignKey(to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='reviewassignment',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='reviewassignment',
            name='user',
            field=models.ForeignKey(to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='tag',
            name='paper',
            field=models.ForeignKey(to='conf.Paper', null=True),
        ),
        migrations.AlterField(
            model_name='userpcconflict',
            name='pc',
            field=models.ForeignKey(related_name=b'userpcconflict_pc', to='conf.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='userpcconflict',
            name='user',
            field=models.ForeignKey(related_name=b'userpcconflict_user', to='conf.UserProfile', null=True),
        ),
    ]
