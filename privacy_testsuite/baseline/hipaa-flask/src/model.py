"""Models for HIPAA case study
@author: Hoang Nguyen

Similar to the ones in Jeeves
"""

from flask_sqlalchemy import SQLAlchemy
from flask_user import UserMixin


db = SQLAlchemy()

class Address(db.Model):
    __tablename__ = "Address"
    # Mailing address of a person's residency.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Street = db.Column(db.String(100))
    City = db.Column(db.String(30), nullable=False)
    State = db.Column(db.String(20), nullable=False)
    ZipCode = db.Column(db.String(5), nullable=False)

    def __repr__(self):
        # Returns a string representation of this address.
        return f"{self.Street}\n{self.ZipCode} {self.City}, {self.State}"
    
class Individual(db.Model):
    __tablename__ = "Individual"
    # A single person who can be a patient to a covered entity, and might have
    # any/all other attributes of people.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    FirstName = db.Column(db.String(1024), nullable=False)
    LastName = db.Column(db.String(1024), nullable=False)
    Email = db.Column(db.String(1024))
    AddressId = db.Column(db.Integer, db.ForeignKey(Address.id))
    Address = db.relationship('Address', backref='individuals')
    BirthDate = db.Column(db.Date)
    Sex = db.Column(db.String(6))
    SSN = db.Column(db.String(9))
    TelephoneNumber = db.Column(db.String(10))
    FaxNumber = db.Column(db.String(10))
    DriversLicenseNumber = db.Column(db.String(20))
    Employer = db.Column(db.String(50))
    ReligiousAffiliation = db.Column(db.String(100))

    @property
    def Name(self):
        if self.FirstName == "" and self.LastName == "":
            return "Unknown"
        else:
            return self.FirstName + " " + self.LastName

class BusinessAssociate(db.Model):
    __tablename__ = "BusinessAssociate"
    # Persons or corporations that perform services for covered entities. They
    # may or may not be covered entities themselves.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Name = db.Column(db.String(1024), nullable=False)
    Covered = db.Column(db.Boolean, nullable=False)

class InformationTransferSet(db.Model):
    __tablename__ = "InformationTransferSet"
    # Collection of private information that can be shared
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

class CoveredEntity(db.Model):
    __tablename__ = "CoveredEntity"
    # Health plan, health clearinghouse, or health care provider making
    # sensitive transactions. This includes hospitals.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ein = db.Column(db.String(10), nullable=False)
    name = db.Column(db.String(1024), nullable=False)

class BusinessAssociateAgreement(db.Model):
    __tablename__ = "BusinessAssociateAgreement"
    # Agreement between a business associate and a covered entity where 
    # the covered entity gives the business associate its data,
    # and the business associate completes the entity's transactions by standard
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    BusinessAssociateID = db.Column(db.Integer, db.ForeignKey(BusinessAssociate.id), nullable=False)
    BusinessAssociate = db.relationship('BusinessAssociate', backref='agreements')
    SharedInformationID = db.Column(db.Integer, db.ForeignKey(InformationTransferSet.id), nullable=False)
    SharedInformation = db.relationship('InformationTransferSet', backref='agreements')
    CoveredEntityID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id), nullable=False)
    CoveredEntity = db.relationship('CoveredEntity', backref='agreements')
    Purpose = db.Column(db.Text)

class HospitalVisit(db.Model):
    __tablename__ = "HospitalVisit"
    # Patient's visit to a hospital for medical purposes
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    patientID = db.Column(db.Integer, db.ForeignKey(Individual.id), nullable=False)
    patient = db.relationship('Individual', backref='visits')
    hospitalID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id), nullable=False)
    hospital = db.relationship('CoveredEntity', backref='Patients')
    date_admitted = db.Column(db.Date, nullable=False)
    location = db.Column(db.Text)
    condition = db.Column(db.Text)
    date_released = db.Column(db.Date)

class Treatment(db.Model):
    __tablename__ = "Treatment"
    # Provided medical Treatment, medication, or service.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    service = db.Column(db.String(100), nullable=False)
    date_performed = db.Column(db.Date, nullable=False)
    prescribing_entityID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id))
    prescribing_entity = db.relationship('CoveredEntity', backref='prescriptions', foreign_keys=[prescribing_entityID])
    performing_entityID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id))
    performing_entity = db.relationship('CoveredEntity', backref='performings', foreign_keys=[performing_entityID])
    patientID = db.Column(db.Integer, db.ForeignKey(Individual.id))
    patient = db.relationship('Individual', backref='treatments')

class Diagnosis(db.Model):
    __tablename__ = "Diagnosis"
    # Recognition of health condition or situation by a medical professional.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Manifestation = db.Column(db.String(100), nullable=False)
    Diagnosis = db.Column(db.String(255), nullable=False)
    DateRecognized = db.Column(db.Date, nullable=False)
    RecognizingEntityID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id), nullable=False)
    RecognizingEntity = db.relationship('CoveredEntity', backref='recognizes')
    PatientID = db.Column(db.Integer, db.ForeignKey(Individual.id))
    Patient = db.relationship('Individual', backref='diagnoses')

class TreatmentTransfer(db.Model):
    __tablename__ = "TreatmentTransfer"
    # Datum about Treatment offered and performed on patients
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    SetID = db.Column(db.Integer, db.ForeignKey(InformationTransferSet.id), nullable=False)
    Set = db.relationship('InformationTransferSet', backref='Treatments')
    TreatmentID = db.Column(db.Integer, db.ForeignKey(Treatment.id), nullable=False)
    Treatment = db.relationship('Treatment', backref='Treatments')

class DiagnosisTransfer(db.Model):
    __tablename__ = "DiagnosisTransfer"
    # Datum about diagnoses made about patients
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    SetID = db.Column(db.Integer, db.ForeignKey(InformationTransferSet.id), nullable=False)
    Set = db.relationship('InformationTransferSet', backref='Diagnoses')
    DiagnosisID = db.Column(db.Integer, db.ForeignKey(Diagnosis.id), nullable=False)
    Diagnosis = db.relationship('Diagnosis', backref='Diagnoses')

class HospitalVisitTransfer(db.Model):
    __tablename__ = "HospitalVisitTransfer"
    # Datum about patient's visiting a hospital
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    SetID = db.Column(db.Integer, db.ForeignKey(InformationTransferSet.id), nullable=False)
    Set = db.relationship('InformationTransferSet', backref='Visits')
    VisitID = db.Column(db.Integer, db.ForeignKey(HospitalVisit.id), nullable=False)
    Visit = db.relationship('HospitalVisit', backref='Visits')

class Transaction(db.Model):
    __tablename__ = "Transaction"
    # A defined standard transaction between covered entitities.
    """
    Attributes:
    Standard - Transaction Code: ICS-10-PCS, HCPCS, e.g.
    FirstParty, SecondParty - Covered entities performing the transaction
    SharedInformation - Information transferred between the parties to fulfill the transaction.
    """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Standard = db.Column(db.String(100), nullable=False)
    FirstPartyID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id), nullable=False)
    FirstParty = db.relationship('CoveredEntity', backref='FirstPartytransactions', foreign_keys=[FirstPartyID])
    SecondPartyID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id), nullable=False)
    SecondParty = db.relationship('CoveredEntity', backref='SecondPartytransactions', foreign_keys=[SecondPartyID])
    SharedInformationID = db.Column(db.Integer, db.ForeignKey(InformationTransferSet.id), nullable=False)
    SharedInformation = db.relationship('InformationTransferSet', backref='transactions')
    DateRequested = db.Column(db.Date, nullable=False)
    DateResponded = db.Column(db.Date, nullable=False)
    Purpose = db.Column(db.String(100))

class PersonalRepresentative(db.Model):
    __tablename__ = "PersonalRepresentative"
    # Relationship of a person who can make medical decisions for another.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    DependentID = db.Column(db.Integer, db.ForeignKey(Individual.id), nullable=False)
    Dependent = db.relationship('Individual', backref='dependents', foreign_keys=[DependentID])
    RepresentativeID = db.Column(db.Integer, db.ForeignKey(Individual.id), nullable=False)
    Representative = db.relationship('Individual', backref='represents', foreign_keys=[RepresentativeID])
    Parent = db.Column(db.Boolean, nullable=False)

class UserProfile(db.Model, UserMixin):
    __tablename__ = "UserProfile"
    # Information about a user of the website.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(1024), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')
    #email = db.Column(db.String(1024), nullable=False)
    #profiletype = db.Column(db.Integer, nullable=False)
    #name = db.Column(db.String(1024), nullable=False)
    email = db.Column(db.String(1024))
    profiletype = db.Column(db.Integer)
    name = db.Column(db.String(1024))
    entityID = db.Column(db.Integer, db.ForeignKey(CoveredEntity.id))
    entity = db.relationship('CoveredEntity', backref='profile')
    associateID = db.Column(db.Integer, db.ForeignKey(BusinessAssociate.id))
    associate = db.relationship('BusinessAssociate', backref='profile')
    individualID = db.Column(db.Integer, db.ForeignKey(Individual.id))
    individual = db.relationship('Individual', backref='profile')

""" end """
