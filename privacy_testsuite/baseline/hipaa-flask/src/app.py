from flask import Flask, render_template, redirect, url_for, Response, request
from flask_user import UserManager, user_registered, current_user, login_required
from model import db, UserProfile, Individual, CoveredEntity, Treatment, Diagnosis, Transaction, \
    Address, HospitalVisit, InformationTransferSet, DiagnosisTransfer, HospitalVisitTransfer, TreatmentTransfer, \
    BusinessAssociate, BusinessAssociateAgreement
from datetime import date

app = Flask(__name__) #nc

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db' #nc
app.config['USER_APP_NAME'] = "HIPAA demonstration" #nc
app.config['USER_ENABLE_EMAIL'] = False #nc
app.config['USER_ENABLE_USERNAME'] = True #nc
app.config['USER_REQUIRE_RETYPE_PASSWORD'] = False #nc
app.config['SECRET_KEY'] = '_5#yfasQ8sansaxec/][#1öalskflkajsöfalfds' #nc
app.config['USER_UNAUTHORIZED_ENDPOINT'] = 'error' #nc

# default error route
@app.route('/error')
def error():
    msg = "You are not allowed to access this page: Not a User"
    return render_template('error.html', message = msg)

# use this if you want to perform extra action after user registered
@user_registered.connect_via(app) #nc
def after_user_registered_hook(sender, user, **extra): #nc
    pass #nc

db.init_app(app) #nc

with app.app_context(): #nc
    #db.drop_all()
    db.create_all()
    #from sampleData import initData
    #initData(db)
    
user_manager = UserManager(app, db, UserProfile) #nc

@app.route('/accounts/login')
def login():
    return redirect(url_for('user.login'))

@app.route('/accounts/register')
def register():
    return redirect(url_for('user.register'))

@app.route('/accounts/logout')
def logout():
    return redirect(url_for('user.logout'))

@app.route('/accounts/profile', methods=['GET', 'POST'])
@login_required
def profile():
    """Displaying and updating profiles.
    """
    if request.method == 'POST':
        current_user.email = request.form["email"]
        current_user.profiletype = request.form["profiletype"]
        current_user.name = request.form["name"]
        db.session.commit()
        return redirect(url_for("profile"))

    data = {
          "profile": current_user
        , "which_page": "profile"}
    return render_template("profile.html", **data)

@app.route('/')
@app.route('/index')
def index():
    """The main page shows patients and entities.
    """
    if not current_user.is_authenticated:
        return redirect(url_for('user.login'))

    patients = Individual.query.all()
    entities = CoveredEntity.query.all()

    data = {"patients": patients
          , "entities": entities
          , 'name': current_user.name if current_user.is_authenticated else "Guest"}
    
    return render_template("index.html", **data)

@app.route('/about')
def about_view():
    """About the system.
    """
    data = {'which_page' : "about"}
    return render_template("about.html", **data)

@app.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    """Viewing all users.
    """
    # TODO: Have a better mechanism than this for letting someone know they
    # can't see something.
    if current_user.profiletype != 3:
        return redirect(url_for("index"))

    user_profiles = UserProfile.query.all()

    if request.method == 'POST':
        for profile in user_profiles:
            query_param_name = 'level-' + profile.username
            level = request.form[query_param_name]
            types = ['individual', 'coveredEntity', 'businessAssociate', 'government', 'research', 'clergy']
            profile.profiletype = types.index(level)
            db.session.commit()
#    profiletype = IntegerField(help_text="Type of body this user is about. \
#        0=None, 1=Individual, 2=CoveredEntity, 3=BusinessAssociate, \
#        4=Government, 5=Research, 6=Clergy")
    data = {
        'user_profiles': user_profiles,
        'which_page' : "users"
    }
    return render_template("users_view.html", **data)

@app.route('/patients/<int:id>/treatments')
@login_required
def patient_treatment(id):
    """Treatments.
    """
    p = Individual.query.get(id)
    treatments = Treatment.query.filter_by(patient=p).all()
    data = {"first_name" : p.FirstName
         , "last_name" : p.LastName
         , "treatments" : treatments}
    return render_template("treatments.html"
        , **data)

@app.route('/patients/<int:id>/diagnoses')
@login_required
def patient_diagnoses(id):
    """Diagnoses.
    """
    p = Individual.query.get(id)
    newDiagnoses = Diagnosis.query.filter_by(Patient=p).all()
    data = {"first_name" : p.FirstName
             , "last_name" : p.LastName
             , "diagnoses" : newDiagnoses}
    return render_template("diagnoses.html"
            , **data)
        
@app.route('/patients/<int:id>/')
@app.route('/patients/<int:id>/info')
@login_required
def info(id):
    """Viewing information about an individual.
    """
    p = Individual.query.get(id)
    dataset = []
    dataset.append(("Sex", p.Sex, False))
    data = {"patient": p
             , "dataset": dataset}
    return render_template("info.html"
            , **data)

@app.route('/entities/<int:id>/transactions')
@login_required
def entity_transaction(id):
    """
    Viewing transactions.
    """
    entity = CoveredEntity.query.get(id)
    transactions = Transaction.query.filter_by(FirstParty=entity).all()
    other_transactions = Transaction.query.filter_by(SecondParty=entity).all()
    data = {"entity": entity
             , "transactions":transactions
             , "other_transactions": other_transactions}
    return render_template("transactions.html"
            , **data)

@app.route('/entities/<int:id>/associates')
@login_required
def entity_associate(id):
    entity = CoveredEntity.query.get(id)
    associates = entity.agreements
    data = {"entity":entity, "associates":associates}
    return render_template("associates.html"
        , **data)

@app.route('/entities/<int:id>/')
@app.route('/entities/<int:id>/directory')
@login_required
def entity_directory(id):
    """Viewing covered entities.
    """
    entity = CoveredEntity.query.get(id)
    visits = entity.Patients

    data = {"entity":entity, "visits":visits}
    return render_template("directory.html", **data)

if __name__ == '__main__': #nc
    app.run()
