from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os
from tqdm import tqdm
import json
import signal

from flask_hashing import Hashing
from flask import Flask
from lorem_text import lorem

from tools import Task, Subtask

bank = Flask("The Databank")
bank.secret_key = "E4kB3BUlTXivYtkaKnCb9XHGIr9erSEIX0n0MWOnAqlqr2PGKWjPgWp2834M5PmDqx2dEvI2EV7YdriY"
hashing = Hashing(bank)

class Scenario:

    login_url = "http://localhost:5000/login"
    logout_url = "http://localhost:5000/logout"
    register_url = "http://localhost:5000/5/accounts/register"
    login_url2 = "http://localhost:5000/5/accounts/do_login"
    submit_url = "http://localhost:5000/5/submit"
    view_papers_url = "http://localhost:5000/5/"
    view_paper_url = "http://localhost:5000/5/paper"

    def __init__(self, sc, app_cmd, cwd, database, database2, db_folder, databank_log, time_log, state_folder, default_pdf):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.database2 = database2
        self.db_folder = db_folder
        self.databank_log = databank_log
        self.time_log = time_log
        self.state_folder = state_folder
        self.default_pdf = default_pdf

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n, self.p_name, self.p = config
        pref = f'conf.initialize (sc={self.sc}, u={self.u}, n={self.n}, p={self.p_name})'

        with Task(pref, 'Setting up users and policies', cplx=True) as task:
            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                policy = cur.execute('SELECT id, policy FROM users ORDER BY id LIMIT 1').fetchall()
                users2 = cur.execute('SELECT id FROM users').fetchall()

            if len(users2) > self.u:
                with Subtask(f'Deleting {len(users2)-self.u} users', task):
                    cur.execute(f'DELETE FROM users ORDER BY id DESC LIMIT {len(users2)-self.u}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)
                
            if policy and policy[0][1].format(policy[0]) != self.p.format(policy[0][0]):
                with Subtask('Updating existing policies', task):
                    for id_ in users2:
                        cur.execute(f"UPDATE users SET policy = '{self.p.format(id_[0])}' WHERE id = {id_[0]}")

            if len(users2) < self.u:
                with Subtask(f'Adding {self.u-len(users2)} users', task):
                    f = lambda i: hashing.hash_value(f"password{i}")
                    g = lambda i: self.p.format(i).replace('"', '""')
                    to_insert = [f'({i}, "user{i}", "{f(i)}", "{g(i)}")' for i in range(len(users2), self.u)]
                    cur.execute(f'INSERT INTO users (id, name, hash, policy) VALUES {",".join(to_insert)}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)

            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Loading test database', cplx=True) as task:
            db_fn = os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}.db')
            loaded = False

            with Subtask(f'Trying to load existing database', task):
                # If it exists, load it
                if os.path.exists(db_fn):
                    shutil.copy(db_fn, self.database)
                    for i in range(self.u):
                        shutil.copy(os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}-state{i}'),
                                    os.path.join(self.state_folder, f'state-{i}'))
                    loaded = True
                    
            if not loaded:
                with Subtask(f'Opening and resetting database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()
                    cur.execute(f'DELETE FROM "5_user_profiles" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_user_profiles_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_papers" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_papers_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_conf_paperversion" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_conf_paperversion_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_conf_papercoauthor" WHERE 1=1')
                    cur.execute(f'DELETE FROM "5_conf_papercoauthor_inputs_" WHERE 1=1')
                    cur.close()
                    db.commit()
                    db.close()


        with Task(pref, 'Loading test database (2)', cplx=True) as task:
            if not loaded:
                with Subtask('Starting Databank', task):
                    with open(self.databank_log, 'w') as f:
                        self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)
                with Subtask('Cleaning monitor states', task):
                    state_fns = [fn for fn in os.listdir(self.state_folder)
                                 if fn.startswith("state-")]
                    for state_fn in state_fns:
                        if os.path.exists(os.path.join(self.state_folder, state_fn)):
                            os.remove(os.path.join(self.state_folder, state_fn))
                with Subtask(f'Registering users', task):
                    # For each user
                    for i in range(self.u):
                        # Log in into Databank
                        c = 0
                        while True:
                            try:
                                self.session = requests.Session()
                                r = self.session.post(self.login_url, data={"name": f"user{i}", "password": f"password{i}"}, timeout=1)
                            except:
                                c += 1
                                print(c, end='')
                                sleep(5)
                            else:
                                break
                        assert(r.ok)
                        # Register user into Conf
                        r = self.session.post(self.register_url, data={"email": f"user{i}@example.com",
                                                                       "name": f"User {i}",
                                                                       "affiliation": f"University of {i}",
                                                                       "acm_number": f"123456789-{i}"})
                        assert(r.ok)
                with Subtask(f'Posting messages', task):
                    authors = random.choices(range(self.u), k=self.n)
                    messages_by_author = {k: authors.count(k) for k in range(self.u)}
                    for a, mn in messages_by_author.items():
                        if mn == 0:
                            continue
                        # Log in into Databank as usera
                        self.session = requests.Session()
                        r = self.session.post(self.login_url, data={"name": f"user{a}", "password": f"password{a}"})
                        assert(r.ok)
                        # Log in into Conf
                        r = self.session.get(self.login_url2)
                        try:
                            assert(r.ok)
                        except:
                            print(r.text)
                        # Insert mn random messages
                        for _ in tqdm(list(range(mn))):
                            while True:
                                try:
                                    r = self.session.post(self.submit_url,
                                                          data={"title": lorem.sentence(),
                                                                "abstract": lorem.paragraph()},
                                                          files={"contents": open(self.default_pdf, 'rb')}, timeout=1)
                                    r.raise_for_status()
                                except:
                                    pass
                                else:
                                    break
                with Subtask('Killing Databank', task):
                    self.proc.send_signal(signal.SIGINT)
                    self.proc.wait()
                    self.proc.kill()
                    self.proc.wait()
                with Subtask(f'Saving database', task):
                    for i in range(self.u):
                        shutil.copy(os.path.join(self.state_folder, f'state-{i}'),
                                    os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}-state{i}'))
                    shutil.copy(self.database, db_fn)
                    
        with Task(pref, f'Starting Databank'):
            with open(self.databank_log, 'w') as f:
                self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

        with Task(pref, 'Logging in as user0', cplx=True) as task:
            user1 = users2[0]
            self.session = requests.Session()
            c = 0
            while True:
                with Subtask(f'Attempt {c}', task):
                    try:
                        r = self.session.post(self.login_url, data={"name": "user0", "password": "password0"})
                    except:
                        c += 1
                        sleep(1)
                    else:
                        break
            assert(r.ok)
            r = self.session.get(self.login_url2)
            try:
                assert(r.ok)
            except:
                print(r.text)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""

        if self.sc == 'submit_paper':
            with Task('conf.run', 'Deleting last message'):
                # should reset the states as well
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                id_to_delete = cur.execute(f'SELECT MAX(id) FROM "5_papers"').fetchall()[0][0]
                cur.execute(f'DELETE FROM "5_papers" WHERE id = {id_to_delete}')
                cur.execute(f'DELETE FROM "5_papers_inputs_" WHERE entry = {id_to_delete}')
                papers = cur.execute('SELECT id FROM "5_papers"').fetchall()
                papers_inputs = cur.execute('SELECT id FROM "5_papers_inputs_"').fetchall()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'submit_paper':
            with Task('conf.run', 'Resetting time counter'):
               with open(self.time_log, 'w') as f:
                   f.write('{"monitoring": 0}')
            with Task('conf.run', 'Measurement for scenario "Submit paper"'):
                #self.session = requests.Session()
                r = self.session.post(self.submit_url,
                                      data={"title": lorem.sentence(), "abstract": lorem.paragraph()},
                                      files={"contents": open(self.default_pdf, 'rb')})
                try:
                    assert(r.ok)
                except:
                    print(r.text)
                t = r.elapsed.total_seconds()
            with open(self.time_log) as f:
                t_monitoring = json.load(f)["monitoring"]
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 'p': self.p_name, 't': t}
        elif self.sc == 'view_paper':
            with Task('conf.run', 'Resetting time counter'):
               with open(self.time_log, 'w') as f:
                   f.write('{"monitoring": 0}')
            with Task('conf.run', 'Measurement for scenario "View paper"'):
                r = self.session.get(self.view_paper_url, params={"id": random.randint(0, self.n-1)})
                try:
                    assert(r.ok)
                except:
                    print(r.text)
                t = r.elapsed.total_seconds()
            with open(self.time_log) as f:
                t_monitoring = json.load(f)["monitoring"]
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 'p': self.p_name, 't': t}
        elif self.sc == 'view_papers':
            with Task('conf.run', 'Resetting time counter'):
               with open(self.time_log, 'w') as f:
                   f.write('{"monitoring": 0}')
            with Task('conf.run', 'Measurement for scenario "View papers"'):
                r = self.session.get(self.view_papers_url)
                try:
                    assert(r.ok)
                except:
                    print(r.text)
                t = r.elapsed.total_seconds()
            with open(self.time_log) as f:
                t_monitoring = json.load(f)["monitoring"]
                #Subtaskprint(t_monitoring, t)
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 'p': self.p_name, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('conf.finalize', 'Killing Databank'):
            self.proc.send_signal(signal.SIGINT)
            self.proc.wait()
            self.proc.kill()
            self.proc.wait()

class Application:
    
    app_cmd  = ["python3", "main.py"]
    database = "../db/database.db"
    database2 = "../db/fundamental.db"
    cwd      = "../src"
    db_folder = "databank_db/conf"
    databank_log = "logs/databank_log"
    time_log = "../evaluation/time.json"
    state_folder = "../monitor_files"
    default_pdf = "mathgen.pdf"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('conf.start', 'Starting evaluation') as task:

            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f'DELETE FROM "5_user_profiles" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_user_profiles_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_papers" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_papers_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_conf_paperversion" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_conf_paperversion_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_conf_papercoauthor" WHERE 1=1')
                cur.execute(f'DELETE FROM "5_conf_papercoauthor_inputs_" WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                
            with Subtask('Cleaning users', task):
                cur.execute(f'DELETE FROM users WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('conf.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('conf.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('submit_paper', self.app_cmd, self.cwd, self.database, self.database2,
                                  self.db_folder, self.databank_log, self.time_log, self.state_folder, self.default_pdf),
                         Scenario('view_paper', self.app_cmd, self.cwd, self.database, self.database2,
                                  self.db_folder, self.databank_log, self.time_log, self.state_folder, self.default_pdf),
                         Scenario('view_papers', self.app_cmd, self.cwd, self.database, self.database2,
                                  self.db_folder, self.databank_log, self.time_log, self.state_folder, self.default_pdf),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('conf.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 5
            u_def = 256
            us = [4**i for i in range(0, N_u+1)]
            # n: number of messages
            N_n = 5
            n_def = 256
            ns = [4**i for i in range(0, N_n+1)]
            # ps: policies
            ps = {"P0": 'FALSE',
                  "P1": 'EXISTS u,p,o,l. (Out(u,p,o) AND Itf(l,o) AND ONCE (EXISTS f,a. In(f,a,l)))',
                  "P2": ('EXISTS u,o,l. ((Out(u,"Marketing",o) AND Itf(l,o) AND ONCE (EXISTS a. In("5/submit_view",a,l))))'
                         ' OR ((Out(u,"Analytics",o) AND Itf(l,o) AND ONCE (EXISTS a. In("5/submit_view",a,l)))) AND NOT (u = "trustedanalytics.com")'
                         ' OR (EXISTS p. (Out(u,p,o) AND Itf(l,o) AND ONCE(604800,*) (EXISTS a. In("5/submit_view",a,l))) AND NOT (p = "Service"))'),
                  "P3": 'EXISTS u,p,o,l. (Out(u,p,o) AND Itf(l,o) AND NOT (u = "{}"))',
                  "PConf": """EXISTS u,p,o,l,f,a.
  Out(u,p,o) AND Itf(l,o)
  AND (ONCE In(f,a,l)
         AND ((f = "5/submit_view" OR f = "5/assign_reviewer" OR f = "5/paper_view"
	    OR f = "5/submit_review_view" OR f = "5/submit_comment_view")))
  AND NOT (u = "{0}"
        OR u = "0"
	OR (u = "1" OR u = "2" OR u = "3" OR u = "4" OR u = "5"
	 OR u = "6" OR u = "7" OR u = "8" OR u = "9" OR u = "10"))
OR
  Out(u,p,o) AND Itf(l,o)
  AND (ONCE In(f,a,l)
         AND f = "5/register" AND a = "email")
  AND NOT (u = "{0}"
        OR u = "0") """
            }
            p_name_def = "P2"
            p_def = ps["P2"]
            # configs = us x ns x ps
#            configs = [(u, n_def, p_name_def, p_def) for u in us] + \
#                [(u_def, n, p_name_def, p_def) for n in ns] + \
#                [(u_def, n_def, p_name, p) for (p_name, p) in ps.items()]
            configs = [(u_def, n_def, p_name, p) for (p_name, p) in list(ps.items())[::-1]]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n", "p"]
