from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os

from bs4 import BeautifulSoup

from werkzeug.security import generate_password_hash
from lorem_text import lorem


from tools import Task, Subtask


class Scenario:

    login_url = "http://localhost:8000/accounts/login/"
    submit_url = "http://localhost:8000/submit"
    view_papers_url = "http://localhost:8000/papers"
    view_paper_url = "http://localhost:8000/paper"

    def __init__(self, sc, app_cmd, cwd, database, default_pdf):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.default_pdf = default_pdf

    def generate_random_paper1(self, id_, users):
        accepted = 0
        author_id = random.choice(users)[0]
        return f'({id_}, {accepted}, {author_id})'

    def generate_random_paper2(self, id_, users):
        title = lorem.sentence()
        contents = f"papers/paper{id_}.pdf"
        shutil.copy(self.default_pdf, os.path.join(self.cwd, "media", "papers", f"paper{id_}.pdf"))
        abstract = lorem.paragraph()
        time = "2023-01-01 00:00:00.000000"
        return f'({id_}, "{title}", "{contents}", "{abstract}", "{time}", {id_})'

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n = config
        pref = f'conf.initialize (sc={self.sc}, u={self.u}, n={self.n})'

        with Task(pref, 'Initializing database', cplx=True) as task:
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                users = cur.execute("SELECT id FROM auth_user").fetchall()
                papers = cur.execute("SELECT id FROM papers").fetchall()

            if len(users) > self.u:
                with Subtask(f'Deleting {len(users)-self.u} users', task):
                    cur.execute(f"DELETE FROM auth_user ORDER BY id DESC LIMIT {len(users)-self.u}")
                    cur.execute(f"DELETE FROM user_profiles ORDER BY id DESC LIMIT {len(users)-self.u}")
                    users = cur.execute("SELECT id, username FROM auth_user").fetchall()
                    profiles = cur.execute("SELECT id FROM user_profiles").fetchall()
                    assert(len(users) == self.u)
                    assert(len(users) == len(profiles))

            elif len(users) < self.u:
                with Subtask(f'Adding {self.u-len(users)} users', task):
                    to_insert = [f'({i}, "pbkdf2_sha256$12000$iV0sZ7R8KrVJ$xcIuLw2+ucijlFbCtpRFIy3DxlIANgGjZxJP1pa4kVo=", ' + \
                                 '"2023-01-01 00:00:00.000000", ' + \
                                 f'1, "user{i}", "firstname", "lastname", "user{i}@example.com", 1, 1, "2023-01-01 00:00:00.000000")'
                                 for i in range(len(users), self.u)]
                    cur.execute((f"INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, "
                                 f"last_name, email, is_staff, is_active, date_joined) VALUES {','.join(to_insert)}"))
                    def level(i):
                        if i == 0:
                            return 'chair'
                        elif i < 11:
                            return 'pc'
                        else:
                            return 'normal'
                    to_insert2 = [f'({i}, "user{i}", "user{i}@example.com", "user{i}", "ABCD", "acmnumber", "{level(i)}")'
                                  for i in range(len(users), self.u)]
                    cur.execute((f"INSERT INTO user_profiles (id, username, email, name, affiliation, acm_number, level) "
                                 f"VALUES {','.join(to_insert2)}"))
                    users = cur.execute("SELECT id, username FROM auth_user").fetchall()
                    profiles = cur.execute("SELECT id FROM user_profiles").fetchall()
                    assert(len(users) == self.u)
                    assert(len(users) == len(profiles))
                    
            if len(papers) > self.n:
                with Subtask(f'Deleting {len(papers)-self.n} papers', task):
                    cur.execute(f"DELETE FROM papers ORDER BY id DESC LIMIT {len(papers)-self.n}")
                    cur.execute(f"DELETE FROM conf_paperversion ORDER BY id DESC LIMIT {len(papers)-self.n}")
                    papers = cur.execute("SELECT id FROM papers").fetchall()
                    versions = cur.execute("SELECT id FROM conf_paperversion").fetchall()
                    assert(len(papers) == self.n)
                    assert(len(versions) == self.n)
                    
            elif len(papers) < self.n:
                with Subtask(f'Adding {self.n-len(papers)} papers', task):
                    to_insert1 = [self.generate_random_paper1(i, users) for i in range(len(papers), self.n)]
                    to_insert2 = [self.generate_random_paper2(i, users) for i in range(len(papers), self.n)]
                    cur.execute(f"INSERT INTO papers (id, accepted, author_id) VALUES {','.join(to_insert1)}")
                    cur.execute(f"INSERT INTO conf_paperversion (id, title, contents, abstract, time, paper_id) VALUES {','.join(to_insert2)}")
                    papers = cur.execute("SELECT id FROM papers").fetchall()
                    versions = cur.execute("SELECT id FROM conf_paperversion").fetchall()
                    assert(len(papers) == self.n)
                    assert(len(versions) == self.n)
            
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Starting Conf Jacqueline application'):
            self.proc = Popen(self.app_cmd, cwd=self.cwd, stdin=PIPE, stdout=PIPE, stderr=STDOUT, text=True)
            sleep(5)

        with Task(pref, 'Logging in with user 0') as task:
            self.session = requests.Session()
            soup = BeautifulSoup(self.session.get(self.login_url).content, features='html.parser')
            csrftoken = soup.find('input', {"name": 'csrfmiddlewaretoken'})['value']
            r = self.session.post(self.login_url, data={"username": "user0", "password": "password",
                                                        "csrfmiddlewaretoken": csrftoken})
            assert(r.ok)

    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        if self.sc == 'submit_paper':
            with Task('conf.run', 'Deleting last paper'):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                cur.execute(f"DELETE FROM papers ORDER BY id DESC LIMIT 1")
                cur.execute(f"DELETE FROM conf_paperversion ORDER BY id DESC LIMIT 1")
                cur.close()
                db.commit()
                db.close()

    def run(self):
        """Function performing the measurement"""
        if self.sc == 'submit_paper':
            with Task('conf.run', 'Measurement for scenario "Submit paper"'):
                #self.session = requests.Session()
                soup = BeautifulSoup(self.session.get(self.login_url).content, features='html.parser')
                csrftoken = soup.find('input', {"name": 'csrfmiddlewaretoken'})['value']
                r = self.session.post(self.submit_url,
                                      data={"title": lorem.sentence(), "abstract": lorem.paragraph(),
                                            "csrfmiddlewaretoken": csrftoken},
                                      files={"contents": open(self.default_pdf, 'rb')})
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_paper':
            with Task('conf.run', 'Measurement for scenario "View paper"'):
                r = self.session.get(self.view_paper_url, params={"id": random.randint(0, self.n-1)})
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}
        elif self.sc == 'view_papers':
            with Task('conf.run', 'Measurement for scenario "View papers"'):
                r = self.session.get(self.view_papers_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 't': t}

    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('conf.finalize', 'Killing Minitwit Flask application'):
            self.proc.kill()

class Application:
    
    app_cmd  = ["python2", "manage.py", "runserver"]
    cwd      = "baseline/cms"
    database = "baseline/cms/db.sqlite3"
    default_pdf = "mathgen.pdf"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('conf.start', 'Starting evaluation') as task:
            
            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f"DELETE FROM auth_user WHERE 1=1")
                cur.execute(f"DELETE FROM user_profiles WHERE 1=1")
                cur.execute(f"DELETE FROM papers WHERE 1=1")
                cur.execute(f"DELETE FROM conf_paperversion WHERE 1=1")
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('conf.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('conf.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('submit_paper', self.app_cmd, self.cwd, self.database, self.default_pdf),
                         Scenario('view_paper', self.app_cmd, self.cwd, self.database, self.default_pdf), 
                         Scenario('view_papers', self.app_cmd, self.cwd, self.database, self.default_pdf),
                         ]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('conf.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 5
            u_def = 256
            us = [4**i for i in range(0, N_u+1)]
            # n: number of papers
            N_n = 5
            n_def = 256
            ns = [4**i for i in range(0, N_n+1)]
            # configs = us x ns
            configs = [(u, n_def) for u in us] + [(u_def, n) for n in ns]
        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n"]
    
