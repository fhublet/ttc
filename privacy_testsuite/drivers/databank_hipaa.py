from subprocess import Popen, PIPE, STDOUT
from datetime import datetime
from time import sleep
import random
import sqlite3
import requests
import shutil
import os
import signal

from werkzeug.security import generate_password_hash
from lorem_text import lorem
import uuid

from flask_hashing import Hashing
from flask import Flask
from lorem_text import lorem

from tools import Task, Subtask

bank = Flask("The Databank")
bank.secret_key = "E4kB3BUlTXivYtkaKnCb9XHGIr9erSEIX0n0MWOnAqlqr2PGKWjPgWp2834M5PmDqx2dEvI2EV7YdriY"
hashing = Hashing(bank)

class Scenario:

    login_url = "http://localhost:5000/login"
    loginput_url = "http://localhost:5000/loginput"
    logout_url = "http://localhost:5000/logout"
    register_url = "http://localhost:5000/6/accounts/register"
    login_url2 = "http://localhost:5000/6/accounts/do_login"
    view_patient_url = "http://localhost:5000/6/patients/{uid}"
    view_index_url = "http://localhost:5000/6/"

    def __init__(self, sc, app_cmd, cwd, database, database2, db_folder, databank_log, time_log, state_folder):
        self.sc = sc
        self.app_cmd = app_cmd
        self.cwd = cwd
        self.database = database
        self.database2 = database2
        self.db_folder = db_folder
        self.databank_log = databank_log
        self.time_log = time_log
        self.state_folder = state_folder

    def initialize(self, config):
        """Function to be executed before running the measurements"""
        self.u, self.n, self.p_name, self.p = config
        pref = f'hipaa.initialize (sc={self.sc}, u={self.u}, n={self.n}, p={self.p_name})'

        with Task(pref, 'Setting up users and policies', cplx=True) as task:
            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                policy = cur.execute('SELECT id, policy FROM users ORDER BY id LIMIT 1').fetchall()
                users2 = cur.execute('SELECT id FROM users').fetchall()

            if len(users2) > self.u:
                with Subtask(f'Deleting {len(users2)-self.u} users', task):
                    cur.execute(f'DELETE FROM users ORDER BY id DESC LIMIT {len(users2)-self.u}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)
                
            if policy and policy[0][1].format(policy[0]) != self.p.format(policy[0][0]):
                with Subtask('Updating existing policies', task):
                    for id_ in users2:
                        cur.execute(f"UPDATE users SET policy = '{self.p.format(id_[0])}' WHERE id = {id_[0]}")

            if len(users2) < self.u:
                with Subtask(f'Adding {self.u-len(users2)} users', task):
                    f = lambda i: hashing.hash_value(f"password{i}")
                    g = lambda i: self.p.format(i).replace('"', '""')
                    to_insert = [f'({i}, "user{i}", "{f(i)}", "{g(i)}")' for i in range(len(users2), self.u)]
                    cur.execute(f'INSERT INTO users (id, name, hash, policy) VALUES {",".join(to_insert)}')
                    users2 = cur.execute('SELECT name FROM users').fetchall()
                    assert(len(users2) == self.u)

            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

        with Task(pref, 'Loading test database', cplx=True) as task:
            db_fn = os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}.db')
            loaded = False

            with Subtask(f'Trying to load existing database', task):
                # If it exists, load it
                if os.path.exists(db_fn):
                    shutil.copy(db_fn, self.database)
                    for i in range(self.u):
                        shutil.copy(os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}-state{i}'),
                                    os.path.join(self.state_folder, f'state-{i}'))
                    loaded = True
                    
            if not loaded:
                with Subtask(f'Opening and resetting database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()
                    cur.execute(f'DELETE FROM "6_UserProfile" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_UserProfile_inputs_" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_Individual" WHERE 1=1')
                    cur.execute(f'DELETE FROM "6_Individual_inputs_" WHERE 1=1')
                    cur.close()
                    db.commit()
                    db.close()

        with Task(pref, 'Initializing database', cplx=True) as task:
            if not loaded:
                with Subtask('Cleaning monitor states', task):
                    state_fns = [fn for fn in os.listdir(self.state_folder)
                                 if fn.startswith("state-")]
                    for state_fn in state_fns:
                        if os.path.exists(os.path.join(self.state_folder, state_fn)):
                            os.remove(os.path.join(self.state_folder, state_fn))

                with Subtask('Starting Databank', task):
                    with open(self.databank_log, 'w') as f:
                        self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

                with Subtask('Generating UTs', task):
                    uts = []
                    for i in range(self.n):
                        uts.append([])
                        for a in ["FirstName", "LastName", "Email", "Sex", "BirthDate"]:
                            while True:
                                try:
                                    r = requests.post(self.loginput_url,
                                                      data={"f": "6/Individual", "a": a, "u": random.randint(0, self.u-1)})
                                    uts[i].append(f'"{r.text}"')
                                except Exception as e:
                                    sleep(1)
                                else:
                                    break
                            assert(r.ok)

                with Subtask('Killing Databank', task):
                    self.proc.send_signal(signal.SIGINT)
                    self.proc.wait()
                    self.proc.kill()
                    self.proc.wait()
                    
                with Subtask(f'Saving states', task):
                    for i in range(self.u):
                        shutil.copy(os.path.join(self.state_folder, f'state-{i}'),
                                    os.path.join(self.db_folder, f'{self.u}_{self.n}_{self.p_name}-state{i}'))

                with Subtask('Opening database', task):
                    db = sqlite3.connect(self.database)
                    cur = db.cursor()

                with Subtask(f'Adding {self.u} users', task):
                    to_insert = [f'({i}, {i}, "user{i}", 1, "user{i}@example.com", 1, "name")'
                                  for i in range(self.u)]
                    cur.execute(f'INSERT INTO "6_UserProfile" (id, user_id, username, is_active, email, profiletype, name) ' +\
                                f"VALUES {','.join(to_insert)}")
                    users = cur.execute('SELECT id FROM "6_UserProfile"').fetchall()
                    assert(len(users) == self.u)

                with Subtask(f'Adding {self.n} individuals', task):
                    to_insert = [f'({i}, "firstname", "lastname", "user{i}@example.com", ' + \
                                 f'"{random.choice(["Male", "Female"])}", "2023-01-01")'
                                 for i in range(self.n)]
                    cur.execute(f'INSERT INTO "6_Individual" (id, FirstName, Lastname, Email, Sex, BirthDate) ' + \
                                f"VALUES {','.join(to_insert)}")
                    to_insert_inputs_ = \
                        [f'({i}, "FirstName", "[[["{uts[i][0]}",{i}]]]")'
                         for i in range(self.n)] + \
                        [f'({i}, "LastName", "[[["{uts[i][1]}",{i}]]]")'
                         for i in range(self.n)] + \
                        [f'({i}, "Email", "[[["{uts[i][2]}",{i}]]]")'
                         for i in range(self.n)] + \
                        [f'({i}, "Sex", "[[["{uts[i][3]}",{i}]]]")'
                         for i in range(self.n)] + \
                        [f'({i}, "BirthDate", "[[["{uts[i][4]}",{i}]]]")'
                         for i in range(self.n)]
                    cur.execute(f'INSERT INTO "6_Individual_inputs_" (entry, field, inputs) ' + \
                                f"VALUES {','.join(to_insert_inputs_)}")
                    individuals = cur.execute('SELECT id FROM "6_Individual"').fetchall()
                    assert(len(individuals) == self.n)

                with Subtask('Committing and closing database', task):
                    cur.close()
                    db.commit()
                    db.close()

                with Subtask(f'Saving database', task):
                    shutil.copy(self.database, db_fn)

        with Task(pref, 'Starting Databank'):
            with open(self.databank_log, 'w') as f:
                self.proc = Popen(self.app_cmd, cwd=self.cwd, stdout=f, stderr=f)

        with Task(pref, 'Logging in as user0', cplx=True) as task:
            user1 = users2[0]
            self.session = requests.Session()
            c = 0
            while True:
                with Subtask(f'Attempt {c}', task):
                    try:
                        r = self.session.post(self.login_url,
                                              data={"name": "user0", "password": "password0"})
                    except:
                        c += 1
                        sleep(1)
                    else:
                        break
            assert(r.ok)
            r = self.session.get(self.login_url2)
            try:
                assert(r.ok)
            except:
                print(r.text)

            
    def continue_(self):
        """Function to be executed between two instances of the same measurement"""
        pass
                
    def run(self):
        """Function performing the measurement"""
        if self.sc == 'view_index':
            with Task('hipaa.run', 'Measurement for scenario "View index"'):
                r = self.session.get(self.view_index_url)
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 'p': self.p_name, 't': t}
        elif self.sc == 'view_patient':
            with Task('hipaa.run', 'Measurement for scenario "View patient"'):
                r = self.session.get(self.view_patient_url.format(uid=random.randint(0, self.u-1)))
                assert(r.ok)
                t = r.elapsed.total_seconds()
            return {'sc': self.sc, 'u': self.u, 'n': self.n, 'p': self.p_name, 't': t}
        
    def finalize(self):
        """Function to be executed after performing the measurements"""
        with Task('hipaa.finalize', 'Killing Hipaa Databank application'):
            self.proc.kill()

class Application:

    app_cmd  = ["python3", "main.py"]
    database = "../db/database.db"
    database2 = "../db/fundamental.db"
    cwd      = "../src"
    db_folder = "databank_db/hipaa"
    databank_log = "logs/databank_log"
    time_log = "../evaluation/time.json"
    state_folder = "../monitor_files"

    def start(self):
        """Function to be executed before running any scenarios"""
        with Task('hipaa.start', 'Starting evaluation') as task:

            with Subtask('Opening database', task):
                db = sqlite3.connect(self.database)
                cur = db.cursor()
                
            with Subtask('Cleaning database', task):
                cur.execute(f'DELETE FROM "6_UserProfile" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_UserProfile_inputs_" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_Individual" WHERE 1=1')
                cur.execute(f'DELETE FROM "6_Individual_inputs_" WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()

            with Subtask('Opening fundamental database', task):
                db = sqlite3.connect(self.database2)
                cur = db.cursor()
                
            with Subtask('Cleaning users', task):
                cur.execute(f'DELETE FROM users WHERE 1=1')
                
            with Subtask('Committing and closing database', task):
                cur.close()
                db.commit()
                db.close()
                
    def stop(self):
        """Function to be executed after running all scenarios"""
        with Task('hipaa.stop', 'Stopping evaluation'):
            pass

    def scenarios(self):
        """Return the list of available scenarios"""
        with Task('hipaa.scenarios', 'Computing scenarios'):
            scenarios = [Scenario('view_patient', self.app_cmd, self.cwd, self.database, self.database2,
                                  self.db_folder, self.databank_log, self.time_log, self.state_folder),
                         Scenario('view_index', self.app_cmd, self.cwd, self.database, self.database2,
                                  self.db_folder, self.databank_log, self.time_log, self.state_folder)]
        return scenarios

    def configurations(self):
        """Return the list of available parameter configurations"""
        with Task('hipaa.configurations', 'Computing configurations'):
            # u: number of users
            N_u = 5
            u_def = 256
            us = [4**i for i in range(0, N_u+1)]
            # n: number of individuals
            N_n = 5
            n_def = 256
            ns = [4**i for i in range(0, N_n+1)]
            # ps: policies
            ps = {"P0": 'FALSE',
                  "P1": 'EXISTS u,p,o,l. (Out(u,p,o) AND Itf(l,o) AND ONCE (EXISTS f,a. In(f,a,l)))',
                  "P2": ('EXISTS u,o,l. ((Out(u,"Marketing",o) AND Itf(l,o) AND ONCE (EXISTS a. In("6",a,l))))'
                         ' OR ((Out(u,"Analytics",o) AND Itf(l,o) AND ONCE (EXISTS a. In("6",a,l)))) AND NOT (u = "trustedanalytics.com")'
                         ' OR (EXISTS p. (Out(u,p,o) AND Itf(l,o) AND ONCE(604800,*) (EXISTS a. In("6",a,l))) AND NOT (p = "Service"))'),
                  "P3": 'EXISTS u,p,o,l. (Out(u,p,o) AND Itf(l,o) AND NOT (u = "{}"))',
                  "PHIPAA": """EXISTS u,p,i,l,f,a.
  Out(u,p,i) AND Itf(l,i)
  AND (ONCE (In (f,a,l)
        AND (f = "6"))) AND NOT (u = "{}")"""
            }
            p_name_def = "P2"
            p_def = ps["P2"]
            # configs = us x ns x ps
            configs = [(u, n_def, p_name_def, p_def) for u in us] + \
                [(u_def, n, p_name_def, p_def) for n in ns] + \
                [(u_def, n_def, p_name, p) for (p_name, p) in ps.items()]
            #configs = [(u_def, n_def, p_name, p) for (p_name, p) in list(ps.items())[::-1]]

        return configs
    
    def dep_vars(self):
        """Return the list of the dependent vars in the results"""
        return ["t"]

    def indep_vars(self):
        """Return the list of the independent vars in the results"""
        return ["sc", "u", "n", "p"]
    
