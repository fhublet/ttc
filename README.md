# User-Controlled Privacy: Taint, Track, and Control

This Git repository contains the artifacts of the paper "User-Controlled Privacy: Taint, Track, and Control."

## Description

The artifact contains formalization and prototype implementation of our Taint, Track, and Control
(TTC) approach for enforcing user-defined privacy policies. Our prototype, called WebTTC supports
running Python/Flask web applications. We also include four case study applications implemented
using WebTTC.

## Basic Requirements

### Hardware Requirements

No specific hardware requirements.
The experiments were run on Intel Core i5-1135G7, 32 GB RAM laptop.

### Software Requirements

VirtualBox 6.1 is used to encapsulate the necessary environment and dependencies.
Isabelle 2022 is used to verify the formal proofs.
Python 3.8.10 is used to run the prototype.

## Paper

The paper is located in the `paper` folder.

## Formal proofs

The proofs are located in the `formalization` folder.

Follow the instructions at https://isabelle.in.tum.de/website-Isabelle2022/installation.html to install Isabelle 2022.

You can verify the proofs in the console by running
```
<path to Isabelle>/bin/isabelle build -v -D formalization
```
You should see a message "Finished TTC_Session (...)" in the end.
In our experience, verification takes about 35 seconds.

Alternatively, open Isabelle's GUI by running
```
<path to Isabelle>/bin/isabelle jedit
```
and open the .thy files. Appendix A of the paper will guide you through the theories.

In the appliance file (see below), Isabelle 2022 is installed in ``home`` under ``Isabelle2022``
and the formalization can be found under ``formalization``. The verification time is
about 2'30''.

## Setup instructions

### Option 1: appliance file

1. Navigate to https://www.virtualbox.org/wiki/Downloads and follow the preferred instructions.

2. Download the ready-to-use VirtualBox appliance file (.ova) containing the full prototype at https://drive.google.com/file/d/1kYjnEXWWLgb89cw4-AewWHy3ouGpe_T2 (file size: ~10 GB).

3. Load the appliance into VirtualBox (we used version 6.1) and its RAM to 4 GB. (This is needed to run tests with a larger number of users.)

4. Start the VM.

### Option 2: manual

The following setup procedure should work on Linux Ubuntu/Debian:

1. Run
```
  git clone --recursive https://gitlab.ethz.ch/fhublet/ttc
```
Note that the `--recursive` option is required to clone the enforcement submodule.

2. Follow the instructions at https://bitbucket.com/jshs/monpoly to complete the installation of Enfpoly/MonPoly within folder `enfpoly_dev`

3. Go back to the root folder, create and open a virtual environment, and install the Python dependencies with
```
   cd ../..
   virtualenv env
   source env/bin/activate
   pip3 install -r requirements.txt
```

4. Make sure that `python3` refers to Python 3.8.10.

5. You can now run the app with
```
  cd src
  python3 main.py
```

## Accessing the case-study applications


When Debian starts, enter the credentials
```
  user:     user
  password: user
```
A browser will automatically open. You can log in with any credentials
```
  Username: user<i>
  Password: password<i>
  
  for <i> in {0..10}
```
On the index page, you will find:
```
  Your profile   -> Change your name and password
  Your policy    -> View and set your policy
  Log out        -> Logging out
  Services/
    conf         -> The Conf case study
    hipaa        -> The HIPAA case study
    minitwit     -> The Minitwit case study
    minitwitplus -> The Minitwitplus case study
```

## Inspecting the code of the prototype

The code of the prototype can be found under `~/ttc`. In this folder, you will find:
```
  db/
    fundamental.db  -> SQLite database storing user authentication / user policies data
    database.db     -> SQLite database storing application data
  enfpoly_dev       -> EnfPoly [58]
  monitor_files     -> Working directory of the monitor (policies, signatures, etc.)
  privacy_testsuite -> Testing scripts (see below)
  src/
    collection/
      <app>.py      -> WebTTC source code of application <app>
      compiled/
        <app>.py    -> Compiled Python source code of application <app>
      templates/
        <app>/      -> HTML templates of application <app>
    databank/       -> Source code of WebTTC prototype
    main.py         -> Main file
    setting.py      -> Settings file
    static/         -> Static files of the WebTTC interface
    templates/      -> Templates of the WebTTC interface
  test/, uploads/   -> Additional working folders for WebTTC
```

[58] François Hublet, David Basin, and Srđan Krstić. 2022. 
Real-time Policy Enforcement with Metric First-Order Temporal Logic. 
In Proceedings of ESORICS’22

The evaluation scripts can be found in the `privacy_testsuite` folder, which contains:
```
  baseline/       -> Code of the Python/Flask and Jacqueline implementations of the case studies
  count_lines.py  -> Utility used to count code lines
  drivers/        -> Driver scripts describing the operations performed during testing for each 
                     pair of a technology and app
  privacy_test.py -> Main testing script
  reporter.py     -> Printing script
  tester.py       -> Script calling the drivers
  tools.py        -> Printing utility script
``` 
The number of lines reported in Table 1 can be checked by running
```
  python3 count_lines.py <source file>
```
on either WebTTC source code (in src/collection) or Python/Flask code (in baseline). The numbers
for Jacqueline were obtained by manual inspection and use of the cloc utility.
  
For each pair of a technology (baseline = Python/Flask; databank = WebTTC; jacqueline) and an
application (conf, hipaa, minitwit, minitwitplus) reported in Table 2, data and graphs can be 
generated by running
```
  python3 privacy_test.py <technology>_<application> -f <output folder>
``` 
The code of the corresponding driver can be inspected in `drivers/<technology>_<app>.py`.

If you get an error, this might be due to several copies of the prototype running in parallel. Try killing existing Python instances with
```
  killall python3
```
and running the evaluation script again.
  
The number of repetitions performed (N) can be set in tester.py. For a quick test, we recommend 
N = 5. The default is N = 100, which can take several hours to run.

## Limitations

Our performance experiments were run without a VirtualBox image, which is provided only to
ease the process of artifact evaluation. Therefore, the performance numbers may not be
the same, but should lead the reviewer to draw the same conclusions as we have in Section 6.
